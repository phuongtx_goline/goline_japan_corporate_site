#!/bin/bash

# list files
files=(build*zip)
zip_name="${files[0]}"

# Check if contains substring
# https://stackoverflow.com/a/229606/6755585
if [[ $zip_name == *"*"* ]]
then
  echo 'ERROR: No valid zip file in current directory'
  exit 1
fi

# Install `zip` on Windows
# https://stackoverflow.com/a/55749636/6755585


# Unzip `config` folder
# https://unix.stackexchange.com/a/115241/338240
time (unzip -q $zip_name 'config/*' -d './')

app_name=''
app_url=''
db_name=''
db_user=''
db_password=''

# While loop until all 3 params not null
# -- Read database info from `env` file
# -- https://stackoverflow.com/a/11746174/6755585
if [ -s env* ]; then
  lines=()
  # Remove trailing new line
  # https://stackoverflow.com/a/24159402/6755585
  while IFS=$' \t\r\n' read -r line; do
    lines+=("$line")
    # echo ${#line}
    if [ ${#lines[@]} == 5 ]; then
      break
    fi
  done <<< $(cat env*)

  app_name="${lines[0]}"
  app_url="${lines[1]}"
  db_name="${lines[2]}"
  db_user="${lines[3]}"
  db_password="${lines[4]}"
fi

# If no env file or either params empty, prompt user to input
while [ -z $app_name ]
do
  echo '> Your app name:'
  read app_name
done
while [ -z $app_url ]
do
  echo '> Your app URL:'
  read app_url
done
while [ -z $db_name ]
do
  echo '> Your database name:'
  read db_name
done
while [ -z $db_user ]
do
  echo '> Your user name:'
  read db_user
done
while [ -z $db_password ]
do
  echo '> Your password:'
  read db_password
done

# TODO: Remove control character
# https://stackoverflow.com/a/22282144/6755585


# Find and replace
sed -i -E "s/('name' =>\s*').*(',)/\1$app_name\2/" config/app.php

# Escape the $REPLACE string so it would be safely accepted by `sed`
# https://stackoverflow.com/a/2705678/6755585
sed -i -E "s_('url' =>\s*').*(',)_\1$app_url\2_" config/app.php

sed -i -E "s/('database' =>\s*').*(',)(.*\/\* REPLACE YOURS \! \*\/)/\1$db_name\2\3/" config/database.php
sed -i -E "s/('username' =>\s*').*(',)(.*\/\* REPLACE YOURS \! \*\/)/\1$db_user\2\3/" config/database.php
sed -i -E "s/('password' =>\s*').*(',)(.*\/\* REPLACE YOURS \! \*\/)/\1$db_password\2\3/" config/database.php

# Adding unzipped files to a zipped folder
# https://unix.stackexchange.com/a/272896/338240
time (zip -ur $zip_name './config' && rm -rf config) | echo '>> Preparation completes!'
