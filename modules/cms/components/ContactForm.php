<?php namespace Cms\Components;

use File;
use Cms\Classes\ComponentBase;
use Exception;
use Illuminate\Support\Facades\Mail as FacadesMail;
use Illuminate\Support\Facades\Redirect;
use System\Classes\CombineAssets;
use Mail;
use October\Rain\Support\Facades\Flash;

/**
 * ContactForm component
 */
class ContactForm extends ComponentBase
{

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'Contact Form',
            'description' => 'Simple contact form component'
        ];
    }

    public function onSubmit()
    {
        try {
            $data = post();
            $emailTo = 'contact@goline.vn'; // Địa chỉ email đích
            $subject = '新しいお問い合わせフォームの送信';

            // Tạo nội dung email
            $content = "Name: {$data['name1']} {$data['name2']}\n";
            $content .= "Phone: {$data['phone']}\n";
            $content .= "Email: {$data['email']}\n";
            $content .= "Message: {$data['message']}";

            // Gửi email
            FacadesMail::sendTo($emailTo, $subject, $content);

            // Chuyển hướng hoặc hiển thị thông báo thành công
            Flash::success('フォームは正常に送信されました!');
            return Redirect::back();
        } catch (Exception $ex) {
            Flash::error($ex->getMessage());
        }
    }
}
