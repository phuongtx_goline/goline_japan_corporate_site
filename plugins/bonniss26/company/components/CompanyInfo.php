<?php

namespace Bonniss26\Company\Components;

use Cms\Classes\ComponentBase;
use Bonniss26\Company\Models\CompanyInfo as CompanyInfoModel;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;

class CompanyInfo extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'CompanyInfo',
            'description' => 'Thêm thông tin công ty vào trang'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $ci = 'gComp';
        $cacheKey = 'g__company_info';
        if (Cache::has($cacheKey)) {
            $this->page[$ci] = Cache::get($cacheKey);
        } else {
            $this->page[$ci] = Cache::rememberForever($cacheKey, function () {
                return CompanyInfoModel::first()->toArray();
            });
            Log::info('Company info read from DB');
        }

        /**
         * Inject request params into `page`
         * Example
         * {
         *     inputs: {
         *         color: "red",
         *         sort: "asc"
         *     },
         *     path: "/san-pham/binh-giu-nhiet-lock-n-lock",
         *     q: "color=red&sort=asc",
         *     slug: "binh-giu-nhiet-lock-n-lock"
         * }
         */
        $parsedUrl = parse_url(Request::fullUrl());
        $this->page['gRequest'] = [
            'slug' => $this->param('slug'),
            'path' => $parsedUrl['path'] ?? NULL,
            'q' => $parsedUrl['query'] ?? NULL,
            'inputs' => Input::all(),
        ];
    }
}
