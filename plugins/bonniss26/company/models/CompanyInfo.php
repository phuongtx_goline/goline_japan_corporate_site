<?php

namespace Bonniss26\Company\Models;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Model;

/**
 * Model
 */
class CompanyInfo extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'bonniss26_company_info';

    /**
     * @var array Validation rules
     */
    public $rules = [];

    public function filterFields($fields, $context = null)
    {
        if (!empty($this->is_bct_register) || !empty($this->is_bct_notified)) {
            $fields->bct_url->hidden = false;
        }
        if (!empty($this->biz_license)) {
            $fields->biz_license_authority->hidden = false;
            $fields->biz_license_issue_date->hidden = false;
        } else {
            $fields->biz_license_authority->hidden = true;
            $fields->biz_license_issue_date->hidden = true;
        }
    }

    private function join_paths(...$paths)
    {
        $sep = '/';
        return preg_replace('~[/\\\\]+~', $sep, implode($sep, $paths));
    }

    private function toMediaRelativeUri($innerUri)
    {
        $mediaConfigPath = Config::get('cms.storage.media.path');

        return strpos($innerUri, substr($mediaConfigPath, 1)) !== false ? $innerUri : $this->join_paths($mediaConfigPath, $innerUri);
    }

    // Update cache to push effect on frontend side
    public function afterSave()
    {
        Cache::forget('g__company_info');
        Cache::rememberForever('g__company_info', function () {
            return $this->toArray();
        });
    }
}
