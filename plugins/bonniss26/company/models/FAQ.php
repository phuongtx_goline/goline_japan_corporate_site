<?php namespace Bonniss26\Company\Models;

use Model;
use RainLab\Translate\Classes\Translator;

/**
 * Model
 */
class FAQ extends Model
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'faq_id';
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'bonniss26_company_faq';

    /**
     * @var array Validation rules
     */
    public $rules = [];

    private function isEmptyHTMLText($str)
    {
        return $str === null || $str === '' || trim(strip_tags($str)) === '';
    }

    // Pre-process before save
    protected function beforeSave()
    {
        if ($this->isEmptyHTMLText($this->question_vi)) {
            $this->question_vi = '';
        }
        if ($this->isEmptyHTMLText($this->answer_vi)) {
            $this->answer_vi = '';
        }
        if ($this->isEmptyHTMLText($this->question_en)) {
            $this->question_en = '';
        }
        if ($this->isEmptyHTMLText($this->answer_en)) {
            $this->answer_en = '';
        }
    }

    // Scope filters
    public function scopeFilterByLocale($query)
    {
        $translator = Translator::instance();
        $activeLocale = $translator->getLocale();
        $field = 'question_' . $activeLocale;
        return $query->where($field, '<>', '');
    }
}
