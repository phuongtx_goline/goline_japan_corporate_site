<?php

namespace Bonniss26\Company\Models;

use Illuminate\Http\Resources\Json\Resource;
use Carbon\Carbon;

class BlogCategoryResource extends Resource
{
  public function toArray($request)
  {
    $postEach = is_null($request->query('postEach')) ? 2 : (int) $request->query('postEach');
    $posts = $this->posts->where('published', 1)->sortByDesc('published_at');

    /*
    * Embed `postEach` posts into each category
    */
    if ($postEach > 0) {
      $posts = $posts->take($postEach);
    }

    return [
      'id' => $this->id,
      'name' => $this->name,
      'slug' => $this->slug,
      'posts' => $this->when($postEach > 0, BlogPostResource::collection($posts))
    ];
  }
}
