<?php

namespace Bonniss26\Company\Models;

use Illuminate\Http\Resources\Json\Resource;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use ToughDeveloper\ImageResizer\Classes\Image;

class BlogPostResource extends Resource
{
  public function toArray($request)
  {
    $cover_path = $this->featured_images->isEmpty() ? null : $this->featured_images->first()['path'];
    if ($cover_path) {
      $img = new Image($cover_path);
      $img->resize(450, false, ['mode' => 'crop']);
      $cover_path = $img->getCachedImagePath(true);
    }

    return [
      'id' => $this->id,
      'title' => $this->title,
      'slug' => $this->slug,
      'excerpt' => $this->excerpt,
      // 'content' => $this->content,
      // 'content_html' => $this->content_html,
      'published_at' => Carbon::parse($this->published_at)->format('d-m-Y'),
      'cover_image' => $cover_path
    ];
  }
}
