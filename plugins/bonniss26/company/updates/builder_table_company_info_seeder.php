<?php

namespace Bonniss26\Company\Updates;

use Seeder;
use Bonniss26\Company\Models\CompanyInfo;

class CompanyInfoSeeder extends Seeder
{
    public function run()
    {
        $info = CompanyInfo::create([
            'name' => 'Long Thuận Phát',
            'address_vi' => '',
            'address_en' => '',
            'legal_name_vi' => 'Công ty (ví dụ thôi, cần chỉnh lại) Xây dựng Long Thuận Phát',
            'legal_representative' => 'Phan Anh Thuận',
            'biz_license' => '',
            'biz_license_authority' => '',
            'biz_license_issue_date' => '',
            'email' => 'longthuanphat.cons@gmail.com',
            'fax' => '',
            'hotline' => '',
            'tel1' => '',
            'tel2' => '',
            'founded_date' => '',
            'logo1' => '',
            'logo2' => '',
            'bct_url' => '',
            'slogan_vi' => '',
            'skype' => '',
            'linkedin' => '',
            'facebook' => '',
            'youtube' => '',
            'zalo' => '',
            // 'is_bct_notified' => '01/06/2017',
            // 'short_desc_vi' => '',
            // 'short_desc_en' => '',
            // 'description_vi' => '',
            // 'description_en' => '',
            // 'slogan_en' => '',
        ]);
    }
}
