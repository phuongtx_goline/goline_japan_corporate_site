<?php namespace Bonniss26\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBonniss26CompanyInfo extends Migration
{
    public function up()
    {
        Schema::table('bonniss26_company_info', function($table)
        {
            $table->string('bct_notified_url', 100)->nullable();
            $table->renameColumn('bct_url', 'bct_register_url');
        });
    }
    
    public function down()
    {
        Schema::table('bonniss26_company_info', function($table)
        {
            $table->dropColumn('bct_notified_url');
            $table->renameColumn('bct_register_url', 'bct_url');
        });
    }
}
