<?php

namespace Bonniss26\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBonniss26CompanyFaq extends Migration
{
    public function up()
    {
        Schema::create('bonniss26_company_faq', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('faq_id');
            $table->string('question_vi', 500);
            $table->text('answer_vi');
            $table->string('question_en', 500)->nullable();
            $table->text('answer_en')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bonniss26_company_faq');
    }
}
