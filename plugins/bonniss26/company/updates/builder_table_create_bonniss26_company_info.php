<?php

namespace Bonniss26\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBonniss26CompanyInfo extends Migration
{
    public function up()
    {
        Schema::create('bonniss26_company_info', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 100);
            $table->string('legal_name_vi', 150)->nullable();
            $table->string('legal_name_en', 150)->nullable();
            $table->string('address_vi', 300);
            $table->string('address_en', 300);
            $table->string('email', 300);
            $table->string('fax', 30);
            $table->string('hotline', 30);
            $table->string('tel1', 30);
            $table->string('tel2', 30)->nullable();
            $table->string('founded_date', 50)->nullable();
            $table->string('short_desc_vi', 300)->nullable();
            $table->string('short_desc_en', 300)->nullable();
            $table->string('slogan_vi', 250)->nullable();
            $table->string('slogan_en', 250)->nullable();
            $table->text('description_vi')->nullable();
            $table->text('description_en')->nullable();
            // Social network
            $table->string('facebook', 100)->nullable();
            $table->string('linkedin', 100)->nullable();
            $table->string('zalo', 100)->nullable();
            $table->string('skype', 100)->nullable();
            $table->string('youtube', 100)->nullable();
            $table->string('twitter', 100)->nullable();
            $table->string('gplus', 100)->nullable();
            // Thông tin kinh doanh
            $table->string('biz_license', 100)->nullable();     // Lấy phép kinh doanh
            $table->string('biz_license_authority', 100)->nullable();   // Nơi cấp giấy phép kinh doanh
            $table->string('biz_license_issue_date', 20)->nullable();   // Nơi cấp giấy phép kinh doanh
            $table->string('tax_id', 100)->nullable(); // Mã số thuế
            $table->string('legal_representative', 50)->nullable(); // Người đại diện pháp luật
            $table->boolean('is_bct_notified')->nullable();
            $table->boolean('is_bct_register')->nullable();
            $table->string('bct_url', 100)->nullable();
            // Nhận diện thương hiệu
            $table->string('logo1', 300)->nullable();
            $table->string('logo2', 300)->nullable();
            $table->string('favicon', 300)->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bonniss26_company_info');
    }
}
