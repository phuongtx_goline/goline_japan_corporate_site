<?php

namespace Bonniss26\Company\Updates;

use Seeder;
use Bonniss26\Company\Models\FAQ;

class FAQSeeder extends Seeder
{
    public function run()
    {
        $faqs = [
            [
                'question_vi' => 'Xin cho biết thêm vào lịch sử của công ty?',
                'answer_vi' => '<p>Khó nói lắm</p>',
                'question_en' => '',
                'answer_en' => '',
            ]
        ];

        for ($i = 0; $i < count($faqs); $i++) {
            FAQ::create($faqs[$i]);
        }
    }
}
