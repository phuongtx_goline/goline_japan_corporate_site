<?php

namespace Bonniss26\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBonniss26CompanyServices extends Migration
{
    public function up()
    {
        Schema::create('bonniss26_company_services', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name_vi', 100);
            $table->string('name_en', 100);
            $table->string('short_desc_vi', 200)->nullable();
            $table->string('short_desc_en', 200)->nullable();
            $table->text('long_desc_vi')->nullable();
            $table->text('long_desc_en')->nullable();
            $table->string('slug', 120);
            $table->string('icon', 50); // sử dụng icon từ font awesome chẳng hạn
            // $table->string('illustrated', 50); // Hình minh họa, để tự upload
            $table->boolean('show_on_footer');
            $table->boolean('show_on_service_page');
        });
    }

    public function down()
    {
        Schema::dropIfExists('bonniss26_company_services');
    }
}
