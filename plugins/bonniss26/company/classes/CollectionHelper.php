<?php

namespace Bonniss26\Company\Classes;

use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Container\Container;
use Illuminate\Pagination\LengthAwarePaginator;
use Bonniss26\Company\Classes\Resources\ProductResource;

class CollectionHelper
{
    const PER_PAGE = 12;

    /**
     * paginate
     *
     * @param  Collection $results
     * @param  mixed $transformer
     * @param  mixed $total
     * @param  mixed $pageSize
     * @return void
     */
    public static function paginate(Collection $results, $transformer = NULL, $total = NULL, $pageSize = self::PER_PAGE)
    {
        $page = Paginator::resolveCurrentPage('page');
        $pageResult = NULL;

        if (is_string($transformer) && !empty($transformer))
            $pageResult = call_user_func($transformer, $results->forPage($page, $pageSize));
        else
            $pageResult = $results->forPage($page, $pageSize);

        return self::paginator($pageResult, $total ?? $results->count(), $pageSize, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => 'page',
        ]);
    }

    /**
     * Create a new length-aware paginator instance.
     * Reference: https://medium.com/@sam_ngu/laravel-how-to-paginate-collection-8cb4b281bc55
     * @param  \Illuminate\Support\Collection  $items
     * @param  int  $total
     * @param  int  $perPage
     * @param  int  $currentPage
     * @param  array  $options
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public static function paginator($items, $total, $perPage, $currentPage, $options)
    {
        return Container::getInstance()->makeWith(LengthAwarePaginator::class, compact(
            'items',
            'total',
            'perPage',
            'currentPage',
            'options'
        ));
    }
}
