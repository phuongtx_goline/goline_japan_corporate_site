<?php

namespace Bonniss26\Company\Classes\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use ToughDeveloper\ImageResizer\Classes\Image;
use System\Models\File;

class FileResource extends Resource
{
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
            'file_size' => $this->file_size,
            'sort_order' => $this->sort_order,
            'path' => $this->path,
        ];
    }
}
