<?php

namespace Bonniss26\Company\Classes\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use OFFLINE\Mall\Models\Product;
use ToughDeveloper\ImageResizer\Classes\Image;

class ProductResource extends Resource
{
    private $params = [];

    public function __construct($resource, $pr)
    {
        parent::__construct($resource);
        $this->params = $pr;
    }

    public function toArray($request)
    {
        $slug = $this->params['slug'];

        $categories = $this->categories;
        $relatedProducts = [];

        if ($slug) {
            $relatedProductsId = DB::table('offline_mall_products AS t1')
                ->join('offline_mall_category_product AS t2', 't1.id', '=', 't2.product_id')
                ->whereIn('t2.category_id', $categories->map(function ($item, $key) {
                    return $item->id;
                })->toArray())
                ->where('t1.id', '<>', $this->id)
                ->where('published', 1)
                ->take(10)
                ->select('t1.id')
                ->get()->map(function ($item, $key) {
                    return $item->id;
                });

            $relatedProducts = Product::whereIn('id', $relatedProductsId)->with('image_sets')->get()->map(function ($item, $key) {
                return [
                    'user_defined_id' => $item->user_defined_id,
                    'name' => $item->name,
                    'slug' => $item->slug,
                    'main_image_set' => new ImageSetResource($item->image_sets->where('is_main_set', 1)->first())
                ];
            })->toArray();
        }

        return [
            'user_defined_id' => $this->user_defined_id,
            'name' => $this->name,
            'slug' => $this->slug,
            'description_short' => $this->description_short,
            'description' => $this->description,
            'additional_descriptions' => $this->additional_descriptions,
            'additional_properties' => $this->additional_properties,
            'property_values' => PropertyValueResource::collection($this->property_values),
            'variants' => $this->when($this->inventory_management_method === 'variant', VariantResource::collection($this->variants)),
            'brand' => $this->brand,
            'prices' => $this->when($this->inventory_management_method === 'single', PriceResource::collection($this->prices)),
            'main_image_set' => new ImageSetResource($this->image_sets->where('is_main_set', 1)->first()),
            'downloads' => FileResource::collection($this->downloads),
            'categories' => $this->when(!is_null($slug), CategoryResource::collection($categories)),
            'related' => $this->when(!is_null($slug), $relatedProducts)
        ];
    }
}
