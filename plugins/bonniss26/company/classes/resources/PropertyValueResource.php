<?php

namespace Bonniss26\Company\Classes\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class PropertyValueResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->property_id,
            'name' => $this->property->name,
            'value' => $this->value,
            'unit' => $this->property->unit
        ];
    }
}
