<?php

namespace Bonniss26\Company\Classes\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use ToughDeveloper\ImageResizer\Classes\Image;
use System\Models\File;
use OFFLINE\Mall\Models\Price;


class PriceResource extends Resource
{
    public function toArray($request)
    {
        $resource = $this->resource;

        return [
            'price' => $this->price,
            'price_formatted' => $resource->getStringAttribute(),
            'currency' => [
                'code' => $this->currency->code,
                'symbol' => $this->currency->symbol,
            ]
        ];
    }
}
