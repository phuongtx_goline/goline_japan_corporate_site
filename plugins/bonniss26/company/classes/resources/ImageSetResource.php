<?php

namespace Bonniss26\Company\Classes\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use ToughDeveloper\ImageResizer\Classes\Image;

class ImageSetResource extends Resource
{
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'is_main_set' => $this->is_main_set,
            'images' => FileResource::collection($this->images),
        ];
    }
}
