<?php

namespace Bonniss26\Company\Classes\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use ToughDeveloper\ImageResizer\Classes\Image;

class VariantResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'user_defined_id' => $this->user_defined_id,
            'stock' => $this->stock,
            // Relationship
            'property_values' => PropertyValueResource::collection($this->property_values),
            'image_set' => new ImageSetResource($this->image_sets),
            'prices' => PriceResource::collection($this->prices),
        ];
    }
}
