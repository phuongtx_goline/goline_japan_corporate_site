<?php

namespace Bonniss26\Company\Classes\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Carbon\Carbon;
use Bonniss26\Company\Classes\CollectionHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use ToughDeveloper\ImageResizer\Classes\Image;
use System\Models\File;
use OFFLINE\Mall\Models\Category;

class CategoryResource extends Resource
{
    private $params = [];

    private static $using = [];

    public function __construct($resource, $pr)
    {
        parent::__construct($resource);
        $this->params = $pr;
    }

    public static function using($using = [])
    {
        static::$using = $using;
    }

    public function toArray($request)
    {
        $category = $this->resource;

        if ($category instanceof Category) {
            $children = Category::where('parent_id', $category->id)->get();

            $products = $this->products->where('published', 1);
            $paginatedProducts = CollectionHelper::paginate($products, 'Bonniss26\Company\Classes\Resources\ProductResource::collection');

            $slug = $this->params['slug'];

            return [
                'name' => $this->name,
                'slug' => $this->slug,
                'code' => $this->code,
                'slug' => $this->slug,
                'children' => $this->when($children->isNotEmpty(), CategoryResource::collection($children)),
                'products' => $this->when(!is_null($slug), $paginatedProducts)
            ];
        }

        return NULL;
    }
}
