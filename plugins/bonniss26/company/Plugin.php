<?php

namespace Bonniss26\Company;

use System\Classes\PluginBase;
use Event;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Bonniss26\Company\Components\CompanyInfo'  => 'companyInfo'
        ];
    }

    public function registerSettings()
    {
    }

    public function boot()
    {
        Event::listen('backend.menu.extendItems', function ($manager) {
            if (config('app.debug') === false) {
                $manager->removeMainMenuItem('Rainlab.Builder', 'builder');
            }
            $manager->addMainMenuItems('RainLab.Blog', [
                'blog' => [
                    'iconSvg'   => 'plugins/bonniss26/company/assets/images/pen.svg',
                ],
            ]);
            $manager->addMainMenuItems('RainLab.Pages', [
                'pages' => [
                    'iconSvg'   => 'plugins/bonniss26/company/assets/images/chef.svg',
                ],
            ]);
            $manager->addMainMenuItems('October.Backend', [
                'dashboard' => [
                    'iconSvg'   => 'plugins/bonniss26/company/assets/images/analytics.svg',
                ],
                'media' => [
                    'iconSvg'   => 'plugins/bonniss26/company/assets/images/photograph.svg',
                ],
            ]);
            $manager->addMainMenuItems('October.Cms', [
                'cms' => [
                    'iconSvg'   => 'plugins/bonniss26/company/assets/images/internet.svg',
                ],
            ]);
            $manager->addMainMenuItems('October.System', [
                'system' => [
                    'iconSvg'   => 'plugins/bonniss26/company/assets/images/gear.svg',
                ],
            ]);
        });

        Event::listen('backend.form.extendFieldsBefore', function ($widget) {
            // Extend only the Blog\Posts controller & Extend only Blog\Post model
            if (!($widget->getController() instanceof \RainLab\Blog\Controllers\Posts
                && $widget->model instanceof \RainLab\Blog\Models\Post)) {
                return;
            }

            $widget->secondaryTabs['fields']['content']['type'] = 'richeditor';
        });
    }
}
