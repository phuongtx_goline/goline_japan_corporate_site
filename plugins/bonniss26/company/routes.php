<?php

use RainLab\Blog\Models\Post;
use RainLab\Blog\Models\Category;
use Bonniss26\Company\Models\BlogPostResource;
use Bonniss26\Company\Models\BlogCategoryResource;
use Illuminate\Http\Request;

Route::prefix('api/v1')->group(function () {
  /**
   * api/v1/blog-categories?top=3 || &ordinal=3 || &postEach=5
   */
  Route::get('blog-categories', function (Request $request) {
    $ordinal = (int) $request->query('ordinal');
    $top = (int) $request->query('top');
    $collection = Category::all();

    /**
     * Take the first `top` categories (1-based)
     * Ignore `ordinal` if `top` is a positive integer
     */
    if ($top > 0) {
      $collection = $collection->take($top);
    } else if ($ordinal > 0) {
      /**
       * Get the category at the index of `ordinal` (1-based)
       */
      $collection = $collection->slice($ordinal - 1, 1);
    }

    return $collection->count() === 1 ? new BlogCategoryResource($collection->first()) : BlogCategoryResource::collection($collection);
  });

  Route::get('blog-categories/{slug}', function ($slug) {
    return new BlogCategoryResource(Category::where('slug', $slug)->first());
  });

  /*----------------------------------------------------------------*/
  Route::get('blog-posts', function () {
    return BlogPostResource::collection(Post::where('published', 1)->get());
  });
  Route::get('blog-posts/{slug}', function ($slug) {
    return new BlogPostResource(Post::where('slug', $slug)->first());
  });
});
