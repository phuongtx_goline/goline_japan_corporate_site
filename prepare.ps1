# ----------------------------------#
# Prepare App Info Before Deploy CMS
# Author: Bonniss26
# Github: https://github.com/bonniss
# ----------------------------------#

$zipName = Get-ChildItem build*.zip | Select-Object -Expand Name

if ($zipName -is [array]) {
    $zipName = $zipName[0]
}

# If file is empty
if ([string]::IsNullOrWhitespace($zipName)) {
    Write-Error 'ERROR: No valid zip file in current directory'
    exit
}

# Extract `config` folder
Add-Type -Assembly System.IO.Compression.FileSystem
#-- Extract list entries
$zip = [IO.Compression.ZipFile]::OpenRead($zipName)
$entries = $zip.Entries | Where-Object {$_.FullName -like 'config/*' -and $_.FullName -ne 'config/'}
#-- Create dir for result of extraction
New-Item -ItemType Directory -Path 'config' -Force
#-- Extraction
$entries | ForEach-Object {[IO.Compression.ZipFileExtensions]::ExtractToFile( $_, 'config/' + $_.Name) }
#-- Free object
$zip.Dispose()

# Read `env` file
[string[]] $envLines = Get-Content -Path 'env'

if ($envLines -gt 0) {
    $envLines | ForEach-Object {
        $_ = $_.Trim()
    }
    $appName = $envLines[0]
    $appUrl = $envLines[1]
    $dbName = $envLines[2]
    $dbUser = $envLines[3]
    $dbPassword = $envLines[4]
}

while ([string]::IsNullOrWhiteSpace($appName)) {
    $appName = Read-Host '> Your app name';
}
while ([string]::IsNullOrWhiteSpace($appUrl)) {
    $appUrl = Read-Host '> Your app URL';
}
while ([string]::IsNullOrWhiteSpace($dbName)) {
    $dbName = Read-Host '> Your database name';
}
while ([string]::IsNullOrWhiteSpace($dbUser)) {
    $dbUser = Read-Host '> Your user name';
}
while ([string]::IsNullOrWhiteSpace($dbPassword)) {
    $dbPassword = Read-Host '> Your password';
}

# Find and Replace
(Get-Content 'config/app.php') `
    -replace "('name' =>\s*').*(',)", ('$1' + $appName + '$2') `
    -replace "('url' =>\s*').*(',)", ('$1' + $appUrl + '$2') | Out-File 'config/app.php' -Encoding 'utf8';

(Get-Content 'config/database.php') `
    -replace "('database' =>\s*').*(',)(.*\/\* REPLACE YOURS \! \*\/)", ('$1' + $dbName + '$2$3') `
    -replace "('username' =>\s*').*(',)(.*\/\* REPLACE YOURS \! \*\/)", ('$1' + $dbUser + '$2$3') `
    -replace "('password' =>\s*').*(',)(.*\/\* REPLACE YOURS \! \*\/)", ('$1' + $dbPassword + '$2$3') | Out-File 'config/database.php' -Encoding 'utf8';

# Adding unzipped files to a zipped folder
Compress-Archive -Path 'config' -Update -DestinationPath $zipName
Remove-Item -Path 'config' -Force -Recurse

Write-Host '>> Preparation completes!'
