<?php

namespace frontend\models;

use common\models\ContractType;
use common\models\Customer;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 */
class RegisterForm extends CustomerLite
{

    public $contract_type;
    public $invest_value;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['mobile', 'trim'],
            ['mobile', 'required'],
            ['mobile', 'unique', 'targetClass' => '\common\models\Customer'],
            ['mobile', 'string', 'min' => 10, 'max' => 255],

            ['email_addr', 'trim'],
            ['email_addr', 'required'],
            ['email_addr', 'email'],
            ['email_addr', 'string', 'max' => 255],
            ['email_addr', 'unique', 'targetClass' => '\common\models\Customer'],

            [['mobile', 'name', 'contract_type', 'invest_value'], 'required'],
            [['name', 'id_place',], 'string', 'max' => 200],
            [['tax_code', 'id_number'], 'string', 'max' => 30],
            [['id_issue_date'], 'number'],
            [['mobile'], 'string', 'max' => 12],
            [['email_addr'], 'string', 'max' => 50],


        ];
    }


    /**
     *
     * @return CustomerLite|null the saved model or null if saving fails
     */
    public function register()
    {
        if (!$this->validate()) {
            return null;
        }

        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $contractType = ContractType::findOne(['status' => $recordStatus['active'], 'id' => $this->contract_type]);

        $customer = new CustomerLite();
        $customer->attributes = $this->attributes;
        $customer->password = md5(rand());
        $customer->org_type = 0;
        $customer->foreign_type = 0;
        $customer->gender = 0;
        $customer->need_change_pw = 0;
        $customer->is_locked = 0;
        $customer->image = '';
        $customer->reg_date_time = date('Y-m-d H:i:s');
        $customer->reg_ip_addr = Yii::$app->getRequest()->getUserIP();
        $customer->save();

        $contract = new ContractLite();
        $contract->customer_id = $customer->getPrimaryKey();
        $contract->invest_value = str_replace(',', '', $this->invest_value);
        $contract->contract_date = date('Ymd');
        $contract->contract_type_id = $contractType->getPrimaryKey();
        $contract->period_type = $contractType->period_type;
        $contract->period_val = $contractType->period_val;
        $contract->base_profit_rate = $contractType->base_profit_rate;
        $contract->profit_rate = $contractType->profit_rate;
        $contract->contract_type_code = $contractType->code;
        $contract->contract_type_name = $contractType->name;
        $contract->reg_date_time = date('Y-m-d H:i:s');
        $contract->reg_ip_addr = Yii::$app->getRequest()->getUserIP();
        $contract->save();

        $contract->contract_no = $contract->getPrimaryKey() . '/' . date('Y') . '/' . $contractType->code;
        $contract->save();

        return true;
    }
}
