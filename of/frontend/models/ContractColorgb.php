<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Contract;
use yii\helpers\ArrayHelper;

/**
 * ContractColorgb represents the model behind the search form about `common\models\FundUnit`.
 */
class ContractColorgb extends Contract
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'contract_type_id', 'period_type', 'period_val', 'status', 'annouce_status', 'response_status', 'active_user_id', 'settle_user_id', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['contract_info', 'contract_no', 'contract_type_code', 'contract_type_name', 'remarks', 'response_content', 'active_date_time', 'settle_date_time', 'reg_date_time', 'reg_ip_addr', 'upd_date_time', 'upd_ip_addr'], 'safe'],
            [['fund_net_asset', 'fund_unit_qty', 'fund_unit_price', 'invest_value', 'invest_unit_qty', 'base_profit_rate', 'profit_rate', 'contract_date', 'begin_date', 'end_date_est', 'end_date_real', 'settle_fee_amt'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // Bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $query = Contract::find()->where(['!=', 'status', $recordStatus['deleted']])->andWhere(['customer_id' => Yii::$app->user->identity->getId()]);

        $this->load($params);

        if ($this->status == $recordStatus['deleted']) {
            $query = Contract::find()->where(['status' => $recordStatus['deleted']]);
        }

        // Add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['reg_date_time' => SORT_DESC]]

        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // Grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'fund_net_asset' => $this->fund_net_asset,
            'fund_unit_qty' => $this->fund_unit_qty,
            'fund_unit_price' => $this->fund_unit_price,
            'invest_value' => $this->invest_value,
            'invest_unit_qty' => $this->invest_unit_qty,
            'contract_type_id' => $this->contract_type_id,
            'period_type' => $this->period_type,
            'period_val' => $this->period_val,
            'base_profit_rate' => $this->base_profit_rate,
            'profit_rate' => $this->profit_rate,
            'contract_date' => $this->contract_date,
            'begin_date' => $this->begin_date,
            'end_date_est' => $this->end_date_est,
            'end_date_real' => $this->end_date_real,
            'settle_fee_amt' => $this->settle_fee_amt,
            'status' => $this->status,
            'annouce_status' => $this->annouce_status,
            'response_status' => $this->response_status,
            'active_date_time' => $this->active_date_time,
            'active_user_id' => $this->active_user_id,
            'settle_date_time' => $this->settle_date_time,
            'settle_user_id' => $this->settle_user_id,
            'reg_date_time' => $this->reg_date_time,
            'reg_user_id' => $this->reg_user_id,
            'upd_date_time' => $this->upd_date_time,
            'upd_user_id' => $this->upd_user_id,
        ]);

        $query->andFilterWhere(['like', 'contract_no', $this->contract_no])
            ->andFilterWhere(['like', 'contract_type_code', $this->contract_type_code])
            ->andFilterWhere(['like', 'contract_type_name', $this->contract_type_name])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'response_content', $this->response_content])
            ->andFilterWhere(['like', 'reg_ip_addr', $this->reg_ip_addr])
            ->andFilterWhere(['like', 'upd_ip_addr', $this->upd_ip_addr]);

        return $dataProvider;
    }
}
