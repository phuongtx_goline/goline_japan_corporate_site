<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "contract".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $invest_value
 * @property string $contract_date
 * @property integer $contract_no
 * @property integer $contract_type_id
 * @property string $contract_type_code
 * @property string $contract_type_name
 * @property string $reg_date_time
 * @property string $reg_ip_addr
 * @property integer $period_type
 * @property integer $period_val
 * @property string $base_profit_rate
 * @property string $profit_rate
 */
class ContractLite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contract';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'invest_value', 'contract_date', 'contract_type_id', 'contract_type_code', 'contract_type_name', 'reg_date_time', 'reg_ip_addr'], 'required'],
            [['contract_no', 'period_type', 'period_val', 'base_profit_rate', 'profit_rate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Khách hàng',
            'invest_value' => 'Số vốn đầu tư',
            'contract_no' => 'Số H/Đ',
            'contract_type_id' => 'Loại H/Đ',
            'contract_type_code' => 'Mã gói',
            'contract_type_name' => 'Tên gói',
            'contract_date' => 'Ngày tạo H/Đ',
            'reg_date_time' => 'Thời gian tạo',
            'reg_ip_addr' => 'IP tạo',
        ];
    }
}
