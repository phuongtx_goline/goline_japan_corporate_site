<?php

namespace frontend\models;

use common\models\ContractType;
use common\models\Customer;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 */
class ContractForm extends ContractLite
{

    public $contract_type;
    public $invest_value;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contract_type', 'invest_value'], 'required'],
        ];
    }

    /**
     *
     * @return ContractLite|null the saved model or null if saving fails
     */
    public function register()
    {
        if (!$this->validate()) {
            return null;
        }

        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $contractType = ContractType::findOne(['status' => $recordStatus['active'], 'id' => $this->contract_type]);

        $contract = new ContractLite();
        $contract->customer_id = Yii::$app->user->identity->getId();
        $contract->invest_value = str_replace(',', '', $this->invest_value);
        $contract->contract_date = date('Ymd');
        $contract->contract_type_id = $contractType->getPrimaryKey();
        $contract->contract_type_code = $contractType->code;
        $contract->contract_type_name = $contractType->name;
        $contract->reg_date_time = date('Y-m-d H:i:s');
        $contract->reg_ip_addr = Yii::$app->getRequest()->getUserIP();
        $contract->save();

        $contract->contract_no = $contract->getPrimaryKey() . '/' . date('Y') . '/' . $contractType->code;
        $contract->save();

        return true;
    }
}
