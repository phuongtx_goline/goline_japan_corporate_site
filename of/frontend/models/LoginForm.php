<?php

namespace frontend\models;

use common\models\Customer;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Số điện thoại/email',
            'password' => 'Mật khẩu',
            'rememberMe' => 'Ghi nhớ'
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Thông tin đăng nhập không hợp lệ hoặc tài khoản chưa được kích hoạt.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }

        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return Customer|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $active = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value')['active'];
            $this->_user = Customer::find()
                ->where([
                    'mobile' => $this->username,
                    'status' => $active,
                    'is_locked' => 0
                ])
                ->orWhere([
                    'email_addr' => $this->username,
                    'status' => $active,
                    'is_locked' => 0
                ])->one();
            if ($this->_user) {
                $this->_user->last_login_time = date('Y-m-d H:i:s');
                $this->_user->save();
            }

        }

        return $this->_user;
    }
}
