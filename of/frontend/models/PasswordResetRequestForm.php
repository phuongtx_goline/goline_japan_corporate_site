<?php

namespace frontend\models;

use common\colorgb\ColorgbHelper;
use common\models\Customer;
use Yii;
use yii\helpers\ArrayHelper;

/**
 */
class PasswordResetRequestForm extends Customer
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email_addr', 'trim'],
            ['email_addr', 'required'],
            ['email_addr', 'email'],
            ['email_addr', 'exist',
                'targetClass' => '\common\models\Customer',
                'filter' => [
                    'status' => ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value')['active'],
                    'is_locked' => 0
                ],
                'message' => 'Không thể thiết lập lại mật khẩu với email cung cấp'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail($customer = null)
    {
        /* @var $user Customer */
        if (!$customer) {
            $user = Customer::findOne([
                'status' => ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value')['active'],
                'is_locked' => 0,
                'email_addr' => $this->email_addr,
            ]);
        }

        if (!$user) {
            return false;
        }

        if (!Customer::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

        $sendMail = ColorgbHelper::sendMail('Thiết lập lại mật khẩu','passwordResetToken', $this->email_addr, $user);
        return $sendMail ? true : false;
    }
}
