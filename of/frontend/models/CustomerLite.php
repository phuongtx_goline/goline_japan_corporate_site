<?php

namespace frontend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "customer".
 *
 * @property integer $id
 * @property string $mobile
 * @property string $password
 * @property string $name
 * @property string $email_addr
 * @property integer $org_type
 * @property integer $foreign_type
 * @property string $nationality
 * @property integer $gender
 * @property string $prefix
 * @property string $birthday
 * @property string $address1
 * @property string $address2
 * @property string $tax_code
 * @property string $id_number
 * @property string $id_issue_date
 * @property string $id_expired_date
 * @property string $id_place
 * @property string $bank_name
 * @property string $bank_branch_name
 * @property string $bank_acco_no
 * @property string $bank_acco_name
 * @property string $mobile1
 * @property string $mobile2
 * @property string $email_addr1
 * @property string $email_addr2
 * @property string $image
 * @property string $org_info
 * @property integer $profession_id
 * @property integer $is_locked
 * @property string $last_login_time
 * @property string $need_change_pw
 * @property string $password_reset_token
 * @property string $auth_key
 * @property string $file_upload
 * @property string $reg_date_time
 * @property string $reg_ip_addr
 */
class CustomerLite extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer';
    }

    public $file_upload;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['org_type', 'foreign_type', 'gender', 'info_source_id', 'profession_id'], 'integer'],
            [['birthday', 'id_issue_date', 'id_expired_date', 'need_change_pw'], 'number'],
            [['mobile', 'mobile1', 'mobile2'], 'string', 'max' => 12],
            [['password', 'bank_name'], 'string', 'max' => 100],
            [['name', 'id_place', 'bank_branch_name', 'bank_acco_name'], 'string', 'max' => 200],
            [['email_addr', 'nationality', 'email_addr1', 'email_addr2'], 'string', 'max' => 50],
            [['prefix'], 'string', 'max' => 20],
            [['last_login_time', 'reg_date_time', 'is_locked'], 'safe'],
            [['address1', 'address2', 'character'], 'string', 'max' => 300],
            [['tax_code', 'id_number', 'bank_acco_no'], 'string', 'max' => 30],
            [['image', 'password_reset_token', 'auth_key'], 'string', 'max' => 255],
            [['org_info'], 'string', 'max' => 500],

            [['mobile', 'mobile1', 'mobile2'], 'trim'],
            [['mobile', 'mobile1', 'mobile2'], 'unique', 'targetClass' => '\frontend\models\CustomerLite'],

            [['email_addr', 'email_addr1', 'email_addr2'], 'trim'],
            [['email_addr', 'email_addr1', 'email_addr2'], 'email'],
            [['email_addr', 'email_addr1', 'email_addr2'], 'unique', 'targetClass' => '\frontend\models\CustomerLite'],
            [['file_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'doc, docx, pdf, png, gif, jpg, jpeg'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mobile' => 'Số điện thoại',
            'password' => 'Mật khẩu',
            'name' => 'Họ tên KH',
            'email_addr' => 'Địa chỉ email',
            'org_type' => 'Cá nhân/Tổ chức',
            'foreign_type' => 'Nơi cư ngụ',
            'nationality' => 'Quốc gia',
            'gender' => 'Giới tính',
            'prefix' => 'Danh xưng',
            'birthday' => 'Ngày sinh',
            'address1' => 'Địa chỉ 1',
            'address2' => 'Địa chỉ 2',
            'tax_code' => 'Mã số thuế',
            'id_number' => 'Số CMT',
            'id_issue_date' => 'Ngày cấp',
            'id_expired_date' => 'Ngày hết hạn',
            'id_place' => 'Nơi cấp',
            'bank_name' => 'Ngân hàng',
            'bank_branch_name' => 'Chi nhánh ngân hàng',
            'bank_acco_no' => 'Số TK ngân hàng',
            'bank_acco_name' => 'Tên chủ TK ngân hàng',
            'mobile1' => 'Số điện thoại phụ 1',
            'mobile2' => 'Số điện thoại phụ 2',
            'email_addr1' => 'Địa chỉ email phụ 1',
            'email_addr2' => 'Địa chỉ email phụ 2',
            'image' => 'Ảnh đại diện',
            'file_upload' => 'Ảnh đại diện',
            'info_source_id' => 'Nguồn thông tin',
            'org_info' => 'Thông tin tổ chức',
            'profession_id' => 'Nghề nghiệp',
            'last_login_time' => 'Thời điểm đăng nhập lần cuối',
            'need_change_pw' => 'Yêu cầu đổi mật khẩu',
            'password_reset_token' => 'Mã thiết lập lại mật khẩu',
            'auth_key' => 'Khóa xác thực tài khoản',
            'contract_type' => 'Loại hợp đồng',
            'invest_value' => 'Số vốn đầu tư',
            'reg_date_time' => 'Thời gian tạo',
            'reg_ip_addr' => 'IP tạo',
        ];
    }

}
