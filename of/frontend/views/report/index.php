<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use common\colorgb\ColorgbHelper;
use common\models\Setting;
use common\models\Customer;
use common\models\Contract;

/* @var $this yii\web\View */
/* @var $model common\models\Customer */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Báo cáo hiệu quả đầu tư';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('_index', ['model' => $model]); ?>