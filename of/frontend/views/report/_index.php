<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use common\colorgb\ColorgbHelper;
use common\models\Setting;
use common\models\Customer;
use common\models\Contract;

/* @var $this yii\web\View */
/* @var $model common\models\Customer */
/* @var $form yii\widgets\ActiveForm */

$customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
$recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
$contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
$fundUnit = ColorgbHelper::fundUnit();
$fundUnitHsa = ColorgbHelper::fundUnitHsa();
$fup = common\models\FundUnit::find()->where(['like', 'date', date('Y')])->orderBy(['date' => SORT_ASC])->one()->fund_unit_price;
$fupHsa = common\models\FundUnitHsa::find()->where(['like', 'date', date('Y')])->orderBy(['date' => SORT_ASC])->one()->fund_unit_price;
if ($data['contract'] == true) {
    $contract1 = Contract::find()->where(['id' => $model->id,'contract_type_id'=>[1,2]])->andWhere(['>=', 'status', $contractStatus['active']])->andWhere(['!=', 'status', $contractStatus['settled']])->orderBy(['begin_date' => SORT_ASC])->all();
    $contract2 = Contract::find()->where(['id' => $model->id,'contract_type_id'=>[3]])->andWhere(['>=', 'status', $contractStatus['active']])->andWhere(['!=', 'status', $contractStatus['settled']])->orderBy(['begin_date' => SORT_ASC])->all();
} else {
    $customers = Customer::findAll(['parent_id' => $model->getPrimaryKey(), 'status' => $customerStatus['active']]);
    $customerIds = $data['group'] == true && $customers ? ArrayHelper::map($customers, 'id', 'id') : [];
    $customerIds[$model->getPrimaryKey()] = $model->getPrimaryKey();
    $contract1 = Contract::find()->where(['customer_id' => $customerIds,'contract_type_id'=>[1,2]])->andWhere(['>=', 'status', $contractStatus['active']])->andWhere(['!=', 'status', $contractStatus['settled']])->orderBy(['begin_date' => SORT_ASC])->all();
    $contract2 = Contract::find()->where(['customer_id' => $customerIds,'contract_type_id'=>[3]])->andWhere(['>=', 'status', $contractStatus['active']])->andWhere(['!=', 'status', $contractStatus['settled']])->orderBy(['begin_date' => SORT_ASC])->all();
}

?>
<?php if ($contract1 || $contract2): ?>

    <?php if ($contract1): ?>
        <div class="colorgb-container">
            <div class="panel panel-white colorgb-report-panel">
                <div class="panel-heading">
                    <h6 class="panel-title">Báo cáo hiệu quả đầu tư của khách hàng (PI)</h6>
                    <div class="heading-elements">
                        <?php if ($data['contract'] != true && $data['group'] != true): ?>
                            <a data-pjax="0" href="report/weekly-report-download" class="btn btn-default btn-xs heading-btn"><i class="icon-download position-left"></i> Tải về báo cáo tuần</a>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="panel-body no-padding-bottom">
                    <div class="row">
                        <div class="col-xs-6 content-group">
                            <ul class="list-condensed list-unstyled">
                                <li>Lợi nhuận từ năm <?= date('Y') ?></li>
                                <li><span class="text-semibold"><?= round(($fundUnit['last']->fund_unit_price - $fup) / $fup * 100, 2) ?>%</span></li>
                            </ul>
                        </div>

                        <div class="col-xs-6 content-group">
                            <div class="invoice-details pull-right text-right">
                                <ul class="list-condensed list-unstyled">
                                    <li>Từ ngày: <span class="colorgb-semibold"><?= $contract1[0]->begin_date ? ColorgbHelper::date($contract1[0]->begin_date) : ColorgbHelper::date($fundUnit['first']->date) ?></span></li>
                                    <li>Đến ngày: <span class="colorgb-semibold"><?= ColorgbHelper::date($fundUnit['last']->date) ?></span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb-20 chart-container-<?= $model->getPrimaryKey() ?>" style="min-width: 840px;">
                    <h3 class="text-uppercase text-semibold text-center">Hiệu quả đầu tư - Passion Investment</h3>
                    <div class="chart-responsive">
                        <div class="chart" id="d3-line-<?= $model->getPrimaryKey() ?>"></div>
                    </div>
                </div>

                <h5 class="text-semibold text-uppercase pl-15 mt-20 mb-20 text-blue-800">

                    Khách hàng: <?= $data['contract'] == true ? Customer::findOne($model->customer_id)->name : $model->name ?>

                </h5>

                <?php if (!empty($contract1)): ?>
                    <div class="table-responsive">
                        <table class="table table-lg">
                            <thead>
                            <tr>

                                <?php if ($data['contract'] != true): ?>
                                    <th width="60px">HĐ</th>
                                    <th width="20px">Số HĐ</th>
                                <?php endif; ?>

                                <th width="120px" class="text-right">Ngày ký HĐ</th>
                                <th class="text-right">Số lượng ĐVĐT (1)</th>
                                <th class="text-right">Giá ĐVĐT (2)</th>
                                <th class="text-right">Tổng giá trị mua (1)x(2)</th>

                                <?php if ($data['contract'] == true): ?>
                                    <th class="text-right">Giá ĐVĐT hiện tại</th>
                                <?php endif; ?>

                                <th class="text-right">Lợi nhuận</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($contract1 as $key => $value): ?>
                                <?php
                                $subProfitRate = ($fundUnit['last']->fund_unit_price - $value->fund_unit_price) / $value->fund_unit_price * 100;
                                $subTotal = round($value->fund_unit_price * $value->invest_unit_qty, -5);
                                $subProfit = $subProfitRate * $subTotal / 100;
                                $qty += $value->invest_unit_qty;
                                $total += $subTotal;
                                $profit += $subProfit;
                                ?>
                                <tr>
                                    <?php if ($data['contract'] != true): ?>
                                        <td class="text-right">
                                            <a href="contract/view?id=<?= $value->id ?>" title="Xem hợp đồng" class="btn-colorgb-popup" data-pjax="0"><span class="icon-file-pdf"></span></a>
                                        </td>

                                        <td>
                                            <?= $value->contract_no ?>
                                        </td>
                                    <?php endif; ?>

                                    <td class="text-right"><?= ColorgbHelper::date($value->begin_date) ?></td>
                                    <td class="text-right"><?= \common\colorgb\ColorgbHelper::numberFormat($value->invest_unit_qty) ?> ĐVĐT</td>
                                    <td class="text-right"><?= \common\colorgb\ColorgbHelper::numberFormat($value->fund_unit_price) ?> đ</td>
                                    <td class="text-right"><?= \common\colorgb\ColorgbHelper::numberFormat($subTotal) ?> đ</td>

                                    <?php if ($data['contract'] == true): ?>
                                        <td class="text-right"><?= \common\colorgb\ColorgbHelper::numberFormat($fundUnit['last']->fund_unit_price) ?> đ</td>
                                    <?php endif; ?>

                                    <td class="text-right <?= $subProfit > 0 ? 'text-green-600' : 'text-warning' ?>"><?= \common\colorgb\ColorgbHelper::numberFormat($subProfit) ?> đ <br>(<?= round($subProfitRate, 2) ?>%)</td>
                                </tr>
                            <?php endforeach; ?>

                            <?php $totalCurrent = $qty * $fundUnit['last']->fund_unit_price; ?>

                            <?php if ($data['contract'] != true): ?>
                                <tr>
                                    <td>

                                    </td>
                                    <td>

                                    </td>
                                    <td class="text-right"><strong>Tổng cộng</strong></td>
                                    <td class="text-right"><strong><?= \common\colorgb\ColorgbHelper::numberFormat($qty) ?> ĐVĐT</strong></td>
                                    <td class="text-right"></td>
                                    <td class="text-right"><strong><?= \common\colorgb\ColorgbHelper::numberFormat($total) ?> đ</strong></td>
                                    <td class="text-right <?= $profit > 0 ? 'text-green-600' : 'text-warning' ?>"><strong><?= \common\colorgb\ColorgbHelper::numberFormat($profit) ?> đ <br> (<?= round($profit / $total * 100, 2) ?>%)</strong></td>
                                </tr>
                            <?php endif; ?>

                            </tbody>

                        </table>


                    </div>

                    <?php if ($data['contract'] != true): ?>
                        <div class="panel-body">
                            <div class="row invoice-payment">
                                <div class="col-xs-12 col-sm-7 col-sm-offset-5">
                                    <div class="content-group">
                                        <h6 class="pl-15">Thống kê chi tiết</h6>
                                        <div class="table-responsive no-border">
                                            <table class="table">
                                                <tbody>
                                                <tr>
                                                    <th width="40%">Tổng số đơn vị đầu tư hiện tại của khách hàng <span class="text-regular">(3)</span></th>
                                                    <td class="text-right"><h5 class="text-semibold"><?= \common\colorgb\ColorgbHelper::numberFormat($qty) ?> ĐVĐT</h5></td>
                                                </tr>
                                                <tr>
                                                    <th width="40%">Giá trị 1 ĐVĐT hiện tại <span class="text-regular">(4)</span></th>
                                                    <td class="text-right"><h5 class="text-semibold"><?= \common\colorgb\ColorgbHelper::numberFormat($fundUnit['last']->fund_unit_price) ?> đ</h5></td>
                                                </tr>
                                                <tr>
                                                    <th width="40%">Tổng số tiền hiện tại của khách hàng <span class="text-regular">(3)x(4)</span></th>
                                                    <td class="text-right"><h5 class="text-semibold"><?= \common\colorgb\ColorgbHelper::numberFormat($totalCurrent) ?> đ</h5></td>
                                                </tr>
                                                <tr>
                                                    <th width="40%">Tổng số vốn đầu tư của khách hàng</th>
                                                    <td class="text-right"><h5 class="text-semibold"><?= \common\colorgb\ColorgbHelper::numberFormat($total) ?> đ</h5></td>
                                                </tr>
                                                <tr>
                                                    <th width="40%">Khách hàng có lãi tạm tính trả trước phí</th>
                                                    <td class="text-right <?= $profit > 0 ? 'text-green-600' : 'text-warning' ?>"><h5 class="text-semibold"><?= \common\colorgb\ColorgbHelper::numberFormat($profit) ?> đ</h5></td>
                                                </tr>
                                                <tr>
                                                    <th width="40%">Tổng % lợi nhuận đạt được</th>
                                                    <td class="text-right <?= $profit > 0 ? 'text-green-600' : 'text-warning' ?>"><h5 class="text-semibold"><?= round($profit / $total * 100, 2) ?>%</h5></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    <?php endif; ?>

                <?php endif; ?>

            </div>
        </div>
        <style>
            .pace .pace-progress {
                background: none;
            }

            .colorgb-report i {
                font-size: 13px;
            }

            <?php if(Yii::$app->request->get('layout') == 'sub'):?>
            body {
                background: #fff;
            }

            .page-container {
                padding: 0;
            }

            <?php endif;?>
        </style>
        <script>

            // Chart setup
            function lineBasic<?=$model->getPrimaryKey()?>(element, width, height) {

                // Define main variables
                var d3Container = d3.select(element),
                    margin = {top: 5, right: 20, bottom: 20, left: 50},
                    width = width,
                    height = height - margin.top - margin.bottom - 5;

                // Format data
                var parseDate = d3.time.format("%Y%m%d").parse,
                    bisectDate = d3.bisector(function (d) {
                        return d.date;
                    }).left,
                    formatValue = d3.format(","),
                    formatCurrency = function (d) {
                        return formatValue(d) + '';
                    };


                // Horizontal
                var x = d3.time.scale()
                    .range([0, width]);

                // Vertical
                var y = d3.scale.linear()
                    .range([height, 0]);

                // Horizontal
                var xAxis = d3.svg.axis()
                    .scale(x)
                    .orient("bottom")
                    .tickSize(-height, 0, 0)
                    .tickFormat(d3.time.format("%d/%m/%y"));

                // Vertical
                var yAxis = d3.svg.axis()
                    .scale(y)
                    .tickSize(-width, 0, 0)
                    .orient("left");


                // Add SVG element
                var container = d3Container.append("svg");

                // Add SVG group
                var svg = container
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


                // Line
                var line = d3.svg.line()
                    .interpolate("basis")
                    .x(function (d) {
                        return x(d.date);
                    })
                    .y(function (d) {
                        return y(d.close);
                    });


                // Load data
                d3.tsv("<?=Yii::$app->params['cdnUrl']?>/data/investment/report/<?=$contract1[0]->begin_date ? $contract1[0]->begin_date : $fundUnit['first']->date ?>.tsv?colorgb=<?=time()?>", function (error, data) {

                    // Pull out values
                    data.forEach(function (d) {
                        d.date = parseDate(d.date);
                        d.close = +d.close;
                    });

                    // Sort data
                    data.sort(function (a, b) {
                        return a.date - b.date;
                    });

                    // Horizontal
                    x.domain(d3.extent(data, function (d) {
                        return d.date;
                    }));

                    // Vertical
                    y.domain([0, d3.max(data, function (d) {
                        return d.close;
                    })]);

                    // Add line
                    svg.append("path")
                        .datum(data)
                        .attr("class", "d3-line d3-line-medium")
                        .attr("d", line)
                        .style("fill", "none")
                        .style("stroke-width", 3)
                        .style("stroke", "#4CAF50");


                    // Horizontal
                    svg.append("g")
                        .attr("class", "d3-axis d3-axis-horizontal d3-axis-strong")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    // Vertical
                    var verticalAxis = svg.append("g")
                        .attr("class", "d3-axis d3-axis-vertical d3-axis-strong")
                        .call(yAxis);

                    // Add text label
                    verticalAxis.append("text")
                        .attr("transform", "rotate(-90)")
                        .attr("y", 10)
                        .attr("dy", ".71em")
                        .style("text-anchor", "end")
                        .style("fill", "#999")
                        .style("font-size", 12)
                        .text("");


                    // Group elements
                    var focus = svg.append("g")
                        .attr("class", "d3-crosshair-pointer")
                        .style("display", "none");

                    // Pointer
                    focus.append("circle")
                        .attr("r", 3.5)
                        .style("fill", "#fff")
                        .style("stroke", "#4CAF50")
                        .style("stroke-width", 2);

                    // Text
                    focus.append("text")
                        .attr("dy", ".35em")
                        .style("fill", "#333")
                        .style("stroke", "none");

                    // Overlay
                    svg.append("rect")
                        .attr("class", "d3-crosshair-overlay")
                        .attr("width", width)
                        .attr("height", height)
                        .style("opacity", 0.1)
                        .style("fill", "#eee")
                        .on("mouseover", function () {
                            focus.style("display", null);
                        })
                        .on("mouseout", function () {
                            focus.style("display", "none");
                        })
                        .on("mousemove", mousemove);

                    // Display tooltip on mousemove
                    function mousemove() {
                        var x0 = x.invert(d3.mouse(this)[0]),
                            i = bisectDate(data, x0, 1),
                            d0 = data[i - 1],
                            d1 = data[i],
                            d = x0 - d0.date > d1.date - x0 ? d1 : d0;
                        focus.attr("transform", "translate(" + x(d.date) + "," + y(d.close) + ")");
                        focus.select("text").text(formatCurrency(d.close)).attr("dx", -26).attr("dy", 30);
                    }
                });


                // Call function on window resize
                $(window).on('resize', resize);

                // Call function on sidebar width change
                $('.sidebar-control').on('click', resize);

                // Resize function
                function resize() {

                    // Layout variables
                    width = width,

                        // Main svg width
                        container.attr("width", width + margin.left + margin.right);

                    // Width of appended group
                    svg.attr("width", width + margin.left + margin.right);

                    // Horizontal range
                    x.range([0, width]);

                    // Horizontal axis
                    svg.selectAll('.d3-axis-horizontal').call(xAxis);

                    // Line path
                    svg.selectAll('.d3-line').attr("d", line);

                    // Crosshair
                    svg.selectAll('.d3-crosshair-overlay').attr("width", width);
                }
            }

            function colorgbAjax<?=$model->getPrimaryKey()?>() {

                // Initialize chart
                var width = $('.chart-container-<?= $model->getPrimaryKey() ?>').width();
                lineBasic<?=$model->getPrimaryKey()?>('#d3-line-<?=$model->getPrimaryKey()?>', width, 300);
            }

            function colorgbAjaxReady() {
                var width = $('#colorgb-pjax').width() - 112;
                width = $('#colorgb-pjax').length > 0 ? width : $('.colorgb-container > .panel').width() - 60;

                $('.chart-container-<?= $model->getPrimaryKey() ?>').width(width);
                if ($('.chart-container-<?= $model->getPrimaryKey() ?>').width() > 0 && $('.chart-container-<?= $model->getPrimaryKey() ?> svg').length == 0) {

                    colorgbAjax<?=$model->getPrimaryKey()?>();

                }
            }

            $(document).on('pjax:complete', colorgbAjaxReady);
            $(document).ready(colorgbAjaxReady);

        </script>

        <?php endif; ?>
        <?php if ($contract2): ?>
        <div class="colorgb-container">
            <div class="panel panel-white colorgb-report-panel">
                <div class="panel-heading">
                    <h6 class="panel-title">Báo cáo họat động (HSA)</h6>
                    <div class="heading-elements">
                        <?php if ($data['contract'] != true && $data['group'] != true): ?>
                            <a data-pjax="0" href="report/weekly-report-hsa-download" class="btn btn-default btn-xs heading-btn"><i class="icon-download position-left"></i> Tải về báo cáo tuần</a>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="panel-body no-padding-bottom">
                    <div class="row">
                        <div class="col-xs-6 content-group">
                            <ul class="list-condensed list-unstyled">
                                <li>Lợi nhuận từ năm <?= date('Y') ?></li>
                                <li><span class="text-semibold"><?= round(($fundUnitHsa['last']->fund_unit_price - $fup) / $fupHsa * 100, 2) ?>%</span></li>
                            </ul>
                        </div>

                        <div class="col-xs-6 content-group">
                            <div class="invoice-details pull-right text-right">
                                <ul class="list-condensed list-unstyled">
                                    <li>Từ ngày: <span class="colorgb-semibold"><?= $contract2[0]->begin_date ? ColorgbHelper::date($contract2[0]->begin_date) : ColorgbHelper::date($fundUnitHsa['first']->date) ?></span></li>
                                    <li>Đến ngày: <span class="colorgb-semibold"><?= ColorgbHelper::date($fundUnitHsa['last']->date) ?></span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb-20 chart-container-<?= $model->getPrimaryKey() ?>" style="min-width: 840px;">
                    <h3 class="text-uppercase text-semibold text-center">Hiệu quả đầu tư - Hestia</h3>
                    <div class="chart-responsive">
                        <div class="chart" id="d3-line-hsa-<?= $model->getPrimaryKey() ?>"></div>
                    </div>
                </div>

                <h5 class="text-semibold text-uppercase pl-15 mt-20 mb-20 text-blue-800">

                    Khách hàng: <?= $data['contract'] == true ? Customer::findOne($model->customer_id)->name : $model->name ?>

                </h5>

                <?php if (!empty($contract2)): ?>
                    <div class="table-responsive">
                        <table class="table table-lg">
                            <thead>
                            <tr>

                                <?php if ($data['contract'] != true): ?>
                                    <th width="60px">HĐ</th>
                                    <th width="20px">Số HĐ</th>
                                <?php endif; ?>

                                <th width="120px" class="text-right">Ngày đầu tư</th>
                                <th class="text-right">Số lượng cổ phiếu chuyển đổi (1)</th>
                                <th class="text-right">Giá chuyển đổi/cổ phiếu (2)</th>
                                <th class="text-right">Vốn đầu tư (1)x(2)</th>

                                <?php if ($data['contract'] == true): ?>
                                    <th class="text-right">Giá ĐVĐT hiện tại</th>
                                <?php endif; ?>

                                <th class="text-right">Lợi nhuận</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($contract2 as $key => $value): ?>
                                <?php
                                $subProfitRate = ($fundUnitHsa['last']->fund_unit_price - $value->fund_unit_price) / $value->fund_unit_price * 100;
                                $subTotal = round($value->fund_unit_price * $value->invest_unit_qty, -5);
                                $subProfit = $subProfitRate * $subTotal / 100;
                                $qty += $value->invest_unit_qty;
                                $total += $subTotal;
                                $profit += $subProfit;
                                ?>
                                <tr>
                                    <?php if ($data['contract'] != true): ?>
                                        <td class="text-right">
                                            <a href="contract/view?id=<?= $value->id ?>" title="Xem hợp đồng" class="btn-colorgb-popup" data-pjax="0"><span class="icon-file-pdf"></span></a>
                                        </td>

                                        <td>
                                            <?= $value->contract_no ?>
                                        </td>
                                    <?php endif; ?>

                                    <td class="text-right"><?= ColorgbHelper::date($value->begin_date) ?></td>
                                    <td class="text-right"><?= \common\colorgb\ColorgbHelper::numberFormat($value->invest_unit_qty) ?> CP</td>
                                    <td class="text-right"><?= \common\colorgb\ColorgbHelper::numberFormat($value->fund_unit_price) ?> đ</td>
                                    <td class="text-right"><?= \common\colorgb\ColorgbHelper::numberFormat($subTotal) ?> đ</td>

                                    <?php if ($data['contract'] == true): ?>
                                        <td class="text-right"><?= \common\colorgb\ColorgbHelper::numberFormat($fundUnitHsa['last']->fund_unit_price) ?> đ</td>
                                    <?php endif; ?>

                                    <td class="text-right <?= $subProfit > 0 ? 'text-green-600' : 'text-warning' ?>"><?= \common\colorgb\ColorgbHelper::numberFormat($subProfit) ?> đ <br>(<?= round($subProfitRate, 2) ?>%)</td>
                                </tr>
                            <?php endforeach; ?>

                            <?php $totalCurrent = $qty * $fundUnitHsa['last']->fund_unit_price; ?>

                            <?php if ($data['contract'] != true): ?>
                                <tr>
                                    <td>

                                    </td>
                                    <td>

                                    </td>
                                    <td class="text-right"><strong>Tổng cộng</strong></td>
                                    <td class="text-right"><strong><?= \common\colorgb\ColorgbHelper::numberFormat($qty) ?> ĐVĐT</strong></td>
                                    <td class="text-right"></td>
                                    <td class="text-right"><strong><?= \common\colorgb\ColorgbHelper::numberFormat($total) ?> đ</strong></td>
                                    <td class="text-right <?= $profit > 0 ? 'text-green-600' : 'text-warning' ?>"><strong><?= \common\colorgb\ColorgbHelper::numberFormat($profit) ?> đ <br> (<?= round($profit / $total * 100, 2) ?>%)</strong></td>
                                </tr>
                            <?php endif; ?>

                            </tbody>

                        </table>


                    </div>

                    <?php if ($data['contract'] != true): ?>
                        <div class="panel-body">
                            <div class="row invoice-payment">
                                <div class="col-xs-12 col-sm-7 col-sm-offset-5">
                                    <div class="content-group">
                                        <h6 class="pl-15">Thống kê chi tiết</h6>
                                        <div class="table-responsive no-border">
                                            <table class="table">
                                                <tbody>
                                                <tr>
                                                    <th width="40%">Tổng số cổ phiếu dự kiến chuyển đổi hiện tại của Cổ đông <span class="text-regular">(3)</span></th>
                                                    <td class="text-right"><h5 class="text-semibold"><?= \common\colorgb\ColorgbHelper::numberFormat($qty) ?> CP</h5></td>
                                                </tr>
                                                <tr>
                                                    <th width="40%">Giá trị tài sản ròng/cổ phiếu <span class="text-regular">(4)</span></th>
                                                    <td class="text-right"><h5 class="text-semibold"><?= \common\colorgb\ColorgbHelper::numberFormat($fundUnitHsa['last']->fund_unit_price) ?> đ</h5></td>
                                                </tr>
                                                <tr>
                                                    <th width="40%">Tổng số tiền hiện tại của Cổ đông <span class="text-regular">(3)x(4)</span></th>
                                                    <td class="text-right"><h5 class="text-semibold"><?= \common\colorgb\ColorgbHelper::numberFormat($totalCurrent) ?> đ</h5></td>
                                                </tr>
                                                <tr>
                                                    <th width="40%">Tổng số vốn đầu tư của Cổ đông</th>
                                                    <td class="text-right"><h5 class="text-semibold"><?= \common\colorgb\ColorgbHelper::numberFormat($total) ?> đ</h5></td>
                                                </tr>
                                                <tr>
                                                    <th width="40%">Cổ đông hiện tại có lãi tạm tính trước phí</th>
                                                    <td class="text-right <?= $profit > 0 ? 'text-green-600' : 'text-warning' ?>"><h5 class="text-semibold"><?= \common\colorgb\ColorgbHelper::numberFormat($profit) ?> đ</h5></td>
                                                </tr>
                                                <tr>
                                                    <th width="40%">Tổng % lợi nhuận đạt được</th>
                                                    <td class="text-right <?= $profit > 0 ? 'text-green-600' : 'text-warning' ?>"><h5 class="text-semibold"><?= round($profit / $total * 100, 2) ?>%</h5></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    <?php endif; ?>

                <?php endif; ?>

            </div>
        </div>
        <style>
            .pace .pace-progress {
                background: none;
            }

            .colorgb-report i {
                font-size: 13px;
            }

            <?php if(Yii::$app->request->get('layout') == 'sub'):?>
            body {
                background: #fff;
            }

            .page-container {
                padding: 0;
            }

            <?php endif;?>
        </style>
        <script>

            // Chart setup
            function lineBasicHsa<?=$model->getPrimaryKey()?>(element, width, height) {

                // Define main variables
                var d3Container = d3.select(element),
                    margin = {top: 5, right: 20, bottom: 20, left: 50},
                    width = width,
                    height = height - margin.top - margin.bottom - 5;

                // Format data
                var parseDate = d3.time.format("%Y%m%d").parse,
                    bisectDate = d3.bisector(function (d) {
                        return d.date;
                    }).left,
                    formatValue = d3.format(","),
                    formatCurrency = function (d) {
                        return formatValue(d) + '';
                    };


                // Horizontal
                var x = d3.time.scale()
                    .range([0, width]);

                // Vertical
                var y = d3.scale.linear()
                    .range([height, 0]);

                // Horizontal
                var xAxis = d3.svg.axis()
                    .scale(x)
                    .orient("bottom")
                    .tickSize(-height, 0, 0)
                    .tickFormat(d3.time.format("%d/%m/%y"));

                // Vertical
                var yAxis = d3.svg.axis()
                    .scale(y)
                    .tickSize(-width, 0, 0)
                    .orient("left");


                // Add SVG element
                var container = d3Container.append("svg");

                // Add SVG group
                var svg = container
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


                // Line
                var line = d3.svg.line()
                    .interpolate("basis")
                    .x(function (d) {
                        return x(d.date);
                    })
                    .y(function (d) {
                        return y(d.close);
                    });


                // Load data
                d3.tsv("<?=Yii::$app->params['cdnUrl']?>/data/investment/report/hsa/<?=$contract2[0]->begin_date ? $contract2[0]->begin_date : $fundUnitHsa['first']->date ?>.tsv?colorgb=<?=time()?>", function (error, data) {

                    // Pull out values
                    data.forEach(function (d) {
                        d.date = parseDate(d.date);
                        d.close = +d.close;
                    });

                    // Sort data
                    data.sort(function (a, b) {
                        return a.date - b.date;
                    });

                    // Horizontal
                    x.domain(d3.extent(data, function (d) {
                        return d.date;
                    }));

                    // Vertical
                    y.domain([0, d3.max(data, function (d) {
                        return d.close;
                    })]);

                    // Add line
                    svg.append("path")
                        .datum(data)
                        .attr("class", "d3-line d3-line-medium")
                        .attr("d", line)
                        .style("fill", "none")
                        .style("stroke-width", 3)
                        .style("stroke", "#4CAF50");


                    // Horizontal
                    svg.append("g")
                        .attr("class", "d3-axis d3-axis-horizontal d3-axis-strong")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    // Vertical
                    var verticalAxis = svg.append("g")
                        .attr("class", "d3-axis d3-axis-vertical d3-axis-strong")
                        .call(yAxis);

                    // Add text label
                    verticalAxis.append("text")
                        .attr("transform", "rotate(-90)")
                        .attr("y", 10)
                        .attr("dy", ".71em")
                        .style("text-anchor", "end")
                        .style("fill", "#999")
                        .style("font-size", 12)
                        .text("");


                    // Group elements
                    var focus = svg.append("g")
                        .attr("class", "d3-crosshair-pointer")
                        .style("display", "none");

                    // Pointer
                    focus.append("circle")
                        .attr("r", 3.5)
                        .style("fill", "#fff")
                        .style("stroke", "#4CAF50")
                        .style("stroke-width", 2);

                    // Text
                    focus.append("text")
                        .attr("dy", ".35em")
                        .style("fill", "#333")
                        .style("stroke", "none");

                    // Overlay
                    svg.append("rect")
                        .attr("class", "d3-crosshair-overlay")
                        .attr("width", width)
                        .attr("height", height)
                        .style("opacity", 0.1)
                        .style("fill", "#eee")
                        .on("mouseover", function () {
                            focus.style("display", null);
                        })
                        .on("mouseout", function () {
                            focus.style("display", "none");
                        })
                        .on("mousemove", mousemove);

                    // Display tooltip on mousemove
                    function mousemove() {
                        var x0 = x.invert(d3.mouse(this)[0]),
                            i = bisectDate(data, x0, 1),
                            d0 = data[i - 1],
                            d1 = data[i],
                            d = x0 - d0.date > d1.date - x0 ? d1 : d0;
                        focus.attr("transform", "translate(" + x(d.date) + "," + y(d.close) + ")");
                        focus.select("text").text(formatCurrency(d.close)).attr("dx", -26).attr("dy", 30);
                    }
                });


                // Call function on window resize
                $(window).on('resize', resize);

                // Call function on sidebar width change
                $('.sidebar-control').on('click', resize);

                // Resize function
                function resize() {

                    // Layout variables
                    width = width,

                        // Main svg width
                        container.attr("width", width + margin.left + margin.right);

                    // Width of appended group
                    svg.attr("width", width + margin.left + margin.right);

                    // Horizontal range
                    x.range([0, width]);

                    // Horizontal axis
                    svg.selectAll('.d3-axis-horizontal').call(xAxis);

                    // Line path
                    svg.selectAll('.d3-line').attr("d", line);

                    // Crosshair
                    svg.selectAll('.d3-crosshair-overlay').attr("width", width);
                }
            }

            function colorgbAjaxHsa<?=$model->getPrimaryKey()?>() {

                // Initialize chart
                var width = $('.chart-container-<?= $model->getPrimaryKey() ?>').width();
                lineBasic<?=$model->getPrimaryKey()?>('#d3-line-hsa-<?=$model->getPrimaryKey()?>', width, 300);
            }

            function colorgbAjaxHsaReady() {
                var width = $('#colorgb-pjax').width() - 112;
                width = $('#colorgb-pjax').length > 0 ? width : $('.colorgb-container > .panel').width() - 60;

                $('.chart-container-<?= $model->getPrimaryKey() ?>').width(width);
                if ($('.chart-container-<?= $model->getPrimaryKey() ?>').width() > 0 && $('.chart-container-<?= $model->getPrimaryKey() ?> svg').length == 0) {

                    colorgbAjaxHsa<?=$model->getPrimaryKey()?>();

                }
            }

            $(document).on('pjax:complete', colorgbAjaxHsaReady);
            $(document).ready(colorgbAjaxHsaReady);

        </script>
    <?php endif; ?>

<?php else: ?>
    <div class="colorgb-container">
        <div class="panel panel-white colorgb-report-panel">
            <div class="panel-heading">
                <h6 class="panel-title">Báo cáo hiệu quả đầu tư của khách hàng</h6>

            </div>

            <div class="panel-body no-padding-bottom">
                <div class="alert alert-warning">
                    Không có dữ liệu
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>