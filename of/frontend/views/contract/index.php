<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\colorgb\ColorgbHelper;
use common\models\Customer;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ContractColorgb */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Danh sách hợp đồng';
$this->params['breadcrumbs'][] = $this->title;
$columns = [
    [
        'class' => 'kartik\grid\ExpandRowColumn',
        'width' => '60px',
        'hAlign' => 'left',
        'value' => function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail' => function ($model, $key, $index, $column) use ($data) {
            return Yii::$app->controller->renderPartial('/report/_index', ['model' => $model, 'data' => $data]);
        },
        'headerOptions' => ['class' => 'kartik-sheet-style'],
        'expandOneOnly' => true
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '50px', 'header' => 'Xem',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                $html .= Html::a('<span class="icon-file-pdf"></span>', ['view', 'id' => $model->getPrimaryKey()], ['title' => 'Xem hợp đồng', 'class' => 'btn-colorgb-popup', 'data-pjax' => '0']);
                return $html;
            },
        ]
    ],
    /*[
        'attribute' => 'contract_date',
        'width' => '210px',
        'filterType' => GridView::FILTER_DATE,
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'allowClear' => true,
                'autoclose' => true,
                'format' => 'yyyymmdd'
            ],
        ],
        'filterInputOptions' => ['placeholder' => 'Bộ lọc...'],
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) {
            $date = ColorgbHelper::date($model->contract_date);
            $html = '<h6>';
            $html .= ' <span>' . $date . '</span>';
            $html .= '</h6>';
            return $html;
        },

    ],*/
    [
        'attribute' => 'begin_date',
        'width' => '210px',
        'filterType' => GridView::FILTER_DATE,
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'allowClear' => true,
                'autoclose' => true,
                'format' => 'yyyymmdd'
            ],
        ],
        'filterInputOptions' => ['placeholder' => 'Bộ lọc...'],
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) {
            $date = ColorgbHelper::date($model->begin_date);
            $html = '<h6>';
            $html .= ' <span>' . $date . '</span>';
            $html .= '</h6>';
            $html .= ColorgbHelper::contractExpire($model->end_date_est);
            return $html;
        },

    ],
    [
        'attribute' => 'end_date_est',
        'width' => '210px',
        'filterType' => GridView::FILTER_DATE,
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'allowClear' => true,
                'autoclose' => true,
                'format' => 'yyyymmdd'
            ],
        ],
        'filterInputOptions' => ['placeholder' => 'Bộ lọc...'],
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) {
            $date = ColorgbHelper::date($model->end_date_est);
            $html = '<h6>';
            $html .= ' <span>' . $date . '</span>';
            $html .= '</h6>';
            return $html;
        },

    ],
    [
        'attribute' => 'contract_info',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $html = '<strong class="display-block">' . $model->contract_no . '</strong>';
            $html .= '<p>' . $data['contractType'][$model->contract_type_id] . '</p>';
            return $html;
        },

    ],
    [
        'attribute' => 'status',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $array = $data['contractStatusHtml'];
            $value = !empty($array[$model->status]) ? $array[$model->status] : '';
            return $value;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['contractStatusTxt'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Bộ lọc...'],
        'width' => '160px',
    ],
];

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'colorgb-pjax',
    'showPageSummary' => false,
    'pjax' => true,
    'striped' => true,
    'responsive' => true,
    'responsiveWrap' => true,
    'hover' => true,
    'floatHeader' => true,
    'floatHeaderOptions' => ['top' => 46],
    'toolbar' => [
        'before' => '',//Html::a('<i class="glyphicon glyphicon-plus"></i> Đăng ký hợp đồng', ['register'], ['data-pjax' => 0, 'class' => 'btn btn-success pull-left', 'title' => 'Thêm mới']),
        'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''], ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Làm mới']),
        '{toggleData}',
    ],
    'panel' => ['type' => 'default', 'heading' => $this->title],
    'columns' => $columns,
]);
?>
