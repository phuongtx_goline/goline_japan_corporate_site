<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use kartik\file\FileInput;

$this->title = 'Thay đổi thông tin cá nhân';
$this->params['breadcrumbs'][] = ['label' => 'Thiết lập cá nhân', 'url' => 'user/index'];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $this->title ?></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <?php $form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data'],
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                'horizontalCssClasses' => [
                    'label' => 'col-sm-4 text-right',
                    'offset' => 'col-sm-offset-4',
                    'wrapper' => 'col-sm-7',
                    'error' => '',
                    'hint' => '',
                ],
            ],
        ]); ?>

        <?php if (Yii::$app->session->getFlash('success')): ?>
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
                <?= Yii::$app->session->getFlash('success'); ?>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->session->getFlash('error')): ?>
            <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
                <?= Yii::$app->session->getFlash('error'); ?>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'mobile')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'email_addr')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'org_type')->dropDownList(ArrayHelper::map(Yii::$app->params['orgType'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...', 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'org_info')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'foreign_type')->dropDownList(ArrayHelper::map(Yii::$app->params['foreignType'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...', 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'nationality')->dropDownList(ArrayHelper::map(Yii::$app->params['countryList'], 'code', 'name'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...', 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'gender')->dropDownList(ArrayHelper::map(Yii::$app->params['gender'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...', 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'prefix')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'birthday')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'address1')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'address2')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'tax_code')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>


                <?/*= $form->field($model, 'file_upload')->widget(FileInput::classname(), [
                        'pluginOptions' => $data['fileInput'],
                        'disabled' => 'disabled'
                    ]

                )*/
                ?>

            </div>
            <div class="col-sm-6">

                <?= $form->field($model, 'id_number')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'id_issue_date')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'id_expired_date')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'id_place')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'bank_name')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'bank_branch_name')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'bank_acco_no')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'bank_acco_name')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'mobile1')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'mobile2')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'email_addr1')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

                <?= $form->field($model, 'email_addr2')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

            </div>

        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

