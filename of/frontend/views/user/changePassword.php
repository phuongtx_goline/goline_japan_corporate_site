<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\tinymce\TinyMce;

$this->title = 'Thay đổi mật khẩu';
$this->params['breadcrumbs'][] = ['label' => 'Thiết lập cá nhân', 'url' => 'user/index'];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $this->title ?></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">
        <?php $form = ActiveForm::begin([
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                'horizontalCssClasses' => [
                    'label' => 'col-sm-4 text-right',
                    'offset' => 'col-sm-offset-4',
                    'wrapper' => 'col-sm-6',
                    'error' => '',
                    'hint' => '',
                ],
            ],
        ]); ?>

        <?php if (Yii::$app->session->getFlash('success')): ?>
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
                <?= Yii::$app->session->getFlash('success'); ?>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->session->getFlash('warning')): ?>
            <div class="alert alert-warning alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
                <?= Yii::$app->session->getFlash('warning'); ?>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->session->getFlash('error')): ?>
            <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
                <?= Yii::$app->session->getFlash('error'); ?>
            </div>
        <?php endif; ?>

        <?= $form->field($model, 'password', ['inputOptions' => [
            'placeholder' => 'Nhập mật khẩu hiện tại'
        ]])->passwordInput() ?>

        <?= $form->field($model, 'new_password', ['inputOptions' => [
            'placeholder' => 'Nhập mật khẩu mới'
        ]])->passwordInput() ?>

        <?= $form->field($model, 'confirm_password', ['inputOptions' => [
            'placeholder' => 'Nhập lại mật khẩu mới'
        ]])->passwordInput() ?>

        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-5">
                <?= Html::submitButton('<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Lưu lại</span>', ['class' => 'btn btn-primary  btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

