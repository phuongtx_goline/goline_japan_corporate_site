<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\colorgb\ColorgbHelper;

$baseUrl = Yii::$app->params['frontendUrl'];
$cdnUrl = Yii::$app->params['cdnUrl'];
$controller = Yii::$app->controller->id;
$account = Yii::$app->user->identity;
$bodyClass = $controller == 'site' ? '' : ' navbar-top';
$version = 1.5;

?>
<?php $this->beginPage() ?>
<!--************************************************************-->
<!-- Designed & Developed by COLORGB™  -  http://colorgb.com    -->
<!-- giaphv@colorgb.com, hoangdv@colorgb.com, datnt@colorgb.com -->
<!--************************************************************-->
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <base href="<?= $baseUrl ?>/">
    <link rel="icon" type="image/x-icon" class="js-site-favicon" href="<?= $baseUrl ?>/favicon.ico">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script>var baseUrl = '<?=$baseUrl?>';</script>
    <?= $this->render('_head') ?>
</head>
<body class="<?= $bodyClass ?>">

<?php if (!Yii::$app->user->isGuest && $controller != 'site'): ?>
    <!-- Main navbar -->
    <div class="navbar navbar-fixed-top navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="dashboard/index"><img src="<?= $cdnUrl ?>/bolt/assets/images/logo_light.png" alt=""></a>

            <ul class="nav navbar-nav pull-right visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            </ul>
        </div>

        <div class="navbar-collapse collapse" id="navbar-mobile">

            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?= Yii::$app->params['homeUrl'] ?>" target="_blank"><i class="icon-home2"></i> Trang chủ <?= Yii::$app->name ?></a></li>

                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <img onerror="this.src='<?= $cdnUrl ?>/bolt/assets/images/image.png'" src="<?= ColorgbHelper::dataSrc($account->image) ?>" alt="">
                        <span><?= $account->name ?></span>
                        <i class="caret"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="user/profile"><i class="icon-profile"></i> Thông tin cá nhân</a></li>
                        <li><a href="user/change-password"><i class="icon-lock"></i> Đổi mật khẩu</a></li>
                        <li class="divider"></li>
                        <li><a href="user/logout"><i class="icon-switch2"></i> Đăng xuất</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- /main navbar -->

    <!-- Second navbar -->
    <div class="navbar navbar-default visible-xs" id="navbar-second">
        <ul class="nav navbar-nav no-border visible-xs-block">
            <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-menu7"></i></a></li>
        </ul>

        <div class="navbar-collapse collapse" id="navbar-second-toggle">
            <ul class="nav navbar-nav">
                <li><a href="report"><i class="icon-chart position-left"></i> <span>Báo cáo hiệu quả đầu tư</span></a></li>
                <li><a href="contract"><i class="icon-clipboard3 position-left"></i> <span>Danh sách hợp đồng</span></a></li>
                <li><a href="feedback"><i class="icon-paperplane position-left"></i> <span>Gửi phản hồi</span></a></li>
                <li><a href="user/profile"><i class="icon-profile"></i> <span>Thông tin cá nhân</span></a></li>
                <li><a href="user/change-password"><i class="icon-lock"></i> <span>Đổi mật khẩu</span></a></li>
                <li><a href="user/logout"><i class="icon-switch2"></i> <span>Đăng xuất</span></a></li>
            </ul>

        </div>
    </div>
    <!-- /second navbar -->

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-anchor position-left"></i>
                    <?php if (!empty($this->params['breadcrumbs']) && !empty($this->params['breadcrumbs'][0]['label'])): ?><span class="text-semibold"><?= $this->params['breadcrumbs'][0]['label'] ?></span> - <?php endif; ?>
                    <?= Html::encode($this->title) ?></h4>
                <div class="breadcrumb-caret position-right">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        'homeLink' => ['label' => 'Bảng thông tin', 'url' => 'report/index'],
                        'encodeLabels' => false

                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /page header -->
<?php else: ?>
    <style>
        .login-container .footer a {
            display: none;
        }
    </style>
<?php endif; ?>

<!-- Page container -->
<div class="page-container <?= ($controller == 'site') ? 'login-container' : '' ?>">
    <!-- Page content -->
    <div class="page-content">

        <?php $this->beginBody() ?>

        <?php if (!Yii::$app->user->isGuest && $controller != 'site'): ?>
            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-default">
                <div class="sidebar-content">
                    <!-- Main navigation -->
                    <div class="sidebar-category sidebar-category-visible">
                        <div class="category-title h6">
                            <span>Thao tác hệ thống</span>
                            <ul class="icons-list">
                                <li><a href="#" data-action="collapse"></a></li>
                            </ul>
                        </div>

                        <div class="category-content no-padding">
                            <ul class="navigation navigation-main navigation-accordion">
                                <li class="navigation-header"><span>Danh mục chính</span> <i class="icon-menu" title="" data-original-title="Danh mục chính"></i></li>

                                <li><a href="report"><i class="icon-chart position-left"></i> <span>Báo cáo hiệu quả đầu tư</span></a></li>
                                <li><a href="contract"><i class="icon-clipboard3 position-left"></i> <span>Danh sách hợp đồng</span></a></li>
                                <li><a href="feedback"><i class="icon-paperplane position-left"></i> <span>Gửi phản hồi</span></a></li>

                                <li class="navigation-header"><span>Thiết lập cá nhân</span> <i class="icon-menu" title="" data-original-title="Thiết lập cá nhân"></i></li>
                                <li><a href="user/profile"><i class="icon-profile"></i> <span>Thông tin cá nhân</span></a></li>
                                <li><a href="user/change-password"><i class="icon-lock"></i> <span>Đổi mật khẩu</span></a></li>
                                <li><a href="user/logout"><i class="icon-switch2"></i> <span>Đăng xuất</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /main navigation -->
                </div>
            </div>
            <!-- /main sidebar -->
        <?php endif; ?>

        <!-- Main content -->
        <div class="content-wrapper">
            <?= $content ?>
        </div>
        <!-- /main content -->

        <?php $this->endBody() ?>

    </div>
    <!-- /page content -->
    <!-- Footer -->
    <div class="footer text-muted">
        &copy; <?= date('Y') ?> <span>Developed by <a style="font-weight: bold; letter-spacing: 1px; color: #777; padding: 1px 0; text-decoration: none; font-family: Arial, sans-serif;" title="COLORGB™" href="http://colorgb.com" target="_blank" rel="noopener noreferrer"><strong style="color: #777; display: inline-block;">COLO</strong><strong style="color: #e74c3c; display: inline-block;">R</strong><strong style="color: #2ecc71; display: inline-block;">G</strong><strong style="color: #3498db; display: inline-block;">B</strong>™</a> </span>
    </div>
    <!-- /footer -->

</div>
<!-- /page container -->

<div class="modal" id="colorgbIframeModal" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="model-header">
                <button type="button" class="close" data-dismiss="modal" onclick="colorgbGridReload()">×</button>
            </div>
            <div class="modal-body">
                <iframe width="100%" height="100%" frameborder="0"></iframe>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</body>
</html>
<?php $this->endPage() ?>

