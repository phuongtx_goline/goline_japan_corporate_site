<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */

/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<style>.page-header, .sidebar-main {
        display: none
    }</style>
<div class="container-fluid text-center error">
    <h1 class="error-title">Opps!</h1>
    <h6 class="text-semibold content-group"><?= Html::encode($this->title) ?> <br> <?= nl2br(Html::encode($message)) ?></h6>
    <p>
        Vui lòng liên hệ <a href="mailto:support@colorgb.com?subject=<?= nl2br(Html::encode($message)) . ' - ' . Yii::$app->request->hostInfo . Yii::$app->request->url ?>">support@colorgb.com</a> để giải quyết vấn đề. Xin cảm ơn.
    </p>
</div>