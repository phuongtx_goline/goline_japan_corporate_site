<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model frontend\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\datecontrol\DateControl;

$this->title = 'Đăng ký hợp tác đầu tư';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin() ?>
<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <a title="Passion Investment - Trang chủ" href="<?= Yii::$app->params['frontendUrl']; ?>" class="site-logo">
            <img src="<?= Yii::$app->params['cdnUrl'] ?>/bolt/assets/images/logo-white.png" alt="">
        </a>

        <div class="panel registration-form">
            <div class="panel-body">
                <div class="text-center">
                    <h2 class="content-group-lg"><?= $this->title ?>
                        <small class="display-block">Vui lòng nhập các thông tin yêu cầu</small>
                    </h2>
                </div>

                <?php if (Yii::$app->session->getFlash('success')): ?>
                    <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
                        <?= Yii::$app->session->getFlash('success'); ?>
                    </div>
                <?php endif; ?>

                <?php if (Yii::$app->session->getFlash('error')): ?>
                    <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
                        <?= Yii::$app->session->getFlash('error'); ?>
                    </div>
                <?php endif; ?>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'name')->textInput() ?>
                    </div>

                    <div class="col-md-6">
                        <?= $form->field($model, 'tax_code')->textInput() ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'mobile')->textInput() ?>
                    </div>

                    <div class="col-md-6">
                        <?= $form->field($model, 'email_addr')->textInput() ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'contract_type')->dropDownList($data['contractType'], ['data-placeholder' => 'Chọn một loại hợp đồng...', 'prompt' => 'Chọn một loại hợp đồng...']) ?>
                    </div>

                    <div class="col-md-6">
                        <?= $form->field($model, 'invest_value', [
                            'template' => '{label}<div><div class="input-group">{input}<span class="input-group-addon">đ</span> </div></div>',
                        ])->textInput([
                            'class' => 'price form-control'
                        ]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'id_number')->textInput() ?>
                    </div>

                    <div class="col-md-4">
                        <?= $form->field($model, 'id_issue_date')->widget(DateControl::classname(), [
                            'type' => DateControl::FORMAT_DATE,
                            'displayFormat' => 'dd/MM/yyyy',
                            'saveFormat' => 'yyyyMMdd',
                            'ajaxConversion' => false,
                            'autoWidget' => true
                        ])
                        ?>
                    </div>

                    <div class="col-md-4">
                        <?= $form->field($model, 'id_place')->textInput() ?>
                    </div>
                </div>

                <hr>

                <div class="text-center">
                    <button type="submit" class="btn bg-green btn-lg btn-ladda btn-ladda-spinner btn-ladda-progress" data-style="zoom-out"><b><i class="icon-paperplane"></i></b> Đăng ký mở tài khoản</button>
                    <a href="site/login" class="display-block mt-15"> Trở về trang chủ</a>

                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
