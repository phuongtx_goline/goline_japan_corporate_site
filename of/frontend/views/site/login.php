<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model frontend\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Đăng nhập hệ thống';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .footer{
        display: none;
    }
</style>
<?php $form = ActiveForm::begin() ?>
<a title="Passion Investment - Trang chủ" href="<?= Yii::$app->params['frontendUrl']; ?>" class="site-logo">
    <img src="<?= Yii::$app->params['cdnUrl'] ?>/bolt/assets/images/logo-white.png" alt="">
</a>

<div class="panel panel-body login-form">
    <div class="text-center">
        <h2 class="content-group"><?= $this->title ?>
            <small class="display-block">Vui lòng nhập tài khoản và mật khẩu</small>
        </h2>
    </div>

    <?php if (Yii::$app->session->getFlash('success')): ?>
        <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
            <?= Yii::$app->session->getFlash('success'); ?>
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->getFlash('error')): ?>
        <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
            <?= Yii::$app->session->getFlash('error'); ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
    <?= $form->field($model, 'password')->passwordInput() ?>

    <div class="row">
        <div class="col-xs-6 col-sm-6">
            <?= $form->field($model, 'rememberMe')->checkbox(['class' => 'styled']) ?>
        </div>
        <div class="col-xs-6 col-sm-6 text-right">
            <a href="site/request-password-reset">Quên mật khẩu?</a>
        </div>
    </div>

    <div class="form-group">
        <button type="submit" class="display-block btn btn-primary btn-ladda btn-ladda-spinner btn-ladda-progress" data-style="zoom-out">
            <span class="ladda-label">Đăng nhập</span>
            <i class="icon-circle-right2 position-right"></i>
        </button>
    </div>

    <div class="text-center">
        <a href="site/register">Đăng ký hợp tác đầu tư</a>
    </div>

</div>
<?php ActiveForm::end(); ?>
