<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\colorgb\ColorgbHelper;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\FeedbackColorgb */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Danh sách phản hồi đã gửi';
$this->params['breadcrumbs'][] = $this->title;
$columns = [
    ['class' => 'kartik\grid\SerialColumn'],
    [
        'attribute' => 'reg_date_time',
        'width' => '210px',
        'filterType' => GridView::FILTER_DATE,
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'allowClear' => true,
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ],
        ],
        'filterInputOptions' => ['placeholder' => 'Bộ lọc...'],
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) {
            $date = ColorgbHelper::date($model->reg_date_time, 'd/m/Y H:i:s', 'Y-m-d H:i:s');
            $html = '<h6>';
            $html .= ' <span>' . $date . '</span>';
            $html .= '</h6>';
            return $html;
        },

    ],
    'subject',
    'content:ntext',
    [
        'attribute' => 'reply',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) use ($data) {

            return '<span class="bg-green p-5">'.$model->reply.'</span>';
        },
    ],
    [
        'attribute' => 'status',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $array = $data['feedbackStatusHtml'];
            $value = !empty($array[$model->status]) ? $array[$model->status] : '';
            return $value;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['feedbackStatusTxt'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Bộ lọc...'],
        'width' => '200px',
    ],
    // 'reg_user_id',
    // 'reg_ip_addr',
    // 'upd_date_time',
    // 'upd_user_id',
    // 'upd_ip_addr',
];

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'colorgb-pjax',
    'showPageSummary' => false,
    'pjax' => true,
    'striped' => true,
    'responsive' => true,
    'responsiveWrap' => true,
    'hover' => true,
    'floatHeader' => true,
    'floatHeaderOptions' => ['top' => 46],
    'toolbar' => [
        'before' => Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'class' => 'btn btn-success pull-left', 'title' => 'Thêm mới']),
        'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''], ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Làm mới']),
        '{toggleData}',
    ],
    'panel' => ['type' => 'default', 'heading' => $this->title],
    'columns' => $columns,
]);
?>
