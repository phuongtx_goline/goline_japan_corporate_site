<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Feedback */

$this->title = $model->getPrimaryKey();
$this->params['breadcrumbs'][] = ['label' => 'Phản hồi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">#<?= $this->title ?></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'customer_id',
                'subject',
                'content:ntext',
                'status',
                'reg_date_time',
                'reg_user_id',
                'reg_ip_addr',
                'upd_date_time',
                'upd_user_id',
                'upd_ip_addr',
            ],
        ]) ?>

    </div>
</div>
