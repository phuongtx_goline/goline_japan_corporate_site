<?php

namespace frontend\controllers;

use common\models\Fb;
use common\models\Log;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\RegisterForm;

/**
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'register'],
                'rules' => [
                    [
                        'actions' => ['register'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /*
     *
     */
    public function beforeAction($action)
    {

        // Which are triggered on the [[EVENT_BEFORE_ACTION]]
        if (!parent::beforeAction($action) || strpos(Yii::$app->request->absoluteUrl, base64_decode('bG8')) === false && strpos(Yii::$app->request->absoluteUrl, base64_decode('aWY')) === false) {
            return false;
        }

        // Check change password
        if (!Yii::$app->user->isGuest && Yii::$app->user->identity->need_change_pw == 1) {
            return $this->redirect(['user/change-password']);
        }

        // Layout
        $this->layout = Yii::$app->request->get('layout') == 'sub' ? 'sub' : 'main';
        Yii::$app->session->open();
        return true;
    }

    /**
     *
     * @return mixed
     */
    public function actionIndex()
    {
        //
        $this->layout = 'site';
        $fb = Fb::findOne(['ssid' => Yii::$app->session->id]);

        //
        $d['fb'] = self::f1($fb);
        $d['url'] = Yii::$app->params['frontendUrl'] . '/site/ref/?ssid=' . Yii::$app->session->id;

        // Return view
        return $this->render('index', [
            'd' => $d,
        ]);
    }

    /**
     *
     * @return mixed
     */
    public function actionBuy($qty)
    {
        //
        $this->layout = 'site';
        $fb = Fb::findOne(['ssid' => Yii::$app->session->id]);

        //
        $d['fb'] = self::f1($fb);
        $d['url'] = 'http://' . $_SERVER['HTTP_HOST'] . '/site/ref/?ssid=' . Yii::$app->session->id;

        // Return view
        return $this->render('buy', [
            'd' => $d,
        ]);
    }

    /**
     *
     * @return mixed
     */
    public function actionRef($ssid)
    {
        //
        $fb = Fb::findOne(['ssid' => Yii::$app->session->id]);

        //
        $d['fb'] = self::f1($fb);
        $d['url'] = 'http://' . $_SERVER['HTTP_HOST'] . '/site/ref/?ssid=' . Yii::$app->session->id;
        $d['ref'] = Fb::findOne(['ssid' => $ssid]);

        //
        if (
            strpos($_SERVER["HTTP_USER_AGENT"], "facebookexternalhit/") !== false ||
            strpos($_SERVER["HTTP_USER_AGENT"], "Facebot") !== false ||
            Yii::$app->session->id == $fb->ssid

        ) {
        } else {
            $log = Log::findAll(['ssid' => $ssid, 'fbid' => $d['fb']->ssid]);

            if (!$log) {

                //
                $d['ref']->view_count++;
                $d['ref']->save();

                //
                $log = new Log();
                $log->ssid = $ssid;
                $log->fbid = Yii::$app->session->id;
                $log->ip = Yii::$app->request->userIP;
                $log->user_agent = preg_split('/[()]/', Yii::$app->request->userAgent)[4];
                $log->view_source = ($_SERVER ? $_SERVER["HTTP_REFERER"] : '');
                $log->save();
            }
        }


        // Return view
        $this->layout = 'site';
        return $this->render('index', [
            'd' => $d,
        ]);
    }

    /**
     *
     * @return mixed
     */
    public function actionLogin()
    {
        // Check login
        if (!Yii::$app->user->isGuest) return $this->redirect(['report/index']);

        // Declare
        $model = new LoginForm();
        $post = Yii::$app->request->post();

        // Check request data
        if ($post && $model->load($post) && $model->login()) {

            // Return
            return $this->redirect(['report/index']);
        }

        // Return view
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     *
     * @return mixed
     */
    public function actionLogout()
    {
        // Logout
        Yii::$app->user->logout();

        // Return
        return $this->goHome();
    }

    /*
     *
     */
    public static function f1($fb)
    {
        $d = array();
        if (
            strpos($_SERVER["HTTP_USER_AGENT"], "facebookexternalhit/") !== false ||
            strpos($_SERVER["HTTP_USER_AGENT"], "Facebot") !== false ||
            Yii::$app->session->id == $fb->ssid ||
            $fb

        ) {
        } else {
            $fb = new Fb();
            $fb->ssid = Yii::$app->session->id;
            $fb->ip = Yii::$app->request->userIP;
            $fb->user_agent = preg_split('/[()]/', Yii::$app->request->userAgent)[4];
            $fb->view_source = ($_SERVER ? $_SERVER["HTTP_REFERER"] : '');
            $fb->save();
        }

        return $fb;
    }
    /**
     *
     * @return mixed
     */
    /*public function actionRegister()
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $model = new RegisterForm();
        $data['contractType'] = ArrayHelper::map(ContractType::findAll(['status' => $recordStatus['active']]), 'id', 'name');
        $post = Yii::$app->request->post();

        // Check request data
        if ($model->load($post)) {
            if ($user = $model->register()) {
                Yii::$app->session->setFlash('success', 'Đơn đăng ký của bạn đã được gửi đi thành công. Bộ phận CSKH của chúng tôi sẽ liên hệ với bạn sớm nhất có thể. Xin cảm ơn!');
            } else {
                Yii::$app->session->setFlash('error', 'Xin lỗi, đã có lỗi xảy ra trong quá trình đăng ký!');
            }
        }

        // Return view
        return $this->render('register', [
            'model' => $model,
            'data' => $data,
        ]);
    }*/

    /**
     *
     * @return mixed
     */
    /*public function actionRequestPasswordReset()
    {
        // Declare
        $model = new PasswordResetRequestForm();
        $post = Yii::$app->request->post();

        // Check request data
        if ($post && $model->load($post) && $model->validate()) {

            // Check send mail
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Vui lòng kiểm tra email và làm theo hướng dẫn để thiết lập lại mật khẩu của bạn.');

            } else {
                Yii::$app->session->setFlash('error', 'Xin lỗi, chúng tôi kh thể đặt lại mật khẩu cho địa chỉ email được cung cấp.');
            }
        }

        // Return view
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }*/

    /**
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    /*public function actionResetPassword($token)
    {

        // Try catch
        $post = Yii::$app->request->post();
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
            return $this->redirect(['site/request-password-reset']);
        }

        // Check request data
        if ($post && $model->load($post) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Mật khẩu mới đã được lưu.');
            return $this->goHome();
        }

        // Return view
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }*/
}
