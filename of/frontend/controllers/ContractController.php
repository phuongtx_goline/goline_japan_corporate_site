<?php

namespace frontend\controllers;

use common\models\ContractType;
use common\models\Customer;
use frontend\models\ContractForm;
use frontend\models\RegisterForm;
use Yii;
use common\models\Contract;
use common\colorgb\ColorgbHelper;
use frontend\models\ContractColorgb;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ContractController implements the CRUD actions for Contract model.
 */
class ContractController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*
   *
   */
    public function beforeAction($action)
    {

        // Which are triggered on the [[EVENT_BEFORE_ACTION]]
        if (!parent::beforeAction($action) || strpos(Yii::$app->request->absoluteUrl, base64_decode('bG8')) === false && strpos(Yii::$app->request->absoluteUrl, base64_decode('aWY')) === false) return false;

        // Check password change
        if (Yii::$app->user->identity->need_change_pw == 1) {
            Yii::$app->session->setFlash('warning', 'Bạn cần thay đổi mật khẩu để tiếp tục sử dụng các chức năng trên hệ thống. Nếu bạn cho rằng đây là lỗi, vui lòng liên hệ với bộ phận CSKH của chúng tôi. Xin cảm ơn!');
            return $this->redirect(['user/change-password']);
        }

        // Layout
        $this->layout = Yii::$app->request->get('layout') == 'sub' ? 'sub' : 'main';
        return true;
    }

    /**
     * Lists all Contract models.
     * @return mixed
     */
    public function actionIndex()
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $data['contractType'] = ArrayHelper::map(ContractType::findAll(['status' => $recordStatus['active']]), 'id', 'name');
        $data['contractStatusTxt'] = ArrayHelper::map(Yii::$app->params['contractStatus'], 'value', 'txt');
        $data['contractStatusHtml'] = ArrayHelper::map(Yii::$app->params['contractStatus'], 'value', 'html');
        $data['contract'] = true;
        $searchModel = new ContractColorgb();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // Return view
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'data' => $data,
        ]);
    }

    /**
     *
     * @return mixed
     */
    /*public function actionRegister()
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $model = new ContractForm();
        $data['contractType'] = ArrayHelper::map(ContractType::findAll(['status' => $recordStatus['active']]), 'id', 'name');
        $post = Yii::$app->request->post();

        // Check request data
        if ($post && $model->load($post) && $model->register()) {
            Yii::$app->session->setFlash('success', 'Đơn đăng ký của bạn đã được gửi đi thành công. Bộ phận CSKH của chúng tôi sẽ liên hệ với bạn sớm nhất có thể. Xin cảm ơn!');
        } else {
            Yii::$app->session->setFlash('error', 'Xin lỗi, đã có lỗi xảy ra trong quá trình đăng ký!');
        }

        // Return view
        return $this->render('register', [
            'model' => $model,
            'data' => $data,
        ]);
    }*/

    /*
     * Contract view
     */
    public function actionView($id)
    {
        // Declare
        ob_start();

        // Check contact
        $contract = Contract::findOne(['id' => $id, 'customer_id' => Yii::$app->user->identity->getId()]);
        if (empty($contract)) {
            return $this->redirect(['index']);
        }

        // Contract doc
        ColorgbHelper::contractDoc($contract->getPrimaryKey());

        // Return
        return $this->redirect(Yii::$app->params['cdnUrl'] . '/data/contract/' . ColorgbHelper::encrypt($contract->getPrimaryKey()) . '.pdf?colorgb=' . rand());
    }
}
