<?php

namespace frontend\controllers;

use common\colorgb\ColorgbHelper;
use common\models\Code;
use common\models\Contract;
use common\models\CustomerAttribute;
use common\models\CustomerGroup;
use Yii;
use common\models\Customer;
use frontend\models\customer\AllColorgb;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use common\models\User;

/**
 * ReportController implements the CRUD actions for Customer model.
 */
class ReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*
    *
    */
    public function beforeAction($action)
    {

        // Which are triggered on the [[EVENT_BEFORE_ACTION]]
        if (!parent::beforeAction($action) || strpos(Yii::$app->request->absoluteUrl, base64_decode('bG8')) === false && strpos(Yii::$app->request->absoluteUrl, base64_decode('aWY')) === false) return false;

        // Check change password
        if (Yii::$app->user->identity->need_change_pw == 1) {
            Yii::$app->session->setFlash('warning', 'Bạn cần thay đổi mật khẩu để tiếp tục sử dụng các chức năng trên hệ thống. Nếu bạn cho rằng đây là lỗi, vui lòng liên hệ với bộ phận CSKH của chúng tôi. Xin cảm ơn!');
            return $this->redirect(['user/change-password']);
        }

        // Layout
        $this->layout = Yii::$app->request->get('layout') == 'sub' ? 'sub' : 'main';
        return true;
    }


    /**
     * @return mixed
     */
    public function actionIndex()
    {
        // Declare
        $model = Customer::findOne(Yii::$app->user->identity->getId());

        // Return view
        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /*
     * Weekly report download
     */
    public function actionWeeklyReportDownload()
    {
        ob_start();
        $id = Yii::$app->user->identity->getId();
        $model = Customer::findOne($id);
        $phpPdf = ColorgbHelper::phpPdf($id, 'weekly-report', 'weekly-report/' . ColorgbHelper::encrypt($id) . '.pdf');
        if ($phpPdf) {
            $pdf = '[BC-PI] ' . $model->name . ' (' . date('d-m-Y') . ').pdf';
            header('Content-Disposition: attachment; filename="' . $pdf . '"; filename*=utf-8\' \'' . rawurlencode($pdf));
            header("Content-type: application/pdf");
            readfile(Yii::$app->params['cdnUrl'] . '/data/weekly-report/' . ColorgbHelper::encrypt($id) . '.pdf');
        }
    }

    /*
     * Weekly report download
     */
    public function actionWeeklyReportHsaDownload()
    {
        ob_start();
        $id = Yii::$app->user->identity->getId();
        $model = Customer::findOne($id);
        $phpPdf = ColorgbHelper::phpPdf($id, 'weekly-report-hsa', 'weekly-report-hsa/' . ColorgbHelper::encrypt($id) . '.pdf');
        if ($phpPdf) {
            $pdf = '[BC-HSA] ' . $model->name . ' (' . date('d-m-Y') . ').pdf';
            header('Content-Disposition: attachment; filename="' . $pdf . '"; filename*=utf-8\' \'' . rawurlencode($pdf));
            header("Content-type: application/pdf");
            readfile(Yii::$app->params['cdnUrl'] . '/data/weekly-report-hsa/' . ColorgbHelper::encrypt($id) . '.pdf');
        }
    }
}
