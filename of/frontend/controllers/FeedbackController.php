<?php

namespace frontend\controllers;

use common\models\Customer;
use Yii;
use common\models\Feedback;
use common\colorgb\ColorgbHelper;
use frontend\models\FeedbackColorgb;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FeedbackController implements the CRUD actions for Feedback model.
 */
class FeedbackController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*
   *
   */
    public function beforeAction($action)
    {

        // Which are triggered on the [[EVENT_BEFORE_ACTION]]
        if (!parent::beforeAction($action) || strpos(Yii::$app->request->absoluteUrl, base64_decode('bG8')) === false && strpos(Yii::$app->request->absoluteUrl, base64_decode('aWY')) === false) return false;

        if (Yii::$app->user->identity->need_change_pw == 1) {
            Yii::$app->session->setFlash('warning', 'Bạn cần thay đổi mật khẩu để tiếp tục sử dụng các chức năng trên hệ thống. Nếu bạn cho rằng đây là lỗi, vui lòng liên hệ với bộ phận CSKH của chúng tôi. Xin cảm ơn!');
            return $this->redirect(['user/change-password']);
        }

        // Layout
        $this->layout = Yii::$app->request->get('layout') == 'sub' ? 'sub' : 'main';
        return true;
    }

    /**
     * Lists all Feedback models.
     * @return mixed
     */
    public function actionIndex()
    {
        // Declare
        $data['feedbackStatusTxt'] = ArrayHelper::map(Yii::$app->params['feedbackStatus'], 'value', 'txt');
        $data['feedbackStatusHtml'] = ArrayHelper::map(Yii::$app->params['feedbackStatus'], 'value', 'html');
        $searchModel = new FeedbackColorgb();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // Return view
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'data' => $data,
        ]);
    }

    /**
     * Creates a new Feedback model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        // Declare
        $model = new Feedback();
        $model->customer_id = Yii::$app->user->identity->getId();
        $post = Yii::$app->request->post();

        // Check request data
        if ($post && $model->load($post)) {

            ColorgbHelper::setDataRecord($model);
            if ($model->save()) Yii::$app->session->setFlash('success', 'Tạo mới thành công');

        }

        // Return view
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Feedback model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
   /* public function actionDelete($id)
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $model = $this->findModel($id);

        // Save data
        $model->status = $recordStatus['deleted'];
        $model->save();
        return true;
    }*/

    /**
     * Finds the Feedback model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Feedback the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Feedback::findOne(['id' => $id, 'customer_id' => Yii::$app->user->identity->getId()])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Không tìm thấy thông tin yêu cầu');
        }
    }
}
