<?php

namespace frontend\controllers;

use common\colorgb\ColorgbHelper;
use common\models\Code;
use frontend\models\CustomerLite;
use common\models\Customer;
use common\models\CustomerAttribute;
use common\models\CustomerGroup;
use frontend\models\ChangePasswordForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\AccessControl;

class UserController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /*
     *
     */
    public function beforeAction($action)
    {

        // Which are triggered on the [[EVENT_BEFORE_ACTION]]
        if (!parent::beforeAction($action) || strpos(Yii::$app->request->absoluteUrl, base64_decode('bG8')) === false && strpos(Yii::$app->request->absoluteUrl, base64_decode('aWY')) === false) {
            return false;
        }

        // Layout
        $this->layout = Yii::$app->request->get('layout') == 'sub' ? 'sub' : 'main';
        return true;

    }

    /**
     *
     * @return mixed
     */

    public function actionIndex()
    {
        // Return view
        return $this->render('index');
    }

    /**
     *
     * @return mixed
     */

    public function actionChangePassword()
    {
        // Declare
        $model = new ChangePasswordForm();
        $user = Customer::findOne(Yii::$app->user->identity->getId());
        $post = Yii::$app->request->post();

        // Check data
        if ($post && $model->load($post)) {

            // Check validate
            if ($model->validate() && $user->validatePassword($model->password)) {

                // Save data
                if ($model->saveData()) {
                    Yii::$app->user->logout();
                    Yii::$app->session->setFlash('success', 'Thay đổi mật khẩu thành công');
                    return $this->redirect(['site/login']);
                } else {
                    Yii::$app->session->setFlash('error', 'Không thể thay đổi mật khẩu');
                }

            } else {
                Yii::$app->session->setFlash('error', 'Mật khẩu hiện tại không đúng');
            }
        }

        // Return view
        return $this->render('changePassword', [
            'model' => $model,
        ]);
    }

    /**
     *
     * @return mixed
     */

    public function actionProfile()
    {
        // Declare
        $model = CustomerLite::findOne(Yii::$app->user->identity->getId());
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $data['fileInput'] = Yii::$app->params['fileInput'];
        $data['fileInput']['initialPreview'] = $model->image ? Yii::$app->params['cdnUrl'] . '/data/' . $model->image : '';
        $data['customerGroup'] = ArrayHelper::map(CustomerGroup::findAll(['status' => $recordStatus['active']]), 'id', 'name');
        $data['customerProfession'] = ArrayHelper::map(Code::findAll(['group' => 'PROFESSION', 'status' => $recordStatus['active']]), 'id', 'value1');
        $data['infoSource'] = ArrayHelper::map(Code::findAll(['group' => 'INFO_SOURCE', 'status' => $recordStatus['active']]), 'id', 'value1');

        // Return view
        return $this->render('profile', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     *
     * @return mixed
     */
    public function actionLogout()
    {
        // Logout
        Yii::$app->user->logout();

        // Return
        return $this->goHome();
    }

}
