<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\admin\UserGroupColorgb */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Danh sách nhóm tài khoản';
$this->params['breadcrumbs'][] = ['label' => 'Người dùng', 'url' => ['/admin/user/index']];
$this->params['breadcrumbs'][] = $this->title;
$columns = [
    ['class' => 'kartik\grid\SerialColumn'],
    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '50px',        'header' => 'Xem',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                $html .= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->getPrimaryKey()], ['title' => 'Xem', 'data-pjax' => '0']);
                return $html;
            },
        ]
    ],
    [
        'attribute' => 'name',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) {
            $html = '<h6 class="display-block">' . $model->name . '</h6>';
            return $html;
        },

    ],
    'code',
    'remarks',
    [
        'attribute' => 'status',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $array = $data['recordStatusHtml'];
            $value = !empty($array[$model->status]) ? $array[$model->status] : '';
            return $value;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['recordStatusTxt'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Bộ lọc...'],
        'width' => '200px',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '150px',
        'template' => '{colorgb_button}{delete}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                $html .= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->getPrimaryKey()], ['title' => 'Sửa', 'data-pjax' => '0']);
                return $html;
            },
        ],
    ],
];

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'colorgb-pjax',
    'showPageSummary' => false,
    'pjax' => true,
    'striped' => true,
    'responsive' => true,
    'responsiveWrap' => true,
    'hover' => true,
    'floatHeader' => true,
    'floatHeaderOptions' => ['top' => 46],
    'toolbar' => [
        'before' => Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'class' => 'btn btn-success pull-left', 'title' => 'Thêm mới']),
        'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''], ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Làm mới']),
        '{toggleData}',
    ],
    'panel' => ['type' => 'default', 'heading' => $this->title],
    'columns' => $columns,
]);
?>
