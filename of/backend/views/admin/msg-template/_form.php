<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\FileInput;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model common\models\MsgTemplate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="colorgb-container">
    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3 text-right',
                'offset' => 'col-sm-offset-3',
                'wrapper' => 'col-sm-7',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'msg_type')->dropDownList(ArrayHelper::map(Yii::$app->params['msgType'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

    <?= $form->field($model, 'send_type')->dropDownList(ArrayHelper::map(Yii::$app->params['sendType'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

    <?= $form->field($model, 'foreign_type')->dropDownList(ArrayHelper::map(Yii::$app->params['foreignType'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->widget(TinyMce::className(), [
        'language' => 'vi_VN',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview",
                "searchreplace code fullscreen codesample",
                "image media table"
            ],
            'image_advtab' => true,
            'image_title' => true,
            'height' => '250px',
            'menubar' => true,
        ]

    ]) ?>

    <?= $form->field($model, 'file_upload')->widget(FileInput::classname(), [
        'pluginOptions' => $data['fileInput']]);
    ?>

    <?= $form->field($model, 'mail_bcc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'remarks')->textarea() ?>

    <?= $form->field($model, 'status')->dropDownList(ArrayHelper::map(Yii::$app->params['recordStatus'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>
    <div class="hide">
        <?= $form->field($model, 'upd_date_time')->textInput() ?>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-5">
            <?= Html::submitButton($model->isNewRecord ? '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Tạo mới</span>' : '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Lưu lại</span>', ['class' => $model->isNewRecord ? 'btn btn-success  btn-ladda btn-ladda-spinner btn-ladda-progress' : 'btn btn-primary  btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
