<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;
use common\colorgb\ColorgbHelper;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Permission */

$this->title = $model->getPrimaryKey();
$this->params['breadcrumbs'][] = ['label' => 'Quyền truy cập', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">#<?= $this->title ?></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'role_cd',
                'role_name',
                'remarks',
            ],
        ]) ?>

    </div>
</div>
