<?php

use yii\helpers\Html;
use \yii\bootstrap\ActiveForm;
use common\models\Permission;


/* @var $this yii\web\View */
/* @var $model common\models\Permission */
$id = Yii::$app->request->get('id');
$this->title = 'Thiết lập quyền truy cập';
$this->params['breadcrumbs'][] = ['label' => 'Quyền truy cập', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $this->title ?></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <?php if (Yii::$app->session->getFlash('success')): ?>
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
                <?= Yii::$app->session->getFlash('success'); ?>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->session->getFlash('error')): ?>
            <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
                <?= Yii::$app->session->getFlash('error'); ?>
            </div>
        <?php endif; ?>

        <div class="colorgb-form">

            <div class="colorgb-container">

                <div class="row">
                    <div class="col-sm-5">
                        <fieldset>
                            <legend>Nhóm người dùng</legend>
                            <ul class="list-group list-user-group">
                                <?php foreach ($data['userGroup'] as $item): ?>
                                    <li title="Chọn nhóm người dùng để thiết lập quyền truy cập" class="list-group-item <?= $id == $item->id ? 'active' : '' ?>"><a href="admin/permission/set?id=<?= $item->id ?>" data-id="<?= $item->id ?>"><span class="text-bold"><?= $item->name ?></span>
                                            <p class="small text-grey"><?= $item->remarks ?></p></a></li>
                                <?php endforeach; ?>
                            </ul>
                            <?php if (Yii::$app->request->get('id')): ?>
                            <button type="button" class="btn btn-save btn-primary btn-ladda btn-ladda-spinner btn-ladda-progress" data-style="zoom-out"><i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Lưu lại</span><span class="ladda-spinner"></span><span class="ladda-spinner"></span></button>
                            <?php endif; ?>
                        </fieldset>
                    </div>
                    <div class="col-sm-7">
                        <fieldset>
                            <legend>Quyền truy cập</legend>
                            <div id="treeRole"></div>
                        </fieldset>
                    </div>
                </div>

            </div>

        </div>

    </div>
</div>
<script>
    $(function () {
        var tree = $('#treeRole').tree({
            primaryKey: 'id',
            uiLibrary: 'bootstrap',
            dataSource: baseUrl + '/admin/permission/get-role?id=<?=$id?>',
            checkboxes: true
        });

        <?php foreach ($data['checked'] as $item): $roleCdParent = explode('_', $item->role_cd)[0];?>
        tree.on('dataBound', function () {
            var node = tree.getNodeByText('<?=Permission::findOne(['role_cd' => $roleCdParent])->role_name?>');
            tree.expand(node);
        });
        <?php endforeach;?>

        $('.btn-save').on('click', function () {
            var checkedIds = tree.getCheckedNodes();
            $.ajax({url: baseUrl + '/admin/permission/save-role?id=<?=$id?>', data: {checkedIds: checkedIds}})
                .success(function (result) {
                    var json = $.parseJSON(result);
                    if (json.status === 1) {
                        colorgbJGrowl('bg-green', json.message);

                    }
                    else {
                        colorgbJGrowl('bg-danger', json.message);
                    }
                })
                .fail(function () {
                    colorgbJGrowl('bg-danger', 'Đã có lỗi xảy ra');
                    $btn.removeClass('text-blue').addClass('text-danger');
                });
        });


    })
</script>