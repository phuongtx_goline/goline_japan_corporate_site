<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\ContractType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="colorgb-container">
    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4 text-right',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-7',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <div class="tabbable">
        <ul class="nav nav-tabs nav-tabs-highlight nav-justified">
            <li class="active"><a href="#justified-left-icon-tab1" data-toggle="tab"><i class="icon-flag4 position-left"></i> Thông tin gói hợp đồng</a></li>
            <li><a href="#justified-left-icon-tab2" data-toggle="tab"><i class="icon-reading position-left"></i> Thông tin các bậc phí rút trước hạn</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="justified-left-icon-tab1">
                <div class="row">
                    <div class="col-sm-6">
                        <?php if ($model->isNewRecord): ?>
                            <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
                        <?php else: ?>
                            <?= $form->field($model, 'code')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>
                        <?php endif; ?>

                        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'period_type')->dropDownList(ArrayHelper::map(Yii::$app->params['periodType'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                        <?= $form->field($model, 'period_val')->textInput() ?>


                        <?= $form->field($model, 'file_upload')->widget(FileInput::classname(), [
                            'pluginOptions' => $data['fileInput']]);
                        ?>
                        <?= $form->field($model, 'status')->dropDownList(ArrayHelper::map(Yii::$app->params['recordStatus'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'min_invest_value', [
                            'template' => '{label}<div class="col-sm-7"><div class="input-group">{input}<span class="input-group-addon">đ</span> </div></div>',
                        ])->textInput([
                            'class' => 'price form-control'
                        ]) ?>

                        <?= $form->field($model, 'max_invest_value', [
                            'template' => '{label}<div class="col-sm-7"><div class="input-group">{input}<span class="input-group-addon">đ</span> </div></div>',
                        ])->textInput([
                            'class' => 'price form-control'
                        ]) ?>
                        <?= $form->field($model, 'base_profit_rate', [
                            'template' => '{label}<div class="col-sm-7"><div class="input-group">{input}<span class="input-group-addon">%</span> </div></div>',
                        ])->textInput([
                            'class' => 'price form-control'
                        ]) ?>
                        <?= $form->field($model, 'profit_rate', [
                            'template' => '{label}<div class="col-sm-7"><div class="input-group">{input}<span class="input-group-addon">%</span> </div></div>',
                        ])->textInput([
                            'class' => 'price form-control'
                        ]) ?>


                        <?= $form->field($model, 'remarks')->textarea() ?>


                    </div>
                </div>

            </div>
            <div class="tab-pane" id="justified-left-icon-tab2">
                <div class="row">
                    <div class="col-sm-offset-2 col-sm-7">
                        <table class="table table-bordered table-hover" id="tableRowSettleFee">
                            <thead>
                            <tr>
                                <th width="32%">Từ thời hạn</th>
                                <th width="32%">Đến thời hạn</th>
                                <th width="32%">Tỷ lệ phí</th>
                                <th width="4%">
                                    <button type="button" class="btn btn-success btn-add-settle-fee-row" data-i="<?= count($data['settleFee']) ?>"><i class="icon-plus3"></i></button>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($data['settleFee'])): ?>

                                <?php foreach ($data['settleFee'] as $key => $value): ?>
                                    <tr data-i="<?= $key ?>">
                                        <td>
                                            <div class="input-group"><input type="number" min="0" class="form-control" name="ContractType[settleFee][<?= $key ?>][from_period_val]" value="<?= $value->from_period_val ?>"><span class="input-group-addon">tháng</span></div>

                                        </td>
                                        <td>
                                            <div class="input-group"><input type="number" min="0" class="form-control" name="ContractType[settleFee][<?= $key ?>][to_period_val]" value="<?= $value->to_period_val ?>"><span class="input-group-addon">tháng</span></div>
                                        </td>
                                        <td>
                                            <div class="input-group"><input type="number" min="0" class="form-control" name="ContractType[settleFee][<?= $key ?>][fee_rate]" value="<?= $value->fee_rate*100 ?>"><span class="input-group-addon">%</span></div>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-danger btn-remove-row"><i class="icon-minus3"></i></button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="hide">
        <?= $form->field($model, 'upd_date_time')->textInput() ?>
    </div>
    <div class="form-group btn-tab">
        <div class="col-sm-offset-2 col-sm-5">
            <?= Html::submitButton($model->isNewRecord ? '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Tạo mới</span>' : '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Lưu lại</span>', ['class' => $model->isNewRecord ? 'btn btn-success  btn-ladda btn-ladda-spinner btn-ladda-progress' : 'btn btn-primary  btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
