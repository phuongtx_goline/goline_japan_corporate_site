<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;
use common\colorgb\ColorgbHelper;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ContractType */

$this->title = $model->getPrimaryKey();
$this->params['breadcrumbs'][] = ['label' => 'Gói hợp đồng', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">#<?= $this->title ?></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'code',
                'name',
                [
                    'attribute' => 'period_type',
                    'format' => 'raw',
                    'value' => function ($model) {
                        $periodType = ArrayHelper::map(Yii::$app->params['periodType'], 'value', 'txt');
                        return $periodType[$model->period_type];
                    }
                ],
                'period_val',
                [
                    'attribute' => 'min_invest_value',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return \common\colorgb\ColorgbHelper::numberFormat($model->min_invest_value) . ' đ';
                    }
                ],
                [
                    'attribute' => 'max_invest_value',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return \common\colorgb\ColorgbHelper::numberFormat($model->max_invest_value) . ' đ';
                    }
                ],
                [
                    'attribute' => 'base_profit_rate',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return ($model->base_profit_rate * 100) . '%';
                    }
                ],
                [
                    'attribute' => 'profit_rate',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return ($model->profit_rate * 100) . '%';
                    }
                ],
                [
                    'attribute' => 'file_template_url',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::a($model->file_template_url,Yii::$app->params['cdnUrl'].'/'.$model->file_template_url);
                    }
                ],
                'remarks',
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function ($model) {
                        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'value', 'txt');
                        return $recordStatus[$model->status];
                    }
                ],
                [
                    'attribute' => 'reg_date_time',
                    'value' => function ($model) {
                        return ColorgbHelper::date($model->reg_date_time, 'd/m/Y H:i:s', 'Y-m-d H:i:s');
                    }
                ],
                [
                    'attribute' => 'reg_user_id',
                    'value' => function ($model) {
                        return User::findOne($model->reg_user_id)->name;
                    }
                ],
                'reg_ip_addr',
                [
                    'attribute' => 'upd_date_time',
                    'value' => function ($model) {
                        return ColorgbHelper::date($model->upd_date_time, 'd/m/Y H:i:s', 'Y-m-d H:i:s');
                    }
                ],
                [
                    'attribute' => 'upd_user_id',
                    'value' => function ($model) {
                        return User::findOne($model->upd_user_id)->name;
                    }
                ],
                'upd_ip_addr',
            ],
        ]) ?>

    </div>
</div>
