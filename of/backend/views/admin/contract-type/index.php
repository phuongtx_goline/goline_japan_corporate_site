<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\admin\ContractTypeColorgb */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Danh sách gói hợp đồng';
$this->params['breadcrumbs'][] = $this->title;
$columns = [
    ['class' => 'kartik\grid\SerialColumn'],
    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '50px',        'header' => 'Xem',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                $html .= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->getPrimaryKey()], ['title' => 'Xem', 'data-pjax' => '0']);
                return $html;
            },
        ]
    ],
    [
        'attribute' => 'name',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) {
            $html = $model->name . '<br/>(' . $model->code . ')';
            return $html;
        },

    ],
    [
        'attribute' => 'period_type',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $array = $data['periodTypeTxt'];
            $value = !empty($array[$model->period_type]) ? $array[$model->period_type] : '';
            return $value;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['periodTypeTxt'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Bộ lọc...'],
        'width' => '100px',
    ],
    'period_val',
    [
        'attribute' => 'min_invest_value',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) {
            $html = \common\colorgb\ColorgbHelper::numberFormat($model->min_invest_value) . ' đ';
            return $html;
        },

    ],
    [
        'attribute' => 'max_invest_value',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) {
            $html = \common\colorgb\ColorgbHelper::numberFormat($model->max_invest_value) . ' đ';
            return $html;
        },

    ],
    [
        'attribute' => 'status',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $array = $data['recordStatusHtml'];
            $value = !empty($array[$model->status]) ? $array[$model->status] : '';
            return $value;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['recordStatusTxt'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Bộ lọc...'],
        'width' => '180px',
    ],
    // 'reg_date_time',
    // 'reg_user_id',
    // 'reg_ip_addr',
    // 'upd_date_time',
    // 'upd_user_id',
    // 'upd_ip_addr',
    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '150px',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                $html .= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->getPrimaryKey()], ['title' => 'Sửa', 'data-pjax' => '0']);
                $html .= Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->getPrimaryKey()], ['title' => 'Xóa', 'data-pjax' => 'false', 'class' => 'colorgb-pjax-action-del colorgb-pjax-action-del-show', 'data-pjax-container' => 'colorgb-pjax-pjax']);
                return $html;
            },
        ]
    ],
];

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'colorgb-pjax',
    'showPageSummary' => false,
    'pjax' => true,
    'striped' => true,
    'responsive' => true,
    'responsiveWrap' => true,
    'hover' => true,
    'floatHeader' => true,
    'floatHeaderOptions' => ['top' => 46],
    'toolbar' => [
        'before' => Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'class' => 'btn btn-success pull-left', 'title' => 'Thêm mới']),
        'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''], ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Làm mới']),
        '{toggleData}',
    ],
    'panel' => ['type' => 'default', 'heading' => $this->title],
    'columns' => $columns,
]);
?>
