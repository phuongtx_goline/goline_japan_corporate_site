<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\CustomerGroup */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="colorgb-container">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4 text-right',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-6',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?php if ($model->isNewRecord): ?>
        <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
    <?php else: ?>
        <?= $form->field($model, 'code')->textInput(['maxlength' => true,'disabled' => 'disabled',]) ?>
    <?php endif; ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'remarks')->textarea() ?>

    <?= $form->field($model, 'user_group_id')
        ->dropDownList($data['userGroup'],
            [
                'class' => 'chosen-select input-md required',
                'multiple' => 'multiple',
                'data-placeholder' => 'Chọn...',
                'prompt' => 'Chọn...'
            ]
        );
    ?>

    <div class="hide">
        <?= $form->field($model, 'upd_date_time')->textInput() ?>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-4 col-sm-5">
            <?= Html::submitButton($model->isNewRecord ? '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Tạo mới</span>' : '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Lưu lại</span>', ['class' => $model->isNewRecord ? 'btn btn-success  btn-ladda btn-ladda-spinner btn-ladda-progress' : 'btn btn-primary  btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
