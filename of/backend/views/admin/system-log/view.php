<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;
use common\colorgb\ColorgbHelper;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\SystemLog */

$this->title = $model->getPrimaryKey();
$this->params['breadcrumbs'][] = ['label' => 'Nhật ký thao tác hệ thống', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">#<?= $this->title ?></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">
        <div class="table-responsive">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'log_time',
                    'table_name',
                    'action',
                    'before_value:ntext',
                    'after_value:ntext',
                    [
                        'attribute' => 'reg_date_time',
                        'value' => function ($model) {
                            return ColorgbHelper::date($model->reg_date_time, 'd/m/Y H:i:s', 'Y-m-d H:i:s');
                        }
                    ],
                    [
                        'attribute' => 'reg_user_id',
                        'value' => function ($model) {
                            return User::findOne($model->reg_user_id)->name;
                        }
                    ],
                    'reg_ip_addr',
                    [
                        'attribute' => 'upd_date_time',
                        'value' => function ($model) {
                            return ColorgbHelper::date($model->upd_date_time, 'd/m/Y H:i:s', 'Y-m-d H:i:s');
                        }
                    ],
                    [
                        'attribute' => 'upd_user_id',
                        'value' => function ($model) {
                            return User::findOne($model->upd_user_id)->name;
                        }
                    ],
                    'upd_ip_addr',
                ],
            ]) ?>
        </div>
    </div>
</div>
