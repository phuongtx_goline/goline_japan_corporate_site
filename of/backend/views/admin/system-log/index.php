<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\colorgb\ColorgbHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\admin\SystemLogColorgb */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Danh sách nhật ký thao tác hệ thống';
$this->params['breadcrumbs'][] = $this->title;
$columns = [
    ['class' => 'kartik\grid\SerialColumn'],
    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '50px',        'header' => 'Xem',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                $html .= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->getPrimaryKey()], ['title' => 'Xem', 'data-pjax' => '0']);
                return $html;
            },
        ]
    ],
    [
        'width' => '210px',
        'attribute' => 'colorgb_date',
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'locale' => [
                    'format' => 'YYYY-MM-DD',
                    'separator' => ' - ',
                ],
                'ranges' => [
                    //"Hôm nay" => ["moment().startOf('day')", "moment()"],
                    "Hôm qua" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                    "Tuần trước" => ["moment().startOf('day').subtract(7, 'days')", "moment()"],
                    "Tháng trước" => ["moment().startOf('day').subtract(31, 'days')", "moment()"],
                ],
                'alwaysShowCalendars' => true,

            ],
        ],
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) {
            $date = ColorgbHelper::date($model->log_time,'d/m/Y H:i:s','Y-m-d H:i:s');
            $html = '<h6>';
            $html .= ' <span>' . $date . '</span>';
            $html .= '</h6>';
            return $html;
        },

    ],
    [
        'attribute' => 'table_name',
    ],
    [
        'attribute' => 'reg_user_id',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $array = $data['userArray'];
            $value = !empty($array[$model->reg_user_id]) ? $array[$model->reg_user_id] : '';
            return $value;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['userArray'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Bộ lọc...'],
        'width' => '200px',
    ],
    [
        'attribute' => 'action',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $array = $data['systemLogHtml'];
            $value = !empty($array[$model->action]) ? $array[$model->action] : '';
            return $value;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['systemLogTxt'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Bộ lọc...'],
        'width' => '200px',
    ],
    // 'reg_date_time',
    // 'reg_ip_addr',
    // 'upd_date_time',
    // 'upd_user_id',
    // 'upd_ip_addr',
];

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'colorgb-pjax',
    'showPageSummary' => false,
    'pjax' => true,
    'striped' => true,
    'responsive' => true,
    'responsiveWrap' => true,
    'hover' => true,
    'floatHeader' => true,
    'floatHeaderOptions' => ['top' => 46],
    'toolbar' => [
        'before' => '',//Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'class' => 'btn btn-success pull-left', 'title' => 'Thêm mới']),
        'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''], ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Làm mới']),
        '{toggleData}',
    ],
    'panel' => ['type' => 'default', 'heading' => $this->title],
    'columns' => $columns,
]);
?>
