<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;
use common\models\UserGroup;
use common\colorgb\ColorgbHelper;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->getPrimaryKey();
$this->params['breadcrumbs'][] = ['label' => 'Người dùng', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">#<?= $this->title ?></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'mobile',
                //'password',
                [
                    'attribute' => 'is_admin',
                    'format' => 'raw',
                    'value' => function ($model) {
                        if ($model->is_admin == 1) {
                            $html = 'Admin';
                        } elseif ($model->is_admin = 0) {
                            $html = 'TeamLeader';
                        } else {
                            $html = 'Nhân viên';
                        }
                        return $html;
                    }
                ],
                'name',
                'address',
                'email_addr:email',
                //'email_pw:email',
                'email_signature:email',
                [
                    'attribute' => 'group_id',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return UserGroup::findOne($model->group_id)->name;
                    }
                ],
                'remarks',
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function ($model) {
                        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'value', 'txt');
                        return $recordStatus[$model->status];
                    }
                ],
                //'password_reset_token',
                //'auth_key',
                [
                    'attribute' => 'reg_date_time',
                    'value' => function ($model) {
                        return ColorgbHelper::date($model->reg_date_time, 'd/m/Y H:i:s', 'Y-m-d H:i:s');
                    }
                ],
                [
                    'attribute' => 'reg_user_id',
                    'value' => function ($model) {
                        return User::findOne($model->reg_user_id)->name;
                    }
                ],
                'reg_ip_addr',
                [
                    'attribute' => 'upd_date_time',
                    'value' => function ($model) {
                        return ColorgbHelper::date($model->upd_date_time, 'd/m/Y H:i:s', 'Y-m-d H:i:s');
                    }
                ],
                [
                    'attribute' => 'upd_user_id',
                    'value' => function ($model) {
                        return User::findOne($model->upd_user_id)->name;
                    }
                ],
                'upd_ip_addr',
            ],
        ]) ?>

    </div>
</div>
