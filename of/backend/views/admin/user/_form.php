<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="colorgb-container">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3 text-right',
                'offset' => 'col-sm-offset-3',
                'wrapper' => 'col-sm-7',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?php if ($model->isNewRecord): ?>
        <?= $form->field($model, 'mobile')->textInput(['maxlength' => true, 'autocomplete' => 'new-password']) ?>
    <?php else: ?>
        <?= $form->field($model, 'mobile')->textInput(['maxlength' => true, 'autocomplete' => 'new-password','disabled' => 'disabled']) ?>
    <?php endif; ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'autocomplete' => 'new-password']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textarea() ?>

    <?= $form->field($model, 'email_addr')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email_pw')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email_signature')->widget(TinyMce::className(), [
        'language' => 'vi_VN',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview",
                "searchreplace code fullscreen codesample",
                "image media table"
            ],
            'image_advtab' => true,
            'image_title' => true,
            'height' => '250px',
            'menubar' => true,
        ]

    ]) ?>

    <?= $form->field($model, 'group_id')->dropDownList($data['userGroup'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

    <?= $form->field($model, 'remarks')->textarea() ?>

    <?= $form->field($model, 'status')->dropDownList(ArrayHelper::map(Yii::$app->params['recordStatus'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

    <?= $form->field($model, 'is_admin')->radioList(Yii::$app->params['userRole'],['class' => 'styled']) ?>
    <div class="hide">
        <?= $form->field($model, 'upd_date_time')->textInput() ?>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-6">
            <?= Html::submitButton($model->isNewRecord ? '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Tạo mới</span>' : '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Lưu lại</span>', ['class' => $model->isNewRecord ? 'btn btn-success  btn-ladda btn-ladda-spinner btn-ladda-progress' : 'btn btn-primary  btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
