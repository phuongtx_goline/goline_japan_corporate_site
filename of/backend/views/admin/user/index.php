<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\admin\UserColorgb */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Danh sách người dùng';
$this->params['breadcrumbs'][] = $this->title;
$columns = [
    ['class' => 'kartik\grid\SerialColumn'],
    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '50px', 'header' => 'Xem',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                $html .= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->getPrimaryKey()], ['title' => 'Xem', 'data-pjax' => '0']);
                return $html;
            },
        ]
    ],
    [
        'attribute' => 'name',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) {
            $html = '<h6 class="display-block">' . $model->name . '</h6>';
            if ($model->is_admin == 1) {
                $html .= '<label class="small label bg-green">Admin</label>';
            } else if ($model->is_admin == 0) {
                $html .= '<label class="small label bg-blue">TeamLeader</label>';
            } else {
                $html .= '<label class="small label bg-grey">Nhân viên</label>';
            }
            return $html;
        },

    ],
    'mobile',
    'email_addr:email',
    [
        'attribute' => 'group_id',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $array = $data['userGroup'];
            $value = !empty($array[$model->group_id]) ? $array[$model->group_id] : '';
            return $value;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['userGroup'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Bộ lọc...'],
        'width' => '200px',
    ],
    [
        'attribute' => 'status',
        'width' => '180px',
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $array = $data['recordStatusHtml'];
            $value = !empty($array[$model->status]) ? $array[$model->status] : '';
            return $value;
        },
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['recordStatusTxt'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Bộ lọc...'],


    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '150px',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                $html .= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->getPrimaryKey()], ['title' => 'Sửa', 'data-pjax' => '0']);
                $html .= Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->getPrimaryKey()], ['title' => 'Xóa', 'data-pjax' => 'false', 'class' => 'colorgb-pjax-action-del colorgb-pjax-action-del-show', 'data-pjax-container' => 'colorgb-pjax-pjax']);
                return $html;
            },
        ]
    ],
];

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'colorgb-pjax',
    'showPageSummary' => false,
    'pjax' => true,
    'striped' => true,
    'responsive' => true,
    'responsiveWrap' => true,
    'hover' => true,
    'floatHeader' => true,
    'floatHeaderOptions' => ['top' => 46],
    'toolbar' => [
        'before' => Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'class' => 'btn btn-success pull-left', 'title' => 'Thêm mới']),
        'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''], ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Làm mới']),
        '{toggleData}',
    ],
    'panel' => ['type' => 'default', 'heading' => $this->title],
    'columns' => $columns,
]);
?>
