<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model common\models\Setting */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="colorgb-container">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3 text-right',
                'offset' => 'col-sm-offset-3',
                'wrapper' => 'col-sm-7',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?php if ($model->isNewRecord): ?>
        <?= $form->field($model, 'param_name')->textInput(['maxlength' => true]) ?>
    <?php else: ?>
        <?= $form->field($model, 'param_name')->textInput(['maxlength' => true,'disabled' => 'disabled']) ?>
    <?php endif; ?>

    <?php if($model->param_name == 'COMPANY_EMAIL_SIGNATURE'):?>
    <?= $form->field($model, 'param_value')->widget(TinyMce::className(), [
        'language' => 'vi_VN',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview",
                "searchreplace code fullscreen codesample",
                "image media table"
            ],
            'image_advtab' => true,
            'image_title' => true,
            'height' => '250px',
            'menubar' => true,
        ]

    ]) ?>
    <?php else: ?>
        <?= $form->field($model, 'param_value')->textarea() ?>
    <?php endif; ?>

    <?= $form->field($model, 'remarks')->textarea() ?>

    <div class="hide">
        <?= $form->field($model, 'upd_date_time')->textInput() ?>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-7">
            <?= Html::submitButton($model->isNewRecord ? '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Tạo mới</span>' : '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Lưu lại</span>', ['class' => $model->isNewRecord ? 'btn btn-success  btn-ladda btn-ladda-spinner btn-ladda-progress' : 'btn btn-primary  btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
