<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Code */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="colorgb-container">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4 text-right',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-6',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?php if ($model->isNewRecord): ?>
        <?= $form->field($model, 'group')->dropDownList(ArrayHelper::map(Yii::$app->params['codeGroup'], 'key', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>
    <?php else: ?>
        <?= $form->field($model, 'group')->dropDownList(ArrayHelper::map(Yii::$app->params['codeGroup'], 'key', 'txt'), ['disabled' => 'disabled','data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>
    <?php endif; ?>

    <?= $form->field($model, 'value1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'value2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'remarks')->textarea() ?>

    <?= $form->field($model, 'status')->dropDownList(ArrayHelper::map(Yii::$app->params['recordStatus'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>
    <div class="hide">
        <?= $form->field($model, 'upd_date_time')->textInput() ?>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-4 col-sm-5">
            <div class="btn-group">
                <?= Html::submitButton($model->isNewRecord ? '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Tạo mới</span>' : '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Lưu lại</span>', ['class' => $model->isNewRecord ? 'btn btn-success  btn-ladda btn-ladda-spinner btn-ladda-progress' : 'btn btn-primary  btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out']) ?>
                <?php if ($back = Yii::$app->request->get('back')): ?>
                    <a class="btn btn-warning" href="<?= preg_match('/create/', $back) ? $back . '?layout=sub' : $back . '&layout=sub' ?>"><i class="icon-arrow-left8"></i> Trở lại</a>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
