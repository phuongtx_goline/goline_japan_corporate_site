<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\colorgb\ColorgbHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\admin\MsgSendRequestColorgb */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Danh sách nhật ký gửi SMS/Email';
$this->params['breadcrumbs'][] = $this->title;
$columns = [
    ['class' => 'kartik\grid\SerialColumn'],
    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '50px', 'header' => 'Xem',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                $html .= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->getPrimaryKey()], ['title' => 'Xem', 'data-pjax' => '0']);
                return $html;
            },
        ]
    ],
    [
        'width' => '150px',
        'attribute' => 'colorgb_date',
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'locale' => [
                    'format' => 'YYYY-MM-DD',
                    'separator' => ' - ',
                ],
                'presetDropdown'=>true,
                'ranges' => [
                    //"Hôm nay" => ["moment().startOf('day')", "moment()"],
                    "Hôm qua" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                    "Tuần trước" => ["moment().startOf('day').subtract(7, 'days')", "moment()"],
                    "Tháng trước" => ["moment().startOf('day').subtract(31, 'days')", "moment()"],
                ],
                'alwaysShowCalendars' => true,

            ],
        ],
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) {
            $date = ColorgbHelper::date($model->date);
            $html = '<h6>';
            $html .= ' <span>' . $date . '</span>';
            if ($model->attachments) {
                $fileUrl = Yii::$app->params['cdnUrl'] . '/data/' . explode(';', $model->attachments)[0];
                $fileUrl = preg_match('/.docx/', $fileUrl) ? 'http://view.officeapps.live.com/op/view.aspx?src=' . $fileUrl : $fileUrl;
                if(!empty(explode(';', $model->attachments)[0])) $html .= Html::a(' <i class="icon-attachment"></i>', $fileUrl.'?colorgb='.rand(), ['class' => 'btn-colorgb-popup']);
            }
            $html .= '</h6>';
            return $html;
        },

    ],
    [
        'attribute' => 'msg_type',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $array = $data['msgTypeTxt'];
            $value = !empty($array[$model->msg_type]) ? $array[$model->msg_type] : '';
            return $value;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['msgTypeTxt'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Bộ lọc...'],
        'width' => '100px',
    ],
    [
        'attribute' => 'send_type',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $array = $data['sendTypeTxt'];
            $value = !empty($array[$model->send_type]) ? $array[$model->send_type] : '';
            return $value;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['sendTypeTxt'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Bộ lọc...'],
        'width' => '100px',
    ],
    [
        'width' => '200px',
        'attribute' => 'send_to',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $sendTo = explode(';',$model->send_to);
            $html = '<div style="max-width: 200px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap">';
            foreach ($sendTo as $item){
                $html .= $item.'<br/>';
            }
            $html .= '</div>';
            return $html;
        },
    ],
    'subject',
    // 'send_to',
    // 'from_address',
    // 'from_name',
    // 'content:ntext',
    // 'attachments',
    // 'remarks',
    // 'failed_count',
    [
        'attribute' => 'status',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $array = $data['msgStatusHtml'];
            $value = !empty($array[$model->status]) ? $array[$model->status] : '';
            return $value;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['msgStatusTxt'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Bộ lọc...'],
        'width' => '180px',
    ],
    // 'reg_date_time',
    // 'reg_user_id',
    // 'reg_ip_addr',
    // 'upd_date_time',
    // 'upd_user_id',
    // 'upd_ip_addr',
];

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'colorgb-pjax',
    'showPageSummary' => false,
    'pjax' => true,
    'striped' => true,
    'responsive' => true,
    'responsiveWrap' => true,
    'hover' => true,
    'floatHeader' => true,
    'floatHeaderOptions' => ['top' => 46],
    'toolbar' => [
        'before' => '',//Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'class' => 'btn btn-success pull-left', 'title' => 'Thêm mới']),
        'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''], ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Làm mới']),
        '{toggleData}',
    ],
    'panel' => ['type' => 'default', 'heading' => $this->title],
    'columns' => $columns,
]);
?>
