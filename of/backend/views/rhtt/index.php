<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\colorgb\ColorgbHelper;
use common\models\Customer;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FEERATEColorgb */
/* @var $dataProvider yii\data\ActiveDataProvider */
//$data['CUST_CD'] = ArrayHelper::map(\common\models\CORCUSTOMER::findAll(['status' => 1]), 'CUST_CD', 'CUST_NAME');
$data['SEC_CD'] = ArrayHelper::map(\common\models\CORTFUNDINFO::findAll(['status' => 1]), 'SEC_CD', 'SEC_NAME');
$data['status'] = ArrayHelper::map(Yii::$app->params['commonStatus'], 'value', 'txt');
$data['statusHtml'] = ArrayHelper::map(Yii::$app->params['commonStatus'], 'value', 'html');

$this->title = 'Quản lý thông tin thực hiện quyền';
$this->params['breadcrumbs'][] = $this->title;
$columns = [

    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '50px', 'header' => 'Xem',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                if (ColorgbHelper::checkPermission('RHTT_VIEW')) $html .= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->getPrimaryKey()], ['title' => 'Xem', 'data-pjax' => '0']);
                return $html;
            },
        ]
    ],
    [
        'attribute' => 'SEC_CD',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['SEC_CD'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Lọc...'],
        'value' => function ($model) use($data) {
            $h = '';
            $h .= '<p>' . $data[$model->SEC_CD] . '</p>';
            $h .= '<p>' . Yii::$app->params['RIGHT_TYPE'][$model->RIGHT_TYPE] . '</p>';
            return $h;
        }
    ],
    [
        'attribute' => 'NO_RIGHT_DATE',
        'format' => 'raw',
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
            'options' => [
                'autocomplete' => 'off',
            ],
            'pluginOptions' => [
                'locale' => [
                    'format' => 'DD/MM/YYYY',
                    'separator' => ' - ',
                ],
                'ranges' => [
                    //"Hôm nay" => ["moment().startOf('day')", "moment()"],
                    "Hôm qua" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                    "Tuần trước" => ["moment().startOf('day').subtract(7, 'days')", "moment()"],
                    "Tháng trước" => ["moment().startOf('day').subtract(31, 'days')", "moment()"],
                ],
                'alwaysShowCalendars' => true,

            ],
        ],
        'value' => function ($model) use ($data) {

            return ColorgbHelper::date($model->NO_RIGHT_DATE);
        }

    ],
    [
        'attribute' => 'OWNER_FIX_DATE',
        'format' => 'raw',
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
            'options' => [
                'autocomplete' => 'off',
            ],
            'pluginOptions' => [
                'locale' => [
                    'format' => 'DD/MM/YYYY',
                    'separator' => ' - ',
                ],
                'ranges' => [
                    //"Hôm nay" => ["moment().startOf('day')", "moment()"],
                    "Hôm qua" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                    "Tuần trước" => ["moment().startOf('day').subtract(7, 'days')", "moment()"],
                    "Tháng trước" => ["moment().startOf('day').subtract(31, 'days')", "moment()"],
                ],
                'alwaysShowCalendars' => true,

            ],
        ],
        'value' => function ($model) use ($data) {

            return ColorgbHelper::date($model->OWNER_FIX_DATE);
        }

    ],
    [
        'label' => 'Tỷ lệ',
        'mergeHeader' => true,
        'format' => 'raw',
        'value' => function ($model) use($data) {
            $h = '';
            $h .= '<p>TL quyền: ' . $model->RATES1/$model->RATED1 . '</p>';
            $h .= '<p>TL thực hiện: ' . $model->RATES/$model->RATED . '</p>';
            return $h;
        }
    ],
    [
        'attribute' => 'STATUS',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['status'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Lọc...'],
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $array = $data['statusHtml'];
            $value = !empty($array[$model->STATUS]) ? $array[$model->STATUS] : '';
            return $value;
        },
    ],
    [
        'attribute' => 'RIGHT_EXC_DATE',
        'format' => 'raw',
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
            'options' => [
                'autocomplete' => 'off',
            ],
            'pluginOptions' => [
                'locale' => [
                    'format' => 'DD/MM/YYYY',
                    'separator' => ' - ',
                ],
                'ranges' => [
                    //"Hôm nay" => ["moment().startOf('day')", "moment()"],
                    "Hôm qua" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                    "Tuần trước" => ["moment().startOf('day').subtract(7, 'days')", "moment()"],
                    "Tháng trước" => ["moment().startOf('day').subtract(31, 'days')", "moment()"],
                ],
                'alwaysShowCalendars' => true,

            ],
        ],
        'value' => function ($model) use ($data) {

            return ColorgbHelper::date($model->RIGHT_EXC_DATE);
        }

    ],

    [
        'attribute' => 'EXPECTED_EXC_DATE',
        'format' => 'raw',
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
            'options' => [
                'autocomplete' => 'off',
            ],
            'pluginOptions' => [
                'locale' => [
                    'format' => 'DD/MM/YYYY',
                    'separator' => ' - ',
                ],
                'ranges' => [
                    //"Hôm nay" => ["moment().startOf('day')", "moment()"],
                    "Hôm qua" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                    "Tuần trước" => ["moment().startOf('day').subtract(7, 'days')", "moment()"],
                    "Tháng trước" => ["moment().startOf('day').subtract(31, 'days')", "moment()"],
                ],
                'alwaysShowCalendars' => true,

            ],
        ],
        'value' => function ($model) use ($data) {

            return ColorgbHelper::date($model->EXPECTED_EXC_DATE);
        }

    ],



];

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'colorgb-pjax',
    'showPageSummary' => false,
    'pjax' => true,
    'striped' => true,
    'responsive' => false,
    'responsiveWrap' => false,
    'hover' => true,
    'floatHeader' => true,
    'floatHeaderOptions' => ['top' => 46],
    'toolbar' => [
        'before' => (ColorgbHelper::checkPermission('RHTT_CREATE') ? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'class' => 'btn btn-success pull-left', 'title' => 'Thêm mới']) : ''),
        'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''], ['data-pjax' => 1, 'class' => 'btn btn-default ', 'title' => 'Làm mới']),
        '{toggleData}',
    ],
    'panel' => ['type' => 'default', 'heading' => $this->title],
    'columns' => $columns,
]);
?>