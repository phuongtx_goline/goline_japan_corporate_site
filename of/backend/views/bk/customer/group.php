<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use common\colorgb\ColorgbHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\customer\CustomerColorgb */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Theo dõi tài sản nhóm khách hàng';
$this->params['breadcrumbs'][] = ['label' => 'Khách hàng', 'url' => ['/customer']];
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '20px',
        'headerOptions' => ['class' => 'kartik-sheet-style'],
    ],
    [
        'class' => 'kartik\grid\ExpandRowColumn',
        'width' => '60px',
        'hAlign' => 'left',
        'value' => function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail' => function ($model, $key, $index, $column) use ($data) {
            return Yii::$app->controller->renderPartial('_report', ['model' => $model, 'data' => $data]);
        },
        'headerOptions' => ['class' => 'kartik-sheet-style'],

    ],
    [
        'attribute' => 'image',
        'format' => 'raw',
        'hAlign' => 'center',
        'width' => '110px',
        'value' => function ($model, $key, $index, $widget) {
            $src = $model->image ? ColorgbHelper::dataSrc($model->image) : Yii::$app->params['cdnUrl'] . '/bolt/assets/images/placeholder.jpg';
            return Html::img($src, ['width' => 60]);
        },
        'filter' => false,
        'mergeHeader' => true,
    ],
    [
        'attribute' => 'group',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $html = '<strong class="display-block"><a data-pjax="0" title="Xem các hợp đồng của khách hàng" href="contract?ContractColorgb%5Bcontract_date%5D=&ContractColorgb%5Bcustomer_id%5D='.$model->getPrimaryKey().'&ContractColorgb%5Bcontract_info%5D=&ContractColorgb%5Bstatus%5D=">' . $model->name . '</a></strong>';
            $html .= '<p>' . $model->mobile . '</p>';
            $html .= '<p>' . $model->email_addr . '</p>';
            return $html;
        },

    ],


];

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'colorgb-pjax',
    'showPageSummary' => false,
    'pjax' => true,
    'striped' => true,
    'responsive' => true,
    'responsiveWrap' => true,
    'hover' => true,
    'floatHeader' => true,
    'floatHeaderOptions' => ['top' => 46],
    'toolbar' => [
        'before' =>
            Html::a('<i class="icon-arrow-left8"></i> Danh sách KH', ['/customer'], ['data-pjax' => 0, 'class' => 'btn btn-default pull-left']),
            //Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'class' => 'btn btn-success pull-left', 'title' => 'Thêm mới']),
        'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''], ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Làm mới']),
        '{toggleData}',
    ],
    'panel' => ['type' => 'default', 'heading' => $this->title],
    'columns' => $columns,
]);