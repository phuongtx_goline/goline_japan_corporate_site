<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Customer */

$this->title = 'Xem chi tiết khách hàng #'.$model->getPrimaryKey();
$this->params['breadcrumbs'][] = ['label' => 'Khách hàng', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .file-caption-main{
        display: none;
    }
</style>
<script>
    $(function () {
        $('.form-control').each(function () {
            $(this).attr('disabled','disabled');
        });
    });
</script>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $this->title ?></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="colorgb-form">

            <?= $this->render('_view', [
                'model' => $model,
                'data' => $data,
            ]) ?>

        </div>

    </div>
</div>
