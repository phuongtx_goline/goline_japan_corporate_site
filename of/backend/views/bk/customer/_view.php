<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use kartik\file\FileInput;
use common\models\User;
use common\models\CustomerAttribute;

/* @var $this yii\web\View */
/* @var $model common\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="colorgb-container">
    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4 text-right',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-7',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <div class="tabbable">
        <ul class="nav nav-tabs nav-tabs-highlight nav-justified">
            <li class="active"><a href="#justified-left-icon-tab1" data-toggle="tab"><i class="icon-flag4 position-left"></i> Thông tin cơ bản</a></li>
            <li><a href="#justified-left-icon-tab2" data-toggle="tab"><i class="icon-reading position-left"></i> Thuộc tính tùy chỉnh</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="justified-left-icon-tab1">
                <div class="row">
                    <div class="col-sm-6">

                        <?php if ($model->isNewRecord): ?>
                            <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>

                        <?php else: ?>
                            <?= $form->field($model, 'mobile')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>
                        <?php endif; ?>

                        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                        <?php if ($model->isNewRecord): ?>
                            <?= $form->field($model, 'email_addr')->textInput(['maxlength' => true]) ?>

                        <?php else: ?>
                            <?= $form->field($model, 'email_addr')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>
                        <?php endif; ?>

                        <?= $form->field($model, 'org_type')->dropDownList(ArrayHelper::map(Yii::$app->params['orgType'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                        <?= $form->field($model, 'org_info')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

                        <?= $form->field($model, 'foreign_type')->dropDownList(ArrayHelper::map(Yii::$app->params['foreignType'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                        <?= $form->field($model, 'nationality')->dropDownList(ArrayHelper::map(Yii::$app->params['countryList'], 'code', 'name'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                        <?= $form->field($model, 'gender')->dropDownList(ArrayHelper::map(Yii::$app->params['gender'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                        <?= $form->field($model, 'prefix')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'birthday')->widget(DateControl::classname(), [
                            'type' => DateControl::FORMAT_DATE,
                            'displayFormat' => 'dd/MM/yyyy',
                            'saveFormat' => 'yyyyMMdd',
                            'ajaxConversion' => false,
                            'autoWidget' => true
                        ])
                        ?>

                        <?= $form->field($model, 'address1')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'address2')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'tax_code')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'id_number')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'id_issue_date')->widget(DateControl::classname(), [
                            'type' => DateControl::FORMAT_DATE,
                            'displayFormat' => 'dd/MM/yyyy',
                            'saveFormat' => 'yyyyMMdd',
                            'ajaxConversion' => false,
                            'autoWidget' => true
                        ])
                        ?>

                        <?= $form->field($model, 'id_expired_date')->widget(DateControl::classname(), [
                            'type' => DateControl::FORMAT_DATE,
                            'displayFormat' => 'dd/MM/yyyy',
                            'saveFormat' => 'yyyyMMdd',
                            'ajaxConversion' => false,
                            'autoWidget' => true
                        ])
                        ?>

                        <?= $form->field($model, 'id_place')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'file_upload')->widget(FileInput::classname(), [
                            'pluginOptions' => $data['fileInput']]);
                        ?>

                        <?= $form->field($model, 'status')->dropDownList(ArrayHelper::map(Yii::$app->params['customerStatus'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>


                    </div>
                    <div class="col-sm-6">

                        <?= $form->field($model, 'bank_name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'bank_branch_name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'bank_acco_no')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'bank_acco_name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'mobile1')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'mobile2')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'email_addr1')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'email_addr2')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'parent_id')->dropDownList(
                            [$model->parent_id => $data['customerParent'] ? $data['customerParent']->name : 'Chọn...'],
                            ['class' => 'colorgb-customer-search form-control',],
                            ['options' => [$model->parent_id => ['selected' => true]]]
                        ) ?>

                        <?= $form->field($model, 'customer_group_id')->dropDownList($data['customerGroup'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                        <?= $form->field($model, 'info_source_id')->dropDownList($data['infoSource'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                        <?= $form->field($model, 'cared_user_id')->dropDownList($data['userArray'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                        <?= $form->field($model, 'intro_user_id')->dropDownList($data['userArray'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                        <?= $form->field($model, 'profession_id')->dropDownList($data['customerProfession'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                        <?= $form->field($model, 'character')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'remarks')->textarea() ?>


                    </div>

                </div>
            </div>
            <div class="tab-pane" id="justified-left-icon-tab2">
                <br/>
                <div class="row">
                    <div class="col-sm-offset-2 col-sm-8">
                        <table class="table table-bordered table-hover" id="tableAddRow">
                            <thead>
                            <tr>
                                <th width="50%">Tên thuộc tính</th>
                                <th width="50%">Giá trị</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($data['customerAttribute'])): ?>

                                <?php foreach ($data['customerAttribute'] as $key => $value): ?>
                                    <tr data-i="<?= $key ?>">
                                        <td><input type="text" name="Customer[attribute][<?= $key ?>][key]" value="<?= $value->value1 ?>" class="form-control" style="pointer-events: none;background-color: #fafafa;"/></td>
                                        <td><input type="text" name="Customer[attribute][<?= $key ?>][value]" value="<?= CustomerAttribute::findOne(['customer_id'=>$model->id,'attribute_id'=>$value->id,'status'=>1])->value ?>" class="form-control"/></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>


        <hr class="mt-15">

        <div class="row pt-10">

            <div class="col-sm-6">

                <?= $form->field($model, 'reg_date_time')->textInput(['value' => date('d/m/Y H:i:s', strtotime($model->reg_date_time))]) ?>

                <?= $form->field($model, 'reg_user_id')->textInput(['value' => User::findOne($model->reg_user_id)->name]) ?>

                <?= $form->field($model, 'reg_ip_addr')->textInput(['maxlength' => true]) ?>

            </div>

            <div class="col-sm-6">

                <?= $form->field($model, 'upd_date_time')->textInput(['value' => date('d/m/Y H:i:s', strtotime($model->upd_date_time))]) ?>

                <?= $form->field($model, 'upd_user_id')->textInput(['value' => User::findOne($model->upd_user_id)->name]) ?>

                <?= $form->field($model, 'upd_ip_addr')->textInput(['maxlength' => true]) ?>

            </div>

        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>