<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use kartik\file\FileInput;
use common\models\Customer;
use common\models\CustomerAttribute;

/* @var $this yii\web\View */
/* @var $model common\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="colorgb-container">
    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4 text-right',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-7',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <div class="tabbable">
        <ul class="nav nav-tabs nav-tabs-highlight nav-justified">
            <li class="active"><a href="#justified-left-icon-tab1" data-toggle="tab"><i class="icon-flag4 position-left"></i> Thông tin cơ bản</a></li>
            <li><a href="#justified-left-icon-tab2" data-toggle="tab"><i class="icon-reading position-left"></i> Thuộc tính tùy chỉnh</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="justified-left-icon-tab1">
                <div class="row">
                    <div class="col-sm-6">

                        <?php if ($model->isNewRecord || Yii::$app->user->identity->is_admin == 1): ?>
                            <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>

                        <?php else: ?>
                            <?= $form->field($model, 'mobile')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>
                        <?php endif; ?>

                        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                        <?php if ($model->isNewRecord || Yii::$app->user->identity->is_admin == 1): ?>
                            <?= $form->field($model, 'email_addr')->textInput(['maxlength' => true]) ?>

                        <?php else: ?>
                            <?= $form->field($model, 'email_addr')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>
                        <?php endif; ?>

                        <?= $form->field($model, 'type')->dropDownList(ArrayHelper::map(Yii::$app->params['customerType'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                        <?= $form->field($model, 'org_type')->dropDownList(ArrayHelper::map(Yii::$app->params['orgType'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                        <?= $form->field($model, 'org_info')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>

                        <?= $form->field($model, 'foreign_type')->dropDownList(ArrayHelper::map(Yii::$app->params['foreignType'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                        <?= $form->field($model, 'nationality')->dropDownList(ArrayHelper::map(Yii::$app->params['countryList'], 'code', 'name'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                        <?= $form->field($model, 'gender')->dropDownList(ArrayHelper::map(Yii::$app->params['gender'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                        <?= $form->field($model, 'prefix')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'birthday')->widget(DateControl::classname(), [
                            'type' => DateControl::FORMAT_DATE,
                            'displayFormat' => 'dd/MM/yyyy',
                            'saveFormat' => 'yyyyMMdd',
                            'ajaxConversion' => false,
                            'autoWidget' => true
                        ])
                        ?>

                        <?= $form->field($model, 'address1')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'address2')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'tax_code')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'id_number')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'id_issue_date')->widget(DateControl::classname(), [
                            'type' => DateControl::FORMAT_DATE,
                            'displayFormat' => 'dd/MM/yyyy',
                            'saveFormat' => 'yyyyMMdd',
                            'ajaxConversion' => false,
                            'autoWidget' => true
                        ])
                        ?>

                        <?= $form->field($model, 'id_expired_date')->widget(DateControl::classname(), [
                            'type' => DateControl::FORMAT_DATE,
                            'displayFormat' => 'dd/MM/yyyy',
                            'saveFormat' => 'yyyyMMdd',
                            'ajaxConversion' => false,
                            'autoWidget' => true
                        ])
                        ?>

                        <?= $form->field($model, 'id_place')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'file_upload')->widget(FileInput::classname(), [
                            'pluginOptions' => $data['fileInput']]);
                        ?>

                        <?= $form->field($model, 'is_locked')->checkbox(['class' => 'styled']) ?>

                        <?= $form->field($model, 'need_change_pw')->checkbox(['class' => 'styled']) ?>


                    </div>
                    <div class="col-sm-6">

                        <?= $form->field($model, 'bank_name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'bank_branch_name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'bank_acco_no')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'bank_acco_name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'mobile1')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'mobile2')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'email_addr1')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'email_addr2')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'parent_id')->dropDownList(
                            [$model->parent_id => $data['customerParent'] ? $data['customerParent']->name : 'Chọn...'],
                            ['class' => 'colorgb-customer-search',],
                            ['options' => [$model->parent_id => ['selected' => true]]]
                        ) ?>

                        <?= $form->field($model, 'customer_group_id')->dropDownList($data['customerGroup'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                        <?= $form->field($model, 'info_source_id')->dropDownList($data['infoSource'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                        <?= $form->field($model, 'intro_user_id')->dropDownList($data['userArray'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                        <?= $form->field($model, 'profession_id')->dropDownList($data['customerProfession'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                        <?= $form->field($model, 'character')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'remarks')->textarea() ?>

                        <?php if(Yii::$app->user->identity->is_admin != -1):?>
                        <?= $form->field($model, 'cared_user_id')->dropDownList($data['userCaredArray'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>
                        <?php endif;?>

                    </div>

                </div>
            </div>
            <div class="tab-pane" id="justified-left-icon-tab2">
                <br/>
                <div class="row">
                    <div class="col-sm-offset-2 col-sm-8">
                        <table class="table table-bordered table-hover" id="tableAddRow">
                            <thead>
                            <tr>
                                <th width="49%">Tên thuộc tính</th>
                                <th width="49%">Giá trị</th>
                                <th width="2%">
                                    <button type="button" class="btn btn-success btn-add-row" data-i="<?= count($data['customerAttribute']) ?>"><i class="icon-plus3"></i></button>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($data['customerAttributeSelected'])): ?>

                                <?php foreach ($data['customerAttributeSelected'] as $key => $value): ?>
                                    <tr data-i="<?= $value->code->id ?>">
                                        <td>
                                            <select class="form-control readonly" name="Customer[attribute][<?= $value->code->id ?>][key]" readonly="readonly">
                                                <option value="<?= $value->code->id ?>"><?= $value->code->value1 ?></option>
                                            </select>
                                        <td><input type="text" name="Customer[attribute][<?= $value->code->id ?>][value]" value="<?= $value->value ?>" class="form-control"/></td>
                                        <td>
                                            <button type="button" class="btn btn-danger btn-remove-row"><i class="icon-minus3"></i></button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

        <div class="hide">
            <?= $form->field($model, 'upd_date_time')->textInput() ?>
        </div>

        <div class="form-group btn-tab">
            <div class="col-sm-offset-2 col-sm-8">
                <?php if ($model->isNewRecord): ?>
                    <?= Html::submitButton('<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Tạo mới</span>', ['class' => 'btn btn-success btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out']) ?>
                <?php else: ?>

                    <?= Html::submitButton('<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Lưu lại</span>', ['class' => 'btn btn-primary  btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out']) ?>

                <?php endif; ?>

            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    function addTableRowCustomerAttribute() {
        var tr = $('#tableAddRow tbody tr:last-child');
        var i = tr.length > 0 ? tr.data('i') + 1 : 0;
        var tempTr = $('<tr data-i="' + i + '"><td><select data-placeholder="Chọn..." class="selectAttribute" name="Customer[attribute][' + i + '][key]"><?php foreach ($data["customerAttribute"] as $key => $value): ?><option></option><option value="<?=$value->id?>"><?=$value->value1?></option><?php endforeach; ?></select></td> <td><input type="text" name="Customer[attribute][' + i + '][value]" class="form-control"/></td><td><button type="button" class="btn btn-danger btn-remove-row"><i class="icon-minus3"></i></button></td></tr>').on('click', function () {
            var temp = $(this);
            $('.btn-remove-row').click(function () {
                $(this).closest('tr').remove();
            });

            $('.selectAttribute').change(function () {
                var t = $(this);
                var p = t.parent().parent();
                p.attr('data-i', t.val());
                t.attr('name', 'Customer[attribute][' + t.val() + '][key]');
                p.find('input').attr('name', 'Customer[attribute][' + t.val() + '][value]');

                var i = 0;
                $("#tableAddRow tr").each(function () {
                    var tr = $(this);
                    if (tr.find('select').val() === t.val()) {
                        tr.addClass('has-error');
                        p.addClass('has-error');

                    } else {
                        tr.removeClass('has-error');
                        p.removeClass('has-error');
                    }

                });


            });
        });
        $("#tableAddRow").append(tempTr);
    }


    $('.btn-add-row').on('click', function () {
        addTableRowCustomerAttribute();
        $('.selectAttribute').select2();
    });

    $(function () {
        $('select.readonly').select2("destroy");
    })
</script>