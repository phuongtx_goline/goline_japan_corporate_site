<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use common\models\FundUnit;
use common\models\Contract;
use common\models\Setting;
use common\models\Customer;
use common\colorgb\ColorgbHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Customer */
/* @var $form yii\widgets\ActiveForm */
$contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');

$contract1 = Contract::find()->select('invest_value,invest_unit_qty,fund_unit_price,begin_date,invest_unit_qty')->where(['customer_id' => $model->getPrimaryKey(),'contract_type_id'=>[1,2]])->andWhere(['>=','status', $contractStatus['active']])->andWhere(['!=','status', $contractStatus['settled']])->andWhere(['!=','status', $contractStatus['deleted']])->orderBy(['begin_date' => SORT_ASC])->all();
$fundUnit1 = ColorgbHelper::fundUnit();
$fup1 = common\models\FundUnit::find()->where(['like', 'date', date('Y')])->orderBy(['date' => SORT_ASC])->one()->fund_unit_price;

$contract2 = Contract::find()->select('invest_value,invest_unit_qty,fund_unit_price,begin_date,invest_unit_qty')->where(['customer_id' => $model->getPrimaryKey(),'contract_type_id'=>[3]])->andWhere(['>=','status', $contractStatus['active']])->andWhere(['!=','status', $contractStatus['settled']])->andWhere(['!=','status', $contractStatus['deleted']])->orderBy(['begin_date' => SORT_ASC])->all();
$fundUnit2 = ColorgbHelper::fundUnitHsa();
$fup2 = common\models\FundUnitHsa::find()->where(['like', 'date', date('Y')])->orderBy(['date' => SORT_ASC])->one()->fund_unit_price;

?>
<?php if($contract1 || $contract2):?>
<script>

    // Chart setup
    function lineBasic1<?=$model->getPrimaryKey()?>(element, width, height) {

        // Define main variables
        var d3Container = d3.select(element),
            margin = {top: 5, right: 20, bottom: 20, left: 50},
            width = width,
            height = height - margin.top - margin.bottom - 5;

        // Format data
        var parseDate = d3.time.format("%Y%m%d").parse,
            bisectDate = d3.bisector(function (d) {
                return d.date;
            }).left,
            formatValue = d3.format(","),
            formatCurrency = function (d) {
                return formatValue(d) + '';
            };


        // Horizontal
        var x = d3.time.scale()
            .range([0, width]);

        // Vertical
        var y = d3.scale.linear()
            .range([height, 0]);

        // Horizontal
        var xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom")
            .tickSize(-height, 0, 0)
            .tickFormat(d3.time.format("%d/%m/%y"));

        // Vertical
        var yAxis = d3.svg.axis()
            .scale(y)
            .tickSize(-width, 0, 0)
            .orient("left");


        // Add SVG element
        var container = d3Container.append("svg");

        // Add SVG group
        var svg = container
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


        // Line
        var line = d3.svg.line()
            .interpolate("basis")
            .x(function (d) {
                return x(d.date);
            })
            .y(function (d) {
                return y(d.close);
            });


        // Load data
        d3.tsv("<?=Yii::$app->params['cdnUrl']?>/data/investment/report/<?=$contract1[0]->begin_date ? $contract1[0]->begin_date : $fundUnit1['first']->date ?>.tsv?colorgb=<?=time()?>", function (error, data) {

            // Pull out values
            data.forEach(function (d) {
                d.date = parseDate(d.date);
                d.close = +d.close;
            });

            // Sort data
            data.sort(function (a, b) {
                return a.date - b.date;
            });

            // Horizontal
            x.domain(d3.extent(data, function (d) {
                return d.date;
            }));

            // Vertical
            y.domain([0, d3.max(data, function (d) {
                return d.close;
            })]);

            // Add line
            svg.append("path")
                .datum(data)
                .attr("class", "d3-line d3-line-medium")
                .attr("d", line)
                .style("fill", "none")
                .style("stroke-width", 3)
                .style("stroke", "#4CAF50");


            // Horizontal
            svg.append("g")
                .attr("class", "d3-axis d3-axis-horizontal d3-axis-strong")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis);

            // Vertical
            var verticalAxis = svg.append("g")
                .attr("class", "d3-axis d3-axis-vertical d3-axis-strong")
                .call(yAxis);

            // Add text label
            verticalAxis.append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 10)
                .attr("dy", ".71em")
                .style("text-anchor", "end")
                .style("fill", "#ccc")
                .style("font-size", 12)
                .text("");


            // Group elements
            var focus = svg.append("g")
                .attr("class", "d3-crosshair-pointer")
                .style("display", "none");

            // Pointer
            focus.append("circle")
                .attr("r", 3.5)
                .style("fill", "#fff")
                .style("stroke", "#4CAF50")
                .style("stroke-width", 2);

            // Text
            focus.append("text")
                .attr("dy", ".35em")
                .style("fill", "#999")
                .style("stroke", "none");

            // Overlay
            svg.append("rect")
                .attr("class", "d3-crosshair-overlay")
                .attr("width", width)
                .attr("height", height)
                .style("opacity", 0.1)
                .style("fill", "#eee")
                .on("mouseover", function () {
                    focus.style("display", null);
                })
                .on("mouseout", function () {
                    focus.style("display", "none");
                })
                .on("mousemove", mousemove);

            // Display tooltip on mousemove
            function mousemove() {
                var x0 = x.invert(d3.mouse(this)[0]),
                    i = bisectDate(data, x0, 1),
                    d0 = data[i - 1],
                    d1 = data[i],
                    d = x0 - d0.date > d1.date - x0 ? d1 : d0;
                focus.attr("transform", "translate(" + x(d.date) + "," + y(d.close) + ")");
                focus.select("text").text(formatCurrency(d.close)).attr("dx", -26).attr("dy", 30);
            }
        });


        // Call function on window resize
        $(window).on('resize', resize);

        // Call function on sidebar width change
        $('.sidebar-control').on('click', resize);

        // Resize function
        function resize() {

            // Layout variables
            width = width,

                // Main svg width
                container.attr("width", width + margin.left + margin.right);

            // Width of appended group
            svg.attr("width", width + margin.left + margin.right);

            // Horizontal range
            x.range([0, width]);

            // Horizontal axis
            svg.selectAll('.d3-axis-horizontal').call(xAxis);

            // Line path
            svg.selectAll('.d3-line').attr("d", line);

            // Crosshair
            svg.selectAll('.d3-crosshair-overlay').attr("width", width);
        }
    }

    // Chart setup
    function lineBasic2<?=$model->getPrimaryKey()?>(element, width, height) {

        // Define main variables
        var d3Container = d3.select(element),
            margin = {top: 5, right: 20, bottom: 20, left: 50},
            width = width,
            height = height - margin.top - margin.bottom - 5;

        // Format data
        var parseDate = d3.time.format("%Y%m%d").parse,
            bisectDate = d3.bisector(function (d) {
                return d.date;
            }).left,
            formatValue = d3.format(","),
            formatCurrency = function (d) {
                return formatValue(d) + '';
            };


        // Horizontal
        var x = d3.time.scale()
            .range([0, width]);

        // Vertical
        var y = d3.scale.linear()
            .range([height, 0]);

        // Horizontal
        var xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom")
            .tickSize(-height, 0, 0)
            .tickFormat(d3.time.format("%d/%m/%y"));

        // Vertical
        var yAxis = d3.svg.axis()
            .scale(y)
            .tickSize(-width, 0, 0)
            .orient("left");


        // Add SVG element
        var container = d3Container.append("svg");

        // Add SVG group
        var svg = container
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


        // Line
        var line = d3.svg.line()
            .interpolate("basis")
            .x(function (d) {
                return x(d.date);
            })
            .y(function (d) {
                return y(d.close);
            });


        // Load data
        d3.tsv("<?=Yii::$app->params['cdnUrl']?>/data/investment/report-hsa/<?=$contract1[0]->begin_date ? $contract1[0]->begin_date : $fundUnit1['first']->date ?>.tsv?colorgb=<?=time()?>", function (error, data) {

            // Pull out values
            data.forEach(function (d) {
                d.date = parseDate(d.date);
                d.close = +d.close;
            });

            // Sort data
            data.sort(function (a, b) {
                return a.date - b.date;
            });

            // Horizontal
            x.domain(d3.extent(data, function (d) {
                return d.date;
            }));

            // Vertical
            y.domain([0, d3.max(data, function (d) {
                return d.close;
            })]);

            // Add line
            svg.append("path")
                .datum(data)
                .attr("class", "d3-line d3-line-medium")
                .attr("d", line)
                .style("fill", "none")
                .style("stroke-width", 3)
                .style("stroke", "#4CAF50");


            // Horizontal
            svg.append("g")
                .attr("class", "d3-axis d3-axis-horizontal d3-axis-strong")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis);

            // Vertical
            var verticalAxis = svg.append("g")
                .attr("class", "d3-axis d3-axis-vertical d3-axis-strong")
                .call(yAxis);

            // Add text label
            verticalAxis.append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 10)
                .attr("dy", ".71em")
                .style("text-anchor", "end")
                .style("fill", "#ccc")
                .style("font-size", 12)
                .text("");


            // Group elements
            var focus = svg.append("g")
                .attr("class", "d3-crosshair-pointer")
                .style("display", "none");

            // Pointer
            focus.append("circle")
                .attr("r", 3.5)
                .style("fill", "#fff")
                .style("stroke", "#4CAF50")
                .style("stroke-width", 2);

            // Text
            focus.append("text")
                .attr("dy", ".35em")
                .style("fill", "#999")
                .style("stroke", "none");

            // Overlay
            svg.append("rect")
                .attr("class", "d3-crosshair-overlay")
                .attr("width", width)
                .attr("height", height)
                .style("opacity", 0.1)
                .style("fill", "#eee")
                .on("mouseover", function () {
                    focus.style("display", null);
                })
                .on("mouseout", function () {
                    focus.style("display", "none");
                })
                .on("mousemove", mousemove);

            // Display tooltip on mousemove
            function mousemove() {
                var x0 = x.invert(d3.mouse(this)[0]),
                    i = bisectDate(data, x0, 1),
                    d0 = data[i - 1],
                    d1 = data[i],
                    d = x0 - d0.date > d1.date - x0 ? d1 : d0;
                focus.attr("transform", "translate(" + x(d.date) + "," + y(d.close) + ")");
                focus.select("text").text(formatCurrency(d.close)).attr("dx", -26).attr("dy", 30);
            }
        });


        // Call function on window resize
        $(window).on('resize', resize);

        // Call function on sidebar width change
        $('.sidebar-control').on('click', resize);

        // Resize function
        function resize() {

            // Layout variables
            width = width,

                // Main svg width
                container.attr("width", width + margin.left + margin.right);

            // Width of appended group
            svg.attr("width", width + margin.left + margin.right);

            // Horizontal range
            x.range([0, width]);

            // Horizontal axis
            svg.selectAll('.d3-axis-horizontal').call(xAxis);

            // Line path
            svg.selectAll('.d3-line').attr("d", line);

            // Crosshair
            svg.selectAll('.d3-crosshair-overlay').attr("width", width);
        }
    }

    function colorgbAjax<?=$model->getPrimaryKey()?>() {

        // Initialize chart
        var width = $('.chart-container-<?= $model->getPrimaryKey() ?>').width();
        lineBasic1<?=$model->getPrimaryKey()?>('#d3-line-1-<?=$model->getPrimaryKey()?>', width, 300);
        lineBasic2<?=$model->getPrimaryKey()?>('#d3-line-2-<?=$model->getPrimaryKey()?>', width, 300);
    }

    function colorgbAjaxReady() {
        var width = $('#colorgb-pjax').width() - 112;
        width = $('#colorgb-pjax').length > 0 ? width : $('.colorgb-container .colorgb-report').width() - 70;
        $('.chart-container-<?= $model->getPrimaryKey() ?>').width(width);
        if ($('.chart-container-<?= $model->getPrimaryKey() ?>').width() > 0 && $('.chart-container-<?= $model->getPrimaryKey() ?> svg').length == 0) {

            colorgbAjax<?=$model->getPrimaryKey()?>();

        }
    }

    $(document).on('pjax:complete', colorgbAjaxReady);
    $(document).ready(colorgbAjaxReady);

</script>

<div class="colorgb-container">
    <div class="panel panel-white colorgb-report colorgb-report-panel">
        <div class="panel-heading">
            <h6 class="panel-title">Báo cáo hiệu quả đầu tư của khách hàng (PI)</h6>
            <div class="heading-elements">
                <?php if ($contract1): ?>
                    <a data-pjax="0" href="customer/weekly-report-download?id=<?= $model->getPrimaryKey() ?>" class="btn btn-default btn-xs heading-btn"><i class="icon-download position-left"></i> Tải về báo cáo tuần</a>
                <?php endif; ?>
            </div>
        </div>

        <div class="panel-body no-padding-bottom">
            <div class="row">
                <div class="col-xs-6 content-group">
                    <ul class="list-condensed list-unstyled">
                        <li>Lợi nhuận từ năm <?= date('Y') ?></li>
                        <li><span class="text-semibold"><?= round(($fundUnit1['last']->fund_unit_price - $fup1) / $fup1 * 100, 2) ?>%</span></li>
                    </ul>
                </div>

                <div class="col-xs-6 content-group">
                    <div class="invoice-details pull-right text-right">
                        <ul class="list-condensed list-unstyled">
                            <li>Từ ngày: <span class="colorgb-semibold"><?= $contract1[0]->begin_date ? ColorgbHelper::date($contract1[0]->begin_date) : ColorgbHelper::date($fundUnit1['first']->date) ?></span></li>
                            <li>Đến ngày: <span class="colorgb-semibold"><?= ColorgbHelper::date($fundUnit1['last']->date) ?></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="mb-20 chart-container-<?= $model->getPrimaryKey() ?>" style="min-width: 880px;">
            <h3 class="text-uppercase text-semibold text-center">Hiệu quả đầu tư - Passion Investment</h3>
            <div class="chart-responsive">
                <div class="chart" id="d3-line-1-<?= $model->getPrimaryKey() ?>"></div>
            </div>
        </div>

        <h5 class="text-semibold text-uppercase pl-15 mt-20 mb-20 text-blue-800">

            Khách hàng: <?= $model->name ?>

        </h5>

        <?php if (!empty($contract1)): ?>
            <div class="table-responsive">
                <table class="table table-lg">
                    <thead>
                    <tr>

                        <th width="20px">STT</th>
                        <th width="120px" class="text-right">Ngày ký HĐ</th>
                        <th class="text-right">Số lượng ĐVĐT (1)</th>
                        <th class="text-right">Giá ĐVĐT (2)</th>
                        <th class="text-right">Tổng giá trị mua (1)x(2)</th>
                        <th class="text-right">Lợi nhuận</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($contract1 as $key => $value): ?>
                        <?php
                            $subProfitRate = ($fundUnit2['last']->fund_unit_price - $value->fund_unit_price) / $value->fund_unit_price * 100;
                            $subTotal1 = $value->invest_value;//round($value->invest_unit_qty * $value->fund_unit_price, -5);
                            $subTotal2 = $value->invest_unit_qty * $fundUnit1['last']->fund_unit_price;
                            $subProfit = $subProfitRate * $subTotal1 / 100;
                            $qty += $value->invest_unit_qty;
                            $total1 += $subTotal1;
                            $total2 += $subTotal2;
                            $profit += $subProfit;

                            ?>
                            <tr>
                                <td>
                                    <?= ++$key ?>
                                </td>
                                <td class="text-right"><?= ColorgbHelper::date($value->begin_date) ?></td>
                                <td class="text-right"><?= \common\colorgb\ColorgbHelper::numberFormat($value->invest_unit_qty) ?> ĐVĐT</td>
                                <td class="text-right"><?= \common\colorgb\ColorgbHelper::numberFormat($value->fund_unit_price) ?> đ</td>
                                <td class="text-right"><?= \common\colorgb\ColorgbHelper::numberFormat($subTotal1) ?> đ</td>
                                <td class="text-right <?= $subProfit > 0 ? 'text-green-600' : 'text-warning' ?>"><?= \common\colorgb\ColorgbHelper::numberFormat($subProfit) ?> đ <br>(<?= round($subProfitRate, 2) ?>%)</td>
                            </tr>
                    <?php endforeach; ?>

                    <?php $total1Current = $qty * $data['fundUnit']['last']->fund_unit_price; ?>

                    <tr>
                        <td>

                        </td>
                        <td class="text-right"><strong>Tổng cộng</strong></td>
                        <td class="text-right"><strong><?= \common\colorgb\ColorgbHelper::numberFormat($qty) ?> ĐVĐT</strong></td>
                        <td class="text-right"></td>
                        <td class="text-right"><strong><?= \common\colorgb\ColorgbHelper::numberFormat($total1) ?> đ</strong></td>
                        <td class="text-right <?= $profit > 0 ? 'text-green-600' : 'text-warning' ?>"><strong><?= \common\colorgb\ColorgbHelper::numberFormat($profit) ?> đ <br> (<?= round($profit / $total1 * 100, 2) ?>%)</strong></td>
                    </tr>

                    </tbody>

                </table>


            </div>

            <div class="panel-body">
                <div class="row invoice-payment">
                    <div class="col-xs-12 col-sm-7 col-sm-offset-5">
                        <div class="content-group">
                            <h6 class="pl-15">Thống kê chi tiết</h6>
                            <div class="table-responsive no-border">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th width="40%">Tổng số đơn vị đầu tư hiện tại của khách hàng <span class="text-regular">(3)</span></th>
                                        <td class="text-right"><h5 class="text-semibold"><?= \common\colorgb\ColorgbHelper::numberFormat($qty) ?> ĐVĐT</h5></td>
                                    </tr>
                                    <tr>
                                        <th width="40%">Giá trị 1 ĐVĐT hiện tại <span class="text-regular">(4)</span></th>
                                        <td class="text-right"><h5 class="text-semibold"><?= \common\colorgb\ColorgbHelper::numberFormat($data['fundUnit']['last']->fund_unit_price) ?> đ</h5></td>
                                    </tr>
                                    <tr>
                                        <th width="40%">Tổng số tiền hiện tại của khách hàng <span class="text-regular">(3)x(4)</span></th>
                                        <td class="text-right"><h5 class="text-semibold"><?= \common\colorgb\ColorgbHelper::numberFormat($total1Current) ?> đ</h5></td>
                                    </tr>
                                    <tr>
                                        <th width="40%">Tổng số vốn đầu tư của khách hàng</th>
                                        <td class="text-right"><h5 class="text-semibold"><?= \common\colorgb\ColorgbHelper::numberFormat($total1) ?> đ</h5></td>
                                    </tr>
                                    <tr>
                                        <th width="40%">Khách hàng có lãi tạm tính trả trước phí</th>
                                        <td class="text-right <?= $profit > 0 ? 'text-green-600' : 'text-warning' ?>"><h5 class="text-semibold"><?= \common\colorgb\ColorgbHelper::numberFormat($profit) ?> đ</h5></td>
                                    </tr>
                                    <tr>
                                        <th width="40%">Tổng % lợi nhuận đạt được</th>
                                        <td class="text-right <?= $profit > 0 ? 'text-green-600' : 'text-warning' ?>"><h5 class="text-semibold"><?= round($profit / $total1 * 100, 2) ?>%</h5></td>
                                    </tr>

                                    <!--<tr class="hide">
                                        <th width="40%">Tổng số vốn hiện tại của khách hàng</th>
                                        <td class="text-right text-violet-600"><h5 class="text-semibold"><?/*= \common\colorgb\ColorgbHelper::numberFormat($total2) */?> đ</h5></td>
                                    </tr>-->

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        <?php endif; ?>

    </div>

    <div class="panel panel-white colorgb-report colorgb-report-panel mt-20">
        <div class="panel-heading">
            <h6 class="panel-title">Báo cáo họat động (HSA)</h6>
            <div class="heading-elements">
                <?php if ($contract2): ?>
                    <a data-pjax="0" href="customer/weekly-report-hsa-download?id=<?= $model->getPrimaryKey() ?>" class="btn btn-default btn-xs heading-btn"><i class="icon-download position-left"></i> Tải về báo cáo tuần</a>
                <?php endif; ?>
            </div>
        </div>

        <div class="panel-body no-padding-bottom">
            <div class="row">
                <div class="col-xs-6 content-group">
                    <ul class="list-condensed list-unstyled">
                        <li>Lợi nhuận từ năm <?= date('Y') ?></li>
                        <li><span class="text-semibold"><?= round(($fundUnit2['last']->fund_unit_price - $fup2) / $fup2 * 100, 2) ?>%</span></li>
                    </ul>
                </div>

                <div class="col-xs-6 content-group">
                    <div class="invoice-details pull-right text-right">
                        <ul class="list-condensed list-unstyled">
                            <li>Từ ngày: <span class="colorgb-semibold"><?= $contract2[0]->begin_date ? ColorgbHelper::date($contract2[0]->begin_date) : ColorgbHelper::date($fundUnit2['first']->date) ?></span></li>
                            <li>Đến ngày: <span class="colorgb-semibold"><?= ColorgbHelper::date($fundUnit2['last']->date) ?></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="mb-20 chart-container-<?= $model->getPrimaryKey() ?>" style="min-width: 880px;">
            <h3 class="text-uppercase text-semibold text-center">Giá trị tài sản ròng/cổ phiếu - Hestia</h3>
            <div class="chart-responsive">
                <div class="chart" id="d3-line-2-<?= $model->getPrimaryKey() ?>"></div>
            </div>
        </div>

        <h5 class="text-semibold text-uppercase pl-15 mt-20 mb-20 text-blue-800">

            Khách hàng: <?= $model->name ?>

        </h5>

        <?php if (!empty($contract2)): ?>
            <div class="table-responsive">
                <table class="table table-lg">
                    <thead>
                    <tr>

                        <th width="20px">STT</th>
                        <th width="120px" class="text-right">Ngày đầu tư</th>
                        <th class="text-right">Số lượng cổ phiếu chuyển đổi (1)</th>
                        <th class="text-right">Giá chuyển đổi/cổ phiếu (2)</th>
                        <th class="text-right">Vốn đầu tư (1)x(2)</th>
                        <th class="text-right">Lợi nhuận</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($contract2 as $key => $value): ?>
                        <?php
                        $subProfitRate = ($data['fundUnitHsa']['last']->fund_unit_price - $value->fund_unit_price) / $value->fund_unit_price * 100;
                        $subTotal1 = $value->invest_value;//round($value->invest_unit_qty * $value->fund_unit_price, -5);
                        $subTotal2 = $value->invest_unit_qty * $fundUnit2['last']->fund_unit_price;
                        $subProfit = $subProfitRate * $subTotal1 / 100;
                        $qty += $value->invest_unit_qty;
                        $total1 += $subTotal1;
                        $total2 += $subTotal2;
                        $profit += $subProfit;

                        ?>
                        <tr>
                            <td>
                                <?= ++$key ?>
                            </td>
                            <td class="text-right"><?= ColorgbHelper::date($value->begin_date) ?></td>
                            <td class="text-right"><?= \common\colorgb\ColorgbHelper::numberFormat($value->invest_unit_qty) ?> CP</td>
                            <td class="text-right"><?= \common\colorgb\ColorgbHelper::numberFormat($value->fund_unit_price) ?> đ</td>
                            <td class="text-right"><?= \common\colorgb\ColorgbHelper::numberFormat($subTotal1) ?> đ</td>
                            <td class="text-right <?= $subProfit > 0 ? 'text-green-600' : 'text-warning' ?>"><?= \common\colorgb\ColorgbHelper::numberFormat($subProfit) ?> đ <br>(<?= round($subProfitRate, 2) ?>%)</td>
                        </tr>
                    <?php endforeach; ?>

                    <?php $total1Current = $qty * $data['fundUnit']['last']->fund_unit_price; ?>

                    <tr>
                        <td>

                        </td>
                        <td class="text-right"><strong>Tổng cộng</strong></td>
                        <td class="text-right"><strong><?= \common\colorgb\ColorgbHelper::numberFormat($qty) ?> CP</strong></td>
                        <td class="text-right"></td>
                        <td class="text-right"><strong><?= \common\colorgb\ColorgbHelper::numberFormat($total1) ?> đ</strong></td>
                        <td class="text-right <?= $profit > 0 ? 'text-green-600' : 'text-warning' ?>"><strong><?= \common\colorgb\ColorgbHelper::numberFormat($profit) ?> đ <br> (<?= round($profit / $total1 * 100, 2) ?>%)</strong></td>
                    </tr>

                    </tbody>

                </table>


            </div>

            <div class="panel-body">
                <div class="row invoice-payment">
                    <div class="col-xs-12 col-sm-7 col-sm-offset-5">
                        <div class="content-group">
                            <h6 class="pl-15">Thống kê chi tiết</h6>
                            <div class="table-responsive no-border">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th width="40%">Tổng số cổ phiếu dự kiến chuyển đổi hiện tại của Cổ đông <span class="text-regular">(3)</span></th>
                                        <td class="text-right"><h5 class="text-semibold"><?= \common\colorgb\ColorgbHelper::numberFormat($qty) ?> ĐVĐT</h5></td>
                                    </tr>
                                    <tr>
                                        <th width="40%">Giá trị tài sản ròng/cổ phiếu <span class="text-regular">(4)</span></th>
                                        <td class="text-right"><h5 class="text-semibold"><?= \common\colorgb\ColorgbHelper::numberFormat($fundUnit2['last']->fund_unit_price) ?> đ</h5></td>
                                    </tr>
                                    <tr>
                                        <th width="40%">Tổng số tiền hiện tại của Cổ đông <span class="text-regular">(3)x(4)</span></th>
                                        <td class="text-right"><h5 class="text-semibold"><?= \common\colorgb\ColorgbHelper::numberFormat($total1Current) ?> đ</h5></td>
                                    </tr>
                                    <tr>
                                        <th width="40%">Tổng số vốn đầu tư của Cổ đông</th>
                                        <td class="text-right"><h5 class="text-semibold"><?= \common\colorgb\ColorgbHelper::numberFormat($total1) ?> đ</h5></td>
                                    </tr>
                                    <tr>
                                        <th width="40%">Cổ đông hiện tại có lãi tạm tính trước phí</th>
                                        <td class="text-right <?= $profit > 0 ? 'text-green-600' : 'text-warning' ?>"><h5 class="text-semibold"><?= \common\colorgb\ColorgbHelper::numberFormat($profit) ?> đ</h5></td>
                                    </tr>
                                    <tr>
                                        <th width="40%">Tổng % lợi nhuận đạt được</th>
                                        <td class="text-right <?= $profit > 0 ? 'text-green-600' : 'text-warning' ?>"><h5 class="text-semibold"><?= round($profit / $total1 * 100, 2) ?>%</h5></td>
                                    </tr>

                                    <!--<tr class="hide">
                                        <th width="40%">Tổng số vốn hiện tại của khách hàng</th>
                                        <td class="text-right text-violet-600"><h5 class="text-semibold"><?/*= \common\colorgb\ColorgbHelper::numberFormat($total2) */?> đ</h5></td>
                                    </tr>-->

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        <?php endif; ?>

    </div>
</div>

<?php else: ?>
    <div class="colorgb-container">
        <div class="panel panel-white colorgb-report-panel">
            <div class="panel-heading">
                <h6 class="panel-title">Báo cáo hiệu quả đầu tư của khách hàng</h6>

            </div>

            <div class="panel-body no-padding-bottom">
                <div class="alert alert-warning">
                    Không có dữ liệu
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>