<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use common\colorgb\ColorgbHelper;
use common\models\Setting;
use common\models\Contract;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\CustomerColorgb */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Danh sách khách hàng';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .d3-axis-strong line {
        stroke: #eee !important;
    }
</style>
<?php
$columns = [
    [
        'class' => 'kartik\grid\ExpandRowColumn',
        'width' => '60px',
        'hAlign' => 'left',
        'value' => function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail' => function ($model, $key, $index, $column) use ($data) {
            return Yii::$app->controller->renderPartial('_expand', ['model' => $model, 'data' => $data]);
        },
        'headerOptions' => ['class' => 'kartik-sheet-style'],
        'expandOneOnly' => true,
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '50px', 'header' => 'Xem',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                if (ColorgbHelper::checkPermission('CUSTOMER_VIEW')) $html .= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->getPrimaryKey()], ['title' => 'Xem', 'data-pjax' => '0']);
                return $html;
            },
        ]
    ],
    [
        'attribute' => 'image',
        'format' => 'raw',
        'hAlign' => 'center',
        'width' => '110px',
        'value' => function ($model, $key, $index, $widget) {
            $src = $model->image ? ColorgbHelper::dataSrc($model->image) : Yii::$app->params['cdnUrl'] . '/bolt/assets/images/placeholder.jpg';
            return Html::img($src, ['width' => 60]);
        },
        'filter' => false,
        'mergeHeader' => true,
    ],
    [
        'attribute' => 'name',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) {
            $type = '';
            $customerType = ArrayHelper::map(Yii::$app->params['customerType'], 'key', 'value');
            if ($model->type == $customerType['old']) $type = '<span class="small label bg-grey">Khách hàng cũ</span>';
            if ($model->type == $customerType['new']) $type = '<span class="small label bg-green">Khách hàng mới</span>';
            $html = '<strong class="display-block"><a data-pjax="0" title="Xem các hợp đồng của khách hàng" href="contract?ContractColorgb%5Bcontract_date%5D=&ContractColorgb%5Bcustomer_id%5D=' . $model->getPrimaryKey() . '&ContractColorgb%5Bcontract_info%5D=&ContractColorgb%5Bstatus%5D=">' . $model->name . '</a></strong>';
            $html .= '<p>' . $model->mobile . '</p>';
            $html .= '<p>' . $model->email_addr . '</p>';
            $html .= '<p>' . $model->tax_code . '</p>';
            $html .= '<p>' . $type . '</p>';
            return $html;
        },
    ],
    /*[
        'attribute' => 'customer_group_id',
        'width' => '150px',
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $array = $data['customerGroup'];
            $value = !empty($array[$model->customer_group_id]) ? $array[$model->customer_group_id] : '';
            return $value;
        },
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['customerGroup'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Bộ lọc...'],
    ],*/
    [
        'attribute' => 'contract_info',
        'format' => 'raw',
        'width' => '170px',
        'value' => function ($model, $key, $index, $widget) {
            $html = '<span class="badge bg-success" title="Tổng số hợp đồng đang hiệu lực">' . $model->contract_active . '</span>';
            $html .= '<span class="badge bg-warning" title="Tổng số hợp đồng sắp hết hạn">' . $model->contract_expire . '</span>';
            $html .= '<span class="badge bg-grey" title="Tổng số hợp đồng chưa hiệu lực">' . $model->contract_inactive . '</span>';
            $html .= '<span class="badge bg-info" title="Tổng số hợp đồng đã tất toán">' . $model->contract_settled . '</span>';
            $html .= '<span class="badge bg-danger" title="Tổng số hợp đồng đã xóa">' . $model->contract_deleted . '</span>';
            return $html;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['contractStatusTxt'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Bộ lọc...'],
    ],
    [
        'attribute' => 'status',
        'width' => '130px',
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $array = $data['customerStatusHtml'];
            $value = !empty($array[$model->status]) ? $array[$model->status] : '';
            return $value;
        },
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['customerStatusTxt'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Bộ lọc...'],
    ],
    [
        'class' => '\kartik\grid\ActionColumn',
        'width' => '230px',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) use ($data) {
                $html = '';
                if (ColorgbHelper::checkPermission('CUSTOMER_UPDATE')) $html .= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->getPrimaryKey()], ['title' => 'Sửa', 'data-pjax' => '0']);
                if (ColorgbHelper::checkPermission('CUSTOMER_ACTIVATE')) $html .= Html::a('<span class="glyphicon glyphicon-ok"></span>', ['activate', 'id' => $model->getPrimaryKey()], ['disabled' => $model->status != 2 ? 'false' : 'disabled', 'title' => 'Kích hoạt khách hàng ' . $model->name, 'class' => 'btn-colorgb-jg', 'data-pjax' => '0']);
                if (ColorgbHelper::checkPermission('CUSTOMER_SENDNOTICE')) $html .= Html::a('<span class="glyphicon glyphicon-send"></span>', ['send-notice', 'action' => 'weekly_report', 'id' => $model->getPrimaryKey()], ['disabled' => $model->contract_active != 0 ? 'false' : 'disabled', 'title' => 'Gửi báo cáo tuần đến ' . $model->email_addr, 'class' => 'btn-colorgb-jg', 'data-pjax' => '0']);
                if (ColorgbHelper::checkPermission('CUSTOMER_RESET')) $html .= Html::a('<span class="icon-unlocked2"></span>', ['reset-password', 'id' => $model->getPrimaryKey()], ['disabled' => $model->status >= $data['customerStatus']['active'] ? 'false' : 'disabled', 'title' => 'Thiết lập lại mật khẩu gửi đến ' . $model->email_addr, 'class' => 'btn-colorgb-jg', 'data-pjax' => '0']);
                if (ColorgbHelper::checkPermission('CUSTOMER_DELETE')) $html .= Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->getPrimaryKey()], ['disabled' => $model->status >= $data['customerStatus']['active'] ? 'disabled' : 'false', 'title' => 'Xóa', 'data-pjax' => 'false', 'class' => 'colorgb-pjax-action-del colorgb-pjax-action-del-show', 'data-pjax-container' => 'colorgb-pjax-pjax']);
                return $html;
            },
        ]
    ]
];
if (Yii::$app->user->identity->is_admin != -1) {
    $columns[] = [
        'attribute' => 'cared_user_id',
        'width' => '160px',
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $array = $data['userCaredArray'];
            $value = !empty($array[$model->cared_user_id]) ? $array[$model->cared_user_id] : '';
            return $value;
        },
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['userCaredArray'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Bộ lọc...'],
    ];
}
$before = ColorgbHelper::checkPermission('CUSTOMER_GROUP') ? Html::a('<span class="fa fa-info-circle"></span> Theo dõi tài sản nhóm KH', ['/customer/group'], ['data-pjax' => 0, 'class' => 'btn btn-default pull-left btn-xs ']) : '';
$before .= ColorgbHelper::checkPermission('CUSTOMER_SENDNOTICE') ? Html::a('<span class="glyphicon glyphicon-send"></span> <span>Gửi lại báo cáo tuần đến tất cả KH</span>', ['send-notice?action=weekly_report&type=all'], ['title' => 'Gửi lại báo cáo tuần đến tất cả', 'data-pjax' => 0, 'class' => 'btn btn-xs btn-colorgb-report-all btn-default pull-left']) : '';
//$before .= ColorgbHelper::checkPermission('CUSTOMER_SENDNOTICE') ? Html::a('<span class="glyphicon glyphicon-send"></span> <span>Gửi báo cáo tuần đến KH chưa được gửi</span>', ['send-notice?action=weekly_report&type=pending'], ['title' => 'Gửi báo cáo tuần đến KH chưa được gửi', 'data-pjax' => 0, 'class' => 'btn btn-xs btn-colorgb-report-pending btn-default pull-left']) : '';
$before .= ColorgbHelper::checkPermission('CUSTOMER_WARNING') ? Html::a('<span class="glyphicon glyphicon-send"></span> <span>Gửi thông báo HĐ sắp hết hạn</span>', ['send-notice?action=contract_renewal'], ['title' => 'Gửi thông báo HĐ sắp hết hạn', 'data-pjax' => 0, 'class' => 'btn btn-xs btn-colorgb-warning btn-default pull-left']) : '';
$before .= ColorgbHelper::checkPermission('CUSTOMER_CREATE') ? Html::a('<span class="glyphicon glyphicon-plus"></span>', ['create'], ['data-pjax' => 0, 'class' => 'btn btn-success pull-left', 'title' => 'Thêm mới']) : '';
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'colorgb-pjax',
    'showPageSummary' => false,
    'pjax' => true,
    'striped' => true,
    'responsive' => true,
    'responsiveWrap' => true,
    'hover' => true,
    'floatHeader' => true,
    'floatHeaderOptions' => ['top' => 46],
    'toolbar' => [
        'before' => $before,
        'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''], ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Làm mới']),
        '{toggleData}',
    ],
    'panel' => ['type' => 'default', 'heading' => $this->title],
    'columns' => $columns,
]);
?>
<script>
    function colorgbAjaxReady() {
        var bcra = $('.btn-colorgb-report-all');
        var bcrp = $('.btn-colorgb-report-pending');
        var bcw = $('.btn-colorgb-warning');
        $.ajax({
            url: baseUrl + '/ajax/customer-contract-count',
            success: function (result) {
                var json = $.parseJSON(result);
                bcra.attr('title', bcra.attr('title') + ' (' + json.customerContractActiveAll + ' khách hàng)');
                bcrp.attr('title', bcrp.attr('title') + ' (' + json.customerContractActivePending + ' khách hàng)');
                bcw.attr('title', bcw.attr('title') + ' (' + json.customerContractActiveExpire + ' khách hàng)');
            }
        });
        $('#colorgb-pjax-filters input[name="CustomerColorgb[name]"]').attr('title','Tìm kiếm theo Tên, SĐT, Email. Lọc theo loại KH nhập 1 trong 2 từ khóa "KHÁCH HÀNG CŨ/KHÁCH HÀNG MỚI" (VD: KHÁCH HÀNG CŨ) ');
    }
    //$(document).on('pjax:complete', colorgbAjaxReady);
    //$(document).ready(colorgbAjaxReady);
</script>