<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FundUnit */

$this->title = 'Cập nhật mã: #' . $model->getPrimaryKey();
$this->params['breadcrumbs'][] = ['label' => 'Đơn vị đầu tư - PI', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getPrimaryKey(), 'url' => ['view', 'id' => $model->getPrimaryKey()]];
$this->params['breadcrumbs'][] = 'Cập nhật';
?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $this->title ?></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <?php if (Yii::$app->session->getFlash('success')): ?>
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
                <?= Yii::$app->session->getFlash('success'); ?>
            </div>
            <script>
                $(function () {
                    $.ajax({
                        url: baseUrl+'/ajax/set-data-report', success: function (response) {
                        }
                    });
                })
            </script>
        <?php endif; ?>

        <?php if (Yii::$app->session->getFlash('error')): ?>
            <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
                <?= Yii::$app->session->getFlash('error'); ?>
            </div>
        <?php endif; ?>

        <div class="colorgb-form">

            <?= $this->render('_form', [
                'model' => $model,
                'data' => $data,
            ]) ?>

        </div>

    </div>
</div>
