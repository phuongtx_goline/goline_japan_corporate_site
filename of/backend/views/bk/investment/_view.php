<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use common\models\User;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\FundUnit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="colorgb-container">
    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4 text-right',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-7',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <div class="row">
        <div class="col-sm-12">

            <?= $form->field($model, 'fund_net_asset', [
                'template' => '{label}<div class="col-sm-7"><div class="input-group">{input}<span class="input-group-addon">đ</span> </div></div>',
            ])->textInput([
                'class' => 'price form-control'
            ]) ?>

            <?= $form->field($model, 'fund_unit_qty')->textInput([
                'class' => 'price form-control'
            ]) ?>

            <?= $form->field($model, 'fund_unit_price', [
                'template' => '{label}<div class="col-sm-7"><div class="input-group">{input}<span class="input-group-addon">đ</span> </div></div>',
            ])->textInput([
                'class' => 'price form-control'
            ]) ?>

            <?= $form->field($model, 'file_upload[]')->widget(FileInput::classname(), [
                'pluginOptions' => $data['fileInput'],
                'options' => ['multiple' => true]
            ]);
            ?>
            <?= $form->field($model, 'date')->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'displayFormat' => 'dd/MM/yyyy',
                'saveFormat' => 'yyyyMMdd',
                'ajaxConversion' => false,
                'autoWidget' => true,
                'disabled' => 'disabled'
            ])
            ?>

            <?= $form->field($model, 'remarks')->textarea() ?>

            <?= $form->field($model, 'status')->dropDownList(ArrayHelper::map(Yii::$app->params['recordStatus'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

        </div>
    </div>


    <hr class="mt-15">

    <div class="row pt-10">

        <div class="col-sm-6">

            <?= $form->field($model, 'reg_date_time')->textInput(['value' => date('d/m/Y H:i:s', strtotime($model->reg_date_time))]) ?>

            <?= $form->field($model, 'reg_user_id')->textInput(['value' => User::findOne($model->reg_user_id)->name]) ?>

            <?= $form->field($model, 'reg_ip_addr')->textInput(['maxlength' => true]) ?>

        </div>

        <div class="col-sm-6">

            <?= $form->field($model, 'upd_date_time')->textInput(['value' => date('d/m/Y H:i:s', strtotime($model->upd_date_time))]) ?>

            <?= $form->field($model, 'upd_user_id')->textInput(['value' => User::findOne($model->upd_user_id)->name]) ?>

            <?= $form->field($model, 'upd_ip_addr')->textInput(['maxlength' => true]) ?>

        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
<style>
    .input-group.file-caption-main{
        display: none !important;
    }
</style>