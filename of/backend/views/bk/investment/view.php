<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\FundUnit */

$this->title = $model->getPrimaryKey();
$this->params['breadcrumbs'][] = ['label' => 'Đơn vị đầu tư - PI', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">#<?= $this->title ?></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="colorgb-form">

            <?= $this->render('_view', [
                'model' => $model,
                'data' => $data,
            ]) ?>

        </div>
    </div>
</div>
<script>
    $(function () {
        $('.form-control').each(function () {
            $(this).attr('disabled','disabled');
        });
    });
</script>