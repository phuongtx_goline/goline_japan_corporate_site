<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\FundUnit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="colorgb-container">
    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4 text-right',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-6',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?php if ($model->isNewRecord): ?>
        <?= $form->field($model, 'date')->widget(DateControl::classname(), [
            'type' => DateControl::FORMAT_DATE,
            'displayFormat' => 'dd/MM/yyyy',
            'saveFormat' => 'yyyyMMdd',
            'ajaxConversion' => false,
            'autoWidget' => true,
        ])
        ?>
    <?php else: ?>
        <?= $form->field($model, 'date')->widget(DateControl::classname(), [
            'type' => DateControl::FORMAT_DATE,
            'displayFormat' => 'dd/MM/yyyy',
            'saveFormat' => 'yyyyMMdd',
            'ajaxConversion' => false,
            'autoWidget' => true,
            'disabled' => 'disabled'
        ])
        ?>
    <?php endif; ?>


    <?= $form->field($model, 'fund_net_asset', [
        'template' => '{label}<div class="col-sm-6"><div class="input-group">{input}<span class="input-group-addon">đ</span> </div></div>',
    ])->textInput([
        'class' => 'form-control price'
    ]) ?>

    <?= $form->field($model, 'fund_unit_qty')->textInput([
        'class' => 'price form-control disabled'
    ]) ?>

    <?= $form->field($model, 'fund_unit_price', [
        'template' => '{label}<div class="col-sm-6"><div class="input-group">{input}<span class="input-group-addon">đ</span> </div></div>',
    ])->textInput([
        'class' => 'price form-control disabled'
    ]) ?>

    <?= $form->field($model, 'file_upload[]')->widget(FileInput::classname(), [
        'pluginOptions' => $data['fileInput'],
        'options' => ['multiple' => true]
    ]);
    ?>

    <?= $form->field($model, 'remarks')->textarea() ?>

    <?= $form->field($model, 'status')->dropDownList(ArrayHelper::map(Yii::$app->params['recordStatus'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>
    <div class="hide">
        <?= $form->field($model, 'upd_date_time')->textInput() ?>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-4 col-sm-5">
            <?= Html::submitButton($model->isNewRecord ? '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Tạo mới</span>' : '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Lưu lại</span>', ['class' => $model->isNewRecord ? 'btn btn-success  btn-ladda btn-ladda-spinner btn-ladda-progress' : 'btn btn-primary  btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $(function () {

        $('#fundunit-date').change(function () {

            var date = $(this).val();
            $.ajax({
                url: baseUrl+'/ajax/fund-unit-qty?date='+date, success: function (response) {
                    $('#fundunit-fund_unit_qty').val(response);
                }
            });

        });

        /*$('#fundunit-date').datepicker()
            .on('changeDate', function(ev){
                $('#fundunit-date').datepicker('hide');
            });*/
    })
</script>