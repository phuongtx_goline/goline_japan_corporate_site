<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\colorgb\ColorgbHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\InvestmentColorgb */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Danh sách đơn vị đầu tư - PI';
$this->params['breadcrumbs'][] = $this->title;
$columns = [
    ['class' => 'kartik\grid\SerialColumn'],
    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '50px',        'header' => 'Xem',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                if (ColorgbHelper::checkPermission('INVESTMENT_VIEW')) $html .= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->getPrimaryKey()], ['title' => 'Xem', 'data-pjax' => '0']);
                return $html;
            },
        ]
    ],
    [
        'attribute' => 'colorgb_date',
        'width' => '220px',
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'locale' => [
                    'format' => 'YYYY-MM-DD',
                    'separator' => ' - ',
                ],
                'ranges' => [
                    //"Hôm nay" => ["moment().startOf('day')", "moment()"],
                    "Hôm qua" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                    "Tuần trước" => ["moment().startOf('day').subtract(7, 'days')", "moment()"],
                    "Tháng trước" => ["moment().startOf('day').subtract(31, 'days')", "moment()"],
                ],
                'alwaysShowCalendars' => true,

            ],
        ],
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) {
            $date = ColorgbHelper::date($model->getPrimaryKey());
            $html = ' <span>' . $date . '</span> ';
            return $html;
        },
    ],
    [
        'attribute' => 'fund_net_asset',
        'format' => 'raw',
        'width' => '200px',
        'value' => function ($model, $key, $index, $widget) {
            $html = \common\colorgb\ColorgbHelper::numberFormat($model->fund_net_asset) . ' đ';
            return $html;
        },

    ],
    [
        'attribute' => 'fund_unit_qty',
        'format' => 'raw',
        'width' => '200px',
        'value' => function ($model, $key, $index, $widget) {
            $html = \common\colorgb\ColorgbHelper::numberFormat($model->fund_unit_qty);
            return $html;
        },

    ], [
        'attribute' => 'fund_unit_price',
        'format' => 'raw',
        'width' => '200px',
        'value' => function ($model, $key, $index, $widget) {
            $html = \common\colorgb\ColorgbHelper::numberFormat($model->fund_unit_price) . ' đ';
            return $html;
        },

    ],
    [
        'attribute' => 'status',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $array = $data['recordStatusHtml'];
            $value = !empty($array[$model->status]) ? $array[$model->status] : '';
            return $value;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['recordStatusTxt'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Bộ lọc...'],
        'width' => '180px',
    ],
    // 'reg_date_time',
    // 'reg_user_id',
    // 'reg_ip_addr',
    // 'upd_date_time',
    // 'upd_user_id',
    // 'upd_ip_addr',
    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '150px',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                if (ColorgbHelper::checkPermission('INVESTMENT_UPDATE')) $html .= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->getPrimaryKey()], ['title' => 'Sửa', 'data-pjax' => '0']);
                if (ColorgbHelper::checkPermission('INVESTMENT_DELETE')) $html .= Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->getPrimaryKey()], ['title' => 'Xóa', 'data-pjax' => 'false', 'class' => 'colorgb-pjax-action-del colorgb-pjax-action-del-show', 'data-pjax-container' => 'colorgb-pjax-pjax']);
                return $html;
            },
        ],
    ],
];

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'colorgb-pjax',
    'showPageSummary' => false,
    'pjax' => true,
    'striped' => true,
    'responsive' => true,
    'responsiveWrap' => true,
    'hover' => true,
    'floatHeader' => true,
    'floatHeaderOptions' => ['top' => 46],
    'toolbar' => [
        'before' => ColorgbHelper::checkPermission('INVESTMENT_CREATE') ? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'class' => 'btn btn-success pull-left', 'title' => 'Thêm mới']) :'',
        'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''], ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Làm mới']),
        '{toggleData}',
    ],
    'panel' => ['type' => 'default', 'heading' => $this->title],
    'columns' => $columns,
]);
?>
