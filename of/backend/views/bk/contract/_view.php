<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use common\models\Customer;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\Contract */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="colorgb-container">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4 text-right',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-7',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <div class="row">
        <div class="col-sm-6">

            <?= $form->field($model, 'customer_id')->dropDownList(
                [$model->customer_id => $model->customer_id ? Customer::findOne($model->customer_id)->name : 'Chọn...'],
                ['class' => 'colorgb-customer-search disabled', 'disabled'=>'disabled' ],
                ['options' => [$model->customer_id => ['selected' => true]]]
            ) ?>

            <?= $form->field($model, 'invest_value', [
                'template' => '{label}<div class="col-sm-7"><div class="input-group">{input}<span class="input-group-addon">đ</span> </div></div>',
            ])->textInput([
                'class' => 'price form-control',


            ]) ?>

            <?= $form->field($model, 'invest_unit_qty')->textInput([
                'class' => 'price form-control',

            ]) ?>

            <?= $form->field($model, 'contract_type_id')->dropDownList($data['contractType'], ['options' => $data['contractTypeOption'], 'data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...', ]) ?>

            <?= $form->field($model, 'annouce_status')->dropDownList(ArrayHelper::map(Yii::$app->params['annouceStatus'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...', ]) ?>

            <?= $form->field($model, 'response_status')->dropDownList(ArrayHelper::map(Yii::$app->params['responseStatus'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...', ]) ?>

            <?= $form->field($model, 'response_content')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'status')->dropDownList(ArrayHelper::map(Yii::$app->params['contractStatus'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>


        </div>
        <div class="col-sm-6">

            <?= $form->field($model, 'fund_net_asset', [
                'template' => '{label}<div class="col-sm-7"><div class="input-group">{input}<span class="input-group-addon">đ</span> </div></div>',
            ])->textInput([
                'class' => 'price form-control',
                
            ]) ?>

            <?= $form->field($model, 'fund_unit_qty')->textInput([
                'class' => 'price form-control',
                
            ]) ?>

            <?= $form->field($model, 'fund_unit_price', [
                'template' => '{label}<div class="col-sm-7"><div class="input-group">{input}<span class="input-group-addon">đ</span> </div></div>',
            ])->textInput([
                'class' => 'price form-control',
                
            ]) ?>

            <?= $form->field($model, 'contract_date')->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'displayFormat' => 'dd/MM/yyyy',
                'saveFormat' => 'yyyyMMdd',
                'ajaxConversion' => false,
                'autoWidget' => true,
            ])
            ?>

            <?= $form->field($model, 'begin_date')->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'displayFormat' => 'dd/MM/yyyy',
                'saveFormat' => 'yyyyMMdd',
                'ajaxConversion' => false,
                'autoWidget' => true,
            ])
            ?>

            <?= $form->field($model, 'end_date_est')->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'displayFormat' => 'dd/MM/yyyy',
                'saveFormat' => 'yyyyMMdd',
                'ajaxConversion' => false,
                'autoWidget' => true,
                
            ])
            ?>

            <?= $form->field($model, 'representative')->dropDownList(ArrayHelper::map(Yii::$app->params['representative'], 'txt', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...',]) ?>

            <?= $form->field($model, 'remarks')->textarea() ?>


        </div>
    </div>


    <hr class="mt-15">

    <div class="row pt-10">

        <div class="col-sm-6">

            <?= $form->field($model, 'active_date_time')->textInput(['value' => date('d/m/Y H:i:s', strtotime($model->active_date_time))]) ?>

            <?= $form->field($model, 'active_user_id')->textInput(['value' => User::findOne($model->active_user_id)->name]) ?>

        </div>

        <div class="col-sm-6">

            <?= $form->field($model, 'settle_date_time')->textInput(['value' => $model->settle_date_time ? date('d/m/Y H:i:s', strtotime($model->settle_date_time)) :'']) ?>

            <?= $form->field($model, 'settle_user_id')->textInput(['value' => User::findOne($model->settle_user_id)->name]) ?>


        </div>

    </div>

    <div class="row pt-10">

        <div class="col-sm-6">

            <?= $form->field($model, 'reg_date_time')->textInput(['value' => date('d/m/Y H:i:s', strtotime($model->reg_date_time))]) ?>

            <?= $form->field($model, 'reg_user_id')->textInput(['value' => User::findOne($model->reg_user_id)->name]) ?>

            <?= $form->field($model, 'reg_ip_addr')->textInput(['maxlength' => true]) ?>

        </div>

        <div class="col-sm-6">

            <?= $form->field($model, 'upd_date_time')->textInput(['value' => date('d/m/Y H:i:s', strtotime($model->upd_date_time))]) ?>

            <?= $form->field($model, 'upd_user_id')->textInput(['value' => User::findOne($model->upd_user_id)->name]) ?>

            <?= $form->field($model, 'upd_ip_addr')->textInput(['maxlength' => true]) ?>

        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>

<?= $this->render('_extend_hist', [
    'model' => $model,
    'data' => $data,
]) ?>
