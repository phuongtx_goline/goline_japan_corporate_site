<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use common\models\ContractExtendHist;
use common\models\User;
use common\colorgb\ColorgbHelper;

$contractExtendHist = ContractExtendHist::findAll(['contract_id' => $model->getPrimaryKey()]);
/* @var $this yii\web\View */
/* @var $model common\models\Contract */
/* @var $form yii\widgets\ActiveForm */
?>
<?php if ($contractExtendHist): ?>
<div class="colorgb-container">
    <hr>
    <h4 class="pl-15 text-green-600">Lịch sử gia hạn hợp đồng</h4>
    <div class="table-responsive">
        <table class="table table-lg">
            <thead>
            <tr>

                <th width="20px">STT</th>

                <th width="150px" class="text-right">Ngày hết hạn cũ</th>
                <th width="150px" class="text-right">Ngày hết hạn mới</th>
                <th class="text-right">Thời gian tạo</th>
                <th class="text-right">Người tạo</th>
                <th class="text-right">IP tạo</th>
                <th class="text-right">Ghi chú</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($contractExtendHist as $key => $value): ?>

                <tr>
                    <td>
                        <?= ++$key ?>
                    </td>
                    <td class="text-right"><?= ColorgbHelper::date($value->old_end_date) ?></td>
                    <td class="text-right"><?= ColorgbHelper::date($value->new_end_date) ?></td>
                    <td class="text-right"><?= date('d/m/Y H:i:s', strtotime($value->reg_date_time)) ?></td>
                    <td class="text-right"><?= User::findOne($value->reg_user_id)->name ?></td>
                    <td class="text-right"><?= $value->reg_ip_addr ?></td>
                    <td class="text-right"><?= $value->remarks ?></td>
                </tr>
            <?php endforeach; ?>

            </tbody>

        </table>


    </div>
</div>
<?php endif; ?>
