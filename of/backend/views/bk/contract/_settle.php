<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use common\colorgb\ColorgbHelper;
use common\models\Customer;
use common\models\Setting;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="colorgb-container <?= $data['liquidateFormEdit'] == 1 ? 'liquidate-form-edit' : '' ?>">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4 text-right',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-6',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'customer_id')->dropDownList(
                [$model->customer_id => $model->customer_id ? Customer::findOne($model->customer_id)->name : 'Chọn...'],
                ['class' => 'colorgb-customer-search', 'disabled' => 'disabled'],
                ['options' => [$model->customer_id => ['selected' => true]]]
            ) ?>
            <?= $form->field($model, 'invest_value', [
                'template' => '{label}<div class="col-sm-6"><div class="input-group">{input}<span class="input-group-addon">đ</span> </div></div>',
            ])->textInput([
                'class' => 'price form-control',
                'disabled' => 'disabled',
            ]) ?>
            <?= $form->field($model, 'invest_unit_qty')->textInput([
                'class' => 'price form-control',
                'disabled' => 'disabled'
            ]) ?>
            <?= $form->field($model, 'contract_type_id')->dropDownList($data['contractType'], ['options' => $data['contractTypeOption'], 'data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...', 'disabled' => 'disabled']) ?>
            <?= $form->field($model, 'period_type')->dropDownList(ArrayHelper::map(Yii::$app->params['periodType'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...', 'disabled' => 'disabled']) ?>
            <?= $form->field($model, 'period_val')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>
            <?= $form->field($model, 'response_status')->dropDownList(ArrayHelper::map(Yii::$app->params['responseStatus'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...', 'disabled' => 'disabled']) ?>
            <?= $form->field($model, 'response_content')->textarea(['disabled' => 'disabled']) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'fund_net_asset', [
                'template' => '{label}<div class="col-sm-6"><div class="input-group">{input}<span class="input-group-addon">đ</span> </div></div>',
            ])->textInput([
                'class' => 'price form-control',
                'disabled' => 'disabled'
            ]) ?>
            <?= $form->field($model, 'fund_unit_qty')->textInput([
                'class' => 'price form-control',
                'disabled' => 'disabled'
            ]) ?>
            <?= $form->field($model, 'fund_unit_price', [
                'template' => '{label}<div class="col-sm-6"><div class="input-group">{input}<span class="input-group-addon">đ</span> </div></div>',
            ])->textInput([
                'class' => 'price form-control',
                'disabled' => 'disabled'
            ]) ?>
            <?= $form->field($model, 'base_profit_rate', [
                'template' => '{label}<div class="col-sm-6"><div class="input-group">{input}<span class="input-group-addon">%</span> </div></div>',
            ])->textInput([
                'class' => 'form-control',
                'disabled' => 'disabled'
            ]) ?>
            <?= $form->field($model, 'profit_rate', [
                'template' => '{label}<div class="col-sm-6"><div class="input-group">{input}<span class="input-group-addon">%</span> </div></div>',
            ])->textInput([
                'class' => 'form-control',
                'disabled' => 'disabled'
            ]) ?>
            <?= $form->field($model, 'begin_date')->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'displayFormat' => 'dd/MM/yyyy',
                'saveFormat' => 'yyyyMMdd',
                'ajaxConversion' => false,
                'autoWidget' => true,
                'disabled' => 'disabled'
            ])
            ?>
            <?= $form->field($model, 'end_date_est')->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'displayFormat' => 'dd/MM/yyyy',
                'saveFormat' => 'yyyyMMdd',
                'ajaxConversion' => false,
                'autoWidget' => true,
                'disabled' => 'disabled'
            ])
            ?>
            <?= $form->field($model, 'tax_rate', [
                'template' => '{label}<div class="col-sm-6"><div class="input-group">{input}<span class="input-group-addon">%</span> </div></div>',
            ])->textInput([
                'class' => 'form-control',
                'disabled' => 'disabled'
            ]) ?>
            <?= $form->field($model, 'representative')->dropDownList(ArrayHelper::map(Yii::$app->params['representative'], 'txt', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

        </div>
    </div>
    <hr>
    <h4 class="pl-15 text-blue">Doanh thu kinh doanh <span class="small text-pink">(giá của 1 ĐVĐT hiện tại là <?= \common\colorgb\ColorgbHelper::numberFormat($data['fundUnit']['last']->fund_unit_price) ?> đ)<input type="hidden" id="fund_unit_price" value="<?= $data['fundUnit']['last']->fund_unit_price ?>"></span></h4>
    <div class="table-responsive table-8-10">
        <table class="table table-lg">
            <thead>
            <tr>

                <th class="text-right">Vốn hợp tác (1)</th>
                <th class="text-right">Ngày bắt đầu</th>
                <th class="text-right">Ngày kết thúc</th>
                <th class="text-right">Số ngày hợp tác</th>
                <th class="text-right">Tỷ suất doanh thu kinh doanh cơ sở (2)</th>
                <th class="text-right">Doanh thu kinh doanh cơ sở (3)=(1)*(2)</th>
                <th class="text-right">Tổng giá trị khi kết thúc hợp đồng (4)</th>
                <th class="text-right">Doanh thu kinh doanh thực tế (5)=(4)-(1)</th>
            </tr>
            </thead>
            <tbody>

            <tr>
                <td class="text-right" id="attr_1">
                    <span><?= \common\colorgb\ColorgbHelper::numberFormat($model->invest_value) ?></span>
                    <input type="hidden" value="<?= $model->invest_value ?>" name="ColorgbForm[attr_1]">
                </td>
                <td class="text-right" id="begin_date">
                    <span><?= ColorgbHelper::date($model->begin_date) ?></span>
                    <input type="hidden" value="<?= $model->begin_date ?>" name="ColorgbForm[begin_date]">
                </td>
                <td class="text-right" id="end_date_real">
                    <span>-</span>
                    <input type="hidden" name="ColorgbForm[end_date_real]">
                </td>
                <td class="text-right" id="contract_days">
                    <span>-</span>
                    <input type="hidden" name="ColorgbForm[contract_days]">
                </td>
                <td class="text-right" id="attr_2">
                    <span>-</span>
                    <input type="hidden" name="ColorgbForm[attr_2]">
                </td>
                <td class="text-right" id="attr_3">
                    <span>-</span>
                    <input type="hidden" name="ColorgbForm[attr_3]">
                </td>
                <td class="text-right" id="attr_4">
                    <span>-</span>
                    <input type="hidden" name="ColorgbForm[attr_4]">
                </td>
                <td class="text-right" id="attr_5">
                    <span>-</span>
                    <input type="hidden" name="ColorgbForm[attr_5]">
                </td>
            </tr>

            </tbody>

        </table>

    </div>
    <hr>
    <h4 class="pl-15 text-blue">Doanh thu kinh doanh BÊN A thu về</h4>
    <div class="table-responsive table-8-10">
        <table class="table table-lg">
            <thead>
            <tr>
                <th class="text-right">Doanh thu kinh doanh BÊN A thu về trước khi trừ phí phạt (6)</th>
                <th class="text-right">Phí phạt rút trước hạn (7)</th>
                <th class="text-right">Tiền phí phạt rút trước hạn (8)=(1)*(7)</th>
                <th class="text-right">Phần bù lỗ BÊN A thu về (9)</th>
                <th class="text-right">Doanh thu kinh doanh BÊN A tính thuế TNCN (10)=(6)-(8)+(9)</th>
                <th class="text-right">Thuế TNCN BÊN A phải nộp cho nhà nước (11)=(10)*<?= $model->tax_rate ?>% <input type="hidden" id="tax_rate" value="<?= $model->tax_rate ?>"></th>
                <th class="text-right">Doanh thu BÊN A thu về sau khi trừ phí và thuế TNCN (12)=(10)-(11)</th>
                <th class="text-right">Tổng BÊN A thu về (gốc + lãi) (13)=(1)+(12)</th>
            </tr>
            </thead>
            <tbody>

            <tr>
                <td class="text-right" id="attr_6">
                    <span>-</span>
                    <input type="hidden" name="ColorgbForm[attr_6]">
                </td>
                <td class="text-right" id="attr_7">
                    <span>-</span>
                    <input type="hidden" name="ColorgbForm[attr_7]">
                </td>
                <td class="text-right" id="attr_8">
                    <span>-</span>
                    <input type="hidden" name="ColorgbForm[attr_8]">
                </td>
                <td class="text-right" id="attr_9">
                    <span>-</span>
                    <input type="hidden" name="ColorgbForm[attr_9]">
                </td>
                <td class="text-right" id="attr_10">
                    <span>-</span>
                    <input type="hidden" name="ColorgbForm[attr_10]">
                </td>
                <td class="text-right" id="attr_11">
                    <span>-</span>
                    <input type="hidden" name="ColorgbForm[attr_11]">
                </td>
                <td class="text-right" id="attr_12">
                    <span>-</span>
                    <input type="hidden" name="ColorgbForm[attr_12]">
                </td>
                <td class="text-right" id="attr_13">
                    <span>-</span>
                    <input type="hidden" name="ColorgbForm[attr_13]">
                </td>
            </tr>

            </tbody>

        </table>

    </div>
    <hr>
    <h4 class="pl-15 text-blue">Doanh thu kinh doanh BÊN B thu về</h4>
    <div class="table-responsive table-4-10">
        <table class="table table-lg">
            <thead>
            <tr>
                <th class="text-right">Doanh thu kinh doanh BÊN B thu về (14)</th>
                <th class="text-right">Phí phạt BÊN B thu về từ BÊN A (15)</th>
                <th class="text-right">Phần bù lỗ trả cho BÊN A (16)</th>
                <th class="text-right">Tổng tiền BÊN B thu về (17) = (14) + (15) - (16)</th>
            </tr>
            </thead>
            <tbody>

            <tr>
                <td class="text-right" id="attr_14">
                    <span>-</span>
                    <input type="hidden" name="ColorgbForm[attr_14]">
                </td>
                <td class="text-right" id="attr_15">
                    <span>-</span>
                    <input type="hidden" name="ColorgbForm[attr_15]">
                </td>
                <td class="text-right" id="attr_16">
                    <span>-</span>
                    <input type="hidden" name="ColorgbForm[attr_16]">
                </td>
                <td class="text-right" id="attr_17">
                    <span>-</span>
                    <input type="hidden" name="ColorgbForm[attr_17]">
                </td>
            </tr>

            </tbody>

        </table>

    </div>
    <hr>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'end_date_real')->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'displayFormat' => 'dd/MM/yyyy',
                'saveFormat' => 'yyyyMMdd',
                'ajaxConversion' => false,
                'autoWidget' => true,
                'disabled' => Yii::$app->user->identity->is_admin == 1 && $model->status == $data['contractStatus']['active'] ? false : 'disabled', //$model->status > $data['contractStatus']['active'] ? 'inactive' : ''
            ])
            ?>
            <?= $form->field($model, 'remarks')->textarea(['disabled' => $model->status >= $data['contractStatus']['settled'] ? 'disabled' : false]) ?>

        </div>
        <div class="col-sm-6">

            <div class="table-responsive">
                <table class="table table-lg">
                    <thead>
                    <tr>
                        <th class="text-right col-xs-4">Từ thời hạn</th>
                        <th class="text-right col-xs-4">Đến thời hạn</th>
                        <th class="text-right col-xs-4">Phí rút trước hạn</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($data['contractSettleFee'] as $key => $value): ?>
                        <tr class="settle-fee-row" data-from="<?= $value->from_period_val * 30 ?>" data-to="<?= $value->to_period_val * 30 ?>" data-fee="<?= $value->fee_rate * 100 ?>">
                            <td class="text-right">
                                <span><?= $value->from_period_val ?> tháng</span>
                                <input type="hidden" value="<?= $value->from_period_val ?>">
                            </td>
                            <td class="text-right">
                                <span><?= $value->to_period_val ?> tháng</span>
                                <input type="hidden" value="<?= $value->to_period_val ?>">
                            </td>
                            <td class="text-right">
                                <span><?= $value->fee_rate * 100 ?> %</span>
                                <input type="hidden" value="<?= $value->fee_rate ?>">
                            </td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>

                </table>

            </div>

        </div>
    </div>


    <div class="hide">
        <?= $form->field($model, 'upd_date_time')->textInput() ?>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-8">

            <?php if ($model->status == $data['contractStatus']['active']): ?>
                <?= Html::submitButton('<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Tạo biên bản tất toán hợp đồng</span>', ['class' => 'btn bg-violet btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out']) ?>
            <?php endif; ?>

            <?php if ($model->status >= $data['contractStatus']['settle']): ?>
                <?= Html::a('<span class="icon-printer"></span> In biên bản tất toán', ['print', 'id' => $model->getPrimaryKey(), 'type' => 'settle'], ['class' => 'btn bg-warning-600', 'title' => 'In hợp đồng tất toán', 'data-pjax' => '0']) ?>
                <?= Html::a('<span class="icon-file-download"></span> Tải về biên bản tất toán', ['download', 'id' => $model->getPrimaryKey(), 'type' => 'settle'], ['class' => 'btn bg-info-600', 'title' => 'Tải về hợp đồng đã tất toán', 'data-pjax' => '0']) ?>
                <?= Html::submitButton('<i class="glyphicon glyphicon-send"></i> <span class="ladda-label">Gửi biên bản tất toán</span>', ['class' => 'btn btn-primary btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out', 'title' => 'Dữ liệu tất toán hợp dồng sẽ được cập nhật dự vào ngày kết thúc thực tế']) ?>
                <?= Html::a('<span class="fa fa-sign-in"></span> Tất toán hợp đồng', ['ajax/contract-settle', 'id' => $model->getPrimaryKey()], ['disabled' => $model->status == $data['contractStatus']['settled'] ? true : false, 'data-pjax' => 0, 'class' => 'btn btn-success btn-colorgb-settle-now', 'title' => 'Dữ liệu tất toán hợp dồng sẽ được cập nhật dự vào ngày kết thúc thực tế. Bạn có chắc chắn muốn tất toán hợp đồng này không?']) ?>
            <?php endif; ?>


        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?= $this->render('_extend_hist', [
    'model' => $model,
    'data' => $data,
]) ?>
