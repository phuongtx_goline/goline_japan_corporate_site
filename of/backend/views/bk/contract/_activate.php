<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use common\models\Customer;

/* @var $this yii\web\View */
/* @var $model common\models\Contract */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="colorgb-container contract-active-form">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4 text-right',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-6',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <div class="row">
        <div class="col-sm-6">

            <?= $form->field($model, 'customer_id')->dropDownList(
                [$model->customer_id => $model->customer_id ? Customer::findOne($model->customer_id)->name : 'Chọn...'],
                ['class' => 'colorgb-customer-search', 'disabled' => 'disabled'],
                ['options' => [$model->customer_id => ['selected' => true]]]
            ) ?>

            <?= $form->field($model, 'invest_value', [
                'template' => '{label}<div class="col-sm-6"><div class="input-group">{input}<span class="input-group-addon">đ</span> </div></div>',
            ])->textInput([
                'class' => 'price form-control',

            ]) ?>

            <?= $form->field($model, 'invest_unit_qty')->textInput([
                'class' => 'price form-control disabled',
            ]) ?>

            <?= $form->field($model, 'contract_type_id')->dropDownList($data['contractType'], ['options' => $data['contractTypeOption'], 'data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

            <?= $form->field($model, 'period_type')->dropDownList(ArrayHelper::map(Yii::$app->params['periodType'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...', 'disabled' => 'disabled']) ?>

            <?= $form->field($model, 'period_val')->textInput(['disabled' => 'disabled']) ?>

            <?= $form->field($model, 'response_status')->dropDownList(ArrayHelper::map(Yii::$app->params['responseStatus'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...', 'disabled' => $model->status == $data['contractStatus']['active'] ? true : false]) ?>

            <?= $form->field($model, 'response_content')->textarea(['disabled' => $model->status == $data['contractStatus']['active'] ? true : false]) ?>

            <?= $form->field($model, 'annouce_status')->dropDownList(ArrayHelper::map(Yii::$app->params['annouceStatus'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...', 'disabled' => 'disabled'])->label('Gửi email thông báo và hướng dẫn chuyển tiền') ?>
            <?= $form->field($model, 'representative')->dropDownList(ArrayHelper::map(Yii::$app->params['representative'], 'txt', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

            <?php if($model->contract_type_id == 3):?>
            <?= $form->field($model, 'bank_id')->dropDownList(ArrayHelper::map(Yii::$app->params['bank'], 'key', 'name'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>
            <?= $form->field($model, 'transfer_note')->textarea() ?>
            <?php endif; ?>

        </div>
        <div class="col-sm-6">

            <?= $form->field($model, 'fund_net_asset', [
                'template' => '{label}<div class="col-sm-6"><div class="input-group">{input}<span class="input-group-addon">đ</span> </div></div>',
            ])->textInput([
                'class' => 'price form-control disabled',
            ]) ?>

            <?= $form->field($model, 'fund_unit_qty')->textInput([
                'class' => 'price form-control disabled',
            ]) ?>

            <?= $form->field($model, 'fund_unit_price', [
                'template' => '{label}<div class="col-sm-6"><div class="input-group">{input}<span class="input-group-addon">đ</span> </div></div>',
            ])->textInput([
                'class' => 'price form-control disabled',
            ]) ?>

            <?= $form->field($model, 'base_profit_rate', [
                'template' => '{label}<div class="col-sm-6"><div class="input-group">{input}<span class="input-group-addon">%</span> </div></div>',
            ])->textInput([
                'class' => 'form-control',
                'disabled' => 'disabled'
            ]) ?>

            <?= $form->field($model, 'profit_rate', [
                'template' => '{label}<div class="col-sm-6"><div class="input-group">{input}<span class="input-group-addon">%</span> </div></div>',
            ])->textInput([
                'class' => 'form-control',
                'disabled' => 'disabled'
            ]) ?>

            <?= $form->field($model, 'contract_date')->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'displayFormat' => 'dd/MM/yyyy',
                'saveFormat' => 'yyyyMMdd',
                'ajaxConversion' => false,
                'autoWidget' => true,
                'disabled' => 'disabled'
            ])
            ?>

            <?= $form->field($model, 'begin_date')->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'displayFormat' => 'dd/MM/yyyy',
                'saveFormat' => 'yyyyMMdd',
                'ajaxConversion' => false,
                'autoWidget' => true,
                'disabled' => $model->status != $data['contractStatus']['active'] && Yii::$app->user->identity->is_admin == 1 ? false : 'disabled'
            ])
            ?>

            <?= $form->field($model, 'end_date_est')->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'displayFormat' => 'dd/MM/yyyy',
                'saveFormat' => 'yyyyMMdd',
                'ajaxConversion' => false,
                'autoWidget' => true,
                'disabled' => 'disabled'
            ])
            ?>

            <?= $form->field($model, 'active_date')->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'displayFormat' => 'dd/MM/yyyy',
                'saveFormat' => 'yyyyMMdd',
                'ajaxConversion' => false,
                'autoWidget' => true,
            ])
            ?>

            <?= $form->field($model, 'remarks')->textarea(['maxlength' => true, 'disabled' => $model->status == $data['contractStatus']['active'] ? true : false]) ?>

        </div>
    </div>

    <div class="hide">
        <?= $form->field($model, 'upd_date_time')->textInput() ?>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-8">
            <?php if ($model->status == $data['contractStatus']['active']): ?>
                <?= Html::submitButton('<i class="icon-file-check"></i> <span class="ladda-label">Hợp đồng đã được kích hoạt</span>', ['disabled' => 'disabled', 'class' => 'btn btn-primary btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out']) ?>
            <?php else: ?>
                <?= Html::submitButton('<i class="icon-file-check"></i> <span class="ladda-label">Kích hoạt hợp đồng</span>', ['class' => 'btn btn-primary  btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out']) ?>
            <?php endif; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?= $this->render('_extend_hist', [
    'model' => $model,
    'data' => $data,
]) ?>
