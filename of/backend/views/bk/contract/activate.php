<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FundUnit */

$this->title = 'Kích hoạt hợp đồng: #' . $model->contract_no;
$this->params['breadcrumbs'][] = ['label' => 'Hợp đồng', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getPrimaryKey(), 'url' => ['view', 'id' => $model->getPrimaryKey()]];
$this->params['breadcrumbs'][] = 'Kích hoạt';
?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $this->title ?></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <?php if (Yii::$app->session->getFlash('success')): ?>
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
                <?= Yii::$app->session->getFlash('success'); ?>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->session->getFlash('error')): ?>
            <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
                <?= Yii::$app->session->getFlash('error'); ?>
            </div>
        <?php endif; ?>

        <div class="colorgb-form">

            <?= $this->render('_activate', [
                'model' => $model,
                'data' => $data,
            ]) ?>

        </div>

    </div>
</div>

<script>
    $(function () {

        setInterval(function () {
            var date = $('#contract-begin_date').val();
            var ct = $('#contract-contract_type_id').val();
            $.ajax({
                url: baseUrl+'/ajax/fund-unit?ct='+ct+'&date='+date, success: function (response) {
                    var obj = JSON.parse(response);
                    $('#contract-fund_unit_price').val(obj.fund_unit_price);
                    $('#contract-fund_net_asset').val(obj.fund_net_asset);
                    $('#contract-fund_unit_qty').val(obj.fund_unit_qty);
                }
            });
        },1000)

    })
</script>