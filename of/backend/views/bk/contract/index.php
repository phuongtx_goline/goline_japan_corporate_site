<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\colorgb\ColorgbHelper;
use common\models\Customer;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ContractColorgb */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Danh sách hợp đồng';
$this->params['breadcrumbs'][] = $this->title;
$columns = [
    /*[
        'class' => 'kartik\grid\ExpandRowColumn',
        'width' => '60px',
        'hAlign' => 'left',
        'value' => function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail' => function ($model, $key, $index, $column) use ($data) {
            return Yii::$app->controller->renderPartial('/customer/_report', ['model' => $model, 'data' => $data]);
        },
        'headerOptions' => ['class' => 'kartik-sheet-style'],
        'expandOneOnly' => true
    ],*/
    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '50px', 'header' => 'Xem',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                if (ColorgbHelper::checkPermission('CONTRACT_VIEW')) $html .= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->getPrimaryKey()], ['title' => 'Xem', 'data-pjax' => '0']);
                return $html;
            },
        ]
    ],
    [
        'attribute' => 'colorgb_date',
        'width' => '130px',
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'locale' => [
                    'format' => 'YYYY-MM-DD',
                    'separator' => ' - ',
                ],
                'ranges' => [
                    //"Hôm nay" => ["moment().startOf('day')", "moment()"],
                    "Hôm qua" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                    "Tuần trước" => ["moment().startOf('day').subtract(7, 'days')", "moment()"],
                    "Tháng trước" => ["moment().startOf('day').subtract(31, 'days')", "moment()"],
                ],
                'alwaysShowCalendars' => true,

            ],
        ],
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) {
            $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
            $date = ColorgbHelper::date($model->begin_date);
            $html = '<h6>';
            $html .= ' <span> ' . $date . '</span> ';
            $html .= '</h6>';
            $html .= ColorgbHelper::contractExpire($model->end_date_est);
            return $html;
        },

    ],
    [
        'attribute' => 'customer_name',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) {
            $type = '';
            $customerType = ArrayHelper::map(Yii::$app->params['customerType'], 'key', 'value');
            if ($model->customer->type == $customerType['old']) $type = '<span class="small label bg-grey">Khách hàng cũ</span>';
            if ($model->customer->type == $customerType['new']) $type = '<span class="small label bg-green">Khách hàng mới</span>';
            $html = '<strong class="display-block"><a data-pjax="0" title="Xem chi tiết khách hàng" href="customer?CustomerColorgb%5Bid%5D=' . $model->customer_id . '&CustomerColorgb%5Bcontract_info%5D=&CustomerColorgb%5Bstatus%5D=&CustomerColorgb%5Bcared_user_id%5D=">' . $model->customer->name . '</a></strong>';
            $html .= '<p title="Số điện thoại">' . $model->customer->mobile . '</p>';
            $html .= '<p title="Email chính">' . $model->customer->email_addr . '</p>';
            $html .= '<p title="Mã số thuế">' . $model->customer->tax_code . '</p>';
            $html .= '<p title="Loại khách hàng">' . $type . '</p>';
            return $html;
        },
        /*'filterType' => GridView::FILTER_SELECT2,
        'filter' => [],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => [
            'class' => 'colorgb-customer-search',
            'placeholder' => 'Bộ lọc...'
        ],*/

    ],
    [
        'attribute' => 'contract_info',
        'format' => 'raw',
        'width' => '200px',
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $html = '<strong class="display-block">' . $model->contract_no . '</strong>';
            $html .= '<p>' .$model->contract_type_name . '</p>';
            return $html;
        },

    ],
    [
        'attribute' => 'status',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $array = $data['contractStatusHtml'];
            $value = !empty($array[$model->status]) ? $array[$model->status] : '';
            return $value;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['contractStatusTxt'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Bộ lọc...'],
        'width' => '150px',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '340px',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                $data['contractStatus'] = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
                if (ColorgbHelper::checkPermission('CONTRACT_SENDNOTICE')) $html .= Html::a('<span class="icon-cash3"></span>', ['send-notice', 'action' => 'money_transfer', 'id' => $model->getPrimaryKey()], ['title' => 'Gửi hướng dẫn chuyển tiền cho khách hàng', 'disabled' => $model->status >= $data['contractStatus']['active'] ? 'disabled' : 'false', 'class' => 'btn-colorgb-jg', 'data-pjax' => 'false']);
                if (ColorgbHelper::checkPermission('CONTRACT_SENDNOTICE')) $html .= Html::a('<span class="icon-bell3"></span>', ['send-notice', 'action' => 'contract_renewal', 'id' => $model->getPrimaryKey()], ['title' => 'Gửi thông báo gia hạn hợp đồng cho khách hàng', 'disabled' => ColorgbHelper::contractExpire($model->end_date_est) ? 'false' : 'disabled', 'class' => 'btn-colorgb-jg', 'data-pjax' => 'false']);
                if (ColorgbHelper::checkPermission('CONTRACT_SENDNOTICE')) $html .= Html::a('<span class="icon-file-upload2"></span>', ['send-notice', 'action' => 'contract_doc', 'id' => $model->getPrimaryKey()], ['title' => 'Gửi hợp đồng cho khách hàng', 'disabled' => $model->status == $data['contractStatus']['inactive'] ? 'disabled' : 'false', 'class' => 'btn-colorgb-jg', 'data-pjax' => 'false']);
                $html .= '<span class="text-warning"> | </span>';
                if (Yii::$app->user->identity->is_admin == 1 && ColorgbHelper::checkPermission('CONTRACT_UPDATE')) $html .= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->getPrimaryKey()], ['title' => 'Sửa đổi hợp đồng', 'class' => 'btn-colorgb-popup', 'data-pjax' => '0']);
                if (ColorgbHelper::checkPermission('CONTRACT_ACTIVATE')) $html .= Html::a('<span class="icon-file-check"></span>', ['activate', 'id' => $model->getPrimaryKey()], ['title' => 'Kích hoạt hợp đồng', 'disabled' => $model->status >= $data['contractStatus']['active'] ? 'disabled' : 'false', 'class' => 'btn-colorgb-popup', 'data-pjax' => '0']);
                if (ColorgbHelper::checkPermission('CONTRACT_RENEW')) $html .= Html::a('<span class="icon-reload-alt"></span>', ['renew', 'id' => $model->getPrimaryKey()], ['title' => 'Gia hạn hợp đồng', 'disabled' => $model->status == $data['contractStatus']['active'] ? 'false' : 'disabled', 'class' => 'btn-colorgb-popup', 'data-pjax' => 'false']);
                if (ColorgbHelper::checkPermission('CONTRACT_DOWNLOAD')) $html .= Html::a('<span class="icon-file-download"></span>', ['download', 'id' => $model->getPrimaryKey()], ['title' => 'Tải về hợp đồng', 'data-pjax' => '0']);
                if (ColorgbHelper::checkPermission('CONTRACT_PRINT')) $html .= Html::a('<span class="icon-printer"></span>', ['print', 'id' => $model->getPrimaryKey()], ['title' => 'In hợp đồng', 'class' => 'btn-colorgb-popup',  'data-pjax' => 'false']);
                if (ColorgbHelper::checkPermission('CONTRACT_SETTLE')) $html .= Html::a('<span class="icon-enter"></span>', ['settle', 'id' => $model->getPrimaryKey()], ['title' => 'Tất toán hợp đồng', 'disabled' => $model->status >= $data['contractStatus']['active'] ? 'false' : 'disabled', 'class' => 'btn-colorgb-popup', 'data-pjax' => 'false']);
                if (ColorgbHelper::checkPermission('CONTRACT_DELETE')) $html .= Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->getPrimaryKey()], ['title' => 'Xóa', 'disabled' => $model->status >= $data['contractStatus']['active'] ? 'disabled' : 'false', 'data-pjax' => 'false', 'class' => 'colorgb-pjax-action-del colorgb-pjax-action-del-show', 'data-pjax-container' => 'colorgb-pjax-pjax']);
                return $html;
            },
        ],

    ],
];

$cid = Yii::$app->request->get('ContractColorgb')['customer_id'] ? Yii::$app->request->get('ContractColorgb')['customer_id'] : 0;
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'colorgb-pjax',
    'showPageSummary' => false,
    'pjax' => true,
    'striped' => true,
    'responsive' => true,
    'responsiveWrap' => true,
    'hover' => true,
    'floatHeader' => true,
    'floatHeaderOptions' => ['top' => 46],
    'toolbar' => [
        'before' => ColorgbHelper::checkPermission('CONTRACT_CREATE') ? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create?customer_id=' . $cid], ['data-pjax' => 0, 'class' => 'btn btn-success pull-left', 'title' => 'Thêm mới']) : '',
        'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''], ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Làm mới']),
        '{toggleData}',
    ],
    'panel' => ['type' => 'default', 'heading' => $this->title],
    'columns' => $columns,
]);
?>

<script>
    function colorgbAjaxReady() {

        $('#colorgb-pjax-filters input[name="ContractColorgb[customer_name]"]').attr('title', 'Tìm kiếm theo Tên, SĐT, Email. Lọc theo loại KH nhập 1 trong 2 từ khóa "KHÁCH HÀNG CŨ/KHÁCH HÀNG MỚI" (VD: KHÁCH HÀNG CŨ) ');
    }

    $(document).on('pjax:complete', colorgbAjaxReady);
    $(document).ready(colorgbAjaxReady);
</script>