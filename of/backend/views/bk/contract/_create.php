<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use common\models\Customer;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="colorgb-container">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4 text-right',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-6',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <div class="row">
        <div class="col-sm-6">

            <?= $form->field($model, 'customer_id')->dropDownList(
                [$model->customer_id => $model->customer_id ? Customer::findOne($model->customer_id)->name : 'Chọn...'],
                ['class' => 'colorgb-customer-search'],
                ['options' => [$model->customer_id => ['selected' => true]]]
            ) ?>

            <?= $form->field($model, 'invest_value', [
                'template' => '{label}<div class="col-sm-6"><div class="input-group">{input}<span class="input-group-addon">đ</span> </div></div>',
            ])->textInput([
                'class' => 'price form-control'
            ]) ?>

            <?= $form->field($model, 'invest_unit_qty')->textInput([
                'class' => 'price form-control',
            ]) ?>

            <?= $form->field($model, 'tax_rate', [
                'template' => '{label}<div class="col-sm-6"><div class="input-group">{input}<span class="input-group-addon">%</span> </div></div>',
            ])->textInput([
                'class' => 'form-control',
            ]) ?>

            <?= $form->field($model, 'contract_type_id')->dropDownList($data['contractType'], ['options' => $data['contractTypeOption'],'data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

            <?= $form->field($model, 'period_type')->dropDownList(ArrayHelper::map(Yii::$app->params['periodType'], 'value', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...', 'disabled' => 'disabled']) ?>

            <?= $form->field($model, 'period_val')->textInput(['disabled' => 'disabled']) ?>

            <?= $form->field($model, 'annouce_status')->checkbox(['class' => 'styled'])->label('Gửi email thông báo và hướng dẫn chuyển tiền') ?>


        </div>
        <div class="col-sm-6">

            <?= $form->field($model, 'fund_net_asset', [
                'template' => '{label}<div class="col-sm-6"><div class="input-group">{input}<span class="input-group-addon">đ</span> </div></div>',
            ])->textInput([
                'class' => 'price form-control',
                'disabled' => 'disabled'
            ])->label('Tổng GTTS ròng hiện tại') ?>

            <?= $form->field($model, 'fund_unit_qty')->textInput([
                'class' => 'price form-control',
                'disabled' => 'disabled'
            ])->label('Số lượng ĐVĐT hiện tại') ?>

            <?= $form->field($model, 'fund_unit_price', [
                'template' => '{label}<div class="col-sm-6"><div class="input-group">{input}<span class="input-group-addon">đ</span> </div></div>',
            ])->textInput([
                'class' => 'price form-control',
                'disabled' => 'disabled'
            ])->label('Giá của 1 ĐVĐT hiện tại') ?>

            <?= $form->field($model, 'base_profit_rate', [
                'template' => '{label}<div class="col-sm-6"><div class="input-group">{input}<span class="input-group-addon">%</span> </div></div>',
            ])->textInput([
                'class' =>'form-control',
                'disabled' => 'disabled'
            ]) ?>

            <?= $form->field($model, 'profit_rate', [
                'template' => '{label}<div class="col-sm-6"><div class="input-group">{input}<span class="input-group-addon">%</span> </div></div>',
            ])->textInput([
                'class' => 'form-control',
                'disabled' => 'disabled'
            ]) ?>

            <?= $form->field($model, 'representative')->dropDownList(ArrayHelper::map(Yii::$app->params['representative'], 'txt', 'txt'), ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

            <?= $form->field($model, 'remarks')->textarea() ?>


        </div>
    </div>

    <div class="hide">
        <?= $form->field($model, 'upd_date_time')->textInput() ?>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-8">
            <?= Html::submitButton($model->isNewRecord ? '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Tạo mới</span>' : '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Lưu lại</span>', ['class' => $model->isNewRecord ? 'btn btn-success  btn-ladda btn-ladda-spinner btn-ladda-progress' : 'btn btn-primary  btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
