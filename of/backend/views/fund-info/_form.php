<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use common\models\Customer;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */


$data['checkList'] = ['T2' => 'T2', 'T3' => 'T3', 'T4' => 'T4', 'T5' => 'T5', 'T6' => 'T6', 'T7' => 'T7', 'CN' => 'CN'];
$data['status'] = ArrayHelper::map(Yii::$app->params['fundInfoStatus'], 'value', 'txt');
?>

<div class="colorgb-container">
    <?php $form = ActiveForm::begin(); ?>

    <?php if ($model->getErrors()): ?>
        <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
            <?= json_encode($model->getErrors(),JSON_UNESCAPED_UNICODE) ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'SEC_CD')->textInput(['maxlength' => true, 'disabled' => $model->STATUS == 1 ? 'disabled' : false]) ?>

        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'SEC_NAME')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'IPO_PRICE')->textInput(['maxlength' => true, 'class' => 'price form-control']) ?>

        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'TOTAL_QTY')->textInput(['maxlength' => true, 'class' => 'price form-control']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'MIN_IPO_AMOUNT')->textInput(['maxlength' => true, 'class' => 'price form-control']) ?>

        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'MIN_BALANCE_QTY')->textInput(['maxlength' => true, 'class' => 'price form-control']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'TRANS_DATE_TYPE')->dropDownList(Yii::$app->params['TRANS_DATE_TYPE'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

        </div>
        <div class="col-sm-6">
            <div class="form-group field-cortfundinfo-week_days">
                <label class="control-label" for="cortfundinfo-week_days">Ngày GD</label>
                <div class="WEEK_DAYS hide inline" id="wd">
                    <?= $form->field($model, 'WEEK_DAYS')->inline(true)->checkboxList($data['checkList'])->label(false) ?>
                </div>
                <div class="TRANS_DATE hide inline" id="td">
                    <?= $form->field($model, 'TRANS_DATE')->textInput(['type' => 'number', 'min' => '1', 'max' => '31', 'placeholder' => 'Từ ngày 01-31'])->label(false) ?>
                </div>
                <p class="help-block help-block-error"></p>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'END_ORDER_DATE_NUM', [
                'template' => '{label}<div class="col-sm-7-2 inline"><div class="input-group">{input}<span class="input-group-addon">ngày</span> </div></div>',
            ])->textInput([
                'class' => 'price form-control'
            ]) ?>

        </div>
        <div class="col-sm-6">

            <?= $form->field($model, 'END_ORDER_HOUR')->textInput(['type' => 'time']) ?>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'STATUS')->dropDownList($data['status'], ['class' => 'disabled']) ?>


        </div>
        <div class="col-sm-6">

            <?= $form->field($model, 'REMARKS')->textInput() ?>

        </div>
    </div>



    <div class="hide">
        <?= $form->field($model, 'UPD_DATE_TIME')->textInput() ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Tạo mới</span>' : '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Lưu lại</span>', ['class' => $model->isNewRecord ? 'btn btn-success  btn-ladda btn-ladda-spinner btn-ladda-progress' : 'btn btn-primary  btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    function f1() {
        if ($('#fundinfoform-trans_date_type').val() == '1') {
            $('#wd').removeClass('hide');
            $('#td').addClass('hide');

        } else {
            $('#wd').addClass('hide');
            $('#td').removeClass('hide');
        }
        $('input[type="checkbox"]').uniform();

    }

    $(function () {
        f1();
        $('#fundinfoform-trans_date_type').change(f1);
    });

</script>