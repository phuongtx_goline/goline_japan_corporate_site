<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\colorgb\ColorgbHelper;
use common\models\Customer;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FUNDINFOColorgb */
/* @var $dataProvider yii\data\ActiveDataProvider */
$data['status'] = ArrayHelper::map(Yii::$app->params['fundInfoStatus'], 'value', 'txt');
$data['statusHtml'] = ArrayHelper::map(Yii::$app->params['fundInfoStatus'], 'value', 'html');

$this->title = 'Danh sách chứng chỉ quỹ';
$this->params['breadcrumbs'][] = $this->title;
$columns = [

    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '50px', 'header' => 'Xem',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                if (ColorgbHelper::checkPermission('FUNDINFO_VIEW')) $html .= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->getPrimaryKey()], ['title' => 'Xem', 'data-pjax' => '0']);
                return $html;
            },
        ]
    ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '150px',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                if (ColorgbHelper::checkPermission('FUNDINFO_UPDATE')) $html .= Html::a('<span class="icon-pencil7"></span>', ['update', 'id' => $model->getPrimaryKey()], ['title' => 'Sửa', 'class' => 'btn-colorgb-popup', 'data-pjax' => 'false']);
                if (ColorgbHelper::checkPermission('FUNDINFO_APPROVE')) $html .= Html::a('<span class="icon-file-check"></span>', ['approve', 'id' => $model->getPrimaryKey()], ['reload'=>'true','title' => 'Duyệt mục này?', 'disabled' => $model->STATUS == 1 ? 'disabled' : false, 'data-pjax' => '1', 'class' => 'btn-colorgb-warning', 'data-pjax-container' => 'colorgb-pjax-pjax']);
                if (ColorgbHelper::checkPermission('FUNDINFO_DELETE')) $html .= Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->getPrimaryKey()], ['title' => 'Xóa', 'data-pjax' => 'false', 'disabled' => $model->STATUS == 1 ? 'disabled' : false,'class' => 'colorgb-pjax-action-del colorgb-pjax-action-del-show', 'data-pjax-container' => 'colorgb-pjax-pjax']);
                return $html;
            },
        ],

    ],

    [
        'attribute' => 'a1',
        'label' => 'Chứng chỉ quỹ',
        'format' => 'raw',
        'value' => function ($model) {
            $h = '';
            $h .= '<p>' . $model->SEC_CD . '</p>';
            $h .= '<p>' . $model->SEC_NAME . '</p>';
            $h .= '<p>' . number_format($model->IPO_PRICE) . '</p>';
            $h .= '<p>' . number_format($model->TOTAL_QTY) . '</p>';
            return $h;
        }
    ],
    [
        'attribute' => 'MIN_IPO_AMOUNT',
        'format' => 'raw',
        'value' => function ($model) {

            return number_format($model->MIN_IPO_AMOUNT);
        }
    ],
    [
        'attribute' => 'MIN_BALANCE_QTY',
        'format' => 'raw',
        'value' => function ($model) {

            return number_format($model->MIN_BALANCE_QTY);
        }
    ],
    [
        'attribute' => 'TRANS_DATE_TYPE',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => Yii::$app->params['TRANS_DATE_TYPE'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Lọc...'],
        'value' => function ($model) {

            return Yii::$app->params['TRANS_DATE_TYPE'][$model->TRANS_DATE_TYPE];
        }

    ],
    [
        'attribute' => 'TRANS_DATE',
        'format' => 'raw',
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $value = $model->TRANS_DATE_TYPE == 1 ? $model->WEEK_DAYS : $model->TRANS_DATE;
            return $value;
        },

    ],
    [
        'attribute' => 'STATUS',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['status'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Lọc...'],
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $array = $data['statusHtml'];
            $value = !empty($array[$model->STATUS]) ? $array[$model->STATUS] : '';
            return $value;
        },
    ],
];

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'colorgb-pjax',
    'showPageSummary' => false,
    'pjax' => true,
    'striped' => true,
    'responsive' => false,
    'responsiveWrap' => false,
    'hover' => true,
    'floatHeader' => true,
    'floatHeaderOptions' => ['top' => 46],
    'toolbar' => [
        'before' => '<a class="btn btn-default" href="balance"><i class="icon-list-unordered position-left"></i> Tra cứu số dư CCQ</a> <a class="btn btn-default" href="movement"><i class="icon-list-unordered position-left"></i> Giao dịch phong tỏa/giải tỏa CCQ</a><a class="btn btn-default" href="transfer"><i class="icon-list-unordered position-left"></i> Chuyển khoản, tất toán tài khoản CCQ</a> <a class="btn btn-default" href="transaction"><i class="icon-list-unordered position-left"></i>Nộp/rút CCQ</a> '.(ColorgbHelper::checkPermission('FUNDINFO_CREATE') ? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'class' => 'btn btn-success pull-left', 'title' => 'Thêm mới']) : ''),
        'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''], ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Làm mới']),
        '{toggleData}',
    ],
    'panel' => ['type' => 'default', 'heading' => $this->title],
    'columns' => $columns,
]);
?>