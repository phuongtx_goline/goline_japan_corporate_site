<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use common\models\Customer;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */


$data['customer'] = ArrayHelper::map(\common\models\CORCUSTOMER::findAll(['status' => 1]), 'CUST_NO', 'CUST_NO');
$data['location'] = ArrayHelper::map(\common\models\Code::findAll(['group' => 'LOCATION']), 'id', 'value1');
$data['country'] = ArrayHelper::map(\common\models\Code::findAll(['group' => 'COUNTRY']), 'id', 'value1');
$data['job'] = ArrayHelper::map(\common\models\Code::findAll(['group' => 'CAREER']), 'id', 'value1');
$data['user'] = ArrayHelper::map(\common\models\User::findAll(['status' => 1]), 'id', 'name');
$data['transaction'] = ArrayHelper::map(\common\models\MSTTBRANCH::findAll(['STATUS' => 1]), 'TRANSACTION_CD', 'BRANCH_NAME');
?>

<div class="colorgb-container">
    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <?php if ($model->getErrors()): ?>
        <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
            <?= json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE) ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group field-msttbranch-status required">
                <label class="control-label" for="msttbranch-status">Sinh số tài khoản</label>
                <select <?= Yii::$app->controller->action->id == 'create' ? '' : 'disabled="disabled"' ?> id="id" class="form-control">
                    <option value="1" selected>Tự động sinh</option>
                    <option value="2">Chọn số</option>
                </select>

            </div>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'CUST_NO')->textInput(['class' => 'form-control disabled']) ?>

        </div>
    </div>


    <div class="tabbable">
        <ul class="nav nav-tabs nav-tabs-highlight nav-justified">
            <li class="active"><a href="#justified-left-icon-tab1" data-toggle="tab"> Thông tin cơ bản</a></li>
            <li><a href="#justified-left-icon-tab2" data-toggle="tab"> Người đại diện</a></li>
            <li><a href="#justified-left-icon-tab3" data-toggle="tab"> TK ngân hàng </a></li>
            <li><a href="#justified-left-icon-tab4" data-toggle="tab"> Ủy quyền</a></li>
            <li><a href="#justified-left-icon-tab5" data-toggle="tab"> Chữ ký </a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="justified-left-icon-tab1">
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'CUST_NAME')->textInput(['maxlength' => true]) ?>

                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'CUST_TYPE')->dropDownList(Yii::$app->params['CUST_TYPE'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'FOREIGN_TYPE')->dropDownList(Yii::$app->params['FOREIGN_TYPE'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'BIRTHDAY')->widget(DateControl::classname(), [
                            'type' => DateControl::FORMAT_DATE,
                            'displayFormat' => 'dd/MM/yyyy',
                            'saveFormat' => 'yyyyMMdd',
                            'ajaxConversion' => false,
                            'autoWidget' => true,
                        ])
                        ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'SEX')->dropDownList(Yii::$app->params['SEX'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'TEL_NO')->textInput(['maxlength' => true]) ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'MOBILE_NO')->textInput(['maxlength' => true]) ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'EMAIL_ADRS')->textInput(['maxlength' => true]) ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'EMAIL_FLG')->checkbox(['class' => 'styled']) ?>

                    </div>
                </div>

                <hr>
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'APPROVAL_CD')->dropDownList(Yii::$app->params['APPROVAL_CD'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'ACCO_DIVIDE')->dropDownList(Yii::$app->params['ACCO_DIVIDE'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'ID_NUMBER')->textInput(['maxlength' => true]) ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'ISSUE_LOCATION_CD')->dropDownList($data['location'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'ISSUE_DATE')->widget(DateControl::classname(), [
                            'type' => DateControl::FORMAT_DATE,
                            'displayFormat' => 'dd/MM/yyyy',
                            'saveFormat' => 'yyyyMMdd',
                            'ajaxConversion' => false,
                            'autoWidget' => true,
                        ])
                        ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'EXPIRE_DATE')->widget(DateControl::classname(), [
                            'type' => DateControl::FORMAT_DATE,
                            'displayFormat' => 'dd/MM/yyyy',
                            'saveFormat' => 'yyyyMMdd',
                            'ajaxConversion' => false,
                            'autoWidget' => true,
                        ])
                        ?>

                    </div>
                </div>
                <hr>

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'BROKER_CD')->dropDownList($data['user'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>


                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'ADDR1')->textInput(['maxlength' => true]) ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'ADDR2')->textInput(['maxlength' => true]) ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'COUNTRY_CD')->dropDownList($data['country'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'JOB')->dropDownList($data['job'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'POSITION')->textInput(['maxlength' => true]) ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'COMPANY')->textInput(['maxlength' => true]) ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'COMPANY_TEL')->textInput(['maxlength' => true]) ?>

                    </div>
                </div>

                <hr>


                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'TRADING_CODE')->textInput(['maxlength' => true]) ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'TRANSACTION_CD')->dropDownList($data['transaction'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'CONTRACT_NO')->textInput(['maxlength' => true]) ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'FATCA')->dropDownList(Yii::$app->params['FATCA'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'OPEN_DATE')->widget(DateControl::classname(), [
                            'type' => DateControl::FORMAT_DATE,
                            'displayFormat' => 'dd/MM/yyyy',
                            'saveFormat' => 'yyyyMMdd',
                            'ajaxConversion' => false,
                            'autoWidget' => true,
                            'disabled' => true,
                        ])
                        ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'CLOSE_DATE')->widget(DateControl::classname(), [
                            'type' => DateControl::FORMAT_DATE,
                            'displayFormat' => 'dd/MM/yyyy',
                            'saveFormat' => 'yyyyMMdd',
                            'ajaxConversion' => false,
                            'autoWidget' => true,
                            'disabled' => true,
                        ])
                        ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'ACCO_STS')->dropDownList(Yii::$app->params['ACCO_STS'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...','disabled'=>'disabled']) ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'VSD_STATUS')->dropDownList(Yii::$app->params['VSD_STATUS'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...','disabled'=>'disabled']) ?>

                    </div>
                </div>


            </div>
            <div class="tab-pane" id="justified-left-icon-tab2">

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'PRES_NAME')->textInput(['maxlength' => true]) ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'PRES_APPROVAL_CD')->dropDownList(Yii::$app->params['APPROVAL_CD'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'PRES_ID_NUMBER')->textInput(['maxlength' => true]) ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'PRES_ISSUE_LOCATION_CD')->dropDownList($data['location'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'PRES_ISSUE_DATE')->widget(DateControl::classname(), [
                            'type' => DateControl::FORMAT_DATE,
                            'displayFormat' => 'dd/MM/yyyy',
                            'saveFormat' => 'yyyyMMdd',
                            'ajaxConversion' => false,
                            'autoWidget' => true,
                        ])
                        ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'PRES_TEL_NO')->textInput(['maxlength' => true]) ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'PRES_COUNTRY_CD')->dropDownList($data['country'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'PRES_POSITION')->textInput(['maxlength' => true]) ?>

                    </div>
                </div>


            </div>
            <div class="tab-pane" id="justified-left-icon-tab3">
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'BANK_ACCO_NAME')->textInput(['maxlength' => true]) ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'BANK_ACCO_NO')->textInput(['maxlength' => true]) ?>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'BANK_NAME')->textInput(['maxlength' => true]) ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'BANK_BRANCH_NAME')->textInput(['maxlength' => true]) ?>

                    </div>
                </div>
            </div>
            <div class="tab-pane" id="justified-left-icon-tab4">

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'CE_ENTRUST_CUST_NO')->dropDownList($data['customer'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'CE_ENTRUST_CUST_NAME')->textInput(['maxlength' => true]) ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'CE_ENTRUST_SCOPE')->dropDownList(Yii::$app->params['ENTRUST_SCOPE'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'CE_ID_NUMBER')->textInput(['maxlength' => true]) ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'CE_ISSUE_LOCATION_CD')->dropDownList($data['location'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'CE_ISSUE_DATE')->widget(DateControl::classname(), [
                            'type' => DateControl::FORMAT_DATE,
                            'displayFormat' => 'dd/MM/yyyy',
                            'saveFormat' => 'yyyyMMdd',
                            'ajaxConversion' => false,
                            'autoWidget' => true,
                        ])
                        ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'CE_BIRTHDAY')->widget(DateControl::classname(), [
                            'type' => DateControl::FORMAT_DATE,
                            'displayFormat' => 'dd/MM/yyyy',
                            'saveFormat' => 'yyyyMMdd',
                            'ajaxConversion' => false,
                            'autoWidget' => true,
                        ])
                        ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'CE_SEX')->dropDownList(Yii::$app->params['SEX'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'CE_ADDR')->textInput(['maxlength' => true]) ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'CE_TEL_NO')->textInput() ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'CE_EMAIL_ADRS')->textInput(['maxlength' => true]) ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'CE_ENTRUST_START_DATE')->widget(DateControl::classname(), [
                            'type' => DateControl::FORMAT_DATE,
                            'displayFormat' => 'dd/MM/yyyy',
                            'saveFormat' => 'yyyyMMdd',
                            'ajaxConversion' => false,
                            'autoWidget' => true,
                        ])
                        ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'CE_ENTRUST_END_DATE')->widget(DateControl::classname(), [
                            'type' => DateControl::FORMAT_DATE,
                            'displayFormat' => 'dd/MM/yyyy',
                            'saveFormat' => 'yyyyMMdd',
                            'ajaxConversion' => false,
                            'autoWidget' => true,
                        ])
                        ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">

                        <?= $form->field($model, 'CE_APPROVAL_CD')->dropDownList(Yii::$app->params['APPROVAL_CD'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'CE_COUNTRY_CD')->dropDownList($data['country'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">

                        <?= $form->field($model, 'ce_file_upload')->widget(\kartik\file\FileInput::classname(), [
                            'pluginOptions' => $data['fileInputCe'],
                        ]);
                        ?>
                    </div>
                    <div class="col-sm-6">

                        <?= $form->field($model, 'CE_REMARKS')->textInput() ?>

                    </div>

                </div>


            </div>
            <div class="tab-pane" id="justified-left-icon-tab5">

                <div class="row">
                    <div class="col-sm-12">

                        <?= $form->field($model, 'cs_file_upload[]')->widget(\kartik\file\FileInput::classname(), [
                            'pluginOptions' => $data['fileInputCs'],
                            'options' => ['multiple' => true]
                        ]);
                        ?>

                        <?php if ($data['cs']): ?>
                            <table>
                                <div class="table-responsive">
                                    <table class="table table-striped table-ce">
                                        <thead>
                                        <tr>
                                            <th>Xóa</th>
                                            <th>#</th>
                                            <th>Ghi chú</th>
                                            <th>Người tạo</th>
                                            <th>Thời gian tạo</th>
                                            <th>Người cập nhật</th>
                                            <th>Thời gian cập nhật</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($data['cs'] as $v): ?>
                                            <tr data-id="<?= $v->ID ?>">
                                                <td>
                                                    <a class="btn-colorgb-warning" data-remove="tr" href="ajax/delete-cs?id=<?= $v->ID ?>" title="Xóa mục này?" data-pjax="false" data-id="<?= $v->ID ?>"><span class="glyphicon glyphicon-trash"></span></a>
                                                </td>
                                                <td><img src="<?= \common\colorgb\ColorgbHelper::dataSrc($v->SIGNER_DOC) ?>" width="100" alt=""></td>
                                                <td><input data-id="<?= $v->ID ?>" placeholder="Nhập ghi chú..." type="text" value="<?= $v->REMARKS ?>" class="form-control"></td>
                                                <td><?= \common\models\User::findOne($v->REG_USER_ID)->name ?></td>
                                                <td><?= $v->REG_DATE_TIME ?></td>
                                                <td><?= \common\models\User::findOne($v->UPD_USER_ID)->name ?></td>
                                                <td><?= $v->UPD_DATE_TIME ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </table>
                        <?php endif; ?>

                    </div>
                </div>

            </div>
        </div>

    </div>


    <div class="hide">
        <?= $form->field($model, 'UPD_DATE_TIME')->textInput() ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Tạo mới</span>' : '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Lưu lại</span>', ['class' => $model->isNewRecord ? 'btn btn-success  btn-ladda btn-ladda-spinner btn-ladda-progress' : 'btn btn-primary  btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    function f() {
        $('#corcustomer-pres_name').val($('#corcustomer-cust_name').val());

    }

    $(function () {
        $('#corcustomer-ce_entrust_cust_no').change(function () {
            var cd = $(this).val();
            $.ajax({
                url: baseUrl + '/ajax/get-customer?cd=' + cd, success: function (response) {
                    var json = JSON.parse(response);
                    $('input[name="CORCUSTOMER[CE_ENTRUST_CUST_NAME]"]').val(json.CUST_NAME)
                    $('input[name="CORCUSTOMER[CE_ENTRUST_CUST_NO]"]').val(json.CUST_NO);
                    $('select[name="CORCUSTOMER[CE_APPROVAL_CD]"]').val(json.APPROVAL_CD).trigger('change');
                    $('input[name="CORCUSTOMER[CE_ID_NUMBER]"]').val(json.ID_NUMBER);
                    $('select[name="CORCUSTOMER[CE_ISSUE_LOCATION_CD]"]').val(json.ISSUE_LOCATION_CD).trigger('change');
                    $('input[name="CORCUSTOMER[CE_ISSUE_DATE]"]').val(json.ISSUE_DATE);
                    $('input[name="CORCUSTOMER[CE_BIRTHDAY]"]').val(json.BIRTHDAY);
                    $('select[name="CORCUSTOMER[CE_SEX]"]').val(json.SEX).trigger('change');
                    $('input[name="CORCUSTOMER[CE_ADDR]"]').val(json.ADDR);
                    $('input[name="CORCUSTOMER[CE_TEL_NO]"]').val(json.TEL_NO);
                    $('input[name="CORCUSTOMER[CE_EMAIL_ADRS]"]').val(json.EMAIL_ADRS);
                    $('select[name="CORCUSTOMER[CE_COUNTRY_CD]"]').val(json.COUNTRY_CD).trigger('change');
                    $('select[name="CORCUSTOMER[ENTRUST_SCOPE]"]').val(0).trigger('change');
                }
            });
        });

        $('.table-ce input').change(function () {
            var id = $(this).data('id');
            var txt = $(this).val();
            $.ajax({
                url: baseUrl + '/ajax/set-cs?id=' + id + '&txt=' + txt, success: function (response) {
                    var json = JSON.parse(response);
                    colorgbJGrowl('bg-green', json.message);
                }
            });
        });

        $('#id').change(function () {
            var val = $(this).val();
            if (val == '2') {
                $('#corcustomer-cust_no').removeClass('disabled');
            } else {
                $('#corcustomer-cust_no').addClass('disabled');
            }
        });
    })
</script>
