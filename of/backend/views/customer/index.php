<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\colorgb\ColorgbHelper;
use common\models\Customer;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CUSTOMERColorgb */
/* @var $dataProvider yii\data\ActiveDataProvider */
$data['status'] = ArrayHelper::map(Yii::$app->params['customerStatus'], 'value', 'txt');
$data['statusHtml'] = ArrayHelper::map(Yii::$app->params['customerStatus'], 'value', 'html');
$data['user'] = ArrayHelper::map(\common\models\User::findAll(['status' => 1]), 'id', 'name');

$this->title = 'Danh sách khách hàng';
$this->params['breadcrumbs'][] = $this->title;
$columns = [

    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '50px', 'header' => 'Xem',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                if (ColorgbHelper::checkPermission('CUSTOMER_VIEW')) $html .= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->getPrimaryKey()], ['title' => 'Xem', 'data-pjax' => '0']);
                return $html;
            },
        ]
    ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '220px',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                if (ColorgbHelper::checkPermission('CUSTOMER_UPDATE')) $html .= Html::a('<span class="icon-pencil7"></span>', ['update', 'id' => $model->getPrimaryKey()], ['title' => 'Sửa', 'class' => 'btn-colorgb-popup', 'data-pjax' => 'false']);
                if (ColorgbHelper::checkPermission('CUSTOMER_APPROVE')) $html .= Html::a('<span class="icon-user-check"></span>', ['approve', 'id' => $model->getPrimaryKey()], ['reload' => 'true', 'title' => 'Duyệt tài khoản ' . $model->CUST_NO . ' - ' . $model->CUST_NAME, 'disabled' => $model->STATUS == 1 ? 'disabled' : false, 'data-pjax' => '1', 'class' => 'btn-colorgb-warning', 'data-pjax-container' => 'colorgb-pjax-pjax']);
                if (ColorgbHelper::checkPermission('CUSTOMER_DEACTIVATE')) $html .= Html::a('<span class="icon-user-block"></span>', ['deactivate', 'id' => $model->getPrimaryKey()], ['reload' => 'true', 'title' => 'Bạn có chắc chắn muốn đóng tài khoản ' . $model->CUST_NO . ' - ' . $model->CUST_NAME, 'disabled' => $model->ACCO_STS == 2 ? 'disabled' : false, 'data-pjax' => '1', 'class' => 'btn-colorgb-warning', 'data-pjax-container' => 'colorgb-pjax-pjax']);
                if (ColorgbHelper::checkPermission('CUSTOMER_VSDCONFIRM')) $html .= Html::a('<span class="icon-file-check"></span>', ['vsd-confirm', 'id' => $model->getPrimaryKey()], ['reload' => 'true', 'title' => 'Bạn có chắc chắn muốn xác nhận VSD tài khoản ' . $model->CUST_NO . ' - ' . $model->CUST_NAME, 'disabled' => $model->VSD_STATUS == 1 ? 'disabled' : false, 'data-pjax' => '1', 'class' => 'btn-colorgb-warning', 'data-pjax-container' => 'colorgb-pjax-pjax']);
                if (ColorgbHelper::checkPermission('CUSTOMER_PRINT')) $html .= Html::a('<span class="icon-printer"></span>', ['print', 'id' => $model->getPrimaryKey()], ['title' => 'In hợp đồng mở tài khoản ' . $model->CUST_NO . ' - ' . $model->CUST_NAME, 'data-pjax' => '1', 'class' => 'btn-colorgb-popup', 'data-pjax-container' => 'colorgb-pjax-pjax']);
                if (ColorgbHelper::checkPermission('CUSTOMER_ORDER')) $html .= Html::a('<span class="icon-cart-add"></span>', ['/order/buy', 'OrderColorgb[CUST_NO]' => $model->CUST_NO], ['target'=>'_blank','title' => 'Đặt lệnh tài khoản ' . $model->CUST_NO . ' - ' . $model->CUST_NAME, 'disabled' => $model->STATUS == 1 ? false : 'disabled','data-pjax' => '0', 'class' => 'btn-colorgb-popup-no', 'data-pjax-container' => 'colorgb-pjax-pjax']);
                if (ColorgbHelper::checkPermission('CUSTOMER_DELETE')) $html .= Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->getPrimaryKey()], ['reload' => 'true', 'title' => 'Bạn có muốn xóa tài khoản ' . $model->CUST_NO . ' - ' . $model->CUST_NAME, 'data-pjax' => 'false', 'class' => 'btn-colorgb-warning', 'disabled' => $model->STATUS == 1 ? 'disabled' : false, 'data-pjax-container' => 'colorgb-pjax-pjax']);
                return $html;
            },
        ],

    ],
    [
        'attribute' => 'CUST_NO',
        'format' => 'raw',
        'value' => function ($model) {
            $h = '';
            $h .= '<p>' . $model->CUST_NO . '</p>';
            $h .= '<p>' . Yii::$app->params['STATUS'][$model->STATUS] . '</p>';
            return $h;
        }
    ],
    [
        'attribute' => 'CUST_NAME',
        'format' => 'raw',
        'value' => function ($model) {
            $h = '';
            $h .= '<p>' . $model->CUST_NAME . '</p>';
            $h .= '<p>' . $model->MOBILE_NO . '</p>';
            $h .= '<p>' . $model->EMAIL_ADRS . '</p>';
            return $h;
        }
    ],
    [
        'attribute' => 'APPROVAL_CD',
        'label' => 'Số CMND/CCCD/HC',
        'format' => 'raw',
        'value' => function ($model) {
            $h = '';
            $h .= '<p>' . $model->ID_NUMBER . '</p>';
            $h .= '<p>' . $model->ISSUE_DATE . '</p>';
            $h .= '<p>' . \common\models\Code::findOne($model->ISSUE_LOCATION_CD)->value1 . '</p>';
            return $h;
        }
    ],
    [
        'attribute' => 'BROKER_CD',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['user'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Lọc...'],
        'value' => function ($model) use ($data) {
            $h = '';
            $h .= '<p>' . $data['user'][$model->BROKER_CD] . '</p>';
            return $h;
        }
    ],
    [
        'attribute' => 'ACCO_STS',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => Yii::$app->params['ACCO_STS'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Lọc...'],
        'value' => function ($model) {
            $h = '';
            $h .= '<p>' . Yii::$app->params['ACCO_STS'][$model->ACCO_STS] . '</p>';
            return $h;
        }
    ],
    [
        'attribute' => 'VSD_STATUS',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => Yii::$app->params['VSD_STATUS'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Lọc...'],
        'value' => function ($model) {
            $h = '';
            $h .= '<p>' . Yii::$app->params['VSD_STATUS'][$model->VSD_STATUS] . '</p>';
            return $h;
        }
    ],

    // 'BIRTHDAY',
    // 'SEX',
    // 'TEL_NO',
    // 'MOBILE_NO',
    // 'EMAIL_ADRS:email',
    // 'EMAIL_FLG:email',
    // 'APPROVAL_CD',
    // 'ID_NUMBER',
    // 'ISSUE_LOCATION_CD',
    // 'ISSUE_DATE',
    // 'EXPIRE_DATE',
    // 'ADDR1',
    // 'ADDR2',
    // 'COUNTRY_CD',
    // 'BROKER_CD',
    // 'JOB',
    // 'POSITION',
    // 'COMPANY',
    // 'COMPANY_TEL',
    // 'ACCO_DIVIDE',
    // 'ACCO_STS',
    // 'OPEN_DATE',
    // 'CLOSE_DATE',
    // 'CONTRACT_NO',
    // 'FATCA',
    // 'TRADING_CODE',
    // 'PRES_NAME',
    // 'PRES_APPROVAL_CD',
    // 'PRES_ID_NUMBER',
    // 'PRES_ISSUE_LOCATION_CD',
    // 'PRES_ISSUE_DATE',
    // 'PRES_TEL_NO',
    // 'PRES_COUNTRY_CD',
    // 'PRES_POSITION',
    // 'BANK_ACCO_NAME',
    // 'BANK_ACCO_NO',
    // 'BANK_NAME',
    // 'BANK_CUSTOMER_NAME',
    // 'TRANSACTION_CD',
    // 'STATUS',
    // 'VSD_STATUS',
    // 'REG_DATE_TIME',
    // 'REG_USER_ID',
    // 'UPD_DATE_TIME',
    // 'UPD_USER_ID',
];

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'colorgb-pjax',
    'showPageSummary' => false,
    'pjax' => true,
    'striped' => true,
    'responsive' => true,
    'responsiveWrap' => false,
    'hover' => true,
    'floatHeader' => true,
    'floatHeaderOptions' => ['top' => 46],
    'toolbar' => [
        'before' => ColorgbHelper::checkPermission('CUSTOMER_CREATE') ? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'class' => 'btn btn-success pull-left', 'title' => 'Thêm mới']) : '',
        'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''], ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Làm mới']),
        '{toggleData}',
    ],
    'panel' => ['type' => 'default', 'heading' => $this->title],
    'columns' => $columns,
]);
?>