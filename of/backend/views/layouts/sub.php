<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
$baseUrl = Yii::$app->params['backendUrl'];
?>
<?php $this->beginPage() ?>
<!--************************************************************-->
<!-- Designed & Developed by COLORGB™  -  http://colorgb.com    -->
<!-- giaphv@colorgb.com, hoangdv@colorgb.com, datnt@colorgb.com -->
<!--************************************************************-->
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <base href="<?= $baseUrl ?>/">
    <link rel="icon" type="image/x-icon" class="js-site-favicon" href="<?= $baseUrl ?>/favicon.ico">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <script>
        var baseUrl = '<?=$baseUrl?>';
        var cdnUrl = '<?=Yii::$app->params['cdnUrl']?>';
    </script>

    <?=$this->render('_main')?>

    <style>
        .pace .pace-progress {
            background: #00a651;
        }
    </style>
</head>
<body>

<!-- Page container -->
<div class="page-container">
    <!-- Page content -->
    <div class="page-content">

        <?php $this->beginBody() ?>

        <!-- Main content -->
        <div class="content-wrapper">
            <?= $content ?>
        </div>
        <!-- /main content -->

        <?php $this->endBody() ?>

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
<?php $this->endPage() ?>
