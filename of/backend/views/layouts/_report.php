<?php
/**
 * Created by PhpStorm.
 * User: giaphv
 * Date: 22/12/2017
 * Time: 4:57 CH
 */
$version = 1.52;
$cdnUrl = Yii::$app->params['cdnUrl'];
?>
<link href="<?= $cdnUrl ?>/bolt/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?= $cdnUrl ?>/bolt/assets/css/core.css" rel="stylesheet" type="text/css">
<link href="<?= $cdnUrl ?>/bolt/assets/css/components.css" rel="stylesheet" type="text/css">
<link href="<?= $cdnUrl ?>/bolt/assets/css/colors.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/core/libraries/jquery.min.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/plugins/visualization/d3/d3.min.js"></script>
<style>
    @import url(<?= $cdnUrl ?> /bolt/assets/fonts/roboto-gh-pages/roboto.css);

    .panel {
        color: #000 !important;
    }

    .text-semibold,
    .table-lg > thead > tr > th,
    b, strong {
        font-weight: bold !important;
    }

    body {
        font-weight: 300;
        background: #fff;
        color: #000 !important;
        font-size: 15px;

    }

    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 5px 20px;
    }

    .colorgb-report .text-center.text-uppercase {
        margin-left: 50px;
        font-weight: bold !important;
    }


</style>