<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\ArrayHelper;
use common\colorgb\ColorgbHelper;
use common\models\UserGroupPermission;

$recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
$baseUrl = Yii::$app->params['backendUrl'];
$cdnUrl = Yii::$app->params['cdnUrl'];
$controller = Yii::$app->controller->id;
$account = Yii::$app->user->identity;
$bodyClass = $controller == 'site' ? '' : ' navbar-top ';
$bodyClass .= (isset($_COOKIE['sidebar-main-toggle']) && $_COOKIE['sidebar-main-toggle'] == '1') ? '' : ' sidebar-xs-2 ';

?>
<?php $this->beginPage() ?>
<!--************************************************************-->
<!-- Designed & Developed by COLORGB™  -  http://colorgb.com    -->
<!-- giaphv@colorgb.com, hoangdv@colorgb.com, datnt@colorgb.com -->
<!--************************************************************-->
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <base href="<?= $baseUrl ?>/">
    <link rel="icon" type="image/x-icon" class="js-site-favicon" href="<?= $baseUrl ?>/favicon.ico">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script>
        var baseUrl = '<?=$baseUrl?>';
        var cdnUrl = '<?=Yii::$app->params['cdnUrl']?>';
    </script>
    <?= $this->render('_main') ?>
</head>
<body class="<?= $bodyClass ?>">

<?php if (!Yii::$app->user->isGuest && $controller != 'site'): ?>
    <!-- Main navbar -->
    <div class="navbar navbar-fixed-top navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="dashboard"><img src="<?= $cdnUrl ?>/bolt/assets/images/logo_light.png" alt=""></a>

            <ul class="nav navbar-nav pull-right visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            </ul>
        </div>

        <div class="navbar-collapse collapse" id="navbar-mobile">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?= Yii::$app->params['homeUrl'] ?>" target="_blank"><i class="icon-home2"></i> Trang chủ <?= Yii::$app->name ?></a></li>

                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <img onerror="this.src='<?= $cdnUrl ?>/bolt/assets/images/image.png'" src="<?= $cdnUrl ?>/bolt/assets/images/img-logo-white.png?colorgb=<?= time() ?>" alt="">
                        <span><?= $account->name ?></span>
                        <i class="caret"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="user/profile"><i class="icon-profile"></i> Thông tin cá nhân</a></li>
                        <li><a href="user/change-password"><i class="icon-lock"></i> Đổi mật khẩu</a></li>
                        <li class="divider"></li>
                        <li><a href="user/logout"><i class="icon-switch2"></i> Đăng xuất</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- /main navbar -->

    <!-- Second navbar -->
    <div class="navbar navbar-default" id="navbar-second">
        <ul class="nav navbar-nav no-border visible-xs-block">
            <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-menu7"></i></a></li>
        </ul>

        <div class="navbar-collapse collapse" id="navbar-second-toggle">
            <ul class="nav navbar-nav">
                <?php if (ColorgbHelper::checkPermission('BRANCH_INDEX')): ?>
                    <li><a href="branch"><i class="icon-git-branch position-left"></i> Đơn vị GD</a></li>
                <?php endif; ?>

                <?php if (ColorgbHelper::checkPermission('FUNDINFO_INDEX')): ?>
                    <li><a href="fund-info"><i class="icon-certificate position-left"></i> CCQ</a></li>
                <?php endif; ?>

                <?php if (ColorgbHelper::checkPermission('CUSTOMER_INDEX')): ?>
                    <li><a href="customer"><i class="icon-users2 position-left"></i> Khách hàng</a></li>
                <?php endif; ?>

                <?php if (ColorgbHelper::checkPermission('FEERATE_INDEX')): ?>
                    <li><a href="fee-rate"><i class="icon-insert-template position-left"></i> Biểu phí GD</a></li>
                <?php endif; ?>

                <?php if (ColorgbHelper::checkPermission('NAV_INDEX')): ?>
                    <li><a href="nav"><i class="icon-coins position-left"></i> NAV</a></li>
                <?php endif; ?>

                <?php if (ColorgbHelper::checkPermission('ORDER_INDEX')): ?>
                    <li><a href="order"><i class="icon-cart5 position-left"></i> Lệnh & Thanh toán</a></li>
                <?php endif; ?>

                <?php if (ColorgbHelper::checkPermission('RHTT_INDEX')): ?>
                    <li><a href="rhtt"><i class="icon-file-locked position-left"></i> Thông tin THQ</a></li>
                <?php endif; ?>
                <?php if (ColorgbHelper::checkPermission('PORTFOLIO_INDEX')): ?>
                    <li><a href="portfolio"><i class="icon-pie-chart8 position-left"></i> Danh mục lãi/lỗ</a></li>
                <?php endif; ?>

            </ul>

            <?php if (Yii::$app->user->identity->is_admin == 1): ?>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="hidden-xs"> Thiết lập hệ thống </span>
                            <i class="icon-cog3"></i>
                            <span class="visible-xs-inline-block position-right"> Thiết lập hệ thống </span>
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">

                            <li><a href="admin/code"><i class="icon-circle-code"></i> <span>Quản lý bảng mã</span></a></li>
                            <li><a href="admin/setting"><i class="icon-cog2"></i> <span>Tham số hệ thống</span></a></li>
                            <li><a href="admin/msg-template"><i class="icon-insert-template"></i> <span>Mẫu nội dung SMS/Email</span></a>
                                <hr/>
                            </li>

                            <li><a href="admin/user"><i class="icon-user-tie"></i> <span>Quản lý người dùng</span></a></li>
                            <li><a href="admin/user-group"><i class="fa fa-users"></i> <span>Quản lý nhóm người dùng</span></a></li>
                            <li><a href="admin/customer-group"><i class="icon-users"></i> <span>Quản lý nhóm khách hàng</span></a>
                                <hr>
                            </li>

                            <li><a href="admin/permission"><i class="icon-file-locked"></i> <span>Khai báo quyền truy cập</span></a></li>
                            <li><a href="admin/permission/set"><i class="icon-user-lock"></i> <span>Thiết lập phân quyền truy cập</span></a>
                                <hr/>
                            </li>

                            <li><a href="admin/msg-send-request"><i class="icon-git-pull-request"></i> <span>Nhật ký gửi SMS/Email</span></a></li>
                            <li><a href="admin/system-log"><i class="icon-history"></i> <span>Nhật ký thao tác hệ thống</span></a></li>

                        </ul>
                    </li>

                </ul>

            <?php endif; ?>
        </div>
    </div>
    <!-- /second navbar -->

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-anchor position-left"></i>
                    <?php if (!empty($this->params['breadcrumbs']) && !empty($this->params['breadcrumbs'][0]['label'])): ?><span class="text-semibold"><?= $this->params['breadcrumbs'][0]['label'] ?></span> - <?php endif; ?>
                    <?= Html::encode($this->title) ?></h4>
                <div class="breadcrumb-caret position-right">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        'homeLink' => ['label' => 'Bảng thông tin', 'url' => 'dashboard'],
                        'encodeLabels' => false

                    ]) ?>
                </div>
            </div>


        </div>
    </div>
    <!-- /page header -->
<?php else: ?>
    <style>
        .login-container .footer a {
            background: #fff;
            padding: 1px 2px !important;
        }
    </style>
<?php endif; ?>

<!-- Page container -->
<div class="page-container <?= ($controller == 'site') ? 'login-container' : '' ?>">
    <!-- Page content -->
    <div class="page-content">

        <?php $this->beginBody() ?>

        <!-- Main content -->
        <div class="content-wrapper">
            <?= $content ?>
        </div>
        <!-- /main content -->

        <?php $this->endBody() ?>

    </div>
    <!-- /page content -->
    <!-- Footer -->
    <div class="footer text-muted">
        &copy; <?= date('Y') ?> <span>Developed by <a style="font-weight: bold; letter-spacing: 1px; color: #777; padding: 1px 0; text-decoration: none; font-family: Arial, sans-serif;" title="COLORGB™" href="http://colorgb.com" target="_blank" rel="noopener noreferrer"><strong style="color: #777; display: inline-block;">COLO</strong><strong style="color: #e74c3c; display: inline-block;">R</strong><strong style="color: #2ecc71; display: inline-block;">G</strong><strong style="color: #3498db; display: inline-block;">B</strong>™</a> </span>
    </div>
    <!-- /footer -->

</div>
<!-- /page container -->
<div class="modal" id="colorgbIframeModal" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="model-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <iframe width="100%" height="100%" frameborder="0"></iframe>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php if(Yii::$app->session->getFlash('colorgbJGrowl')): ?>
<script>
    $(function () {
        colorgbJGrowl('bg-warning', '<?=Yii::$app->session->getFlash('colorgbJGrowl')?>');

    })
</script>
<?php endif; ?>

</body>
</html>
<?php $this->endPage() ?>
