<?php
/**
 * Created by PhpStorm.
 * User: giaphv
 * Date: 22/12/2017
 * Time: 4:57 CH
 */
$version = 1.91;
$cdnUrl = Yii::$app->params['cdnUrl'];
?>
<link href="<?= $cdnUrl ?>/bolt/assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
<link href="<?= $cdnUrl ?>/bolt/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
<link href="<?= $cdnUrl ?>/bolt/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?= $cdnUrl ?>/bolt/assets/css/core.css" rel="stylesheet" type="text/css">
<link href="<?= $cdnUrl ?>/bolt/assets/css/components.css" rel="stylesheet" type="text/css">
<link href="<?= $cdnUrl ?>/bolt/assets/css/colors.css" rel="stylesheet" type="text/css">
<link href="<?= $cdnUrl ?>/bolt/assets/css/extras/animate.min.css" rel="stylesheet" type="text/css">
<link href="<?= $cdnUrl ?>/bolt/assets/js/plugins/gijgo/gijgo.min.css" rel="stylesheet" type="text/css">
<link href="<?= $cdnUrl ?>/bolt/assets/css/main.css?colorgb=<?= $version ?>" rel="stylesheet" type="text/css">
<link href="<?= $cdnUrl ?>/bolt/assets/css/colorgb.min.css?colorgb=<?= rand() ?>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/core/libraries/jquery.min.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/core/libraries/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/plugins/ui/nicescroll.min.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/plugins/ui/drilldown.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/plugins/loaders/pace.min.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/plugins/loaders/blockui.min.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/plugins/ui/prism.min.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/plugins/buttons/spin.min.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/plugins/buttons/ladda.min.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/pages/components_buttons.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/plugins/forms/inputs/touchspin.min.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/plugins/jquery-number-master/jquery.number.min.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/plugins/forms/selects/select2.min.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/plugins/forms/styling/switchery.min.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/plugins/forms/styling/switch.min.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/pages/form_select2.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/pages/form_checkboxes_radios.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/plugins/visualization/d3/d3.min.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/plugins/notifications/jgrowl.min.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/plugins/gijgo/gijgo.min.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/core/app.js"></script>
<script type="text/javascript" src="<?= $cdnUrl ?>/bolt/assets/js/main.js?colorgb=<?= $version ?>"></script>