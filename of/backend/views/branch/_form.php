<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use common\models\Customer;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */


$data['location'] = ArrayHelper::map(\common\models\Code::findAll(['group'=>'LOCATION']),'id','value1');
$data['status'] = ArrayHelper::map(Yii::$app->params['branchStatus'],'value','txt');
?>

<div class="colorgb-container">
    <?php $form = ActiveForm::begin(); ?>

    <?php if ($model->getErrors()): ?>
        <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
            <?= json_encode($model->getErrors(),JSON_UNESCAPED_UNICODE) ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-sm-12">

            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'TRANSACTION_CD')->textInput(['maxlength' => true]) ?>

                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'LOCATION_CD')->dropDownList($data['location'],['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'BRANCH_NAME')->textInput(['maxlength' => true]) ?>

                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'BRANCH_ADRS')->textInput(['maxlength' => true]) ?>

                </div>
            </div>


            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'TEL_NO')->textInput(['maxlength' => true]) ?>

                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'FAX_NO')->textInput(['maxlength' => true]) ?>

                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'EMAIL_ADRS')->textInput(['maxlength' => true]) ?>

                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'DEPUTY')->textInput(['maxlength' => true]) ?>

                </div>
            </div>


            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'STATUS')->dropDownList($data['status'], ['class' => 'disabled']) ?>


                </div>
                <div class="col-sm-6">

                    <?= $form->field($model, 'REMARKS')->textInput() ?>

                </div>
            </div>

        </div>

    </div>

    <div class="hide">
        <?= $form->field($model, 'UPD_DATE_TIME')->textInput() ?>
    </div>

    <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Tạo mới</span>' : '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Lưu lại</span>', ['class' => $model->isNewRecord ? 'btn btn-success  btn-ladda btn-ladda-spinner btn-ladda-progress' : 'btn btn-primary  btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
