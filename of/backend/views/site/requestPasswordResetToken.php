<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Yêu cầu đặt lại mật khẩu';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin() ?>
<a title="Passion Investment - Trang chủ" href="<?= Yii::$app->params['frontendUrl']; ?>" class="site-logo">
    <img src="<?= Yii::$app->params['cdnUrl'] ?>/bolt/assets/images/logo-white.png" alt="">
</a>

<div class="panel panel-body login-form">
    <div class="text-center">
        <h2 class="content-group"><?= $this->title ?>
            <small class="display-block">Vui lòng điền vào email của bạn. <br> Một liên kết để đặt lại mật khẩu sẽ được gửi tới đó</small>
        </h2>
    </div>

    <?php if (Yii::$app->session->getFlash('success')): ?>
        <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
            <?= Yii::$app->session->getFlash('success'); ?>
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->getFlash('error')): ?>
        <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
            <?= Yii::$app->session->getFlash('error'); ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model, 'email_addr')->textInput() ?>

    <div class="form-group">
        <button type="submit" class="display-block btn btn-primary btn-ladda btn-ladda-spinner btn-ladda-progress" data-style="zoom-out">
            <span class="ladda-label">Gửi yêu cầu</span>
            <i class="icon-circle-right2 position-right"></i>
        </button>
    </div>

    <div class="text-center">
        <a href="site/login">Đăng nhập hệ thống</a>
    </div>

</div>
<?php ActiveForm::end(); ?>
