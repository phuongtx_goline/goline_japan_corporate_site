<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\tinymce\TinyMce;

$this->title = 'Thiết lập cá nhân';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title"><?= $this->title ?></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">
        <div class="row">
            <div class="col-sm-4">
                <a href="user/profile" class="btn btn-block btn-float btn-default btn-float-lg">
                    <i class="icon-profile"></i> <span>Thông tin cá nhân</span>
                </a>

            </div>
            <div class="col-sm-4">
                <a href="user/change-password" class="btn btn-block btn-float  btn-default btn-float-lg">
                    <i class="icon-lock"></i> <span>Đổi mật khẩu</span>
                </a>

            </div>
            <div class="col-sm-4">
                <a href="user/logout" class="btn btn-block btn-float  btn-default btn-float-lg">
                    <i class="icon-switch2"></i> <span>Đăng xuất</span>
                </a>

            </div>
        </div>
    </div>
</div>

