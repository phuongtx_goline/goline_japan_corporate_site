<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use common\models\Customer;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
$data['SEC_CD'] = [];
foreach (\common\models\CORTFUNDINFO::findAll(['status' => 1]) as $v) {
    $data['SEC_CD'][$v->SEC_CD] = $v->SEC_CD . ' - ' . $v->SEC_NAME;
}

?>
<?php if ($model->getErrors()): ?>
    <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
        <?= json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE) ?>
    </div>
<?php endif; ?>

<?php if (Yii::$app->session->getFlash('success')): ?>
    <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
        <?= Yii::$app->session->getFlash('success'); ?>
    </div>
<?php endif; ?>

<?php if (Yii::$app->session->getFlash('error')): ?>
    <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
        <?= Yii::$app->session->getFlash('error'); ?>
    </div>
<?php endif; ?>

<?php if (Yii::$app->session->getFlash('warning')): ?>
    <div class="alert alert-warning alert-styled-left alert-arrow-left alert-bordered">
        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
        <?= Yii::$app->session->getFlash('warning'); ?>
    </div>
<?php endif; ?>

<div class="colorgb-container bg bg-green-600">
    <?php $form = ActiveForm::begin(); ?>


    <div class="row">
        <div class="col-sm-2">
            <?= $form->field($model, 'acc')->dropDownList($data['CUST_CD'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>
        </div>
        <div class="col-sm-2">
            <?= $form->field($model, 'ccq')->dropDownList($data['SEC_CD'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>
        </div>
        <div class="col-sm-2">
            <?= $form->field($model, 'amount', [
                'template' => '{label}<div class="row"><div class="col-sm-12"><div class="input-group">{input}<span class="input-group-addon">đ</span> </div></div></div>',
            ])->textInput([
                'class' => 'price form-control',

            ]) ?>
        </div>
        <div class="col-sm-2">
            <?= $form->field($model, 'date')->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'displayFormat' => 'dd/MM/yyyy',
                'saveFormat' => 'yyyyMMdd',
                'ajaxConversion' => false,
                'autoWidget' => true,
            ])
            ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'nav', [
                'template' => '{label}<div class="row"><div class="col-sm-12"><div class="input-group">{input}<span class="input-group-addon">đ</span> </div></div></div>',
            ])->textInput([
                'class' => 'price form-control',
                'disabled' => 'disabled',


            ]) ?>

        </div>
        <div class="col-sm-12">
            <div class="btn-group">
                <?= Html::submitButton('<i class="icon-cart-add2 position-left"></i> <span class="ladda-label">Mua</span>', ['class' => 'btn  bg-orange-400 btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out']) ?>
                <button type="button" class="btn btn-ccq btn-default">Vấn tin</button>
                <button type="reset" class="btn btn-default">Bỏ qua</button>
            </div>

        </div>

    </div>


    <?php ActiveForm::end(); ?>

</div>
<script>


    function f1() {
        $('.table-ccq tbody').html('');
        $.ajax({
            url: baseUrl + '/ajax/get-customer-ccq?cd=<?=$c->CUST_CD?>', success: function (response) {
                $('.table-ccq tbody').html(response);
                f2($('#buyform-ccq').val());
            }
        });

    }

    function f2(cd){
        $.ajax({
            method: 'get',
            data: {
                ccd: $('#buyform-acc').val(),
                scd: cd,
            },
            url: baseUrl + '/ajax/get-customer-ccq-json', success: function (response) {
                $('#buyform-amount').attr('max', response);
                $('tr').removeClass('active');
                $('tr[data-ccq="' + cd + '"]').addClass('active');

            }
        });

    }

    $(function () {

        f1();
        $('.btn-ccq').click(f1);
        $('#buyform-acc').change(f1);

        $('#buyform-ccq').change(function () {
            var cd = $(this).val();

            $.ajax({
                method: 'get',
                data: {
                    cd: cd,
                },
                url: baseUrl + '/ajax/get-nav', success: function (response) {
                    $('#buyform-nav').val(response);
                }
            });

            f2(cd);

        });

        f2($('#buyform-ccq').val());


    });
</script>
