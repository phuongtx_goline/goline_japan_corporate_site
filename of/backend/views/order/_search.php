<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MsttbranchColorgb */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="msttbranch-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

        <?= $form->field($model, 'TRANSACTION_CD') ?>

    <?= $form->field($model, 'LOCATION_CD') ?>

    <?= $form->field($model, 'BRANCH_NAME') ?>

    <?= $form->field($model, 'BRANCH_ADRS') ?>

    <?= $form->field($model, 'TEL_NO') ?>

    <?php // echo $form->field($model, 'FAX_NO') ?>

    <?php // echo $form->field($model, 'EMAIL_ADRS') ?>

    <?php // echo $form->field($model, 'DEPUTY') ?>

    <?php // echo $form->field($model, 'STATUS') ?>

    <?php // echo $form->field($model, 'REMARKS') ?>

    <?php // echo $form->field($model, 'REG_DATE_TIME') ?>

    <?php // echo $form->field($model, 'REG_USER_ID') ?>

    <?php // echo $form->field($model, 'UPD_DATE_TIME') ?>

    <?php // echo $form->field($model, 'UPD_USER_ID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
