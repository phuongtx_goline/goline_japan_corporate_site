<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\colorgb\ColorgbHelper;
use common\models\Customer;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FEERATEColorgb */
/* @var $dataProvider yii\data\ActiveDataProvider */
$data['SEC_CD'] = ArrayHelper::map(\common\models\CORTFUNDINFO::findAll(['status' => 1]), 'SEC_CD', 'SEC_NAME');
$data['CUST_NO'] = ArrayHelper::map(\common\models\CORCUSTOMER::findAll(['status' => 1]), 'CUST_NO', 'CUST_NAME');
$data['status'] = ArrayHelper::map(Yii::$app->params['orderStatus'], 'value', 'txt');
$data['statusHtml'] = ArrayHelper::map(Yii::$app->params['orderStatus'], 'value', 'html');

$this->title = 'Danh sách đặt lệnh';
$this->params['breadcrumbs'][] = $this->title;
$columns = [

    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '50px', 'header' => 'Xem',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                if (ColorgbHelper::checkPermission('ORDER_VIEW')) $html .= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->getPrimaryKey()], ['title' => 'Xem', 'data-pjax' => '0']);
                return $html;
            },
        ]
    ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '150px',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                if (ColorgbHelper::checkPermission('ORDER_UPDATE')) $html .= Html::a('<span class="icon-pencil7"></span>', ['update', 'id' => $model->getPrimaryKey()], ['title' => 'Sửa', 'class' => 'btn-colorgb-popup', 'data-pjax' => 'false']);
                if (ColorgbHelper::checkPermission('ORDER_DELETE')) $html .= Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->getPrimaryKey()], ['title' => 'Xóa', 'data-pjax' => 'false', 'disabled' => $model->STATUS == 1 ? 'disabled' : false, 'class' => 'colorgb-pjax-action-del colorgb-pjax-action-del-show', 'data-pjax-container' => 'colorgb-pjax-pjax']);
                return $html;
            },
        ],

    ],

    [
        'attribute' => 'STATUS',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['status'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Lọc...'],
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $array = $data['statusHtml'];
            $value = !empty($array[$model->STATUS]) ? $array[$model->STATUS] : '';
            return $value;
        },
    ],
    [
        'attribute' => 'CUST_NO',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['CUST_NO'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Lọc...'],
        'value' => function ($model) use ($data) {

            return $data['CUST_NO'][$model->CUST_NO];
        }

    ],
    [
        'attribute' => 'SEC_CD',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['SEC_CD'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Lọc...'],
        'value' => function ($model) use ($data) {
            $h = '';
            $h .= '<p>' . Yii::$app->params['TRADE_TYPE'][$model->TRADE_TYPE] . '</p>';
            $h .= '<p>' . $data['SEC_CD'][$model->SEC_CD] . '</p>';
            return $h;
        }

    ],
    [
        'attribute' => 'TRADE_DATE',
        'format' => 'raw',
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
            'options' => [
                'autocomplete' => 'off',
            ],
            'pluginOptions' => [
                'locale' => [
                    'format' => 'DD/MM/YYYY',
                    'separator' => ' - ',
                ],
                'ranges' => [
                    //"Hôm nay" => ["moment().startOf('day')", "moment()"],
                    "Hôm qua" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                    "Tuần trước" => ["moment().startOf('day').subtract(7, 'days')", "moment()"],
                    "Tháng trước" => ["moment().startOf('day').subtract(31, 'days')", "moment()"],
                ],
                'alwaysShowCalendars' => true,

            ],
        ],
        'value' => function ($model) use ($data) {

            return ColorgbHelper::date($model->TRADE_DATE);
        }

    ],
    [
        'label' => 'Đặt',
        'format' => 'raw',
        'mergeHeader' => true,
        'value' => function ($model) use ($data) {
            $n = $model->ORD_AMT ? $model->ORD_AMT : $model->ORD_QTY;
            return number_format($n);
        }

    ],
    [
        'label' => 'NAV',
        'format' => 'raw',
        'mergeHeader' => true,
        'value' => function ($model) use ($data) {
            $n = $model->MAT_PRICE;
            return number_format($n);
        }

    ],
    [
        'label' => 'Khớp',
        'format' => 'raw',
        'mergeHeader' => true,
        'value' => function ($model) use ($data) {
            $h = '';
            $h .= '<p>' . number_format($model->MAT_AMT) . '</p>';
            $h .= '<p>' . number_format($model->MAT_QTY) . '</p>';
            return $h;
        }

    ],
    [
        'label' => 'Phí GD',
        'format' => 'raw',
        'mergeHeader' => true,
        'value' => function ($model) use ($data) {
            $h = '';
            $h .= '<p>' . ($model->FEE_RATE) . '</p>';
            $h .= '<p>' . ($model->FEE_AMT) . '</p>';
            return $h;
        }

    ],
    [
        'label' => 'Thuế TNCN',
        'mergeHeader' => true,
        'format' => 'raw',
        'value' => function ($model) use ($data) {
            $h = '';
            $h .= '<p>' . ($model->TAX_RATE) . '</p>';
            $h .= '<p>' . ($model->TAX_AMT) . '</p>';
            return $h;
        }

    ],
];
?>

<?php if ($cn = Yii::$app->request->get('OrderColorgb')['CUST_NO']): $c = \common\models\CORCUSTOMER::findOne(['CUST_NO'=>$cn]) ?>
    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Thông tin tài khoản <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table">

                        <tbody>
                        <tr>
                            <td class="text-bold">Họ và tên</td>
                            <td><?=$c->CUST_NAME?></td>
                        </tr>
                        <tr>
                            <td class="text-bold">Số CMND/CCCD/HC</td>
                            <td><?=$c->ID_NUMBER?></td>
                        </tr>
                        <tr>
                            <td class="text-bold">Di động</td>
                            <td><?=$c->TEL_NO?></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><a href="customer/view?id=<?=$c->primaryKey?>" class="btn-colorgb-popup">Chi tiết</a> - <a class="btn-colorgb-popup" href="customer/sing?id=<?=$c->primaryKey?>" class="btn-colorgb-popup">Chữ ký</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Số dư  CCQ<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-ccq">
                        <thead>
                        <tr>
                            <th>Mã CCQ</th>
                            <th>Số dư</th>
                            <th>Có thể bán</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="panel panel-flat">

                <div class="panel-body">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#basic-tab1" data-toggle="tab">Mua</a></li>
                            <li><a href="#basic-tab2" data-toggle="tab">Bán</a></li>

                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="basic-tab1">
                                <?=$this->render('_buy',['model'=>$model])?>

                            </div>

                            <div class="tab-pane" id="basic-tab2">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <script>
        function f1() {
            $.ajax({
                url: baseUrl + '/ajax/get-customer-ccq?cd=<?=$c->CUST_CD?>', success: function (response) {
                    $('.table-ccq tbody').html(response);
                }
            });

        }

        $(function () {
            f1();
            $('.btn-ccq').click(f1);
        })
    </script>

<?php endif; ?>
<?php
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'colorgb-pjax',
    'showPageSummary' => false,
    'pjax' => true,
    'striped' => true,
    'responsive' => false,
    'responsiveWrap' => false,
    'hover' => true,
    'floatHeader' => true,
    'floatHeaderOptions' => ['top' => 46],
    'toolbar' => [
        'before' => '',//(ColorgbHelper::checkPermission('ORDER_CREATE') ? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'class' => 'btn btn-success pull-left', 'title' => 'Thêm mới']) : ''),
        'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index' . (Yii::$app->request->get('OrderColorgb')['CUST_NO'] ? '?OrderColorgb[CUST_NO]=' . Yii::$app->request->get('OrderColorgb')['CUST_NO'] : '')], ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Làm mới']),
        '{toggleData}',
    ],
    'panel' => ['type' => 'default', 'heading' => $this->title],
    'columns' => $columns,
]);
?>