<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\colorgb\ColorgbHelper;
use common\models\Customer;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FEERATEColorgb */
/* @var $dataProvider yii\data\ActiveDataProvider */
$data['SEC_CD'] = ArrayHelper::map(\common\models\CORTFUNDINFO::findAll(['status' => 1]), 'SEC_CD', 'SEC_NAME');
$data['CUST_NO'] = ArrayHelper::map(\common\models\CORCUSTOMER::findAll(['status' => 1]), 'CUST_NO', 'CUST_NAME');
$data['status'] = ArrayHelper::map(Yii::$app->params['orderStatus'], 'value', 'txt');
$data['statusHtml'] = ArrayHelper::map(Yii::$app->params['orderStatus'], 'value', 'html');

$this->title = 'Danh sách đặt lệnh';
$this->params['breadcrumbs'][] = $this->title;
$columns = [

    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '50px', 'header' => 'Xem',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                if (ColorgbHelper::checkPermission('ORDER_VIEW')) $html .= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->getPrimaryKey()], ['title' => 'Xem', 'data-pjax' => '0']);
                return $html;
            },
        ]
    ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '150px',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                if (ColorgbHelper::checkPermission('ORDER_UPDATE')) $html .= Html::a('<span class="icon-pencil7"></span>', ['update', 'id' => $model->getPrimaryKey()], ['title' => 'Sửa', 'class' => 'btn-colorgb-popup', 'data-pjax' => 'false']);
                if (ColorgbHelper::checkPermission('ORDER_DELETE')) $html .= Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->getPrimaryKey()], ['title' => 'Xóa', 'data-pjax' => 'false', 'disabled' => $model->STATUS == 1 ? 'disabled' : false, 'class' => 'colorgb-pjax-action-del colorgb-pjax-action-del-show', 'data-pjax-container' => 'colorgb-pjax-pjax']);
                return $html;
            },
        ],

    ],

    [
        'attribute' => 'STATUS',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['status'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Lọc...'],
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $array = $data['statusHtml'];
            $value = !empty($array[$model->STATUS]) ? $array[$model->STATUS] : '';
            return $value;
        },
    ],
    [
        'attribute' => 'CUST_NO',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['CUST_NO'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Lọc...'],
        'value' => function ($model) use ($data) {

            return $data['CUST_NO'][$model->CUST_NO];
        }

    ],
    [
        'attribute' => 'SEC_CD',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['SEC_CD'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Lọc...'],
        'value' => function ($model) use ($data) {
            $h = '';
            $h .= '<p class="' . ($model->TRADE_TYPE == 1 ? 'text-green-600' : 'text-warning') . '">' . Yii::$app->params['TRADE_TYPE'][$model->TRADE_TYPE] . '</p>';
            $h .= '<p>' . $data['SEC_CD'][$model->SEC_CD] . '</p>';
            return $h;
        }

    ],
    [
        'attribute' => 'TRADE_DATE',
        'format' => 'raw',
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
            'options' => [
                'autocomplete' => 'off',
            ],
            'pluginOptions' => [
                'locale' => [
                    'format' => 'DD/MM/YYYY',
                    'separator' => ' - ',
                ],
                'ranges' => [
                    //"Hôm nay" => ["moment().startOf('day')", "moment()"],
                    "Hôm qua" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                    "Tuần trước" => ["moment().startOf('day').subtract(7, 'days')", "moment()"],
                    "Tháng trước" => ["moment().startOf('day').subtract(31, 'days')", "moment()"],
                ],
                'alwaysShowCalendars' => true,

            ],
        ],
        'value' => function ($model) use ($data) {

            return ColorgbHelper::date($model->TRADE_DATE);
        }

    ],
    [
        'label' => 'Đặt',
        'format' => 'raw',
        'mergeHeader' => true,
        'value' => function ($model) use ($data) {
            $n = $model->ORD_AMT ? $model->ORD_AMT : $model->ORD_QTY;
            return number_format($n);
        }

    ],
    [
        'label' => 'NAV',
        'format' => 'raw',
        'mergeHeader' => true,
        'value' => function ($model) use ($data) {
            $n = $model->MAT_PRICE;
            return number_format($n);
        }

    ],
    [
        'label' => 'Khớp',
        'format' => 'raw',
        'mergeHeader' => true,
        'value' => function ($model) use ($data) {
            $h = '';
            $h .= '<p>' . number_format($model->MAT_AMT) . '</p>';
            $h .= '<p>' . number_format($model->MAT_QTY) . '</p>';
            return $h;
        }

    ],
    [
        'label' => 'Phí GD',
        'format' => 'raw',
        'mergeHeader' => true,
        'value' => function ($model) use ($data) {
            $h = '';
            $h .= '<p>' . ($model->FEE_RATE) . '</p>';
            $h .= '<p>' . ($model->FEE_AMT) . '</p>';
            return $h;
        }

    ],
    [
        'label' => 'Thuế TNCN',
        'mergeHeader' => true,
        'format' => 'raw',
        'value' => function ($model) use ($data) {
            $h = '';
            $h .= '<p>' . ($model->TAX_RATE) . '</p>';
            $h .= '<p>' . ($model->TAX_AMT) . '</p>';
            return $h;
        }

    ],
];
?>
<?php
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'colorgb-pjax',
    'showPageSummary' => false,
    'pjax' => true,
    'striped' => true,
    'responsive' => false,
    'responsiveWrap' => false,
    'hover' => true,
    'floatHeader' => true,
    'floatHeaderOptions' => ['top' => 46],
    'toolbar' => [
        'before' => (ColorgbHelper::checkPermission('ORDER_CHECKOUT') ? Html::a('<i class="icon-cash3"></i> Thanh toán', ['checkout'], ['data-pjax' => 0, 'class' => 'btn btn-success pull-left', 'title' => 'Thanh toán']) : ''),
        'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index' . (Yii::$app->request->get('OrderColorgb')['CUST_NO'] ? '?OrderColorgb[CUST_NO]=' . Yii::$app->request->get('OrderColorgb')['CUST_NO'] : '')], ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Làm mới']),
        '{toggleData}',
    ],
    'panel' => ['type' => 'default', 'heading' => $this->title],
    'columns' => $columns,
]);
?>