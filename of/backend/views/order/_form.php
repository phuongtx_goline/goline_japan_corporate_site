<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use common\models\Customer;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */

$data['SEC_CD'] = ArrayHelper::map(\common\models\CORTFUNDINFO::findAll(['status' => 1]), 'SEC_CD', 'SEC_NAME');
?>

<div class="colorgb-container">
    <?php $form = ActiveForm::begin(); ?>

    <?php if ($model->getErrors()): ?>
        <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
            <?= json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE) ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'ALLO_DATE')->textInput() ?>

            <?= $form->field($model, 'TRADE_DATE')->textInput() ?>

            <?= $form->field($model, 'ORDER_NO')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'TRADE_TIME')->textInput() ?>

            <?= $form->field($model, 'ORD_TYPE')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'ORD_CHANEL')->textInput() ?>

            <?= $form->field($model, 'SEC_CD')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'TRADE_TYPE')->textInput() ?>

            <?= $form->field($model, 'ORD_QTY')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'ORD_AMT')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'CUST_CD')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'CUST_NO')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'CUST_NAME')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'CUST_TRANSACTION_CD')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'FEE_RATE')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'FEE_AMT')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'TAX_RATE')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'TAX_AMT')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'RIGHT_TAX_RATE')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'RIGHT_TAX_QTY')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'RIGHT_TAX_AMT')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'MAT_QTY')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'MAT_PRICE')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'MAT_AMT')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'NOTES')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'TRANSACTION_CD')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'BROKER_CD')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'PAYMENT_DATE')->textInput() ?>

            <?= $form->field($model, 'ENTRY_DATE')->textInput() ?>

            <?= $form->field($model, 'STATUS')->textInput() ?>

        </div>

    </div>


    <div class="hide">
        <?= $form->field($model, 'UPD_DATE_TIME')->textInput() ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Tạo mới</span>' : '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Lưu lại</span>', ['class' => $model->isNewRecord ? 'btn btn-success  btn-ladda btn-ladda-spinner btn-ladda-progress' : 'btn btn-primary  btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
