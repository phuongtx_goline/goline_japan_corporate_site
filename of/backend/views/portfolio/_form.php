<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use common\models\Customer;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */

$data['SEC_CD'] = ArrayHelper::map(\common\models\CORTFUNDINFO::findAll(['status' => 1]), 'SEC_CD', 'SEC_NAME');
?>

<div class="colorgb-container">
    <?php $form = ActiveForm::begin(); ?>

    <?php if ($model->getErrors()): ?>
        <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
            <?= json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE) ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'SEC_CD')->dropDownList($data['SEC_CD'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'RIGHT_TYPE')->dropDownList(Yii::$app->params['RIGHT_TYPE'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>
        </div>
    </div>

    <div class="row">

        <div class="col-sm-6">
            <?= $form->field($model, 'OWNER_FIX_DATE')->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'displayFormat' => 'dd/MM/yyyy',
                'saveFormat' => 'yyyyMMdd',
                'ajaxConversion' => false,
                'autoWidget' => true,
            ])
            ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'NO_RIGHT_DATE')->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'displayFormat' => 'dd/MM/yyyy',
                'saveFormat' => 'yyyyMMdd',
                'ajaxConversion' => false,
                'autoWidget' => true,
            ])
            ?>
        </div>
    </div>

    <div class="row">

        <div class="col-sm-6">
            <?= $form->field($model, 'EXPECTED_EXC_DATE')->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'displayFormat' => 'dd/MM/yyyy',
                'saveFormat' => 'yyyyMMdd',
                'ajaxConversion' => false,
                'autoWidget' => true,
            ])
            ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'RIGHT_EXC_DATE')->widget(DateControl::classname(), [
                'type' => DateControl::FORMAT_DATE,
                'displayFormat' => 'dd/MM/yyyy',
                'saveFormat' => 'yyyyMMdd',
                'ajaxConversion' => false,
                'autoWidget' => true,
            ])
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">

            <?= $form->field($model, 'REMARKS')->textInput() ?>

        </div>

    </div>

    <div class="type">

        <h6>Chi tiết <span></span></h6>
        <hr>

        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'RATES')->textInput(['class'=>'form-control required']) ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'RATED')->textInput(['class'=>'form-control required']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'ROUNDTYPE')->dropDownList(Yii::$app->params['ROUNDTYPE'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'DECIMAL_PLACE')->dropDownList(Yii::$app->params['DECIMAL_PLACE'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'ROUND_PRICE')->textInput(['class'=>'form-control']) ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'ODD_DECIMAL_PLACE')->textInput(['class'=>'form-control']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'TAX_RATE')->textInput(['class'=>'form-control']) ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'BEGIN_TRADE_DATE')->widget(DateControl::classname(), [
                    'type' => DateControl::FORMAT_DATE,
                    'displayFormat' => 'dd/MM/yyyy',
                    'saveFormat' => 'yyyyMMdd',
                    'ajaxConversion' => false,
                    'autoWidget' => true,
                ])
                ?>
            </div>
        </div>

    </div>


    <div class="hide">
        <?= $form->field($model, 'UPD_DATE_TIME')->textInput() ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Tạo mới</span>' : '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Lưu lại</span>', ['class' => $model->isNewRecord ? 'btn btn-success  btn-ladda btn-ladda-spinner btn-ladda-progress' : 'btn btn-primary  btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
