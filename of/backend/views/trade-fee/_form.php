<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use common\models\Customer;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */

$data['TRANSACTION_CD'] = ArrayHelper::map(\common\models\MSTTBRANCH::findAll(['status' => 1]), 'TRANSACTION_CD', 'BRANCH_NAME');
$data['FEE_RATE'] = ArrayHelper::map(\common\models\ORDTTRADEFEERATE::findAll(['status' => 1]), 'ID', 'FEE_RATE_CODE');
?>

<div class="colorgb-container">
    <?php $form = ActiveForm::begin(); ?>

    <?php if ($model->getErrors()): ?>
        <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
            <?= json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE) ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-sm-6">

            <?= $form->field($model, 'TRANSACTION_CD')->dropDownList($data['TRANSACTION_CD'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

            <?= $form->field($model, 'CUST_TYPE')->dropDownList(Yii::$app->params['CUST_TYPE'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

            <?= $form->field($model, 'FOREIGN_TYPE')->dropDownList(Yii::$app->params['FOREIGN_TYPE'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

            <?= $form->field($model, 'TRADE_TYPE')->dropDownList(Yii::$app->params['TRADE_TYPE'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

            <?= $form->field($model, 'FEE_RATE_ID')->dropDownList($data['FEE_RATE'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

        </div>

    </div>


    <div class="hide">
        <?= $form->field($model, 'UPD_DATE_TIME')->textInput() ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Tạo mới</span>' : '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Lưu lại</span>', ['class' => $model->isNewRecord ? 'btn btn-success  btn-ladda btn-ladda-spinner btn-ladda-progress' : 'btn btn-primary  btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
