<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\colorgb\ColorgbHelper;
use common\models\Customer;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FEERATEColorgb */
/* @var $dataProvider yii\data\ActiveDataProvider */
$data['TRANSACTION_CD'] = ArrayHelper::map(\common\models\MSTTBRANCH::findAll(['status' => 1]), 'TRANSACTION_CD', 'BRANCH_NAME');
$data['FEE_RATE'] = ArrayHelper::map(\common\models\ORDTTRADEFEERATE::findAll(['status' => 1]), 'ID', 'FEE_RATE_CODE');
$data['status'] = ArrayHelper::map(Yii::$app->params['commonStatus'], 'value', 'txt');
$data['statusHtml'] = ArrayHelper::map(Yii::$app->params['commonStatus'], 'value', 'html');

$this->title = 'Phạm vi áp dụng biểu phí giao dịch';
$this->params['breadcrumbs'][] = $this->title;
$columns = [

    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '50px', 'header' => 'Xem',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                if (ColorgbHelper::checkPermission('TRADEFEE_VIEW')) $html .= Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->getPrimaryKey()], ['title' => 'Xem', 'data-pjax' => '0']);
                return $html;
            },
        ]
    ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '150px',
        'template' => '{colorgb_button}',
        'buttons' => [
            'colorgb_button' => function ($url, $model, $key) {
                $html = '';
                if (ColorgbHelper::checkPermission('TRADEFEE_UPDATE')) $html .= Html::a('<span class="icon-pencil7"></span>', ['update', 'id' => $model->getPrimaryKey()], ['title' => 'Sửa', 'class' => 'btn-colorgb-popup', 'data-pjax' => 'false']);
                if (ColorgbHelper::checkPermission('TRADEFEE_APPROVE')) $html .= Html::a('<span class="icon-file-check"></span>', ['approve', 'id' => $model->getPrimaryKey()], ['reload' => 'true', 'title' => 'Duyệt mục này?', 'disabled' => $model->STATUS == 1 ? 'disabled' : false, 'data-pjax' => '1', 'class' => 'btn-colorgb-warning', 'data-pjax-container' => 'colorgb-pjax-pjax']);
                if (ColorgbHelper::checkPermission('TRADEFEE_DELETE')) $html .= Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->getPrimaryKey()], ['title' => 'Xóa', 'data-pjax' => 'false', 'disabled' => $model->STATUS == 1 ? 'disabled' : false, 'class' => 'colorgb-pjax-action-del colorgb-pjax-action-del-show', 'data-pjax-container' => 'colorgb-pjax-pjax']);
                return $html;
            },
        ],

    ],
    [
        'attribute' => 'TRANSACTION_CD',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['TRANSACTION_CD'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Lọc...'],
        'value' => function ($model) use ($data) {

            return $data['TRANSACTION_CD'][$model->TRANSACTION_CD];
        }

    ],
    [
        'attribute' => 'CUST_TYPE',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => Yii::$app->params['CUST_TYPE'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Lọc...'],
        'value' => function ($model) use ($data) {

            return Yii::$app->params['CUST_TYPE'][$model->CUST_TYPE];
        }

    ],
    [
        'attribute' => 'FOREIGN_TYPE',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => Yii::$app->params['FOREIGN_TYPE'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Lọc...'],
        'value' => function ($model) use ($data) {

            return Yii::$app->params['FOREIGN_TYPE'][$model->FOREIGN_TYPE];
        }

    ],
    [
        'attribute' => 'FEE_RATE_ID',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['FEE_RATE'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Lọc...'],
        'value' => function ($model) use ($data) {

            return $data['FEE_RATE'][$model->FEE_RATE_ID];
        }

    ],
    [
        'attribute' => 'TRADE_TYPE',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => Yii::$app->params['TRADE_TYPE'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Lọc...'],
        'value' => function ($model) use ($data) {

            return Yii::$app->params['TRADE_TYPE'][$model->TRADE_TYPE];
        }

    ],
    [
        'attribute' => 'STATUS',
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $data['status'],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],

        ],
        'filterInputOptions' => ['placeholder' => 'Lọc...'],
        'value' => function ($model, $key, $index, $widget) use ($data) {
            $array = $data['statusHtml'];
            $value = !empty($array[$model->STATUS]) ? $array[$model->STATUS] : '';
            return $value;
        },
    ],
];

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'colorgb-pjax',
    'showPageSummary' => false,
    'pjax' => true,
    'striped' => true,
    'responsive' => false,
    'responsiveWrap' => false,
    'hover' => true,
    'floatHeader' => true,
    'floatHeaderOptions' => ['top' => 46],
    'toolbar' => [
        'before' => '<a class="btn btn-default" href="fee-rate"><i class="icon-list-unordered position-left"></i> Biểu phí GD</a> ' . (ColorgbHelper::checkPermission('TRADEFEE_CREATE') ? Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['data-pjax' => 0, 'class' => 'btn btn-success pull-left', 'title' => 'Thêm mới']) : ''),
        'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''], ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Làm mới']),
        '{toggleData}',
    ],
    'panel' => ['type' => 'default', 'heading' => $this->title],
    'columns' => $columns,
]);
?>