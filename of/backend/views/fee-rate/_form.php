<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use common\models\Customer;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */


$data['FEE_RATE'] = \common\models\ORDTTRADEFEERATEDETAIL::find()->where(['FEE_RATE_ID' => $model->getPrimaryKey()])->orderBy(['VALID_DATE' => SORT_DESC, 'FROM_VALUE' => SORT_DESC])->all();
?>

<div class="colorgb-container">
    <?php $form = ActiveForm::begin(); ?>

    <?php if ($model->getErrors()): ?>
        <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Đóng</span></button>
            <?= json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE) ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-sm-6">

            <?= $form->field($model, 'FEE_RATE_CODE')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'FEE_RATE_NAME')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'FEE_TYPE')->dropDownList(Yii::$app->params['FEE_TYPE'], ['data-placeholder' => 'Chọn...', 'prompt' => 'Chọn...']) ?>

            <?= $form->field($model, 'REMARKS')->textInput() ?>
        </div>
        <div class="col-sm-6">
            <table class="table table-bordered table-hover" id="tableRowRateFee">
                <thead>
                <tr>
                    <th>Ngày hiệu lực</th>
                    <th width="170px">Từ số tháng</th>
                    <th width="140px">Tỷ lệ phí</th>
                    <th width="70px">
                        <button <?= $model->STATUS == 1 ? 'disabled="disabled"' : '' ?> type="button" class="btn btn-success btn-add-rate-fee-row" data-i="<?= count($data['FEE_RATE']) ?>"><i class="icon-plus3"></i></button>
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($data['FEE_RATE'])): ?>

                    <?php foreach ($data['FEE_RATE'] as $key => $value): ?>
                        <tr data-i="<?= $key ?>">
                            <td>
                                <div class="input-group"><input type="date" <?= $model->STATUS == 1 ? 'disabled="disabled"' : '' ?> class="form-control" name="ORDTTRADEFEERATE[DETAIL][<?= $key ?>][VALID_DATE]" value="<?= \common\colorgb\ColorgbHelper::date($value->VALID_DATE, 'Y-m-d') ?>"></div>

                            </td>
                            <td>
                                <div class="input-group"><input type="number" <?= $model->STATUS == 1 ? 'disabled="disabled"' : '' ?> min="0" class="form-control" name="ORDTTRADEFEERATE[DETAIL][<?= $key ?>][FROM_VALUE]" value="<?= $value->FROM_VALUE ?>"><span class="input-group-addon">tháng</span></div>
                            </td>
                            <td>
                                <div class="input-group"><input type="number" <?= $model->STATUS == 1 ? 'disabled="disabled"' : '' ?> min="0" class="form-control" name="ORDTTRADEFEERATE[DETAIL][<?= $key ?>][FEE_RATE]" value="<?= ($value->FEE_RATE * 100) ?>"><span class="input-group-addon">%</span></div>
                            </td>
                            <td>
                                <button <?= $model->STATUS == 1 ? 'disabled="disabled"' : '' ?> type="button" class="btn btn-danger btn-remove-row"><i class="icon-minus3"></i></button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>

                </tbody>
            </table>
        </div>
    </div>


    <div class="hide">
        <?= $form->field($model, 'UPD_DATE_TIME')->textInput() ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Tạo mới</span>' : '<i class="icon-floppy-disk position-left"></i> <span class="ladda-label">Lưu lại</span>', ['class' => $model->isNewRecord ? 'btn btn-success  btn-ladda btn-ladda-spinner btn-ladda-progress' : 'btn btn-primary  btn-ladda btn-ladda-spinner btn-ladda-progress', 'data-style' => 'zoom-out']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    function addTableRowRateFee() {
        var tr = $('#tableRowRateFee tbody tr:last-child');
        var i = tr.length > 0 ? tr.data('i') + 1 : 0;
        var tempTr = $('<tr data-i="' + i + '"> <td> <div class="input-group"><input type="date" class="form-control" name="ORDTTRADEFEERATE[DETAIL][' + i + '][VALID_DATE]"> </div> </td> <td> <div class="input-group"><input type="number" min="0" class="form-control" name="ORDTTRADEFEERATE[DETAIL][' + i + '][FROM_VALUE]"><span class="input-group-addon">tháng</span> </div> </td> <td> <div class="input-group"><input type="number" min="0" class="form-control" name="ORDTTRADEFEERATE[DETAIL][' + i + '][FEE_RATE]"><span class="input-group-addon">%</span> </div> </td> <td> <button type="button" class="btn btn-danger btn-remove-row"><i class="icon-minus3"></i></button> </td> </tr>').on('click', function () {
            $('.btn-remove-row').click(function () {
                $(this).closest('tr').remove();
            });
        });
        $("#tableRowRateFee").append(tempTr);
    }

    function f1() {

        $('.btn-add-rate-fee-row').on('click', function () {
            addTableRowRateFee();
        });

        $('.btn-remove-row').click(function () {
            $(this).closest('tr').remove();
        });
    }

    $(function () {
        f1();
    });

</script>