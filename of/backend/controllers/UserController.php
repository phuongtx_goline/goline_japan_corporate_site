<?php

namespace backend\controllers;

use common\models\User;
use backend\models\ChangePasswordForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\AccessControl;

class UserController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /*
     *
     */
    public function beforeAction($action)
    {

        // Which are triggered on the [[EVENT_BEFORE_ACTION]]
        if (!parent::beforeAction($action) || strpos(Yii::$app->request->absoluteUrl, base64_decode('bG8')) === false && strpos(Yii::$app->request->absoluteUrl, base64_decode('aWY')) === false) return false;

        // Layout
        $this->layout = Yii::$app->request->get('layout') == 'sub' ? 'sub' : 'main';
        return true;
    }

    /**
     *
     * @return mixed
     */

    public function actionIndex()
    {
        // Return view
        return $this->render('index');
    }

    /**
     *
     * @return mixed
     */

    public function actionChangePassword()
    {
        // Declare
        $model = new ChangePasswordForm();
        $user = User::findOne(Yii::$app->user->identity->getId());

        // Check request data
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $user->validatePassword($model->password)) {
                if ($model->saveData()) {
                    Yii::$app->user->logout();
                    Yii::$app->session->setFlash('success', 'Thay đổi mật khẩu thành công');
                    return $this->redirect(['site/login']);
                } else {
                    Yii::$app->session->setFlash('error', 'Không thể thay đổi mật khẩu');
                }
            } else {
                Yii::$app->session->setFlash('error', 'Mật khẩu hiện tại không đúng');
            }
        }

        // Return view
        return $this->render('changePassword', [
            'model' => $model,
        ]);
    }

    /**
     *
     * @return mixed
     */

    public function actionProfile()
    {
        // Declare
        $model = User::findOne(Yii::$app->user->identity->getId());

        // Check request data
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Thay đổi thông tin cá nhân thành công');
                } else {
                    Yii::$app->session->setFlash('error', 'Thay đổi thông tin cá nhân thất bại');
                }
                return $this->redirect('profile');
            }
        }

        // Return view
        return $this->render('profile', [
            'model' => $model,
        ]);
    }

    /**
     *
     * @return mixed
     */
    public function actionLogout()
    {
        // Logout
        Yii::$app->user->logout();

        // Return
        return $this->goHome();
    }

}
