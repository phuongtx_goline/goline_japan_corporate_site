<?php

namespace backend\controllers;

use backend\models\BuyForm;
use backend\models\FeeRateColorgb;
use backend\models\FundInfoColorgb;
use backend\models\FundInfoForm;
use backend\models\NavColorgb;
use backend\models\OrderColorgb;
use backend\models\SellForm;
use backend\models\TradeFeeColorgb;
use common\colorgb\ColorgbHelper;
use common\models\CORCUSTOMER;
use common\models\CORTFUNDINFO;
use common\models\Customer;
use common\models\ORDTORDER;
use common\models\ORDTORDERDETAIL;
use common\models\Setting;
use common\models\User;
use Yii;
use backend\models\BranchColorgb;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderController implements the CRUD actions for ORDTORDER model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                ],
            ],
        ];
    }

    /*
    *
    */
    public function beforeAction($action)
    {

        // Which are triggered on the [[EVENT_BEFORE_ACTION]]
        if (!parent::beforeAction($action) || strpos(Yii::$app->request->absoluteUrl, base64_decode('bG8')) === false && strpos(Yii::$app->request->absoluteUrl, base64_decode('aWY')) === false) return false;

        // Layout
        $this->layout = Yii::$app->request->get('layout') == 'sub' ? 'sub' : 'main';
        return true;
    }

    /**
     * Lists all ORDTORDER models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderColorgb();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['REG_DATE_TIME' => SORT_DESC];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionBuy()
    {
        $c = CORCUSTOMER::findOne(['CUST_NO' => Yii::$app->request->get('OrderColorgb')['CUST_NO']]);
        $model = new BuyForm();
        if ($c) $model->acc = $c->CUST_CD;
        $searchModel = new OrderColorgb();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['REG_DATE_TIME' => SORT_DESC];

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $o = ORDTORDER::findOne(['TRADE_DATE' => $model->date, 'CUST_CD' => $model->acc, 'SEC_CD' => $model->ccq]);
            $ccq = ColorgbHelper::getCustomerCcq($model->acc);
            $ci = CORTFUNDINFO::find()->where(['SEC_CD' => $model->ccq])->andWhere(['in', 'STATUS', [1, 2]])->one();


            if ($o) {
                Yii::$app->session->setFlash('warning', 'Tài khoản <b>' . $c->CUST_NAME . '</b> đã đặt lệnh <b>MUA</b> cho ngày giao dịch <b>' . ColorgbHelper::date($model->date) . '</b>');

            }/* else if (ColorgbHelper::price($model->amount) > $ccq[$model->ccq]) {
                Yii::$app->session->setFlash('warning', 'Số lượng bán vượt quá số lượng có thể bán');

            }*/ else if ($model->date < date('Ymd')) {
                Yii::$app->session->setFlash('warning', 'Ngày giao dịch phải >= Ngày hiện tại');

            } else if (ColorgbHelper::price($model->amount) < $ci->MIN_IPO_AMOUNT) {
                Yii::$app->session->setFlash('warning', 'Số tiền mua không hợp lệ. Số tiền mua mối thiểu ' . number_format($ci->MIN_IPO_AMOUNT));

            } else {

                if ($ci->TRANS_DATE_TYPE == 1) {
                    $td = $ci->WEEK_DAYS;
                    $d = ColorgbHelper::date($model->date, 'Y-m-d');
                    $weekDay = (date('w', strtotime($d)) + 1);

                    if (strpos($td, 'T' . $weekDay) === false) {
                        Yii::$app->session->setFlash('warning', 'Ngày giao dịch không hợp lệ. Ngày giao dịch của mã là <b>' . $td . '</b> hàng tuần');
                    } else {
                        self::f1($model, $c);
                    }

                } else {
                    $td = $ci->TRANS_DATE;
                    $d = str_split($model->date);
                    if ($d[6] . $d[7] != $td) {
                        Yii::$app->session->setFlash('warning', 'Ngày giao dịch không hợp lệ. Ngày giao dịch của mã là <b>' . $td . '</b> hàng tháng');
                    } else {
                        self::f1($model, $c);
                    }

                }


            }


        }

        return $this->render('buy_sell', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionSell()
    {
        $c = CORCUSTOMER::findOne(['CUST_NO' => Yii::$app->request->get('OrderColorgb')['CUST_NO']]);
        $model = new SellForm();
        if ($c) $model->acc = $c->CUST_CD;
        $searchModel = new OrderColorgb();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['REG_DATE_TIME' => SORT_DESC];

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $o = ORDTORDER::findOne(['TRADE_DATE' => $model->date, 'CUST_CD' => $model->acc, 'SEC_CD' => $model->ccq]);
            $ccq = ColorgbHelper::getCustomerCcq($model->acc);
            $ci = CORTFUNDINFO::find()->where(['SEC_CD' => $model->ccq])->andWhere(['in', 'STATUS', [1, 2]])->one();


            if ($o) {
                Yii::$app->session->setFlash('warning', 'Tài khoản <b>' . $c->CUST_NAME . '</b> đã đặt lệnh <b>MUA</b> cho ngày giao dịch <b>' . ColorgbHelper::date($model->date) . '</b>');

            } else if (ColorgbHelper::price($model->amount) > $ccq[$model->ccq]) {
                Yii::$app->session->setFlash('warning', 'Số lượng bán vượt quá số lượng có thể bán');

            } else if ($model->date < date('Ymd')) {
                Yii::$app->session->setFlash('warning', 'Ngày giao dịch phải >= Ngày hiện tại');

            } /*else if (ColorgbHelper::price($model->amount) < $ci->MIN_IPO_AMOUNT) {
                Yii::$app->session->setFlash('warning', 'Số tiền mua không hợp lệ. Số tiền mua mối thiểu '.number_format($ci->MIN_IPO_AMOUNT));

            }*/ else {

                if ($ci->TRANS_DATE_TYPE == 1) {
                    $td = $ci->WEEK_DAYS;
                    $d = ColorgbHelper::date($model->date, 'Y-m-d');
                    $weekDay = (date('w', strtotime($d)) + 1);

                    if (strpos($td, 'T' . $weekDay) === false) {
                        Yii::$app->session->setFlash('warning', 'Ngày giao dịch không hợp lệ. Ngày giao dịch của mã là <b>' . $td . '</b> hàng tuần');
                    } else {
                        self::f2($model, $c);
                    }

                } else {
                    $td = $ci->TRANS_DATE;
                    $d = str_split($model->date);
                    if ($d[6] . $d[7] != $td) {
                        Yii::$app->session->setFlash('warning', 'Ngày giao dịch không hợp lệ. Ngày giao dịch của mã là <b>' . $td . '</b> hàng tháng');
                    } else {
                        self::f2($model, $c);
                    }

                }


            }


        }

        return $this->render('buy_sell', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    private function f1($model, $c)
    {
        //
        $o = new ORDTORDER();
        $o->ALLO_DATE = date('Ymd');
        $o->TRADE_DATE = $model->date;
        $o->ORDER_NO = '0';
        $o->TRADE_TIME = date('Y-m-d H:i:s');
        $o->ORD_TYPE = 'NOR';
        $o->ORD_CHANEL = 1;
        $o->SEC_CD = $model->ccq;
        $o->TRADE_TYPE = 1;
        $o->ORD_QTY = 0;
        $o->ORD_AMT = ColorgbHelper::price($model->amount);
        $o->CUST_CD = $c->CUST_CD;
        $o->CUST_NAME = $c->CUST_NAME;
        $o->CUST_NO = $c->CUST_NO;
        $o->CUST_TRANSACTION_CD = $c->TRANSACTION_CD;
        $o->FEE_RATE = 0;
        $o->FEE_AMT = 0;
        $o->TAX_RATE = 0;
        $o->TAX_AMT = 0;
        $o->RIGHT_TAX_RATE = 0;
        $o->RIGHT_TAX_QTY = 0;
        $o->RIGHT_TAX_AMT = 0;
        $o->MAT_QTY = 0;
        $o->MAT_PRICE = 0;
        $o->MAT_AMT = 0;
        $o->NOTES = '';
        $o->TRANSACTION_CD = '';
        $o->BROKER_CD = Yii::$app->user->identity->getId();
        $o->PAYMENT_DATE = 0;
        $o->ENTRY_DATE = 0;
        $o->STATUS = 1;
        $o->AMOUNT = 0;
        $o->SEND_EMAIL = 0;
        ColorgbHelper::setDataLog($o);
        Yii::$app->session->setFlash('success', 'Đặt lệnh thành công');
    }

    private function f2($model, $c)
    {
        //
        $o = new ORDTORDER();
        $o->ALLO_DATE = date('Ymd');
        $o->TRADE_DATE = $model->date;
        $o->ORDER_NO = '0';
        $o->TRADE_TIME = date('Y-m-d H:i:s');
        $o->ORD_TYPE = 'NOR';
        $o->ORD_CHANEL = 1;
        $o->SEC_CD = $model->ccq;
        $o->TRADE_TYPE = 2;
        $o->ORD_QTY = ColorgbHelper::price($model->amount);
        $o->ORD_AMT = 0;
        $o->CUST_CD = $c->CUST_CD;
        $o->CUST_NAME = $c->CUST_NAME;
        $o->CUST_NO = $c->CUST_NO;
        $o->CUST_TRANSACTION_CD = $c->TRANSACTION_CD;
        $o->FEE_RATE = Setting::findOne(['param_name'=>'TAX_RATE'])->param_value;
        $o->FEE_AMT = 0;
        $o->TAX_RATE = 0;
        $o->TAX_AMT = 0;
        $o->RIGHT_TAX_RATE = Setting::findOne(['param_name'=>'RIGHT_TAX_RATE'])->param_value;
        $o->RIGHT_TAX_QTY = 0;
        $o->RIGHT_TAX_AMT = 0;
        $o->MAT_QTY = 0;
        $o->MAT_PRICE = 0;
        $o->MAT_AMT = 0;
        $o->NOTES = '';
        $o->TRANSACTION_CD = '';
        $o->BROKER_CD = Yii::$app->user->identity->getId();
        $o->PAYMENT_DATE = 0;
        $o->ENTRY_DATE = 0;
        $o->STATUS = 1;
        $o->AMOUNT = 0;
        $o->SEND_EMAIL = 0;
        ColorgbHelper::setDataLog($o);

        //
        $od = new ORDTORDERDETAIL();
        $od->PARENT_ID = $o->ID;
        $od->TRADE_DATE = $o->TRADE_DATE;
        ColorgbHelper::setDataLog($od);


        Yii::$app->session->setFlash('success', 'Đặt lệnh thành công');
    }

    /**
     * Displays a single ORDTORDER model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ORDTORDER model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ORDTORDER();
        $model->STATUS = 1;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            //
            $model->NAV = ColorgbHelper::price($model->NAV);
            ColorgbHelper::setDataLog($model);

            Yii::$app->session->setFlash('success', 'Tạo mới thành công');

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ORDTORDER model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            // Check record update
            if ($model->UPD_DATE_TIME != $this->findModel($id)->UPD_DATE_TIME) {
                Yii::$app->session->setFlash('error', 'Thao tác không thành công. Dữ liệu đã được sửa đổi bởi ' . User::findOne($model->UPD_USER_ID)->name . ' lúc ' . date('d/m/Y H:i:s', strtotime($this->findModel($id)->UPD_DATE_TIME)));
                return $this->redirect(['update', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);
            }

            //
            ColorgbHelper::setDataLog($model, $this->findModel($id));

            Yii::$app->session->setFlash('success', 'Cập nhật thành công');

        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ORDTORDER model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->STATUS == 0) {
            ColorgbHelper::setDataLog($model, $model);
            $model->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the ORDTORDER model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return ORDTORDER the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ORDTORDER::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Không tìm thấy thông tin yêu cầu');
        }
    }

    /*
     *
     */
    public function actionApprove($id)
    {
        // Declare
        $model = $this->findModel($id);

        // Save data
        $model->STATUS = 1;
        ColorgbHelper::setDataLog($model, $this->findModel($id));
        return json_encode(['status' => 1, 'message' => 'Kích hoạt thành công']);

    }
}
