<?php

namespace backend\controllers;

use backend\models\ContractSettleForm;
use common\colorgb\ColorgbHelper;
use common\models\Contract;
use common\models\CORCUSTOMER;
use common\models\CORTCUSTOMERSIGNER;
use common\models\CORTFUNDNAV;
use common\models\CORTSECBALANCE;
use common\models\CORTSECMOVEMENT;
use common\models\CORTSECTRANSACTION;
use common\models\Customer;
use backend\models\ChangePasswordForm;
use common\models\FundUnit;
use common\models\FundUnitHsa;
use common\models\KPTTDEPOSITTRANSFER;
use common\models\ORDTORDER;
use common\models\ReportSentMark;
use common\models\Setting;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\AccessControl;

class AjaxController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /*
     *
     */
    public function beforeAction($action)
    {

        // Which are triggered on the [[EVENT_BEFORE_ACTION]]
        if (!parent::beforeAction($action) || strpos(Yii::$app->request->absoluteUrl, base64_decode('bG8')) === false && strpos(Yii::$app->request->absoluteUrl, base64_decode('aWY')) === false) return false;

        // Layout
        $this->layout = Yii::$app->request->get('layout') == 'sub' ? 'sub' : 'main';
        return true;
    }

    /**
     * Customer seach
     * @return mixed
     */

    public function actionCustomerSearch($q)
    {
        // Declare
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $customerGroupIds = ColorgbHelper::getCustomerGroupIds();
        $page = Yii::$app->request->get('page') ? Yii::$app->request->get('page') : 1;
        $limit = 20;
        $query = Customer::find()
            ->select('id,name,image,mobile,email_addr')
            ->where(['or',
                ['like', 'mobile', $q],
                ['like', 'name', $q],
                ['like', 'email_addr', $q],
            ])
            ->andWhere(['status' => $customerStatus['active']]);

        // Check user role
        /*if (Yii::$app->user->identity->is_admin == -1) {
            $query->andWhere(['customer.cared_user_id' => Yii::$app->user->identity->getId()]);

        } else {
            $query->andWhere(['customer.customer_group_id' => $customerGroupIds]);
        }*/
        $query->andWhere(['customer.customer_group_id' => $customerGroupIds]);

        $totalPage = ceil($query->count() / $limit);

        // Data
        if ($page > $totalPage) {
            $page = $totalPage;
        } else if ($page < 1) {
            $page = 1;
        }

        $offset = ($page - 1) * $limit;
        $data['total_count'] = $totalPage;
        $data['incomplete_results'] = 'false';
        $data['items'] = $query->asArray()->offset($offset)->limit($limit)->all();

        // Return
        return json_encode($data);

    }

    /*
     *
     */
    public function actionCustomerContractCount()
    {

        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
        $customerGroupIds = ColorgbHelper::getCustomerGroupIds();
        $msgType = ArrayHelper::map(Yii::$app->params['msgType'], 'key', 'value');
        $expireAlert = intval(Setting::findOne(['CONTRACT_EXPIRE_ALERT_DAY'])->param_value);
        $expire = intval(date('Ymd') + $expireAlert);

        // Get customer id by sent mark
        $customer = Customer::find()
            ->select('customer.id')
            ->joinWith('contract')
            ->where([
                'customer.status' => $customerStatus['active'],
                'contract.status' => $contractStatus['active'],

            ]);

        // Check user role
        /*if (Yii::$app->user->identity->is_admin == -1) {
            $customer->andWhere(['customer.cared_user_id' => Yii::$app->user->identity->getId()]);

        } else {
            $customer->andWhere(['customer.customer_group_id' => $customerGroupIds]);
        }*/
        $customer->andWhere(['customer.customer_group_id' => $customerGroupIds]);

        $data['customerContractActiveAll'] = $customer->count();

        $weeklyReport = ArrayHelper::map(ReportSentMark::find()->select('customer_id')->where(['status' => $recordStatus['active'], 'sent_date' => date('Ymd')])->andWhere(['type' => $msgType['weekly_report']])->all(), 'customer_id', 'customer_id');
        $data['customerContractActivePending'] = $customer
            ->andWhere(['not in', 'customer.id', $weeklyReport])
            ->count();

        $contractRenewal = ArrayHelper::map(ReportSentMark::find()->select('customer_id')->where(['status' => $recordStatus['active'], 'sent_date' => date('Ymd')])->andWhere(['type' => $msgType['contract_renewal']])->all(), 'customer_id', 'customer_id');
        $data['customerContractActiveExpire'] = $customer
            ->andWhere(['not in', 'customer.id', $contractRenewal])
            ->andWhere(['<=', 'contract.end_date_est', $expire])->andWhere(['>', 'contract.end_date_est', date('Ymd')])
            ->count();

        return json_encode($data);
    }

    /*
     *
     */
    public function actionFundUnitQty($date)
    {

        return ColorgbHelper::fundUnitQty($date);
    }

    /*
     *
     */
    public function actionFundUnitQtyHsa($date)
    {

        return ColorgbHelper::fundUnitQtyHsa($date);
    }

    /*
     *
     */
    public function actionFundUnit()
    {

        $date = Yii::$app->request->get('date');
        $ct = Yii::$app->request->get('ct');
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');

        if ($ct == 3) {
            $f = FundUnitHsa::find()->where(['!=', 'status', $recordStatus['deleted']])->andWhere(['<', 'date', $date])->asArray()->orderBy(['date' => SORT_DESC])->one();

        } else {

            $f = FundUnit::find()->where(['!=', 'status', $recordStatus['deleted']])->andWhere(['<', 'date', $date])->asArray()->orderBy(['date' => SORT_DESC])->one();
        }

        return json_encode($f, true);
    }

    /*
     *
     */
    public function actionContractSettle($id)
    {
        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
        $customerGroupIds = ColorgbHelper::getCustomerGroupIds();
        $model = ContractSettleForm::find()->where(['contract.id' => $id])->andWhere(['>=', 'contract.status', $contractStatus['active']])->joinWith('customer')->andWhere(['customer.customer_group_id' => $customerGroupIds])->one();
        $model->settle_date_time = date('Y-m-d H:i:s');
        $model->settle_user_id = Yii::$app->user->identity->id;
        $model->status = $contractStatus['settled'];
        $data = $model->save() ? ['status' => 1, 'message' => 'Tất toán HĐ thành công'] : ['status' => 0, 'message' => 'Đã có lỗi xảy ra trong quá trình tất toán HĐ'];
        return json_encode($data, true);
    }

    /*
     *
     */
    public function actionSetDataReport()
    {

        // Declare
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');

        // Set data report
        ColorgbHelper::setDataReport();

        // Get customer & contract active
        /*$model = Customer::find()
            ->select('customer.id')
            ->joinWith('contract')
            ->where([
                'customer.status' => $customerStatus['active'],
                'contract.status' => $contractStatus['active'],
            ])
            ->all();

        // Create weekly report
        foreach ($model as $item) {

            // Generate pdf file
            ColorgbHelper::phpPdf($item->id, 'weekly-report', 'weekly-report/' . ColorgbHelper::encrypt($item->id) . '.pdf');

        }*/

        // Return
        return true;
    }

    /*
     *
     */
    public function actionSetDataReportHsa()
    {

        // Declare
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');

        // Set data report
        ColorgbHelper::setDataReportHsa();

        // Get customer & contract active
        /*$model = Customer::find()
            ->select('customer.id')
            ->joinWith('contract')
            ->where([
                'customer.status' => $customerStatus['active'],
                'contract.status' => $contractStatus['active'],
            ])
            ->all();

        // Create weekly report
        foreach ($model as $item) {

            // Generate pdf file
            ColorgbHelper::phpPdf($item->id, 'weekly-report', 'weekly-report/' . ColorgbHelper::encrypt($item->id) . '.pdf');

        }*/

        // Return
        return true;
    }

    /*
     *
     */
    public function actionGetCustomer($cd)
    {
        $c = CORCUSTOMER::find()->where(['CUST_CD' => $cd])->asArray()->one();

        return json_encode($c);

    }

    /*
    *
    */
    public function actionDeleteCs($id)
    {
        // Declare
        $model = CORTCUSTOMERSIGNER::findOne($id);

        // Save data
        ColorgbHelper::setDataLog($model, $model);
        $model->delete();
        return json_encode(['status' => 1, 'message' => 'Xóa thành công']);

    }

    /*
    *
    */
    public function actionSetCs()
    {
        // Declare
        $id = Yii::$app->request->get('id');
        $model = CORTCUSTOMERSIGNER::findOne($id);

        // Save data
        $model->REMARKS = Yii::$app->request->get('txt');
        ColorgbHelper::setDataLog($model, $model);

        return json_encode(['status' => 1, 'message' => 'Cập nhật thành công']);

    }

    /*
    *
    */
    public function actionGetCustomerCcq($cd)
    {

        return ColorgbHelper::getCustomerCcq($cd,true);

    }

    /*
    *
    */
    public function actionGetCustomerCcqJson($ccd,$scd)
    {

        return ColorgbHelper::getCustomerCcq($ccd,false,$scd,true);

    }

    /*
     *
     */
    public function actionGetNav($cd)
    {
        return CORTFUNDNAV::find()->where(['SEC_CD' => $cd])->andWhere(['in', 'STATUS', [1, 2]])->orderBy(['VALID_DATE' => SORT_DESC])->one()->NAV;
    }

}
