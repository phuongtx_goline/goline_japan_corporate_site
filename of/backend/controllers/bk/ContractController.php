<?php

namespace backend\controllers;

use backend\models\ContractActivateForm;
use backend\models\ContractSettleForm;
use backend\models\ContractRenewForm;
use common\models\ContractExtendHist;
use common\models\ContractNo;
use common\models\ContractSettleFee;
use common\models\ContractType;
use common\models\Customer;
use common\models\FundUnit;
use common\models\ReportSentMark;
use common\models\Setting;
use common\models\SettleFee;
use common\models\User;
use MoneyToWords\MoneyToWordsConverter;
use Yii;
use common\models\Contract;
use common\colorgb\ColorgbHelper;
use backend\models\ContractColorgb;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ContractController implements the CRUD actions for Contract model.
 */
class ContractController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*
   *
   */
    public function beforeAction($action)
    {

        // Which are triggered on the [[EVENT_BEFORE_ACTION]]
        if (!parent::beforeAction($action) || strpos(Yii::$app->request->absoluteUrl, base64_decode('bG8')) === false && strpos(Yii::$app->request->absoluteUrl, base64_decode('aWY')) === false) return false;

        // Check permission
        if (ColorgbHelper::checkPermission() == false) {
            Yii::$app->session->setFlash('error', 'Bạn không được cấp quyền truy cập');
            return $this->redirect(['dashboard/index']);
        }

        // Layout
        $this->layout = Yii::$app->request->get('layout') == 'sub' ? 'sub' : 'main';
        return true;
    }

    /**
     * Lists all Contract models.
     * @return mixed
     */
    public function actionIndex()
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $data['contract'] = true;
        $data['contractType'] = ArrayHelper::map(ContractType::findAll(['status' => $recordStatus['active']]), 'id', 'name');
        $data['contractStatusTxt'] = ArrayHelper::map(Yii::$app->params['contractStatus'], 'value', 'txt');
        $data['contractStatusTxt'][8] = 'Sắp hết hạn';
        ksort($data['contractStatusTxt']);
        $data['contractStatusHtml'] = ArrayHelper::map(Yii::$app->params['contractStatus'], 'value', 'html');
        $searchModel = new ContractColorgb();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // Return view
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'data' => $data,
        ]);
    }

    /**
     * Displays a single Contract model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $data['contractType'] = ArrayHelper::map(ContractType::findAll(['status' => $recordStatus['active']]), 'id', 'name');
        $data['contractStatus'] = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
        $fundUnit = ColorgbHelper::fundUnit();
        $customerGroupIds = ColorgbHelper::getCustomerGroupIds();
        $model = Contract::find()->where(['contract.id' => $id])->joinWith('customer')->andWhere(['customer.customer_group_id' => $customerGroupIds])->one();
        //$model->contract_date = ColorgbHelper::date($model->contract_date);


        // Return view
        return $this->render('view', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Creates a new Contract model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        // Declare
        $transaction = Yii::$app->db->beginTransaction();
        $msgStatus = ArrayHelper::map(Yii::$app->params['msgStatus'], 'key', 'value');
        $msgType = ArrayHelper::map(Yii::$app->params['msgType'], 'key', 'value');
        $sendType = ArrayHelper::map(Yii::$app->params['sendType'], 'key', 'value');
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');

        if (Yii::$app->user->identity->is_admin == 1){
            $data['contractType'] = ArrayHelper::map($contractType = ContractType::findAll(['status' => $recordStatus['active']]), 'id', 'name');

        }else{
            $data['contractType'] = ArrayHelper::map($contractType = ContractType::findAll(['status' => $recordStatus['active'],'id'=>1]), 'id', 'name');

        }

        $fundUnit = ColorgbHelper::fundUnit();
        $post = Yii::$app->request->post();
        $model = new Contract();
        $model->fund_net_asset = $fundUnit['last']->fund_net_asset;
        $model->fund_unit_qty = $fundUnit['last']->fund_unit_qty;
        $model->fund_unit_price = $fundUnit['last']->fund_unit_price;
        $model->customer_id = Yii::$app->request->get('customer_id') ? Yii::$app->request->get('customer_id') : '';
        $model->representative = 'Lã Giang Trung';
        $model->tax_rate = 5;

        // Contract type option
        foreach ($contractType as $item) {
            $data['contractTypeOption'][$item->id]['data-base_profit_rate'] = $item->base_profit_rate * 100;
            $data['contractTypeOption'][$item->id]['data-profit_rate'] = $item->profit_rate * 100;
            $data['contractTypeOption'][$item->id]['data-period_type'] = $item->period_type;
            $data['contractTypeOption'][$item->id]['data-period_val'] = $item->period_val;
            $data['contractTypeOption'][$item->id]['data-min_invest_value'] = $item->min_invest_value;
            $data['contractTypeOption'][$item->id]['data-max_invest_value'] = $item->max_invest_value;
        }

        // Try catch
        try {

            // Check request data
            if ($post && $model->load($post)) {

                // Save data temp
                $model->contract_date = date('Ymd');
                $model->invest_value = str_replace(',', '', $model->invest_value);
                $model->invest_unit_qty = str_replace(',', '', $model->invest_unit_qty);
                $model->annouce_status = $model->annouce_status ? 1 : 0;
                $model->tax_rate = $model->tax_rate / 100;
                $model->status = $contractStatus['inactive'];
                ColorgbHelper::setDataLog($model);

                // Save contract no
                $cn = ContractNo::findOne(1);
                if ($model->contract_type_id == 3) {
                    $cn->id_hsa = $cn->id_hsa + 1;
                    $cn->save();
                } else {
                    $cn->id_pi = $cn->id_pi + 1;
                    $cn->save();
                }


                // Save contract
                $contractType = ContractType::findOne($model->contract_type_id);
                $model->base_profit_rate = $contractType->base_profit_rate;
                $model->period_type = $contractType->period_type;
                $model->period_val = $contractType->period_val;
                $model->profit_rate = $contractType->profit_rate;
                $model->contract_type_code = $contractType->code;
                $model->contract_type_name = $contractType->name;
                $model->contract_no = ColorgbHelper::contractNo($model->contract_type_id);
                $model->save();

                // Save contract settle fee
                $settleFee = SettleFee::findAll(['contract_type_id' => $model->contract_type_id, 'status' => $recordStatus['active']]);
                foreach ($settleFee as $item) {
                    $contractSettleFee = new ContractSettleFee();
                    $contractSettleFee->contract_id = $model->id;
                    $contractSettleFee->from_period_val = $item->from_period_val;
                    $contractSettleFee->to_period_val = $item->to_period_val;
                    $contractSettleFee->fee_rate = $item->fee_rate;
                    ColorgbHelper::setDataLog($contractSettleFee);
                }

                // Commit
                $transaction->commit();

                if ($model->annouce_status == 1) {

                    // Customer
                    $customer = Customer::find()->where(['id' => $model->customer_id])->all();

                    // Contract doc
                    ColorgbHelper::contractDoc($model->getPrimaryKey());

                    // Send mail
                    $emailArray = ArrayHelper::map($customer, 'id', 'email_addr');
                    $contentData['contract_no'] = explode('/', $model->contract_no)[0];
                    $templateArray['msgType'] = $msgType['money_transfer'];
                    $templateArray['msgStatus'] = $msgStatus['pending'];
                    $templateArray['sendType'] = $sendType['email'];
                    $attachments = ';';//'contract/' . ColorgbHelper::encrypt($model->getPrimaryKey()) . '.pdf;';
                    ColorgbHelper::msgSendRequest($emailArray, $templateArray, $attachments, $contentData,$model);

                }

                // Return
                Yii::$app->session->setFlash('success', 'Tạo mới thành công');
                return $this->redirect(['activate', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);


            }


        } catch (Exception $e) {

            // Rollback
            $transaction->rollback();
        }

        // Return view
        return $this->render('create', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Update an existing Contract model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpdate($id)
    {
        // Check is admin
        if (Yii::$app->user->identity->is_admin != 1) return $this->redirect(['dashboard/index']);

        // Declare
        $post = Yii::$app->request->post();
        $msgStatus = ArrayHelper::map(Yii::$app->params['msgStatus'], 'key', 'value');
        $msgType = ArrayHelper::map(Yii::$app->params['msgType'], 'key', 'value');
        $sendType = ArrayHelper::map(Yii::$app->params['sendType'], 'key', 'value');
        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $data['contractStatus'] = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
        $data['customerArray'] = ArrayHelper::map(Customer::findAll(['status' => $customerStatus['active']]), 'id', 'name');
        $data['contractType'] = ArrayHelper::map($contractType = ContractType::findAll(['status' => $recordStatus['active']]), 'id', 'name');

        $model = $this->findModel($id);
        $model->base_profit_rate = $model->base_profit_rate * 100;
        $model->profit_rate = $model->profit_rate * 100;
        $model->tax_rate = $model->tax_rate * 100;
        $temp = ArrayHelper::toArray($model);

        // Contract type option
        foreach ($contractType as $item) {
            $data['contractTypeOption'][$item->id]['data-base_profit_rate'] = $item->base_profit_rate * 100;
            $data['contractTypeOption'][$item->id]['data-profit_rate'] = $item->profit_rate * 100;
            $data['contractTypeOption'][$item->id]['data-period_type'] = $item->period_type;
            $data['contractTypeOption'][$item->id]['data-period_val'] = $item->period_val;
            $data['contractTypeOption'][$item->id]['data-min_invest_value'] = $item->min_invest_value;
            $data['contractTypeOption'][$item->id]['data-max_invest_value'] = $item->max_invest_value;
        }

        // Check request data
        if ($post && $model->load($post)) {

            // Check record update
            if ($model->upd_date_time != $this->findModel($id)->upd_date_time) {
                $model->load($post);
                Yii::$app->session->setFlash('error', 'Thao tác không thành công. Dữ liệu đã được sửa đổi bởi ' . User::findOne($model->upd_user_id)->name . ' lúc ' . date('d/m/Y H:i:s', strtotime($model->upd_date_time)));
                return $this->redirect(['update', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);
            }

            // Save data
            $contractType = ContractType::findOne($model->contract_type_id);
            $model->base_profit_rate = $contractType->base_profit_rate;
            $model->period_type = $contractType->period_type;
            $model->period_val = $contractType->period_val;
            $model->profit_rate = $contractType->profit_rate;
            $model->contract_type_code = $contractType->code;
            $model->contract_type_name = $contractType->name;
            $model->tax_rate = $model->tax_rate / 100;
            unset($model->contract_no);
            //$model->contract_no = ColorgbHelper::contractNo($model->contract_type_id);

            if ($model->status == $contractStatus['active']) $model->end_date_real = 0;

            //$model->contract_no = $model->contract_no ? $model->contract_no : $model->getPrimaryKey() . '/' . date('Y') . '/BCC';

            $model->invest_value = str_replace(',', '', $model->invest_value);
            $model->invest_unit_qty = str_replace(',', '', $model->invest_unit_qty);

            $model->fund_net_asset = str_replace(',', '', $model->fund_net_asset);
            $model->fund_unit_qty = str_replace(',', '', $model->fund_unit_qty);
            $model->fund_unit_price = str_replace(',', '', $model->fund_unit_price);

            ColorgbHelper::setDataLog($model, $temp);
            Yii::$app->session->setFlash('success', 'Sửa đổi hợp đồng thành công #' . $model->getPrimaryKey());
            return $this->redirect(['update', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);

        }

        // Return view
        return $this->render('update', [
            'model' => $model,
            'data' => $data,
        ]);

    }

    /**
     * Activate an existing Contract model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionActivate($id)
    {
        // Declare
        $post = Yii::$app->request->post();
        $msgStatus = ArrayHelper::map(Yii::$app->params['msgStatus'], 'key', 'value');
        $msgType = ArrayHelper::map(Yii::$app->params['msgType'], 'key', 'value');
        $sendType = ArrayHelper::map(Yii::$app->params['sendType'], 'key', 'value');
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $data['contractStatus'] = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
        $data['customerArray'] = ArrayHelper::map(Customer::findAll(['status' => $customerStatus['active']]), 'id', 'name');
        $data['contractType'] = ArrayHelper::map($contractType = ContractType::findAll(['status' => $recordStatus['active']]), 'id', 'name');
        $model = $this->findModel($id);
        $model->begin_date = date('Ymd');


        //$model->contract_date = date('Ymd');
        //$model->begin_date = $post['Contract']['begin_date'] ? $post['Contract']['begin_date'] : date('Ymd');
        $model->active_date = date('Ymd');
        $model->base_profit_rate = $model->base_profit_rate * 100;
        $model->profit_rate = $model->profit_rate * 100;
        $temp = ArrayHelper::toArray($model);
        // Customer
        $customer = Customer::findAll(['id' => $model->customer_id]);

        if($model->contract_type_id == 3) $model->transfer_note = $customer[0]->name.' '.str_replace('/','.',$model->contract_no);

        // Contract type option
        foreach ($contractType as $item) {
            $data['contractTypeOption'][$item->id]['data-base_profit_rate'] = $item->base_profit_rate * 100;
            $data['contractTypeOption'][$item->id]['data-profit_rate'] = $item->profit_rate * 100;
            $data['contractTypeOption'][$item->id]['data-period_type'] = $item->period_type;
            $data['contractTypeOption'][$item->id]['data-period_val'] = $item->period_val;
            $data['contractTypeOption'][$item->id]['data-min_invest_value'] = $item->min_invest_value;
            $data['contractTypeOption'][$item->id]['data-max_invest_value'] = $item->max_invest_value;
        }

        // Check request data
        if ($post && $model->load($post)) {

            // Check record update
            if ($model->upd_date_time != $this->findModel($id)->upd_date_time) {
                $model->load($post);
                Yii::$app->session->setFlash('error', 'Thao tác không thành công. Dữ liệu đã được sửa đổi bởi ' . User::findOne($model->upd_user_id)->name . ' lúc ' . date('d/m/Y H:i:s', strtotime($model->upd_date_time)));
                return $this->redirect(['activate', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);
            }

            // Check customer status
            $customerActive = Customer::findOne(['id' => $model->customer_id, 'status' => $customerStatus['active']]);
            if (!$customerActive) {
                Yii::$app->session->setFlash('error', 'Không thể kích hoạt hợp đồng này bởi vì khách hàng đang trong trạng thái chưa hiệu lực');
                return $this->redirect(['activate', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);
            }

            // Save data
            //$activeDate = $model->active_date ? $model->active_date : date('Ymd');


            $contractType = ContractType::findOne($model->contract_type_id);
            $model->base_profit_rate = $contractType->base_profit_rate;
            $model->period_type = $contractType->period_type;
            $model->period_val = $contractType->period_val;
            $model->profit_rate = $contractType->profit_rate;
            $model->contract_type_code = $contractType->code;
            $model->contract_type_name = $contractType->name;
            unset($model->contract_no);
            //$model->contract_no = $model->contract_no ? $model->contract_no : $model->getPrimaryKey() . '/' . date('Y') . '/BCC';

            $model->invest_value = str_replace(',', '', $model->invest_value);
            $model->invest_unit_qty = str_replace(',', '', $model->invest_unit_qty);

            $model->fund_net_asset = str_replace(',', '', $model->fund_net_asset);
            $model->fund_unit_qty = str_replace(',', '', $model->fund_unit_qty);
            $model->fund_unit_price = str_replace(',', '', $model->fund_unit_price);

            $model->active_date_time = date('Y-m-d H:i:s');
            $model->begin_date = $post['Contract']['begin_date'];
            $model->end_date_est = $post['Contract']['end_date_est'];
            $model->active_user_id = Yii::$app->user->identity->id;
            $model->annouce_status = $recordStatus['inactive'];
            $model->status = $data['contractStatus']['active'];
            ColorgbHelper::setDataLog($model, $temp);
            Yii::$app->session->setFlash('success', 'Kích hoạt hợp đồng thành công #' . $model->getPrimaryKey());

            // Send mail
            ColorgbHelper::contractDoc($id);
            $attachments = 'contract/' . ColorgbHelper::encrypt($id) . '.pdf;';
            $emailArray = ArrayHelper::map($customer, 'id', 'email_addr');
            $templateArray['msgType'] = $msgType['letter_of_thank'];
            $templateArray['msgStatus'] = $msgStatus['pending'];
            $templateArray['sendType'] = $sendType['email'];
            $contentData['customer_name'] = $customer[0]->name;
            $contentData['contract_invest_value'] = \common\colorgb\ColorgbHelper::numberFormat($model->invest_value);
            $contentData['contract_no'] = $model->contract_no;
            $contentData['contract_begin_date'] = ColorgbHelper::date($model->begin_date);
            $contentData['contract_fund_unit_price'] = \common\colorgb\ColorgbHelper::numberFormat($model->fund_unit_price);
            $contentData['contract_invest_unit_qty'] = \common\colorgb\ColorgbHelper::numberFormat($model->invest_unit_qty);
            ColorgbHelper::msgSendRequest($emailArray, $templateArray, $attachments, $contentData,$model);

            // Return
            return $this->redirect(['activate', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);

        }

        // Return view
        return $this->render('activate', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Renew an existing Contract model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRenew($id)
    {
        // Declare
        $msgStatus = ArrayHelper::map(Yii::$app->params['msgStatus'], 'key', 'value');
        $msgType = ArrayHelper::map(Yii::$app->params['msgType'], 'key', 'value');
        $sendType = ArrayHelper::map(Yii::$app->params['sendType'], 'key', 'value');
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $data['contractStatus'] = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
        $data['customerArray'] = ArrayHelper::map(Customer::findAll(['status' => $customerStatus['active']]), 'id', 'name');
        $data['contractType'] = ArrayHelper::map(ContractType::findAll(['status' => $recordStatus['active']]), 'id', 'name');
        $fundUnit = ColorgbHelper::fundUnit();
        $customerGroupIds = ColorgbHelper::getCustomerGroupIds();
        $model = ContractRenewForm::find()->where(['contract.id' => $id])->joinWith('customer')->andWhere(['customer.customer_group_id' => $customerGroupIds])->one();
        //$model->contract_date = date('Ymd');
        $model->base_profit_rate = $model->base_profit_rate * 100;
        $model->profit_rate = $model->profit_rate * 100;
        $temp = ArrayHelper::toArray($model);
        $post = Yii::$app->request->post();

        // Customer
        $customer = Customer::findAll(['id' => $model->customer_id]);

        // Check contact active
        if ($model->status != $data['contractStatus']['active']) {
            return $this->redirect(['index']);

        }

        // Check request data
        if ($post && $model->load($post)) {

            // Check record update
            if ($model->upd_date_time != $this->findModel($id)->upd_date_time) {
                $model->load($post);
                Yii::$app->session->setFlash('error', 'Thao tác không thành công. Dữ liệu đã được sửa đổi bởi ' . User::findOne($model->upd_user_id)->name . ' lúc ' . date('d/m/Y H:i:s', strtotime($model->upd_date_time)));
                return $this->redirect(['renew', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);
            }

            // Save data
            unset($model->base_profit_rate);
            unset($model->profit_rate);
            unset($model->fund_net_asset);
            unset($model->fund_unit_qty);
            unset($model->fund_unit_price);
            $contractExtendHist = new ContractExtendHist();
            $contractExtendHist->contract_id = $model->getPrimaryKey();
            $contractExtendHist->old_end_date = $model->end_date_est;
            $contractExtendHist->new_end_date = $model->new_end_date;
            $contractExtendHist->remarks = $model->remarks;
            ColorgbHelper::setDataLog($contractExtendHist);

            $model->end_date_est = $model->new_end_date;
            ColorgbHelper::setDataLog($model, $temp);

            // Send mail
            $attachments = '';
            $emailArray = ArrayHelper::map($customer, 'id', 'email_addr');
            $templateArray['msgType'] = $msgType['contract_renew_success'];
            $templateArray['msgStatus'] = $msgStatus['pending'];
            $templateArray['sendType'] = $sendType['email'];
            $contentData['customer_name'] = $customer[0]->name;
            $contentData['contract_no'] = $model->contract_no;
            $contentData['contract_begin_date'] = ColorgbHelper::date($model->begin_date);
            $contentData['contract_new_date'] = ColorgbHelper::date($model->new_end_date);
            ColorgbHelper::msgSendRequest($emailArray, $templateArray, $attachments, $contentData,$model);

            Yii::$app->session->setFlash('success', 'Gia hạn hợp đồng thành công #' . $model->getPrimaryKey());

            // Return
            return $this->redirect(['renew', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);

        }

        // Return view
        return $this->render('renew', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Settle an existing Contract model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionSettle($id)
    {
        // Declare
        $contentData = array();
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $msgStatus = ArrayHelper::map(Yii::$app->params['msgStatus'], 'key', 'value');
        $msgType = ArrayHelper::map(Yii::$app->params['msgType'], 'key', 'value');
        $sendType = ArrayHelper::map(Yii::$app->params['sendType'], 'key', 'value');
        $data['contractStatus'] = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
        $data['customerArray'] = ArrayHelper::map(Customer::findAll(['status' => $customerStatus['active']]), 'id', 'name');
        $data['contractType'] = ArrayHelper::map(ContractType::findAll(['status' => $recordStatus['active']]), 'id', 'name');
        $data['contractSettleFee'] = ContractSettleFee::findAll(['contract_id' => $id]);
        $data['liquidateFormEdit'] = Setting::findOne('CONTRACT_SETTLE_FORM_EDIT')->param_value;
        $data['fundUnit'] = ColorgbHelper::fundUnit();
        $customerGroupIds = ColorgbHelper::getCustomerGroupIds();
        $model = ContractSettleForm::find()->where(['contract.id' => $id])->joinWith('customer')->andWhere(['customer.customer_group_id' => $customerGroupIds])->one();
        //$model->contract_date = date('Ymd');
        $model->base_profit_rate = $model->base_profit_rate * 100;
        $model->profit_rate = $model->profit_rate * 100;
        $model->tax_rate = $model->tax_rate * 100;
        $model->end_date_real = $model->end_date_real ? $model->end_date_real : date('Ymd');
        $temp = ArrayHelper::toArray($model);
        $post = Yii::$app->request->post();

        // Check request data
        if ($post && $model->load($post)) {

            // Check record update
            if ($model->upd_date_time != $this->findModel($id)->upd_date_time) {
                $model->load($post);
                Yii::$app->session->setFlash('error', 'Thao tác không thành công. Dữ liệu đã được sửa đổi bởi ' . User::findOne($model->upd_user_id)->name . ' lúc ' . date('d/m/Y H:i:s', strtotime($model->upd_date_time)));
                return $this->redirect(['settle', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);
            }

            // Save data
            unset($model->base_profit_rate);
            unset($model->profit_rate);
            unset($model->tax_rate);
            unset($model->fund_net_asset);
            unset($model->fund_unit_qty);
            unset($model->fund_unit_price);
            $colorgbForm = $post['ColorgbForm'];

            // Check status
            if ($model->status == $data['contractStatus']['sent']) {
                $model->settle_date_time = date('Y-m-d H:i:s');
                $model->settle_user_id = Yii::$app->user->identity->id;
            }

            $model->profit_amt = $colorgbForm['attr_6'];
            $model->settle_fee_amt = $colorgbForm['attr_8'];
            $model->recv_amt = $colorgbForm['attr_13'];

            $model->status = $model->status < $data['contractStatus']['settled'] ? $model->status + 1 : $model->status;

            ColorgbHelper::setDataLog($model, $temp);
            ColorgbHelper::contractDoc($model->getPrimaryKey(), $colorgbForm);

            // Customer
            $customer = Customer::find()->where(['id' => $model->customer_id])->all();

            // Check status
            if ($model->status == $data['contractStatus']['settle']) {
                Yii::$app->session->setFlash('success', 'Tạo biên bản tất toán hợp đồng thành công #' . $model->getPrimaryKey());

            } else if ($model->status == $data['contractStatus']['sent']) {
                // Send mail
                $emailArray = ArrayHelper::map($customer, 'id', 'email_addr');
                $templateArray['msgType'] = $msgType['contract_settle'];
                $templateArray['msgStatus'] = $msgStatus['pending'];
                $templateArray['sendType'] = $sendType['email'];
                $contentData['company_address'] = Setting::findOne('COMPANY_ADDRESS')->param_value;;
                $contentData['company_tel'] = Setting::findOne('COMPANY_TEL')->param_value;;
                $contentData['customer_name'] = $model->customer->name;
                $contentData['customer_email'] = $model->customer->email_addr;
                $contentData['acc_email'] = Yii::$app->user->identity->email_addr;
                $contentData['contract_no'] = $model->contract_no;
                $contentData['contract_end_date_est'] = ColorgbHelper::date($model->end_date_est);
                $contentData['send_date'] = date('d/m/Y');
                $attachments = 'contract-settle/' . ColorgbHelper::encrypt($model->getPrimaryKey()) . '.pdf;';
                ColorgbHelper::msgSendRequest($emailArray, $templateArray, $attachments, $contentData,$model);
                Yii::$app->session->setFlash('success', 'Gửi thông tin tất toán hợp đồng đến khách hàng thành công #' . $model->getPrimaryKey());

            } /*else {
                Yii::$app->session->setFlash('success', 'Tất toán hợp đồng thành công #' . $model->getPrimaryKey());

            }*/

            // Return
            return $this->redirect(['settle', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);

        }

        // Return view
        return $this->render('settle', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Deletes an existing Contract model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
        $model = $this->findModel($id);
        $temp = ArrayHelper::toArray($model);

        // Check contract status
        if ($model->status == $contractStatus['active']) {
            return json_encode(['status' => 0, 'message' => 'Không thể xóa hợp đồng với trạng thái đang hiệu lực']);
        }

        // Save data
        $model->status = $recordStatus['deleted'];
        ColorgbHelper::setDataLog($model, $temp);
        return json_encode(['status' => 1, 'message' => 'Xóa hợp đồng thành công']);

    }


    /*
     * Contract print
     */
    public function actionPrint($id)
    {
        // Declare
        ob_start();
        $customerGroupIds = ColorgbHelper::getCustomerGroupIds();
        $model = $this->findModel($id);
        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');

        // Check contact status
        if ($model->status > $contractStatus['active']) {

            // Download
            $type = 'contract-settle';

        } else {

            // Contract doc
            ColorgbHelper::contractDoc($id);
            $type = 'contract';
        }

        // Return
        return $this->redirect('http://view.officeapps.live.com/op/view.aspx?src=' . Yii::$app->params['cdnUrl'] . '/data/' . $type . '/' . ColorgbHelper::encrypt($id) . '.docx?colorgb=' . rand());
    }

    /*
     * Contract download
     */
    public function actionDownload()
    {
        // Declare
        $id = Yii::$app->request->get('id');
        $type = Yii::$app->request->get('type');
        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
        $model = $this->findModel($id);

        // Check contact status
        /*if ($model->status < $contractStatus['active']) {
            return $this->redirect(['index']);

        }*/

        // Check contact status
        if ($model->status > $contractStatus['active']) {

            // Download
            $docx = '[HDTT] ' . $model->customer->name . ' (' . $model->contract_no . ').docx';
            $readFile = Yii::$app->params['cdnUrl'] . '/data/contract-settle/' . ColorgbHelper::encrypt($id) . '.docx';

        } else {

            // Contract doc
            ColorgbHelper::contractDoc($id);

            // Download
            $docx = '[HD] ' . $model->customer->name . ' (' . $model->contract_no . ').docx';
            $readFile = Yii::$app->params['cdnUrl'] . '/data/contract/' . ColorgbHelper::encrypt($id) . '.docx';
        }

        $docx = str_replace('/', '_', $docx);

        header('Content-type: application/octet-stream');
        //header('Content-Length: ' . filesize($readFile));
        header('Content-Disposition: attachment; filename="' . $docx . '"; filename*=utf-8\' \'' . rawurlencode($docx));

        readfile($readFile);
        exit();
    }

    /*
     * Send notice
     */
    public function actionSendNotice()
    {
        // Declare
        $contentData = array();
        $action = Yii::$app->request->get('action');
        $id = Yii::$app->request->get('id');
        $type = Yii::$app->request->get('type');
        $sentMarkType = ArrayHelper::map(Yii::$app->params['sentMarkType'], 'key', 'value');
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
        $msgStatus = ArrayHelper::map(Yii::$app->params['msgStatus'], 'key', 'value');
        $msgType = ArrayHelper::map(Yii::$app->params['msgType'], 'key', 'value');
        $sendType = ArrayHelper::map(Yii::$app->params['sendType'], 'key', 'value');
        $customerGroupIds = ColorgbHelper::getCustomerGroupIds();
        ColorgbHelper::contractDoc($id);
        $contract = $this->findModel($id);
        $customer = Customer::find()
            ->select('id,email_addr')
            ->where([
                'id' => $contract->customer_id,
                'customer_group_id' => $customerGroupIds
            ])
            ->all();

        // Check action
        if (!$action) return json_encode(['status' => 0, 'message' => 'Thao tác không thành công.']);

        // Contract doc
        ColorgbHelper::contractDoc($id);

        // Check action
        if ($action == 'money_transfer') {
            $contentData['contract_no'] = explode('/', $contract->contract_no)[0];
        }

        // Send mail
        if ($action == 'contract_renewal') {
            $contentData['contract_no'] = $contract->contract_no;
            $contentData['customer_name'] = $contract->customer->name;
            $contentData['contract_begin_date'] = ColorgbHelper::date($contract->begin_date);
            $contentData['contract_end_date'] = ColorgbHelper::date($contract->end_date_est);
        }

        if ($action == 'contract_doc') {
            $contentData['contract_no'] = $contract->contract_no;
            $contentData['contract_invest_value'] = \common\colorgb\ColorgbHelper::numberFormat($contract->invest_value);
            $contentData['contract_fund_unit_price'] = \common\colorgb\ColorgbHelper::numberFormat($contract->fund_unit_price);
            $contentData['contract_invest_unit_qty'] = \common\colorgb\ColorgbHelper::numberFormat($contract->invest_unit_qty);
        }

        $templateArray['msgType'] = $msgType[$action];
        $emailArray = ArrayHelper::map($customer, 'id', 'email_addr');
        $templateArray['msgStatus'] = $msgStatus['pending'];
        $templateArray['sendType'] = $sendType['email'];
        $attachments = $action == 'money_transfer' ? ';' : 'contract/' . ColorgbHelper::encrypt($contract->id) . '.pdf;';
        $msgSendRequest = ColorgbHelper::msgSendRequest($emailArray, $templateArray, $attachments, $contentData,$contract);
        return $msgSendRequest ? json_encode(['status' => 1, 'message' => 'Lịch gửi thông báo đến khách hàng đã được thiếp đặt thành công ']) : json_encode(['status' => 0, 'message' => 'Đã có lỗi xảy ra trong quá trình thiết đặt lịch gửi báo cáo tuần.']);


    }

    /**
     * Finds the Contract model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contract the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $customerGroupIds = ColorgbHelper::getCustomerGroupIds();
        if (Contract::find()->where(['contract.id' => $id])->joinWith('customer')->andWhere(['customer.customer_group_id' => $customerGroupIds])->one() !== null) {
            return Contract::findOne($id);
        } else {
            throw new NotFoundHttpException('Không tìm thấy thông tin yêu cầu');
        }
    }
}
