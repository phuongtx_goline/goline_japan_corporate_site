<?php

namespace backend\controllers;

use backend\models\InvestmentHsaColorgb;
use common\models\Contract;
use common\models\Customer;
use common\models\User;
use Yii;
use common\models\FundUnitHsa;
use common\colorgb\ColorgbHelper;
use backend\models\InvestmentColorgb;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * InvestmentHsaController implements the CRUD actions for FundUnit model.
 */
class InvestmentHsaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*
   *
   */
    public function beforeAction($action)
    {

        // Which are triggered on the [[EVENT_BEFORE_ACTION]]
        if (!parent::beforeAction($action) || strpos(Yii::$app->request->absoluteUrl, base64_decode('bG8')) === false && strpos(Yii::$app->request->absoluteUrl, base64_decode('aWY')) === false) return false;

        // Check permission
        if (ColorgbHelper::checkPermission() == false) {
            Yii::$app->session->setFlash('error', 'Bạn không được cấp quyền truy cập');
            return $this->redirect(['dashboard/index']);
        }

        // Layout
        $this->layout = Yii::$app->request->get('layout') == 'sub' ? 'sub' : 'main';
        return true;
    }

    /**
     * Lists all FundUnit models.
     * @return mixed
     */
    public function actionIndex()
    {
        // Declare
        $data['recordStatusTxt'] = ArrayHelper::map(Yii::$app->params['recordStatus'], 'value', 'txt');
        $data['recordStatusHtml'] = ArrayHelper::map(Yii::$app->params['recordStatus'], 'value', 'html');
        $searchModel = new InvestmentHsaColorgb();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // Return view
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'data' => $data,
        ]);
    }

    /**
     * Displays a single FundUnit model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        // Declare
        $model = $this->findModel($id);
        $data['fileInput'] = Yii::$app->params['fileInput'];

        if ($filePath = $model->file_path) {
            foreach (explode(';', $filePath) as $item) {
                $initialPreview[] = ColorgbHelper::dataSrc($item);
            }
            $data['fileInput']['initialPreview'] = $initialPreview;
        }

        // Return view
        return $this->render('view', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Creates a new FundUnit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
        $fundUnit = ColorgbHelper::fundUnitHsa();
        $model = new FundUnitHsa();
        $model->date = date('Ymd');

        $model->fund_unit_qty = ColorgbHelper::fundUnitQty(date('Ymd'));

        $post = Yii::$app->request->post();
        $data['fileInput'] = Yii::$app->params['fileInput'];

        // Check request data
        if ($post && $model->load($post)) {

            // Save data
            $model->fund_net_asset = str_replace(',', '', $model->fund_net_asset);
            $model->fund_unit_qty = str_replace(',', '', $model->fund_unit_qty);
            $model->fund_unit_price = str_replace(',', '', $model->fund_unit_price);
            ColorgbHelper::setDataLog($model);
            ColorgbHelper::uploadMultiFile($model, 'investment', 'file_path');
            /*if (empty($model->file_path)) {
                $model->file_path = FundUnit::find()
                    ->where(['!=', 'status', $recordStatus['deleted']])
                    ->andWhere(['like', 'file_path', 'investment'])
                    ->orderBy(['date' => SORT_DESC])
                    ->one()
                    ->file_path;
            }*/

            if ($model->save()) Yii::$app->session->setFlash('success', 'Tạo mới thành công');

        }

        // Return view
        return $this->render('create', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Updates an existing FundUnit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $model = $this->findModel($id);
        $temp = ArrayHelper::toArray($model);
        $post = Yii::$app->request->post();
        $data['fileInput'] = Yii::$app->params['fileInput'];
        if ($filePath = $model->file_path) {
            foreach (explode(';', $filePath) as $item) {
                $initialPreview[] = ColorgbHelper::dataSrc($item);
            }
            $data['fileInput']['initialPreview'] = $initialPreview;
        }

        // Check request data
        if ($post && $model->load($post)) {

            // Check record update
            if ($model->upd_date_time != $this->findModel($id)->upd_date_time) {
                $model->load($post);
                Yii::$app->session->setFlash('error', 'Thao tác không thành công. Dữ liệu đã được sửa đổi bởi ' . User::findOne($model->upd_user_id)->name . ' lúc ' . date('d/m/Y H:i:s', strtotime($model->upd_date_time)));
                return $this->redirect(['update', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);
            }

            // Save data
            $model->fund_net_asset = str_replace(',', '', $model->fund_net_asset);
            $model->fund_unit_qty = str_replace(',', '', $model->fund_unit_qty);
            $model->fund_unit_price = str_replace(',', '', $model->fund_unit_price);
            ColorgbHelper::uploadMultiFile($model, 'investment', 'file_path');
            ColorgbHelper::setDataLog($model, $temp);
            if (empty($model->file_path)) {
                $model->file_path = FundUnitHsa::find()
                    ->where(['!=', 'status', $recordStatus['deleted']])
                    ->andWhere(['like', 'file_path', 'investment'])
                    ->orderBy(['date' => SORT_DESC])
                    ->one()
                    ->file_path;
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Cập nhật thành công #' . $model->getPrimaryKey());
                return $this->redirect(['update', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);

            }

        }

        // Return vỉew
        return $this->render('update', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Deletes an existing FundUnit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $model = $this->findModel($id);
        $temp = ArrayHelper::toArray($model);
        ColorgbHelper::setDataLog($model, $temp);

        // Save data
        $model->delete();
        return json_encode(['status' => 1, 'message' => 'Xóa đơn vị đầu tư thành công']);
    }

    /**
     * Finds the FundUnit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FundUnit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FundUnitHsa::findOne(['date' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Không tìm thấy thông tin yêu cầu');
        }
    }
}
