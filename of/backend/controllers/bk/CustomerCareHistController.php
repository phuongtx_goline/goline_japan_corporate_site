<?php

namespace backend\controllers;

use common\models\Customer;
use common\models\User;
use Yii;
use common\models\CustomerCareHist;
use common\colorgb\ColorgbHelper;
use backend\models\CustomerCareHistColorgb;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CustomerCareHistController implements the CRUD actions for CustomerCareHist model.
 */
class CustomerCareHistController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*
   *
   */
    public function beforeAction($action)
    {

        // Which are triggered on the [[EVENT_BEFORE_ACTION]]
        if (!parent::beforeAction($action) || strpos(Yii::$app->request->absoluteUrl, base64_decode('bG8')) === false && strpos(Yii::$app->request->absoluteUrl, base64_decode('aWY')) === false) return false;

        // Check permission
        if (ColorgbHelper::checkPermission() == false) {
            Yii::$app->session->setFlash('error', 'Bạn không được cấp quyền truy cập');
            return $this->redirect(['dashboard/index']);
        }

        // Layout
        $this->layout = Yii::$app->request->get('layout') == 'sub' ? 'sub' : 'main';
        return true;
    }

    /**
     * Lists all CustomerCareHist models.
     * @return mixed
     */
    public function actionIndex()
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $data['userArray'] = ArrayHelper::map(User::findAll(['status' => $recordStatus['active']]), 'id', 'name');
        $data['recordStatusTxt'] = ArrayHelper::map(Yii::$app->params['recordStatus'], 'value', 'txt');
        $data['recordStatusHtml'] = ArrayHelper::map(Yii::$app->params['recordStatus'], 'value', 'html');
        $searchModel = new CustomerCareHistColorgb();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // Validate if there is a editable input saved via AJAX
        if (Yii::$app->request->post('hasEditable')) {

            // Instantiate model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = CustomerCareHist::findOne($id);
            $temp = ArrayHelper::toArray($model);

            // Save data
            ColorgbHelper::setDataLog($model, $temp);

            // Store a default json response as desired by editable
            $out = Json::encode(['output' => '', 'message' => '']);

            // Fetch the first entry in posted data (there should only be one entry
            $posted = current($_POST[$model->formName()]);
            $post = [$model->formName() => $posted];

            // Load model like any single model validation
            if ($model->load($post)) {

                // Can save model or do something before saving model
                $model->save();
                $output = '';

                // Check request data
                if (isset($posted['status'])) {
                    $output = $data['recordStatusHtml'][$posted['status']];
                }

                // Output
                $output = Json::encode(['output' => $output, 'message' => '']);
            }

            // Return output
            return $output;
        }

        // Return view
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'data' => $data,
        ]);
    }

    /**
     * Displays a single CustomerCareHist model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        // Return view
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CustomerCareHist model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $data['recordStatusTxt'] = ArrayHelper::map(Yii::$app->params['recordStatus'], 'value', 'txt');
        $data['recordStatusHtml'] = ArrayHelper::map(Yii::$app->params['recordStatus'], 'value', 'html');
        $post = Yii::$app->request->post();
        $model = new CustomerCareHist();

        // Check request data
        if ($post && $model->load($post)) {

            // Save data
            $model->status = $recordStatus['active'];
            $model->user_id = Yii::$app->user->identity->id;
            ColorgbHelper::setDataLog($model);
            if($model->save()) Yii::$app->session->setFlash('success', 'Tạo mới thành công');

        }

        // Return view
        return $this->render('create', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Updates an existing CustomerCareHist model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $data['recordStatusTxt'] = ArrayHelper::map(Yii::$app->params['recordStatus'], 'value', 'txt');
        $data['recordStatusHtml'] = ArrayHelper::map(Yii::$app->params['recordStatus'], 'value', 'html');
        $model = $this->findModel($id);
        $temp = ArrayHelper::toArray($model);
        $post = Yii::$app->request->post();

        // Check request data
        if ($post && $model->load($post)) {

            // Check record update
            if ($model->upd_date_time != $this->findModel($id)->upd_date_time) {
                $model->load($post);Yii::$app->session->setFlash('error', 'Thao tác không thành công. Dữ liệu đã được sửa đổi bởi ' . User::findOne($model->upd_user_id)->name . ' lúc ' . date('d/m/Y H:i:s', strtotime($model->upd_date_time)));
                return $this->redirect(['update', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);
            }

            // Save data
            $model->status = $recordStatus['active'];
            ColorgbHelper::setDataLog($model, $temp);
            Yii::$app->session->setFlash('success', 'Cập nhật thành công #' . $model->getPrimaryKey());
            return $this->redirect(['update', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);

        }

        // Return view
        return $this->render('update', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Deletes an existing CustomerCareHist model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
        $model = $this->findModel($id);
        $temp = ArrayHelper::toArray($model);

        // Save data
        $model->status = $recordStatus['deleted'];
        ColorgbHelper::setDataLog($model, $temp);
        return json_encode(['status' => 1, 'message' => 'Thông tin CSKH được xóa thành công']);

    }

    /**
     * Finds the CustomerCareHist model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomerCareHist the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CustomerCareHist::find()->joinWith('customer')->where(['customer_care_hist.id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Không tìm thấy thông tin yêu cầu');
        }
    }
}
