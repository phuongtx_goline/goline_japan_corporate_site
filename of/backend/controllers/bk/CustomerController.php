<?php

namespace backend\controllers;

use common\colorgb\ColorgbHelper;
use common\models\Code;
use common\models\Contract;
use common\models\CustomerAttribute;
use common\models\CustomerGroup;
use common\models\FundUnit;
use common\models\ReportSentMark;
use common\models\Setting;
use Symfony\Component\Console\Tests\Output\ConsoleOutputTest;
use Yii;
use common\models\Customer;
use backend\models\CustomerColorgb;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use common\models\User;

/**
 * CustomerController implements the CRUD actions for Customer model.
 */
class CustomerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*
    *
    */
    public function beforeAction($action)
    {

        // Which are triggered on the [[EVENT_BEFORE_ACTION]]
        if (!parent::beforeAction($action) || strpos(Yii::$app->request->absoluteUrl, base64_decode('bG8')) === false && strpos(Yii::$app->request->absoluteUrl, base64_decode('aWY')) === false) return false;

        // Check permission
        if (ColorgbHelper::checkPermission() == false) {
            Yii::$app->session->setFlash('error', 'Bạn không được cấp quyền truy cập');
            return $this->redirect(['dashboard/index']);
        }

        // Layout
        $this->layout = Yii::$app->request->get('layout') == 'sub' ? 'sub' : 'main';
        return true;
    }


    /**
     * Lists all Customer models.
     * @return mixed
     */
    public function actionIndex()
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $msgType = ArrayHelper::map(Yii::$app->params['msgType'], 'key', 'value');
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
        $customerGroupIds = ColorgbHelper::getCustomerGroupIds();
        $data['customerGroup'] = ArrayHelper::map(CustomerGroup::findAll(['id' => $customerGroupIds, 'status' => $recordStatus['active']]), 'id', 'name');
        $data['customerStatus'] = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $data['contractStatusTxt'] = ArrayHelper::map(Yii::$app->params['contractStatus'], 'value', 'txt');
        $data['contractStatusTxt'][4] = 'Sắp hết hạn';
        ksort($data['contractStatusTxt']);
        $data['customerStatusTxt'] = ArrayHelper::map(Yii::$app->params['customerStatus'], 'value', 'txt');
        $data['customerStatusHtml'] = ArrayHelper::map(Yii::$app->params['customerStatus'], 'value', 'html');
        $data['fundUnit'] = ColorgbHelper::fundUnit();
        $data['fundUnitHsa'] = ColorgbHelper::fundUnitHsa();
        $data['userArray'] = ArrayHelper::map(User::findAll(['status' => $recordStatus['active']]), 'id', 'name');
        $data['userCaredArray'] = ArrayHelper::map(User::findAll(['status' => $recordStatus['active'], 'is_admin' => -1]), 'id', 'name');

        $searchModel = new CustomerColorgb();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // Return view
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'data' => $data,
        ]);
    }

    /**
     * Lists all Customer models.
     * @return mixed
     */
    public function actionGroup()
    {
        $data['group'] = true;
        $searchModel = new CustomerColorgb();
        $dataProvider = $searchModel->searchGroup(Yii::$app->request->queryParams);

        // Return view
        return $this->render('group', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'data' => $data,
        ]);
    }

    /**
     * Displays a single Customer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        // Declare
        $model = $this->findModel($id);
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $data['fileInput'] = Yii::$app->params['fileInput'];
        $data['fileInput']['initialPreview'] = $model->image ? ColorgbHelper::dataSrc($model->image) : '';
        $data['customerStatus'] = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $data['customerParent'] = Customer::findOne($model->parent_id);
        $data['customerGroup'] = ArrayHelper::map(CustomerGroup::findAll(['status' => $recordStatus['active']]), 'id', 'name');
        $data['customerAttribute'] = Code::find()->where(['code.group' => 'CUSTOMER_ATTRIBUTE', 'code.status' => $recordStatus['active']])->all();
        $data['customerProfession'] = ArrayHelper::map(Code::findAll(['group' => 'PROFESSION', 'status' => $recordStatus['active']]), 'id', 'value1');
        $data['infoSource'] = ArrayHelper::map(Code::findAll(['group' => 'INFO_SOURCE', 'status' => $recordStatus['active']]), 'id', 'value1');
        $data['userArray'] = ArrayHelper::map(User::findAll(['status' => $recordStatus['active']]), 'id', 'name');
        $data['userCaredArray'] = ArrayHelper::map(User::findAll(['status' => $recordStatus['active'], 'is_admin' => -1]), 'id', 'name');

        // Return view
        return $this->render('view', [
            'model' => $model,
            'data' => $data,
        ]);
    }


    /**
     * Creates a new Customer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        // Declare
        $transaction = Yii::$app->db->beginTransaction();
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $post = Yii::$app->request->post();
        $data['fileInput'] = Yii::$app->params['fileInput'];
        $data['customerParent'] = [];//TODO-ArrayHelper::map(Customer::findAll(['parent_id' => 0]), 'id', 'name');
        $data['customerProfession'] = ArrayHelper::map(Code::findAll(['group' => 'PROFESSION', 'status' => $recordStatus['active']]), 'id', 'value1');
        $data['customerGroup'] = ArrayHelper::map(CustomerGroup::findAll(['status' => $recordStatus['active']]), 'id', 'name');
        $data['infoSource'] = ArrayHelper::map(Code::findAll(['group' => 'INFO_SOURCE', 'status' => $recordStatus['active']]), 'id', 'value1');
        $data['userArray'] = ArrayHelper::map(User::findAll(['status' => $recordStatus['active']]), 'id', 'name');
        $data['userCaredArray'] = ArrayHelper::map(User::findAll(['status' => $recordStatus['active'], 'is_admin' => -1]), 'id', 'name');
        $data['customerAttribute'] = Code::findAll(['group' => 'CUSTOMER_ATTRIBUTE', 'status' => $recordStatus['active']]);
        $model = new Customer();

        // Try catch
        try {

            // Check data
            $obj = Customer::find()->where(['mobile' => $post[$model->formName()]['mobile']])->andWhere(['!=', 'status', $recordStatus['deleted']])->one();
            if ($obj) {
                $model->load($post);
                Yii::$app->session->setFlash('error', 'Thao tác không thành công. Số điện thoại đã tồn tại trong cơ sở dữ liệu');

            } else if ($post && $model->load($post)) {

                // Save data
                $model->customer_group_id = $model->customer_group_id ? $model->customer_group_id : 1;
                $model->parent_id = $model->parent_id ? $model->parent_id : 0;
                //$model->cared_user_id = Yii::$app->user->identity->is_admin == -1 ? Yii::$app->user->identity->getId() : 0;
                $model->cared_user_id = Yii::$app->user->identity->getId();

                /* $model->active_date_time = date('Y-m-d H:i:s');
                $model->active_ip_addr = Yii::$app->getRequest()->getUserIP();
                $model->active_user_id = Yii::$app->user->identity->id;
                $model->status = $customerStatus['active'];*/
                ColorgbHelper::setDataLog($model);
                ColorgbHelper::uploadFile($model, 'customer', 'image');

                // Set customer attribute
                self::setCustomerAttribute($post[$model->formName()]['attribute'], $model->getPrimaryKey());

                // Commit
                $transaction->commit();
                if ($model->getPrimaryKey()) {
                    Yii::$app->session->setFlash('success', 'Tạo mới thành công');
                    //return $this->redirect(['update', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);
                }

            }


        } catch (Exception $e) {

            // Rollback
            $transaction->rollback();
            Yii::$app->session->setFlash('error', 'Thao tác không thành công. Dữ liệu chưa được lưu. '.json_encode($e->errorInfo));

        }

        // Return
        return $this->render('create', [
            'model' => $model,
            'data' => $data,
        ]);

    }

    /**
     * Updates an existing Customer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        // Declare
        $transaction = Yii::$app->db->beginTransaction();
        $model = $this->findModel($id);
        $temp = ArrayHelper::toArray($model);
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $post = Yii::$app->request->post();
        $data['fileInput'] = Yii::$app->params['fileInput'];
        $data['fileInput']['initialPreview'] = $model->image ? ColorgbHelper::dataSrc($model->image) : '';
        $data['customerStatus'] = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $data['customerParent'] = Customer::findOne($model->parent_id);
        $data['customerGroup'] = ArrayHelper::map(CustomerGroup::findAll(['status' => $recordStatus['active']]), 'id', 'name');
        $data['customerAttribute'] = Code::findAll(['group' => 'CUSTOMER_ATTRIBUTE', 'status' => $recordStatus['active']]);
        $data['customerAttributeSelected'] = CustomerAttribute::findAll(['customer_id' => $id, 'status' => $recordStatus['active']]);
        $data['customerProfession'] = ArrayHelper::map(Code::findAll(['group' => 'PROFESSION', 'status' => $recordStatus['active']]), 'id', 'value1');
        $data['infoSource'] = ArrayHelper::map(Code::findAll(['group' => 'INFO_SOURCE', 'status' => $recordStatus['active']]), 'id', 'value1');
        $data['userArray'] = ArrayHelper::map(User::findAll(['status' => $recordStatus['active']]), 'id', 'name');
        $data['userCaredArray'] = ArrayHelper::map(User::findAll(['status' => $recordStatus['active'], 'is_admin' => -1]), 'id', 'name');

        // Try catch
        try {

            // Check request data
            if ($post && $model->load($post)) {

                // Check record update
                if ($model->upd_date_time != $this->findModel($id)->upd_date_time) {
                    $model->load($post);
                    Yii::$app->session->setFlash('error', 'Thao tác không thành công. Dữ liệu đã được sửa đổi bởi ' . User::findOne($model->upd_user_id)->name . ' lúc ' . date('d/m/Y H:i:s', strtotime($model->upd_date_time)));
                    return $this->redirect(['update', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);
                }

                // Save data
                $model->customer_group_id = $model->customer_group_id ? $model->customer_group_id : 0;
                $model->parent_id = $model->parent_id ? $model->parent_id : 0;
                //$model->cared_user_id = Yii::$app->user->identity->is_admin == -1 ? Yii::$app->user->identity->getId() : $model->cared_user_id;

                ColorgbHelper::setDataLog($model, $temp);
                ColorgbHelper::uploadFile($model, 'customer', 'image');

                // Set customer attribute
                self::setCustomerAttribute($post[$model->formName()]['attribute'], $model->getPrimaryKey());

                // Commit
                $transaction->commit();
                Yii::$app->session->setFlash('success', 'Cập nhật thành công #' . $model->getPrimaryKey());
                return $this->redirect(['update', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);

            }


        } catch (Exception $e) {

            // Rollback
            $transaction->rollback();
            Yii::$app->session->setFlash('error', 'Thao tác không thành công. Dữ liệu chưa được lưu');

        }


        // Return view
        return $this->render('update', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Deletes an existing Customer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
        $model = $this->findModel($id);
        $temp = ArrayHelper::toArray($model);

        // Check customer status
        if ($model->status == $customerStatus['active']) {
            return json_encode(['status' => 0, 'message' => 'Không thể xóa khách hàng với trạng thái đang hiệu lực']);
        }

        // Check contract status
        $contractActive = Contract::findOne(['customer_id' => $model->id, 'status' => $contractStatus['active']]);
        if ($contractActive) {
            return json_encode(['status' => 0, 'message' => 'Không thể xóa khách hàng khi có hợp đồng với trạng thái đang hiệu lực']);
        }

        // Save data
        $model->status = $recordStatus['deleted'];
        ColorgbHelper::setDataLog($model, $temp);
        return json_encode(['status' => 1, 'message' => 'Xóa khách hàng thành công']);

    }


    /**
     * Activate customer
     */
    public function actionActivate($id)
    {
        // Declare
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $model = $this->findModel($id);
        $temp = ArrayHelper::toArray($model);

        $model->active_date_time = date('Y-m-d H:i:s');
        $model->active_ip_addr = Yii::$app->getRequest()->getUserIP();
        $model->active_user_id = Yii::$app->user->identity->id;
        $model->status = $customerStatus['active'];
        $save = $model->save();
        ColorgbHelper::setDataLog($model, $temp);
        return $save ? json_encode(['status' => 1, 'message' => 'Khách hàng đã được kích hoạt thành công']) : json_encode(['status' => 0, 'message' => 'Không thể kích hoạt khách hàng này']);
    }


    /**
     * Deactivate customer
     */
   /* public function actionDeactivate($id)
    {
        // Declare
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
        $model = $this->findModel($id);
        $temp = ArrayHelper::toArray($model);

        // Check contract status
        $contractActive = Contract::findOne(['customer_id' => $id, 'status' => $contractStatus['active']]);
        if ($contractActive) {
            Yii::$app->session->setFlash('error', 'Không thể bỏ kích hoạt bởi vì khách hàng này đang có hợp đồng với trạng thái hiệu lực');
            return $this->redirect(['update', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);
        }

        // Save data
        $model->close_date_time = date('Y-m-d H:i:s');
        $model->close_ip_addr = Yii::$app->getRequest()->getUserIP();
        $model->close_user_id = Yii::$app->user->identity->id;
        $model->status = $customerStatus['deactivate'];
        $model->save() ? Yii::$app->session->setFlash('success', 'Khách hàng đã được bỏ kích hoạt thành công') : Yii::$app->session->setFlash('error', 'Không thể bỏ kích hoạt khách hàng này');
        ColorgbHelper::setDataLog($model, $temp);
        return $this->redirect(['update', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);
    }*/


    /**
     * Set customer attribute
     */
    protected function setCustomerAttribute($customerAttribute, $customerId)
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');

        // Update customer attribute status
        CustomerAttribute::updateAll(['status' => $recordStatus['deleted']], ['customer_id' => $customerId]);

        // Check is array
        if (is_array($customerAttribute)) {
            foreach ($customerAttribute as $item) {

                // Customer attribute
                $customerAttribute = CustomerAttribute::findOne(['customer_id' => $customerId, 'attribute_id' => $item['key'], 'value' => $item['value']]);
                $customerAttribute = $customerAttribute ? $customerAttribute : new CustomerAttribute();
                $customerAttribute->customer_id = $customerId;
                $customerAttribute->attribute_id = $item['key'];
                $customerAttribute->value = $item['value'];
                $customerAttribute->status = $recordStatus['active'];
                ColorgbHelper::setDataLog($customerAttribute);

            }
        }

    }

    /*
     * Reset password
     */
    public function actionResetPassword($id)
    {

        // Declare
        $customer = Customer::findOne([
            'status' => ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value')['active'],
            'is_locked' => 0,
            'id' => $id,
        ]);

        // Check customer status
        if (!$customer) {
            return json_encode(['status' => 0, 'message' => 'Không thể đặt lại mật khẩu. Khách hàng chưa được kích hoạt hoặc đang bị khóa đăng nhập. ']);
        }

        // Check request data
        if (!Customer::isPasswordResetTokenValid($customer->password_reset_token)) {
            $customer->generatePasswordResetToken();
            if (!$customer->save()) {
                return false;
            }
        }

        // Send mail
        $sendMail = ColorgbHelper::sendMail('Thiết lập lại mật khẩu', 'passwordResetToken', $customer->email_addr, $customer);
        return $sendMail ? json_encode(['status' => 1, 'message' => 'Một thông tin thiết lập lại mật khẩu đã được gửi đến email của khách hàng']) : json_encode(['status' => 0, 'message' => 'Không thể đặt lại mật khẩu cho cho khách hàng này.']);

    }

    /*
     * Send notice
     */
    public function actionSendNotice()
    {
        // Declare
        $attachments = '';
        $contentData = array();
        $fundUnit = ColorgbHelper::fundUnit();
        $fundUnitHsa = ColorgbHelper::fundUnitHsa();
        $action = Yii::$app->request->get('action');
        $id = Yii::$app->request->get('id');
        $type = Yii::$app->request->get('type');
        $sentMarkType = ArrayHelper::map(Yii::$app->params['sentMarkType'], 'key', 'value');
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
        $msgStatus = ArrayHelper::map(Yii::$app->params['msgStatus'], 'key', 'value');
        $msgType = ArrayHelper::map(Yii::$app->params['msgType'], 'key', 'value');
        $sendType = ArrayHelper::map(Yii::$app->params['sendType'], 'key', 'value');
        $customerGroupIds = ColorgbHelper::getCustomerGroupIds();


        /*
         *
         */
        $customer = Customer::find()
            ->joinWith('contract')
            ->select('customer.id,customer.email_addr')
            ->where([
                'customer.status' => $customerStatus['active'],
                'contract.status' => $contractStatus['active'],
            ]);

        // Check user role
        /*if(Yii::$app->user->identity->is_admin == -1){
            $customer->andWhere(['customer.cared_user_id' => Yii::$app->user->identity->getId()]) ;

        }else{
            $customer->andWhere(['customer.customer_group_id' => $customerGroupIds]) ;
        }*/
        $customer->andWhere(['customer.customer_group_id' => $customerGroupIds]) ;

        // Check action
        if (!$action) return json_encode(['status' => 0, 'message' => 'Thao tác không thành công.']);

        // Get customer id by sent mark
        $reportSentMark = ReportSentMark::find()
            ->select('customer_id')
            ->where([
                'status' => $recordStatus['active'],
                'type' => $msgType[$action],
                'sent_date' => date('Ymd')
            ])
            ->all();


        // Check action
        if ($action == 'weekly_report') {

            // Customer email array
            if ($id) {
                $customer = $customer->andWhere(['customer.id' => $id])->all();
                $attachments = 'weekly-report/' . ColorgbHelper::encrypt($id) . '.pdf;';
                $attachments .= $fundUnit['last']->file_path;

            } elseif ($type == 'pending') {

                $customer = $customer->andWhere(['not in', 'customer.id', ArrayHelper::map($reportSentMark, 'customer_id', 'customer_id')])->all();

            } else {
                $customer = $customer->all();
            }

            // Generate file

            $contentData['investment_to_date'] = ColorgbHelper::date($fundUnit['last']->date);
            $contentData['date'] = date('d/m/Y');
            $emailArray = ArrayHelper::map($customer, 'id', 'email_addr');
            if (!$emailArray) return json_encode(['status' => 0, 'message' => 'Thao tác không thành công. Tài khoản chưa được kích hoạt.']);

        }

        // Check action
        if ($action == 'contract_renewal') {

            // Customer email array
            $expireAlert = intval(Setting::findOne(['CONTRACT_EXPIRE_ALERT_DAY'])->param_value);
            $expire = intval(date('Ymd') + $expireAlert);
            $customer = $customer->joinWith('contract')->andWhere(['not in', 'customer.id', $reportSentMark])->andWhere(['<=', 'contract.end_date_est', $expire])->andWhere(['>', 'contract.end_date_est', date('Ymd')])->all();
            $emailArray = ArrayHelper::map($customer, 'id', 'email_addr');

            if (!$emailArray) {
                return json_encode(['status' => 0, 'message' => 'Thao tác không thành công. Thông báo gia hạn hợp đồng đã được gửi đến khách hàng.']);
            } else {

                // Contract array
                $contractExpire = Contract::find()->select('id')->where(['status' => $contractStatus['active']])->andWhere(['not in', 'customer_id', $reportSentMark])->andWhere(['<=', 'end_date_est', $expire])->andWhere(['>', 'end_date_est', date('Ymd')])->all();
                foreach ($contractExpire as $item) {
                    ColorgbHelper::contractDoc($item->id);
                    $attachments .= 'contract/' . ColorgbHelper::encrypt($item->id) . '.pdf;';
                }
            }

        }

        // Send mail
        $templateArray['msgType'] = $msgType[$action];
        $templateArray['msgStatus'] = $msgStatus['pending'];
        $templateArray['sendType'] = $sendType['email'];
        ColorgbHelper::msgSendRequest($emailArray, $templateArray, $attachments,$contentData);

        return json_encode(['status' => 1, 'message' => 'Lịch gửi thông báo đến khách hàng đã được thiếp đặt thành công ']);

    }

    /*
     * Weekly report download
     */
    public function actionWeeklyReportDownload($id)
    {
        ob_start();
        $model = Customer::findOne($id);
        $phpPdf = ColorgbHelper::phpPdf($id, 'weekly-report', 'weekly-report/' . ColorgbHelper::encrypt($id) . '.pdf');
        if ($phpPdf) {
            $pdf = '[BC-PI] ' . $model->name . ' (' . date('d-m-Y') . ').pdf';
            header('Content-Disposition: attachment; filename="' . $pdf . '"; filename*=utf-8\' \'' . rawurlencode($pdf));
            header("Content-type: application/pdf");
            readfile(Yii::$app->params['cdnUrl'] . '/data/weekly-report/' . ColorgbHelper::encrypt($id) . '.pdf');
        }
    }

    /*
     *
     */
    public function actionWeeklyReportHsaDownload($id)
    {
        ob_start();
        $model = Customer::findOne($id);
        $phpPdf = ColorgbHelper::phpPdf($id, 'weekly-report-hsa', 'weekly-report-hsa/' . ColorgbHelper::encrypt($id) . '.pdf');
        if ($phpPdf) {
            $pdf = '[BC-HSA] ' . $model->name . ' (' . date('d-m-Y') . ').pdf';
            header('Content-Disposition: attachment; filename="' . $pdf . '"; filename*=utf-8\' \'' . rawurlencode($pdf));
            header("Content-type: application/pdf");
            readfile(Yii::$app->params['cdnUrl'] . '/data/weekly-report-hsa/' . ColorgbHelper::encrypt($id) . '.pdf');
        }
    }

    /**
     * Finds the Customer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Customer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        // Check user role
        /*if (Yii::$app->user->identity->is_admin == -1){
            $model = Customer::findOne(['id' => $id, 'cared_user_id' => Yii::$app->user->identity->getId()]);
        }else{
            $customerGroupIds = ColorgbHelper::getCustomerGroupIds();
            $model = Customer::findOne(['id' => $id, 'customer_group_id' => $customerGroupIds]);
        }*/

        $customerGroupIds = ColorgbHelper::getCustomerGroupIds();
        $model = Customer::findOne(['id' => $id, 'customer_group_id' => $customerGroupIds]);

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Không tìm thấy thông tin yêu cầu');
        }
    }
}
