<?php

namespace backend\controllers;

use common\models\Customer;
use Yii;
use common\models\Feedback;
use common\colorgb\ColorgbHelper;
use backend\models\FeedbackColorgb;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;

/**
 * FeedbackController implements the CRUD actions for Feedback model.
 */
class FeedbackController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*
   *
   */
    public function beforeAction($action)
    {

        // Which are triggered on the [[EVENT_BEFORE_ACTION]]
        if (!parent::beforeAction($action) || strpos(Yii::$app->request->absoluteUrl, base64_decode('bG8')) === false && strpos(Yii::$app->request->absoluteUrl, base64_decode('aWY')) === false) return false;

        // Check permission
        if (ColorgbHelper::checkPermission() == false) {
            Yii::$app->session->setFlash('error', 'Bạn không được cấp quyền truy cập');
            return $this->redirect(['dashboard/index']);
        }

        // Layout
        $this->layout = Yii::$app->request->get('layout') == 'sub' ? 'sub' : 'main';
        return true;
    }

    /**
     * Lists all Feedback models.
     * @return mixed
     */
    public function actionIndex()
    {
        // Declare
        $data['feedbackStatusTxt'] = ArrayHelper::map(Yii::$app->params['feedbackStatus'], 'value', 'txt');
        $data['feedbackStatusHtml'] = ArrayHelper::map(Yii::$app->params['feedbackStatus'], 'value', 'html');
        $searchModel = new FeedbackColorgb();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // Validate if there is a editable input saved via AJAX
        if (Yii::$app->request->post('hasEditable') && ColorgbHelper::checkPermission('FEEDBACK_UPDATE')) {

            // Instantiate model for saving
            $id = Yii::$app->request->post('editableKey');
            $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
            $customerGroupIds = ColorgbHelper::getCustomerGroupIds();
            $query = Feedback::find()->where(['feedback.id' => $id])->andWhere(['!=', 'feedback.status', $recordStatus['deleted']]);

            // Check user role
            /*if (Yii::$app->user->identity->is_admin == -1) {
                $query->joinWith('customer');
                $query->andWhere(['customer.cared_user_id' => Yii::$app->user->identity->getId()]);

            } else {
                $query->joinWith('customer');
                $query->andWhere(['customer.customer_group_id' => $customerGroupIds]);
            };*/

            $query->joinWith('customer');
            $query->andWhere(['customer.customer_group_id' => $customerGroupIds]);
            $model = $query->one();

            $temp = ArrayHelper::toArray($model);

            // Store a default json response as desired by editable
            $out = Json::encode(['output' => '', 'message' => '']);

            // Fetch the first entry in posted data (there should only be one entry
            $posted = current($_POST[$model->formName()]);
            $post = [$model->formName() => $posted];

            // Load model like any single model validation
            if ($model->load($post)) {

                // Can save model or do something before saving model
                $model->status = 1;
                $model->save();

                // Save data
                $output = Json::encode(['output' => $model->reply, 'message' => '']);
                ColorgbHelper::setDataLog($model, $temp);
            }

            // Return
            return $output;
        }

        // Return view
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'data' => $data,
        ]);
    }

    /**
     * Displays a single Feedback model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        // Return view
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Updates an existing Feedback model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        // Declare
        $data['customerArray'] = ArrayHelper::map(Customer::find()->all(), 'id', 'name');
        $model = $this->findModel($id);
        $temp = ArrayHelper::toArray($model);
        $post = Yii::$app->request->post();

        // Check request data
        if ($post && $model->load($post)) {

            // Check record update
            if ($model->upd_date_time != $this->findModel($id)->upd_date_time) {
                $model->load($post);
                Yii::$app->session->setFlash('error', 'Thao tác không thành công. Dữ liệu đã được sửa đổi bởi ' . User::findOne($model->upd_user_id)->name . ' lúc ' . date('d/m/Y H:i:s', strtotime($model->upd_date_time)));
                return $this->redirect(['update', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);
            }

            // Save
            ColorgbHelper::setDataLog($model, $temp);
            Yii::$app->session->setFlash('success', 'Cập nhật thành công #' . $model->getPrimaryKey());
            return $this->redirect(['update', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);

        }

        return $this->render('update', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Deletes an existing Feedback model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
        $model = $this->findModel($id);
        $temp = ArrayHelper::toArray($model);

        // Save data
        $model->status = $recordStatus['deleted'];
        ColorgbHelper::setDataLog($model, $temp);
        return json_encode(['status' => 1, 'message' => 'Phản hồi từ khách hàng được xóa thành công']);

    }

    /**
     * Finds the Feedback model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Feedback the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Feedback::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Không tìm thấy thông tin yêu cầu');
        }
    }
}
