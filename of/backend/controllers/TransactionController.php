<?php

namespace backend\controllers;

use backend\models\BalanceColorgb;
use backend\models\FeeRateColorgb;
use backend\models\FundInfoColorgb;
use backend\models\FundInfoForm;
use backend\models\NavColorgb;
use backend\models\TradeFeeColorgb;
use backend\models\TransactionColorgb;
use common\colorgb\ColorgbHelper;
use common\models\CORCUSTOMER;
use common\models\CORTSECTRANSACTION;
use common\models\User;
use Yii;
use backend\models\BranchColorgb;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TransactionController implements the CRUD actions for CORTSECTRANSACTION model.
 */
class TransactionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                ],
            ],
        ];
    }

    /*
    *
    */
    public function beforeAction($action)
    {

        // Which are triggered on the [[EVENT_BEFORE_ACTION]]
        if (!parent::beforeAction($action) || strpos(Yii::$app->request->absoluteUrl, base64_decode('bG8')) === false && strpos(Yii::$app->request->absoluteUrl, base64_decode('aWY')) === false) return false;

        // Layout
        $this->layout = Yii::$app->request->get('layout') == 'sub' ? 'sub' : 'main';
        return true;
    }

    /**
     * Lists all CORTSECTRANSACTION models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TransactionColorgb();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['REG_DATE_TIME' => SORT_DESC];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CORTSECTRANSACTION model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CORTSECTRANSACTION model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CORTSECTRANSACTION();
        $model->STATUS = 0;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            //
            $model->NAV = ColorgbHelper::price($model->NAV);
            ColorgbHelper::setDataLog($model);

            Yii::$app->session->setFlash('success', 'Tạo mới thành công');

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CORTSECTRANSACTION model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            //
            $model->NAV = ColorgbHelper::price($model->NAV);
            ColorgbHelper::setDataLog($model, $this->findModel($id));

            Yii::$app->session->setFlash('success', 'Cập nhật thành công');

        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CORTSECTRANSACTION model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->STATUS == 0) {
            ColorgbHelper::setDataLog($model, $model);
            $model->delete();
        }else{
            Yii::$app->session->setFlash('colorgbJGrowl', 'Thao tác không thành công vì lí do dữ liệu đang có hiệu lực trên hệ thống');

        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the CORTSECTRANSACTION model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CORTSECTRANSACTION the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CORTSECTRANSACTION::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Không tìm thấy thông tin yêu cầu');
        }
    }

    /*
     *
     */
    public function actionApprove($id)
    {
        // Declare
        $model = $this->findModel($id);

        // Save data
        $model->STATUS = 1;
        ColorgbHelper::setDataLog($model, $this->findModel($id));
        return json_encode(['status' => 1, 'message' => 'Kích hoạt thành công']);

    }
}
