<?php

namespace backend\controllers;

use common\colorgb\ColorgbHelper;
use common\models\Code;
use common\models\CORCUSTOMER;
use common\models\CORTCUSTOMERENTRUST;
use common\models\CORTCUSTOMERSIGNER;
use common\models\Setting;
use Yii;
use backend\models\CustomerColorgb;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;

/**
 * CustomerController implements the CRUD actions for CORCUSTOMER model.
 */
class CustomerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST','GET'],
                ],
            ],
        ];
    }

    /*
    *
    */
    public function beforeAction($action)
    {

        // Which are triggered on the [[EVENT_BEFORE_ACTION]]
        if (!parent::beforeAction($action) || strpos(Yii::$app->request->absoluteUrl, base64_decode('bG8')) === false && strpos(Yii::$app->request->absoluteUrl, base64_decode('aWY')) === false) return false;

        // Layout
        $this->layout = Yii::$app->request->get('layout') == 'sub' ? 'sub' : 'main';
        return true;
    }

    /**
     * Lists all CORCUSTOMER models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerColorgb();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['REG_DATE_TIME' => SORT_DESC];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CORCUSTOMER model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CORCUSTOMER model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $st = Setting::findOne(['param_name' => 'TVLK'])->param_value;
        $cn = explode('C', CORCUSTOMER::find()->where(['like', 'CUST_NO', $st . 'C'])->orderBy(['CUST_CD' => SORT_DESC])->one()->CUST_NO)[1];
        $model = new CORCUSTOMER();
        $model->CUST_NO = $st . 'C' . sprintf('%06d', ($cn + 1));
        $model->ACCO_STS = 1;
        $model->STATUS = 0;
        $model->VSD_STATUS = 0;
        $model->OPEN_DATE = 0;
        $model->CLOSE_DATE = 0;
        $model->COUNTRY_CD = 378;
        $data['fileInputCe'] = Yii::$app->params['fileInput'];
        $data['fileInputCs'] = Yii::$app->params['fileInput'];

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            ColorgbHelper::setDataLog($model);

            //
            self::saveCe($model);

            //
            self::saveCs($model);

            Yii::$app->session->setFlash('success', 'Tạo mới thành công');
        }

        return $this->render('create', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Updates an existing CORCUSTOMER model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $data['fileInputCe'] = Yii::$app->params['fileInput'];
        $data['fileInputCs'] = Yii::$app->params['fileInput'];
        $ce = CORTCUSTOMERENTRUST::findOne(['CUST_CD' => $model->CUST_CD]);
        $data['cs'] = CORTCUSTOMERSIGNER::find()->where(['CUST_CD' => $model->CUST_CD])->orderBy(['REF_NO'=>SORT_DESC])->all();

        //
        if ($ce) {
            $model->CE_ENTRUST_CUST_NAME = $ce->ENTRUST_CUST_NAME;
            $model->CE_ENTRUST_CUST_NO = $ce->ENTRUST_CUST_NO;
            $model->CE_APPROVAL_CD = $ce->APPROVAL_CD;
            $model->CE_ID_NUMBER = $ce->ID_NUMBER;
            $model->CE_ISSUE_LOCATION_CD = $ce->ISSUE_LOCATION_CD;
            $model->CE_ISSUE_DATE = $ce->ISSUE_DATE;
            $model->CE_BIRTHDAY = $ce->BIRTHDAY;
            $model->CE_SEX = $ce->SEX;
            $model->CE_ADDR = $ce->ADDR;
            $model->CE_TEL_NO = $ce->TEL_NO;
            $model->CE_EMAIL_ADRS = $ce->EMAIL_ADRS;
            $model->CE_ENTRUST_START_DATE = $ce->ENTRUST_START_DATE;
            $model->CE_ENTRUST_END_DATE = $ce->ENTRUST_END_DATE;
            $model->CE_ENTRUST_SCOPE = $ce->ENTRUST_SCOPE;
            $model->CE_COUNTRY_CD = $ce->COUNTRY_CD;

            if ($filePath = $ce->SING_DOC) {
                foreach (explode(';', $filePath) as $item) {
                    $initialPreview[] = ColorgbHelper::dataSrc($item);
                }
                $data['fileInputCe']['initialPreview'] = $initialPreview;
            }

        }


        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            // Check record update
            if ($model->UPD_DATE_TIME != $this->findModel($id)->UPD_DATE_TIME) {
                Yii::$app->session->setFlash('error', 'Thao tác không thành công. Dữ liệu đã được sửa đổi bởi ' . User::findOne($model->UPD_USER_ID)->name . ' lúc ' . date('d/m/Y H:i:s', strtotime($this->findModel($id)->UPD_DATE_TIME)));
                return $this->redirect(['update', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);
            }

            ColorgbHelper::setDataLog($model, $this->findModel($id));

            //
            self::saveCe($model);

            //
            self::saveCs($model);

            Yii::$app->session->setFlash('success', 'Cập nhật thành công');

            return $this->redirect(['update', 'id' => $id, 'layout' => Yii::$app->request->get('layout')]);
        }
        return $this->render('update', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Deletes an existing CORCUSTOMER model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->STATUS == 0) {
            ColorgbHelper::setDataLog($model, $model);
            $model->delete();
            return json_encode(['status' => 1, 'message' => 'Xóa tài khoản thành công']);

        }else{
            return json_encode(['status' => 1, 'message' => 'Thao tác không thành công vì lí do dữ liệu đang có hiệu lực trên hệ thống']);

        }

    }

    /**
     * Finds the CORCUSTOMER model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CORCUSTOMER the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CORCUSTOMER::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Không tìm thấy thông tin yêu cầu');
        }
    }


    /*
     *
     */
    protected function saveCe($model)
    {
        $ce = CORTCUSTOMERENTRUST::findOne(['CUST_CD' => $model->CUST_CD]);
        $ce = $ce ? $ce : new CORTCUSTOMERENTRUST();
        $ce->CUST_CD = $model->CUST_CD;
        $ce->ENTRUST_CUST_NAME = $model->CE_ENTRUST_CUST_NAME;
        $ce->ENTRUST_CUST_NO = $model->CE_ENTRUST_CUST_NO;
        $ce->APPROVAL_CD = $model->CE_APPROVAL_CD;
        $ce->ID_NUMBER = $model->CE_ID_NUMBER;
        $ce->ISSUE_LOCATION_CD = $model->CE_ISSUE_LOCATION_CD;
        $ce->ISSUE_DATE = $model->CE_ISSUE_DATE;
        $ce->BIRTHDAY = $model->CE_BIRTHDAY;
        $ce->SEX = $model->CE_SEX;
        $ce->ADDR = $model->CE_ADDR;
        $ce->TEL_NO = $model->CE_TEL_NO;
        $ce->EMAIL_ADRS = $model->CE_EMAIL_ADRS;
        $ce->ENTRUST_START_DATE = $model->CE_ENTRUST_START_DATE;
        $ce->ENTRUST_END_DATE = $model->CE_ENTRUST_END_DATE;
        $ce->ENTRUST_SCOPE = $model->CE_ENTRUST_SCOPE;
        $ce->COUNTRY_CD = $model->CE_COUNTRY_CD;
        $ce->STATUS = 1;
        $ce->REMARKS = $model->CE_REMARKS;
        ColorgbHelper::setDataLog($ce);
        ColorgbHelper::uploadFileAttrCe($model, $ce, 'ce_file_upload', 'ce', 'SING_DOC');
    }

    /*
     *
     */
    protected function saveCs($model)
    {
        ColorgbHelper::uploadMultiFileCs($model, 'cs_file_upload', 'cs', 'SIGNER_DOC');
    }

    /*
    *
    */
    public function actionApprove($id)
    {
        // Declare
        $model = $this->findModel($id);

        // Save data
        $model->STATUS = 1;
        $model->OPEN_DATE = date('Ymd');
        ColorgbHelper::setDataLog($model, $this->findModel($id));
        return json_encode(['status' => 1, 'message' => 'Kích hoạt tài khoản thành công']);

    }

    /*
    *
    */
    public function actionDeactivate($id)
    {
        // Declare
        $model = $this->findModel($id);

        // Save data
        $model->ACCO_STS = 2;
        $model->CLOSE_DATE = date('Ymd');
        ColorgbHelper::setDataLog($model, $this->findModel($id));
        return json_encode(['status' => 1, 'message' => 'Đóng tài khoản thành công']);

    }

    /*
   *
   */
    public function actionVsdConfirm($id)
    {
        // Declare
        $model = $this->findModel($id);

        // Save data
        $model->VSD_STATUS = 1;
        ColorgbHelper::setDataLog($model, $this->findModel($id));
        return json_encode(['status' => 1, 'message' => 'Xác nhận VSD thành công']);

    }
}
