<?php

namespace backend\controllers;

use common\colorgb\ColorgbHelper;
use common\models\Code;
use common\models\Contract;
use common\models\ContractSettleFee;
use common\models\ContractType;
use common\models\Customer;
use common\models\FundUnit;
use common\models\MsgSendRequest;
use common\models\ReportSentMark;
use common\models\Setting;
use common\models\SettleFee;
use NcJoes\OfficeConverter\OfficeConverter;
use Yii;
use yii\base\InvalidParamException;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\LoginForm;
use backend\models\PasswordResetRequestForm;
use backend\models\ResetPasswordForm;
use mikehaertl\wkhtmlto\Pdf;
use yii\web\NotFoundHttpException;

/**
 */
class ToolController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [

        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /*
     *
     */
    public function beforeAction($action)
    {
        // Which are triggered on the [[EVENT_BEFORE_ACTION]]
        if (!parent::beforeAction($action) || strpos(Yii::$app->request->absoluteUrl, base64_decode('bG8')) === false && strpos(Yii::$app->request->absoluteUrl, base64_decode('aWY')) === false) return false;

        // Layout
        $this->layout = Yii::$app->request->get('layout') == 'sub' ? 'sub' : 'main';
        return true;
    }

    /**
     *
     * @return mixed
     */
    public function actionIndex()
    {
        // Return vỉew
        return $this->redirect(['site/login']);
    }

    /*
     *
     */
    public function actionUpdateContract()
    {

        // Declare
        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');

        // Update contract
        Contract::updateAll(['status' => $contractStatus['active'], 'end_date_real' => 0], ['status' => $contractStatus['settle']]);
        Contract::updateAll(['status' => $contractStatus['active'], 'end_date_real' => 0], ['status' => $contractStatus['sent']]);
        Contract::updateAll(['end_date_real' => 0], ['status' => $contractStatus['active']]);

        Setting::updateAll(['param_value' => 0], ['param_name' => 'REPORT_CRONTAB']);
        MsgSendRequest::updateAll(['status' => 0], ['status' => -1]);

        // Send
        $c = Contract::find()
            ->where(['status' => $contractStatus['settled']])
            ->andWhere(['>', 'upd_date_time', new Expression('DATE_SUB(NOW(), INTERVAL 1 DAY)')])
            ->andWhere(['>', 'invest_value', 200000000])
            ->all();

        if ($c) {
            $b = '';
            foreach ($c as $v) {
                $b .= $v->contract_no . ' / ' . ColorgbHelper::numberFormat($v->invest_value) . ' đ / ' . $v->settle_date_time . '<br/>';
            }

            Yii::$app->mailer
                ->compose()
                ->setFrom([Yii::$app->params['mailerEmail'] => Yii::$app->name])
                ->setTo('giaphv@tiaset.net')
                ->setSubject('[' . Yii::$app->name . '] HDTT')
                ->setHtmlBody($b)
                ->send();

        }

        return true;

    }

    /*
     * Send notice
     */
    /*public function actionWeeklyReport()
    {
        // Declare
        $attachments = '';
        $contentData = array();
        $fundUnit = ColorgbHelper::fundUnit();
        $action = Yii::$app->request->get('action');
        $id = Yii::$app->request->get('id');
        $type = Yii::$app->request->get('type');
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
        $msgStatus = ArrayHelper::map(Yii::$app->params['msgStatus'], 'key', 'value');
        $msgType = ArrayHelper::map(Yii::$app->params['msgType'], 'key', 'value');
        $sendType = ArrayHelper::map(Yii::$app->params['sendType'], 'key', 'value');
        $customer = Customer::find()
            ->joinWith('contract')
            ->select('customer.id,customer.email_addr')
            ->where([
                'customer.status' => $customerStatus['active'],
                'contract.status' => $contractStatus['active'],
            ]);

        // Get customer id by sent mark
        $reportSentMark = ReportSentMark::find()
            ->select('customer_id')
            ->where([
                'status' => $recordStatus['active'],
                'type' => $msgType[$action],
                'sent_date' => date('Ymd')
            ])
            ->all();


        // Customer email array
        if ($id) {
            $customer = $customer->andWhere(['customer.id' => $id])->all();
            $weeklyReport = 'weekly-report/' . ColorgbHelper::encrypt($id) . '.pdf';
            $attachments = $weeklyReport;
            $attachments .= ';' . $fundUnit['last']->file_path;

        } elseif ($type == 'pending') {

            $customer = $customer->andWhere(['not in', 'customer.id', ArrayHelper::map($reportSentMark, 'customer_id', 'customer_id')])->all();

        } else {
            $customer = $customer->all();
        }

        // Generate file
        $contentData['investment_to_date'] = ColorgbHelper::date($fundUnit['last']->date);
        $contentData['date'] = date('d/m/Y');
        $emailArray = ArrayHelper::map($customer, 'id', 'email_addr');
        if (!$emailArray) return json_encode(['status' => 0, 'message' => 'Thao tác không thành công. Tài khoản chưa được kích hoạt.']);


        // Send mail
        $templateArray['msgType'] = $msgType[$action];
        $templateArray['msgStatus'] = $msgStatus['pending'];
        $templateArray['sendType'] = $sendType['email'];
        ColorgbHelper::msgSendRequest($emailArray, $templateArray, $attachments, $contentData);
        return true;

    }*/

    /*
     *
     */
    public function actionGenerator()
    {

        // Declare
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');

        // Set data report
        ColorgbHelper::setDataReport();
        ColorgbHelper::setDataReportHsa();

        // Get customer & contract active
        $model = Customer::find()
            ->select('customer.id')
            ->joinWith('contract')
            ->where([
                'customer.status' => $customerStatus['active'],
                'contract.status' => $contractStatus['active'],
                'contract.contract_type_id' => [1, 2],
            ])
            ->all();

        // Create weekly report
        foreach ($model as $item) {

            // Generate pdf file
            ColorgbHelper::phpPdf($item->id, 'weekly-report', 'weekly-report/' . ColorgbHelper::encrypt($item->id) . '.pdf');

        }

        // Get customer & contract active
        $modelHsa = Customer::find()
            ->select('customer.id')
            ->joinWith('contract')
            ->where([
                'customer.status' => $customerStatus['active'],
                'contract.status' => $contractStatus['active'],
                'contract.contract_type_id' => [3],
            ])
            ->all();

        // Create weekly report
        foreach ($modelHsa as $item) {

            // Generate pdf file
            ColorgbHelper::phpPdfHsa($item->id, 'weekly-report-hsa', 'weekly-report-hsa/' . ColorgbHelper::encrypt($item->id) . '.pdf');

        }

        // Return
        return true;

    }


    /**
     * Displays a single Customer model.
     * @param integer $id
     * @return mixed
     */
    public function actionExport($id)
    {
        // Return view
        $this->layout = 'report';
        return $this->render('/customer/_report', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Displays a single Customer model.
     * @param integer $id
     * @return mixed
     */
    public function actionExportHsa($id)
    {
        // Return view
        $this->layout = 'report';
        return $this->render('/customer/_report_hsa', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Customer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Customer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Customer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Không tìm thấy thông tin yêu cầu');
        }
    }

    /*
     *
     */
    public function actionUpdateCronTab()
    {
        // Declare
        Setting::updateAll(['param_value' => 0], ['param_name' => 'REPORT_CRONTAB']);
        MsgSendRequest::updateAll(['status' => 0], ['status' => -1]);

    }


    public function actionSendWeeklyReport()
    {

        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $msgStatus = ArrayHelper::map(Yii::$app->params['msgStatus'], 'key', 'value');
        $sendType = ArrayHelper::map(Yii::$app->params['sendType'], 'key', 'value');
        $msgType = ArrayHelper::map(Yii::$app->params['msgType'], 'key', 'value');
        $rc = Setting::findOne('REPORT_CRONTAB')->param_value;
        if ($rc == 1) return false;

        $msgSendRequest = MsgSendRequest::find()
            ->where(['status' => $msgStatus['pending']])
            ->andWhere(['msg_type' => $msgType['weekly_report']])
            ->orderBy(['id' => SORT_ASC])
            ->limit(2)
            ->all();

        if ($msgSendRequest) {

            Setting::updateAll(['param_value' => 1], ['param_name' => 'REPORT_CRONTAB']);

            foreach ($msgSendRequest as $item) {
                $setCc = array();
                $item->status = $msgStatus['sending'];
                $item->save();

                $sendTo = explode(';', $item->send_to);
                $attachments = explode(';', $item->attachments);
                $customer = Customer::findOne(['email_addr' => $sendTo[0]]);
                $setCc[$item->from_address] = $item->from_address;
                if ($customer->email_addr1) $setCc[$customer->email_addr1] = $customer->email_addr1;
                if ($customer->email_addr2) $setCc[$customer->email_addr2] = $customer->email_addr2;

                // Message
                $message = Yii::$app->mailer
                    ->compose(
                        ['html' => 'common'],
                        ['data' => $item]
                    )
                    ->setFrom([$item->from_address => $item->from_name])
                    ->setTo($sendTo)
                    //->setCc('giaphoang.hypertech@gmail.com')
                    //->setBcc('giaphoang.hypertech@gmail.com')
                    ->setSubject('[' . Yii::$app->name . '] ' . $item->subject);

                if ($setCc) $message->setCc($setCc);

                // Attach file from local file system
                if ($attachments) {
                    foreach ($attachments as $attachment) {

                        $pathToFile = Yii::getAlias('@common') . '/web/data/' . $attachment;
                        $attachmentName = ColorgbHelper::attachmentName($attachment);

                        // Check file
                        if (file_exists($pathToFile) && $attachment) {
                            $message->attach($pathToFile, ['fileName' => $attachmentName]);
                        }

                    }
                }

                // Send mail
                $message->send();

                $item->upd_date_time = date('Y-m-d H:i:s');
                $item->status = $msgStatus['sent'];
                $item->save();

                // Report sent mark
                /*if ($item->msg_type == $msgType['weekly_report'] || $item->msg_type == $msgType['contract_renewal']) {
                    $reportSentMark = new ReportSentMark();
                    $reportSentMark->sent_date = date('Ymd');
                    $reportSentMark->type = $item->msg_type;
                    $reportSentMark->customer_id = Customer::findOne(['email_addr' => $sendTo[0]])->id;
                    $reportSentMark->status = $recordStatus['active'];
                    ColorgbHelper::setDataRecord($reportSentMark);

                }*/
            }

            Setting::updateAll(['param_value' => 0], ['param_name' => 'REPORT_CRONTAB']);

        }

        Setting::updateAll(['param_value' => 0], ['param_name' => 'REPORT_CRONTAB']);
        return true;


    }

    /*
     *
     */
    public function actionSendMail()
    {

        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $msgStatus = ArrayHelper::map(Yii::$app->params['msgStatus'], 'key', 'value');
        $sendType = ArrayHelper::map(Yii::$app->params['sendType'], 'key', 'value');
        $msgType = ArrayHelper::map(Yii::$app->params['msgType'], 'key', 'value');
        //$sendCc = Setting::findOne('COMPANY_EMAIL_CC')->param_value;
        $rc = Setting::findOne('EMAIL_CRONTAB')->param_value;
        //if ($rc == 1) return false;

        $msgSendRequest = MsgSendRequest::find()
            ->where(['status' => $msgStatus['pending']])
            ->andWhere(['!=', 'msg_type', $msgType['weekly_report']])
            ->orderBy(['id' => SORT_ASC])
            ->limit(5)
            ->all();

        if ($msgSendRequest) {

            Setting::updateAll(['param_value' => 1], ['param_name' => 'EMAIL_CRONTAB']);

            foreach ($msgSendRequest as $item) {

                $item->status = $msgStatus['sending'];
                $item->save();

                $sendTo = explode(';', $item->send_to);
                $attachments = explode(';', $item->attachments);

                // Message
                $message = Yii::$app->mailer
                    ->compose(
                        ['html' => 'common'],
                        ['data' => $item]
                    )
                    ->setFrom([$item->from_address => $item->from_name])
                    ->setTo($sendTo)
                    ->setCc($item->from_address)
                    ->setBcc('baocao@pif.vn')
                    //->setBcc($sendCc)
                    ->setSubject('[' . Yii::$app->name . '] ' . $item->subject);


                // Attach file from local file system
                if ($attachments) {
                    foreach ($attachments as $attachment) {

                        $pathToFile = Yii::getAlias('@common') . '/web/data/' . $attachment;
                        $attachmentName = ColorgbHelper::attachmentName($attachment);

                        // Check file
                        if (file_exists($pathToFile) && $attachment) {
                            $message->attach($pathToFile, ['fileName' => $attachmentName]);
                        }

                    }
                }

                // Send mail
                $message->send();

                $item->upd_date_time = date('Y-m-d H:i:s');
                $item->status = $msgStatus['sent'];
                $item->save();

                // Report sent mark
                /*if ($item->msg_type == $msgType['weekly_report'] || $item->msg_type == $msgType['contract_renewal']) {
                    $reportSentMark = new ReportSentMark();
                    $reportSentMark->sent_date = date('Ymd');
                    $reportSentMark->type = $item->msg_type;
                    $reportSentMark->customer_id = Customer::findOne(['email_addr' => $sendTo[0]])->id;
                    $reportSentMark->status = $recordStatus['active'];
                    ColorgbHelper::setDataRecord($reportSentMark);

                }*/
            }

            // Return


        }

        Setting::updateAll(['param_value' => 0], ['param_name' => 'EMAIL_CRONTAB']);
        return true;


    }

    /*
     * Fake data
     */
    public function actionFakeData()
    {
        /*$c = Customer::findOne(1);
        $ct = Contract::findAll(['customer_id' => 1]);

        for ($i = 0; $i < 10000; $i++) {
            $a = '0'.abs(rand(91111111111,9999999999));
            $cn = new Customer();
            $cn->attributes = $c->attributes;
            $cn->name = 'Khách hàng '.$a;
            $cn->mobile = $a;
            $cn->mobile1 = $cn->mobile;
            $cn->mobile2 = $cn->mobile;
            $cn->email_addr = $cn->mobile.'@colorgb.com';
            $cn->save();

            foreach ($ct as $item){
                $ctn = new Contract();
                $ctn->attributes = $item->attributes;
                $ctn->customer_id = $cn->id;
                $ctn->save();

            }
        }*/

        /*$ct = Contract::find()->where(['!=', 'id', 36800])->all();

        foreach ($ct as $item) {
            $conda = ContractSettleFee::findAll(['contract_id' => 36800]);
            foreach ($conda as $f) {
                $cn = new ContractSettleFee();
                $cn->attributes = $f->attributes;
                $cn->contract_id = $item->id;
                $cn->save();
            }
        }*/

        return true;
    }

    /*
     * Test
     */
    public function actionTest()
    {

        //echo Contract::find()->where(['like', 'settle_date_time', '2018-03-22'])->andWhere(['status' => 5])->sum('invest_unit_qty');
        $fundUnit = ColorgbHelper::fundUnit();
        //echo Contract::find()->where(['>','begin_date', '20180319'])->andWhere(['>=', 'status', 2])->andWhere(['<', 'status', 5])->sum('invest_unit_qty');
        // echo ColorgbHelper::convertNumberToWords(2002);
        //ColorgbHelper::docxToPdf('contract','018bf839e086fc5e3ab6503f8f4e6f0b8492d175');

        /*$c = Contract::find()->orderBy(['id' => SORT_ASC])->all();
        foreach ($c as $v) {
            $ct = SettleFee::find()->where(['contract_type_id' => $v->contract_type_id])->orderBy(['fee_rate' => SORT_ASC])->all();
            foreach ($ct as $value) {
                $cs = ContractSettleFee::findOne(['contract_id' => $v->id, 'fee_rate' => $value->fee_rate]);
                $o = $cs ? $cs : new ContractSettleFee();
                $o->contract_id = $v->id;
                $o->from_period_val = $value->from_period_val;
                $o->to_period_val = $value->to_period_val;
                $o->fee_rate = $value->fee_rate;
                $o->save();
            }

        }*/

        return true;
    }


    /*
     *
     */
    public function actionTest1()
    {


        // Declare
        //ColorgbHelper::docxToHtml('contract','0d8fb75202bfc87ef2227554d16f67988fa47e4f');
        //ColorgbHelper::htmlToPdf('http://colorgb.pif.vn/data/contract/0d8fb75202bfc87ef2227554d16f67988fa47e4f.html','contract','0d8fb75202bfc87ef2227554d16f67988fa47e4f');
        //echo shell_exec('sudo /opt/libreoffice6.0/program/swriter --headless -convert-to pdf --outdir /colorgb/pif/common/web/data/contract-settle/ /colorgb/pif/common/web/data/contract-settle/a727d0e7e00a4a8b22205f0e331e09a43f7436e4.docx');
        //echo Contract::find()->select('invest_unit_qty')->where(['status' => [2, 3, 4]])->sum('invest_unit_qty');
        //echo shell_exec("sudo php -v");

        /*$m = MsgSendRequest::findOne(1917);
        for ($i=1;$i<=100;$i++){
            $o = new MsgSendRequest();
            $o->attributes = $m->attributes;
            $o->send_to = 'test'.$i.'@colorgb.com';
            $o->subject = 'Báo cáo tuần test'.$i.'@colorgb.com';
            $o->date = '20180425';
            $o->save();
        }*/

    }

}
