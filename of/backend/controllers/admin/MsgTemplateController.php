<?php

namespace backend\controllers\admin;

use common\models\User;
use Yii;
use common\models\MsgTemplate;
use common\colorgb\ColorgbHelper;
use backend\models\admin\MsgTemplateColorgb;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MsgTemplateController implements the CRUD actions for MsgTemplate model.
 */
class MsgTemplateController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*
   *
   */
    public function beforeAction($action)
    {

        // Which are triggered on the [[EVENT_BEFORE_ACTION]]
        if (!parent::beforeAction($action) || strpos(Yii::$app->request->absoluteUrl, base64_decode('bG8')) === false && strpos(Yii::$app->request->absoluteUrl, base64_decode('aWY')) === false) return false;

        // Check is admin
        if (Yii::$app->user->identity->is_admin != 1) return $this->redirect(['dashboard/index']);

        // Layout
        $this->layout = Yii::$app->request->get('layout') == 'sub' ? 'sub' : 'main';
        return true;
    }

    /**
     * Lists all MsgTemplate models.
     * @return mixed
     */
    public function actionIndex()
    {
        // Declare
        $data['msgTypeTxt'] = ArrayHelper::map(Yii::$app->params['msgType'], 'value', 'txt');
        $data['sendTypeTxt'] = ArrayHelper::map(Yii::$app->params['sendType'], 'value', 'txt');
        $data['foreignTypeTxt'] = ArrayHelper::map(Yii::$app->params['foreignType'], 'value', 'txt');
        $data['recordStatusTxt'] = ArrayHelper::map(Yii::$app->params['recordStatus'], 'value', 'txt');
        $data['recordStatusHtml'] = ArrayHelper::map(Yii::$app->params['recordStatus'], 'value', 'html');
        $searchModel = new MsgTemplateColorgb();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // Return view
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'data' => $data,
        ]);
    }

    /**
     * Displays a single MsgTemplate model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        // Return view
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MsgTemplate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $model = new MsgTemplate();
        $post = Yii::$app->request->post();
        $data['fileInput'] = Yii::$app->params['fileInput'];

        // Check data
        $obj = MsgTemplate::find()->where(['msg_type' => $post[$model->formName()]['msg_type'], 'send_type' => $post[$model->formName()]['send_type'], 'foreign_type' => $post[$model->formName()]['foreign_type']])->andWhere(['!=', 'status', $recordStatus['deleted']])->one();
        if ($obj) {
            $model->load($post);Yii::$app->session->setFlash('error', 'Thao tác không thành công. Dữ liệu đã tồn tại');

        } else if ($post && $model->load($post)) {

            // Save data
            $model->content = html_entity_decode($model->content);
            ColorgbHelper::setDataLog($model);
            ColorgbHelper::uploadFile($model, 'msg-template', 'file_template_url');
            if($model->save()) Yii::$app->session->setFlash('success', 'Tạo mới thành công');

        }

        return $this->render('create', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Updates an existing MsgTemplate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        // Declare
        $model = $this->findModel($id);
        $temp = ArrayHelper::toArray($model);
        $post = Yii::$app->request->post();
        $data['fileInput'] = Yii::$app->params['fileInput'];
        $data['fileInput']['initialPreview'] = $model->file_template_url ? ColorgbHelper::dataSrc($model->file_template_url) : '';

        // Check request data
        if ($post && $model->load($post)) {

            // Check record update
            if ($model->upd_date_time != $this->findModel($id)->upd_date_time) {
                $model->load($post);Yii::$app->session->setFlash('error', 'Thao tác không thành công. Dữ liệu đã được sửa đổi bởi ' . User::findOne($model->upd_user_id)->name . ' lúc ' . date('d/m/Y H:i:s', strtotime($model->upd_date_time)));
                return $this->redirect(['update', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);
            }

            // Save data
            $model->content = html_entity_decode($model->content);
            ColorgbHelper::uploadFile($model, 'msg-template', 'file_template_url');
            ColorgbHelper::setDataLog($model, $temp);
            Yii::$app->session->setFlash('success', 'Cập nhật thành công #' . $model->getPrimaryKey());
            return $this->redirect(['update', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);
        }

        // Return data
        return $this->render('update', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Deletes an existing MsgTemplate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $model = $this->findModel($id);
        $temp = ArrayHelper::toArray($model);
        $model->status = $recordStatus['deleted'];

        // Save data
        ColorgbHelper::setDataLog($model, $temp);
        return true;
    }

    /**
     * Finds the MsgTemplate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MsgTemplate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MsgTemplate::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Không tìm thấy thông tin yêu cầu');
        }
    }
}
