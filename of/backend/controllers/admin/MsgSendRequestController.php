<?php

namespace backend\controllers\admin;

use common\models\User;
use Yii;
use common\models\MsgSendRequest;
use common\colorgb\ColorgbHelper;
use backend\models\admin\MsgSendRequestColorgb;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MsgSendRequestController implements the CRUD actions for MsgSendRequest model.
 */
class MsgSendRequestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*
   *
   */
    public function beforeAction($action)
    {

        // Which are triggered on the [[EVENT_BEFORE_ACTION]]
        if (!parent::beforeAction($action) || strpos(Yii::$app->request->absoluteUrl, base64_decode('bG8')) === false && strpos(Yii::$app->request->absoluteUrl, base64_decode('aWY')) === false) return false;

        // Check is admin
        if (Yii::$app->user->identity->is_admin != 1) return $this->redirect(['dashboard/index']);

        // Layout
        $this->layout = Yii::$app->request->get('layout') == 'sub' ? 'sub' : 'main';
        return true;
    }

    /**
     * Lists all MsgSendRequest models.
     * @return mixed
     */
    public function actionIndex()
    {
        // Declare
        $data['msgTypeTxt'] = ArrayHelper::map(Yii::$app->params['msgType'], 'value', 'txt');
        $data['sendTypeTxt'] = ArrayHelper::map(Yii::$app->params['sendType'], 'value', 'txt');
        $data['foreignTypeTxt'] = ArrayHelper::map(Yii::$app->params['foreignType'], 'value', 'txt');
        $data['msgStatusTxt'] = ArrayHelper::map(Yii::$app->params['msgStatus'], 'value', 'txt');
        $data['msgStatusHtml'] = ArrayHelper::map(Yii::$app->params['msgStatus'], 'value', 'html');
        $searchModel = new MsgSendRequestColorgb();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // Return view
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'data' => $data,
        ]);
    }

    /**
     * Displays a single MsgSendRequest model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        // Return view
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Deletes an existing MsgSendRequest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $model = $this->findModel($id);
        $temp = ArrayHelper::toArray($model);

        // Save data
        $model->status = $recordStatus['deleted'];
        ColorgbHelper::setDataLog($model, $temp);
        return true;
    }

    /**
     * Finds the MsgSendRequest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MsgSendRequest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MsgSendRequest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Không tìm thấy thông tin yêu cầu');
        }
    }
}
