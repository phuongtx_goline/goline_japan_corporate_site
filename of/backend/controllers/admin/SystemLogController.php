<?php

namespace backend\controllers\admin;

use common\models\User;
use Yii;
use common\models\SystemLog;
use common\colorgb\ColorgbHelper;
use backend\models\admin\SystemLogColorgb;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SystemLogController implements the CRUD actions for SystemLog model.
 */
class SystemLogController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*
   *
   */
    public function beforeAction($action)
    {

        // Which are triggered on the [[EVENT_BEFORE_ACTION]]
        if (!parent::beforeAction($action) || strpos(Yii::$app->request->absoluteUrl, base64_decode('bG8')) === false && strpos(Yii::$app->request->absoluteUrl, base64_decode('aWY')) === false) return false;

        // Check is admin
        if (Yii::$app->user->identity->is_admin != 1) return $this->redirect(['dashboard/index']);

        // Layout
        $this->layout = Yii::$app->request->get('layout') == 'sub' ? 'sub' : 'main';
        return true;
    }

    /**
     * Lists all SystemLog models.
     * @return mixed
     */
    public function actionIndex()
    {
        // Declare
        $data['userArray'] = ArrayHelper::map(User::find()->all(), 'id', 'name');
        $data['systemLogTxt'] = ArrayHelper::map(Yii::$app->params['systemLog'], 'key', 'txt');
        $data['systemLogHtml'] = ArrayHelper::map(Yii::$app->params['systemLog'], 'key', 'html');
        $searchModel = new SystemLogColorgb();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // Return view
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'data' => $data,
        ]);
    }

    /**
     * Displays a single SystemLog model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        // Declare
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the SystemLog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SystemLog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SystemLog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Không tìm thấy thông tin yêu cầu');
        }
    }
}
