<?php

namespace backend\controllers\admin;

use backend\models\admin\UserPermissionForm;
use common\models\User;
use common\models\UserGroup;
use common\models\UserGroupPermission;
use Yii;
use common\models\Permission;
use common\colorgb\ColorgbHelper;
use backend\models\admin\PermissionColorgb;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PermissionController implements the CRUD actions for Permission model.
 */
class PermissionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*
   *
   */
    public function beforeAction($action)
    {

        // Which are triggered on the [[EVENT_BEFORE_ACTION]]
        if (!parent::beforeAction($action) || strpos(Yii::$app->request->absoluteUrl, base64_decode('bG8')) === false && strpos(Yii::$app->request->absoluteUrl, base64_decode('aWY')) === false) return false;

        // Check is admin
        if (Yii::$app->user->identity->is_admin != 1) return $this->redirect(['dashboard/index']);

        // Layout
        $this->layout = Yii::$app->request->get('layout') == 'sub' ? 'sub' : 'main';
        return true;
    }

    /**
     * Lists all Permission models.
     * @return mixed
     */
    public function actionIndex()
    {
        // Declare
        $searchModel = new PermissionColorgb();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // Return view
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Permission model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        // Return view
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Permission model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        // Declare
        $model = new Permission();
        $post = Yii::$app->request->post();

        // Check request data
        if ($post && $model->load($post) && $model->save()) {

            // Save data
            if($model->save()) Yii::$app->session->setFlash('success', 'Tạo mới thành công');

        }

        // Return view
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Permission model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        // Declare
        $model = $this->findModel($id);
        $temp = ArrayHelper::toArray($model);
        $post = Yii::$app->request->post();

        // Check request data
        if ($post && $model->load($post) && $model->save()) {

            // Save data
            Yii::$app->session->setFlash('success', 'Cập nhật thành công #' . $model->getPrimaryKey());
            return $this->redirect(['update', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);

        }

        // Return view
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Permission model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // Declare
        $model = $this->findModel($id);
        $temp = ArrayHelper::toArray($model);

        // Save data
        $model->delete();
        ColorgbHelper::setDataLog($model, $temp);
        return true;
    }

    /*
     * Get role
     */
    public function actionGetRole()
    {

        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $id = Yii::$app->request->get('id');
        $permissionParent = Permission::find()->where(['like', 'role_cd', '_'])->orderBy(['seq' => SORT_ASC])->all();

        // Push array
        foreach ($permissionParent as $key => $item) {
            if (preg_match('/_/', $item->role_cd)) {
                $checked = UserGroupPermission::findOne(['role_cd' => $item->role_cd, 'user_group_id' => $id, 'status' => $recordStatus['active']]);
                $parentCd = explode('_', $item->role_cd)[0];
                $parentName = Permission::findOne($parentCd)->role_name;
                $permissionRole[$parentCd]['id'] = $parentCd;
                $permissionRole[$parentCd]['checked'] = false;
                $permissionRole[$parentCd]['hasChildren'] = true;
                $permissionRole[$parentCd]['text'] = $parentName;

                $permissionRole[$parentCd]['children'][$item->role_cd]['id'] = $item->role_cd;
                $permissionRole[$parentCd]['children'][$item->role_cd]['checked'] = $checked ? true : false;
                $permissionRole[$parentCd]['children'][$item->role_cd]['hasChildren'] = false;
                $permissionRole[$parentCd]['children'][$item->role_cd]['text'] = $item->role_name;
                $permissionRole[$parentCd]['children'][$item->role_cd]['children'] = [];
            }
        }

        // Change key parent
        foreach ($permissionRole as $key => $value) {
            $permissionArray[] = $value;
        }

        // Change key children
        foreach ($permissionArray as $key => $value) {
            foreach ($value['children'] as $k => $v) {
                unset($permissionArray[$key]['children'][$k]);
                $permissionArray[$key]['children'][] = $v;
            }
        }

        return json_encode($permissionArray);

    }

    /*
     * Save role
     */
    public static function actionSaveRole()
    {
        $id = Yii::$app->request->get('id');
        $checkedIds = Yii::$app->request->get('checkedIds');

        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        UserGroupPermission::updateAll(['status' => $recordStatus['deleted']], ['user_group_id' => $id]);

        // Check is array
        if (is_array($checkedIds)) {

            foreach ($checkedIds as $value) {

                // User group permission
                $userGroupPermission = UserGroupPermission::findOne(['user_group_id' => $id, 'role_cd' => $value]);

                // Check user group permission
                if ($userGroupPermission) {
                    $model = $userGroupPermission;
                    $temp = ArrayHelper::toArray($userGroupPermission);
                } else {
                    $model = new UserGroupPermission();
                }

                // Save data
                $model->user_group_id = $id;
                $model->role_cd = $value;
                $model->status = $recordStatus['active'];
                $model->save();

                // Check user group permission
                if ($userGroupPermission) {
                    ColorgbHelper::setDataLog($model, $temp);
                } else {
                    ColorgbHelper::setDataLog($model);
                }

            }
        }

        return json_encode(['status' => 1, 'message' => 'Thiết lập phân quyền thành công']);

    }

    /**
     * Set an existing Permission model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionSet()
    {
        // Declare
        $id = Yii::$app->request->get('id');
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $data['userGroup'] = UserGroup::findAll(['status' => $recordStatus['active']]);
        $data['checked'] = UserGroupPermission::findAll(['user_group_id' => $id, 'status' => $recordStatus['active']]);


        // Return view
        return $this->render('set', [
            'data' => $data,
        ]);
    }

    /**
     * Finds the Permission model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Permission the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Permission::findOne(['role_cd' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Không tìm thấy thông tin yêu cầu');
        }
    }
}
