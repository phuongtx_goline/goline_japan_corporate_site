<?php

namespace backend\controllers\admin;

use common\models\SettleFee;
use Yii;
use common\models\ContractType;
use common\colorgb\ColorgbHelper;
use backend\models\admin\ContractTypeColorgb;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;

/**
 * ContractTypeController implements the CRUD actions for ContractType model.
 */
class ContractTypeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*
   *
   */
    public function beforeAction($action)
    {

        // Which are triggered on the [[EVENT_BEFORE_ACTION]]
        if (!parent::beforeAction($action) || strpos(Yii::$app->request->absoluteUrl, base64_decode('bG8')) === false && strpos(Yii::$app->request->absoluteUrl, base64_decode('aWY')) === false) return false;

        // Check is admin
        if (Yii::$app->user->identity->is_admin != 1) return $this->redirect(['dashboard/index']);

        // Layout
        $this->layout = Yii::$app->request->get('layout') == 'sub' ? 'sub' : 'main';
        return true;
    }

    /**
     * Lists all ContractType models.
     * @return mixed
     */
    public function actionIndex()
    {
        // Declare
        $data['periodTypeTxt'] = ArrayHelper::map(Yii::$app->params['periodType'], 'value', 'txt');
        $data['recordStatusTxt'] = ArrayHelper::map(Yii::$app->params['recordStatus'], 'value', 'txt');
        $data['recordStatusHtml'] = ArrayHelper::map(Yii::$app->params['recordStatus'], 'value', 'html');
        $searchModel = new ContractTypeColorgb();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // Return view
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'data' => $data,
        ]);
    }

    /**
     * Displays a single ContractType model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        // Return view
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ContractType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        // Declare
        $transaction = Yii::$app->db->beginTransaction();
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $model = new ContractType();
        $post = Yii::$app->request->post();
        $data['fileInput'] = Yii::$app->params['fileInput'];

        // Try catch
        try {

            // Check data
            $obj = ContractType::find()->where(['code' => $post[$model->formName()]['code']])->andWhere(['!=', 'status', $recordStatus['deleted']])->one();
            if ($obj) {
                $model->load($post);
                Yii::$app->session->setFlash('error', 'Thao tác không thành công. Dữ liệu đã tồn tại');

            } else if ($post && $model->load($post)) {

                $model->min_invest_value = str_replace(',', '', $model->min_invest_value);
                $model->max_invest_value = str_replace(',', '', $model->max_invest_value);
                $model->base_profit_rate = $model->base_profit_rate / 100;
                $model->profit_rate = $model->profit_rate * 100;
                ColorgbHelper::uploadFile($model, 'contract-type', 'file_template_url');
                ColorgbHelper::setDataLog($model);
                Yii::$app->session->setFlash('success', 'Tạo mới thành công');

            }

            // Save Settle fee
            self::settleFee($post[$model->formName()]['settleFee'],$model);

            // Commit
            $transaction->commit();

        } catch (Exception $e) {

            // Rollback
            $transaction->rollback();
            Yii::$app->session->setFlash('error', 'Đã có lỗi xảy ra, dữ liệu chưa được lưu');

        }

        // Return view
        return $this->render('create', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Updates an existing ContractType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        // Declare
        $transaction = Yii::$app->db->beginTransaction();
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $model = $this->findModel($id);
        $temp = ArrayHelper::toArray($model);
        $model->base_profit_rate = $model->base_profit_rate * 100;
        $model->profit_rate = $model->profit_rate * 100;
        $post = Yii::$app->request->post();
        $data['fileInput'] = Yii::$app->params['fileInput'];
        $data['fileInput']['initialPreview'] = $model->file_template_url ? ColorgbHelper::dataSrc($model->file_template_url) : '';
        $data['settleFee'] = SettleFee::findAll(['contract_type_id' => $model->id, 'status' => $recordStatus['active']]);

        // Try catch
        try {

            // Check date post
            if ($post && $model->load($post)) {

                // Check record update
                if ($model->upd_date_time != $this->findModel($id)->upd_date_time) {
                    $model->load($post);
                    Yii::$app->session->setFlash('error', 'Thao tác không thành công. Dữ liệu đã được sửa đổi bởi ' . User::findOne($model->upd_user_id)->name . ' lúc ' . date('d/m/Y H:i:s', strtotime($model->upd_date_time)));
                    return $this->redirect(['update', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);
                }

                // Save data
                $model->min_invest_value = str_replace(',', '', $model->min_invest_value);
                $model->max_invest_value = str_replace(',', '', $model->max_invest_value);
                $model->base_profit_rate = $model->base_profit_rate / 100;
                $model->profit_rate = $model->profit_rate / 100;
                ColorgbHelper::setDataLog($model, $temp);
                ColorgbHelper::uploadFile($model, 'contract-type', 'file_template_url');

                // Save Settle fee
                self::settleFee($post[$model->formName()]['settleFee'],$model);

                // Commit
                $transaction->commit();
                Yii::$app->session->setFlash('success', 'Cập nhật thành công #' . $model->getPrimaryKey());
                return $this->redirect(['update', 'id' => $model->getPrimaryKey(), 'layout' => Yii::$app->request->get('layout')]);

            }


        } catch (Exception $e) {

            // Rollback
            $transaction->rollback();
        }

        // Return view
        return $this->render('update', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Deletes an existing ContractType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $model = $this->findModel($id);
        $temp = ArrayHelper::toArray($model);

        // Save data
        $model->status = $recordStatus['deleted'];
        ColorgbHelper::setDataLog($model, $temp);
        return true;
    }

    /**
     * Finds the ContractType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ContractType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ContractType::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Không tìm thấy thông tin yêu cầu');
        }
    }

    /*
     *Settle fee
     */
    protected function settleFee($settleFee,$model){

        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');

        // Check settle fee
        if (is_array($settleFee)) {

            // Update settle fee status
            SettleFee::updateAll(['status' => $recordStatus['deleted']], ['contract_type_id' => $model->getPrimaryKey()]);

            foreach ($settleFee as $item) {

                $modelSettleFee = SettleFee::findOne([
                    'contract_type_id' => $model->id,
                    'from_period_val' => $item['from_period_val'],
                    'to_period_val' => $item['to_period_val'],
                    'status' => $recordStatus['active'],
                ]);

                if ($modelSettleFee) {
                    Yii::$app->session->setFlash('warning', 'Không thể thêm mới bậc phí rút trước hạn đã tồn tại');
                } else {

                    $modelSettleFee = new SettleFee();
                    $modelSettleFee->contract_type_id = $model->id;
                    $modelSettleFee->from_period_val = $item['from_period_val'];
                    $modelSettleFee->to_period_val = $item['to_period_val'];
                    $modelSettleFee->fee_rate = $item['fee_rate'] / 100;
                    $modelSettleFee->status = $recordStatus['active'];
                    ColorgbHelper::setDataLog($modelSettleFee);
                }
            }
        }
    }
}
