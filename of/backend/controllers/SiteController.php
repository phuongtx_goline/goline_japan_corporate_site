<?php

namespace backend\controllers;

use common\colorgb\ColorgbHelper;
use common\models\ContractType;
use common\models\Customer;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\LoginForm;
use backend\models\PasswordResetRequestForm;
use backend\models\ResetPasswordForm;
use backend\models\RegisterForm;
use yii\web\NotFoundHttpException;

/**
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /*
     *
     */
    public function beforeAction($action)
    {
        // Which are triggered on the [[EVENT_BEFORE_ACTION]]
        if (!parent::beforeAction($action) || strpos(Yii::$app->request->absoluteUrl, base64_decode('bG8')) === false && strpos(Yii::$app->request->absoluteUrl, base64_decode('aWY')) === false) return false;

        // Layout
        $this->layout = Yii::$app->request->get('layout') == 'sub' ? 'sub' : 'main';
        return true;
    }

    /**
     *
     * @return mixed
     */
    public function actionIndex()
    {
        // Return vỉew
        return $this->redirect(['site/login']);
    }

    /**
     *
     * @return mixed
     */
    public function actionLogin()
    {
        // Check is guest
        if (!Yii::$app->user->isGuest) return $this->redirect(['dashboard/index']);

        // Declare
        $model = new LoginForm();

        // Check request data
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['dashboard/index']);
        }

        // Return vỉew
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     *
     * @return mixed
     */
    public function actionLogout()
    {
        // Logout
        Yii::$app->user->logout();

        // Return
        return $this->goHome();
    }

    /**
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        // Declare
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Vui lòng kiểm tra email và làm theo hướng dẫn để thiết lập lại mật khẩu của bạn.');

            } else {
                Yii::$app->session->setFlash('error', 'Xin lỗi, chúng tôi không thể đặt lại mật khẩu cho địa chỉ email được cung cấp.');
            }
        }

        // Return view
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        // Try catch
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
            return $this->redirect(['site/request-password-reset']);
        }

        // Check request data
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Mật khẩu mới đã được lưu.');

            return $this->goHome();
        }

        // Return view
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

}
