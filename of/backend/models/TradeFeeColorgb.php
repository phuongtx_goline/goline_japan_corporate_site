<?php

namespace backend\models;

use common\models\ORDTTRADEFEE;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ORDTTRADEFEERATE;

/**
 * TradeFeeColorgb represents the model behind the search form about `common\models\ORDTTRADEFEERATE`.
 */
class TradeFeeColorgb extends ORDTTRADEFEE
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'CUST_TYPE', 'FOREIGN_TYPE', 'TRADE_TYPE', 'FEE_RATE_ID', 'STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['TRANSACTION_CD', 'REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ORDTTRADEFEE::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'CUST_TYPE' => $this->CUST_TYPE,
            'FOREIGN_TYPE' => $this->FOREIGN_TYPE,
            'TRADE_TYPE' => $this->TRADE_TYPE,
            'FEE_RATE_ID' => $this->FEE_RATE_ID,
            'STATUS' => $this->STATUS,
            'REG_DATE_TIME' => $this->REG_DATE_TIME,
            'REG_USER_ID' => $this->REG_USER_ID,
            'UPD_DATE_TIME' => $this->UPD_DATE_TIME,
            'UPD_USER_ID' => $this->UPD_USER_ID,
        ]);

        $query->andFilterWhere(['like', 'TRANSACTION_CD', $this->TRANSACTION_CD]);

        return $dataProvider;
    }
}