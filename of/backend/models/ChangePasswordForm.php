<?php

namespace backend\models;

use common\models\User;
use Yii;

class ChangePasswordForm extends User
{
    public $password;
    public $new_password;
    public $confirm_password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password', 'new_password', 'confirm_password'], 'required'],
            [['password', 'new_password', 'confirm_password'], 'string', 'min' => 6],
            ['confirm_password', 'compare', 'compareAttribute' => 'new_password', 'message' => 'Xác nhận mật khẩu không khớp'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Mật khẩu hiện tại',
            'new_password' => 'Mật khẩu mới',
            'confirm_password' => 'Xác nhận mật khẩu',
        ];
    }

    /*
     * Save data
     */
    function saveData()
    {
        $id = Yii::$app->user->identity->getId();
        $model = User::findOne($id);
        $model->setPassword($this->confirm_password);
        $model->save();

        return true;
    }


}
