<?php

namespace backend\models;

use common\colorgb\ColorgbHelper;
use common\models\Setting;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Contract;
use yii\helpers\ArrayHelper;

/**
 * ContractColorgb represents the model behind the search form about `common\models\Contract`.
 */
class ContractColorgb extends Contract
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'contract_type_id', 'period_type', 'period_val', 'status', 'annouce_status', 'response_status', 'active_user_id', 'settle_user_id', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['colorgb_date','contract_info', 'customer_name', 'contract_no', 'contract_type_code', 'contract_type_name', 'remarks', 'response_content', 'active_date_time', 'settle_date_time', 'reg_date_time', 'reg_ip_addr', 'upd_date_time', 'upd_ip_addr'], 'safe'],
            [['fund_net_asset', 'fund_unit_qty', 'fund_unit_price', 'invest_value', 'invest_unit_qty', 'base_profit_rate', 'profit_rate', 'contract_date', 'begin_date', 'end_date_est', 'end_date_real', 'settle_fee_amt'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // Bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $customerType = ArrayHelper::map(Yii::$app->params['customerType'], 'key', 'value');
        $customerGroupIds = ColorgbHelper::getCustomerGroupIds();

        $query = Contract::find()->joinWith('customer');

        // Check role
        /*if (Yii::$app->user->identity->is_admin == -1) {
            $query->where(['customer.cared_user_id' => Yii::$app->user->identity->getId()]);

        }else{
            $query->where(['!=', 'contract.status', $recordStatus['deleted']])
                ->andWhere(['customer.customer_group_id' => $customerGroupIds]);
        }*/

        $query->where(['!=', 'contract.status', $recordStatus['deleted']])
            ->andWhere(['customer.customer_group_id' => $customerGroupIds]);

        $this->load($params);

        // Filter status deleted
        if ($this->status == $recordStatus['deleted']) {
            if (Yii::$app->user->identity->is_admin == 1) {
                $query = Contract::find()
                    ->joinWith('customer')
                    ->where(['contract.status' => $recordStatus['deleted']]);
            } else {
                $query = Contract::find()
                    ->joinWith('customer')
                    ->where(['contract.status' => $recordStatus['deleted']])
                    ->andWhere(['customer.customer_group_id' => $customerGroupIds]);
            }
        }

        // Filter status expire
        if ($this->status == 8) {
            $expireAlert = intval(Setting::findOne(['CONTRACT_EXPIRE_ALERT_DAY'])->param_value);
            $expire = intval(date('Ymd') + $expireAlert);
            $query->andWhere(['<=', 'contract.end_date_est', $expire])
                ->andWhere(['>', 'contract.end_date_est', date('Ymd')]);
        } else {
            $query->andFilterWhere(['contract.status' => $this->status]);

        }

        // Add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]

        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // Grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'fund_net_asset' => $this->fund_net_asset,
            'fund_unit_qty' => $this->fund_unit_qty,
            'fund_unit_price' => $this->fund_unit_price,
            'invest_value' => $this->invest_value,
            'invest_unit_qty' => $this->invest_unit_qty,
            'contract_type_id' => $this->contract_type_id,
            'period_type' => $this->period_type,
            'period_val' => $this->period_val,
            'base_profit_rate' => $this->base_profit_rate,
            'profit_rate' => $this->profit_rate,
            'contract_date' => $this->contract_date,
            'end_date_est' => $this->end_date_est,
            'end_date_real' => $this->end_date_real,
            'settle_fee_amt' => $this->settle_fee_amt,
            'annouce_status' => $this->annouce_status,
            'response_status' => $this->response_status,
            'active_date_time' => $this->active_date_time,
            'active_user_id' => $this->active_user_id,
            'settle_date_time' => $this->settle_date_time,
            'settle_user_id' => $this->settle_user_id,
            'reg_date_time' => $this->reg_date_time,
            'reg_user_id' => $this->reg_user_id,
            'upd_date_time' => $this->upd_date_time,
            'upd_user_id' => $this->upd_user_id,
        ]);

        // Check type
        if (strpos($this->customer_name, 'KHÁCH HÀNG CŨ') !== false) {
            $query->andFilterWhere(['or', ['customer.type' => $customerType['old']]]);

        } elseif (strpos($this->customer_name, 'KHÁCH HÀNG MỚI') !== false) {
            $query->andFilterWhere(['or', ['customer.type' => $customerType['new']]]);

        } else {
            $query->andFilterWhere(['or',
                ['like', 'customer.mobile', $this->customer_name],
                ['like', 'customer.name', $this->customer_name],
                ['like', 'customer.email_addr', $this->customer_name],
                ['like', 'customer.tax_code', $this->customer_name],
            ]);
        }

        // Check date
        if($this->colorgb_date){
            $date = explode(' - ',$this->colorgb_date);
            $query->andFilterWhere(['>=', 'contract.begin_date', str_replace('-','',$date[0])])
                ->andFilterWhere(['<=', 'contract.begin_date', str_replace('-','',$date[1])]);
        }

        // Check date
        if($this->contract_info){
            $query->andFilterWhere(['or',
                ['like', 'contract.contract_no', $this->contract_info],
                ['like', 'contract.contract_type_name', $this->contract_info],
            ]);
        }

        $query->andFilterWhere(['like', 'contract.contract_no', $this->contract_no])
            ->andFilterWhere(['like', 'contract.contract_type_code', $this->contract_type_code])
            ->andFilterWhere(['like', 'contract.contract_type_name', $this->contract_type_name])
            ->andFilterWhere(['like', 'contract.remarks', $this->remarks])
            ->andFilterWhere(['like', 'contract.response_content', $this->response_content])
            ->andFilterWhere(['like', 'contract.reg_ip_addr', $this->reg_ip_addr])
            ->andFilterWhere(['like', 'contract.upd_ip_addr', $this->upd_ip_addr]);

        return $dataProvider;
    }
}
