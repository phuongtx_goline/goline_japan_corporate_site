<?php

namespace backend\models;

use common\colorgb\ColorgbHelper;
use common\models\Contract;
use common\models\Setting;
use common\models\UserCustomerGroup;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Customer;
use yii\helpers\ArrayHelper;

/**
 * CustomerColorgb represents the model behind the search form about `common\models\Customer`.
 */
class CustomerColorgb extends Customer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'org_type', 'foreign_type', 'gender', 'parent_id', 'customer_group_id', 'info_source_id', 'cared_user_id', 'intro_user_id', 'profession_id', 'status', 'is_locked', 'need_change_pw', 'active_user_id', 'close_user_id', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['type', 'group', 'user', 'contract_info', 'mobile', 'password', 'name', 'email_addr', 'nationality', 'prefix', 'address1', 'address2', 'tax_code', 'id_number', 'id_place', 'bank_name', 'bank_branch_name', 'bank_acco_no', 'bank_acco_name', 'mobile1', 'mobile2', 'email_addr1', 'email_addr2', 'image', 'org_info', 'character', 'remarks', 'last_login_time', 'active_date_time', 'active_ip_addr', 'close_date_time', 'close_ip_addr', 'password_reset_token', 'auth_key', 'reg_date_time', 'reg_ip_addr', 'upd_date_time', 'upd_ip_addr'], 'safe'],
            [['birthday', 'id_issue_date', 'id_expired_date'], 'number'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // Bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $customerType = ArrayHelper::map(Yii::$app->params['customerType'], 'key', 'value');
        $customerGroupIds = ColorgbHelper::getCustomerGroupIds();

        // Query
        $query = Customer::find()
            ->select('
            (select count(*) from contract where customer.id = contract.customer_id and status >= 2 and status < 5) as contract_active,
            (select count(*) from contract where customer.id = contract.customer_id and status = 1) as contract_inactive,
            (select count(*) from contract where customer.id = contract.customer_id and status = 5) as contract_settled,
            (select count(*) from contract where customer.id = contract.customer_id and status = 9) as contract_deleted,
            (select count(*) from contract where customer.id = contract.customer_id and end_date_est <= CURDATE()+ (select param_value from setting where param_name = "CONTRACT_EXPIRE_ALERT_DAY") and end_date_est > CURDATE()) as contract_expire,
            customer.id,customer.reg_date_time,customer.image,customer.mobile,customer.email_addr,customer.tax_code,customer.name,customer.customer_group_id,customer.status,customer.cared_user_id,customer.type')
            ->where(['!=', 'customer.status', $recordStatus['deleted']]);

        // Check role
        if (Yii::$app->user->identity->is_admin != 1) {
            $query->andWhere(['!=', 'customer.status', $recordStatus['deleted']]);

        }

        /*if (Yii::$app->user->identity->is_admin == -1) {
            $query->andWhere(['customer.cared_user_id' => Yii::$app->user->identity->getId()]);

        }*/

        $this->load($params);

        // Check filter
        if ($this->contract_info) {
            if ($this->contract_info == 4) {
                $expireAlert = intval(Setting::findOne(['CONTRACT_EXPIRE_ALERT_DAY'])->param_value);
                $expire = intval(date('Ymd') + $expireAlert);
                $query->joinWith('contract')->andWhere(['<=', 'contract.end_date_est', $expire])
                    ->andWhere(['>', 'contract.end_date_est', date('Ymd')]);

            } else {

                $customerIds = ArrayHelper::map(Contract::find()
                    ->select('customer_id')
                    ->where(['status' => $this->contract_info])
                    ->all(), 'customer_id', 'customer_id');

                $query->andWhere(['id' => $customerIds]);

            }
        }


        // Filter status deleted
        if ($this->status == $recordStatus['deleted']) {

            // Query
            $query = Customer::find()
                ->select('
            (select count(*) from contract where customer.id = contract.customer_id and status >= 2 and status < 5) as contract_active,
            (select count(*) from contract where customer.id = contract.customer_id and status = 1) as contract_inactive,
            (select count(*) from contract where customer.id = contract.customer_id and status = 5) as contract_settled,
            (select count(*) from contract where customer.id = contract.customer_id and status = 9) as contract_deleted,
            (select count(*) from contract where customer.id = contract.customer_id and end_date_est <= CURDATE()+ (select param_value from setting where param_name = "CONTRACT_EXPIRE_ALERT_DAY") and end_date_est > CURDATE()) as contract_expire,
            customer.id,customer.reg_date_time,customer.image,customer.mobile,customer.email_addr,customer.tax_code,customer.name,customer.customer_group_id,customer.status,customer.cared_user_id,customer.type')
                ->where(['customer.status' => $recordStatus['deleted']]);

            // Check is admin
            if (Yii::$app->user->identity->is_admin == 1) {
                $query->andWhere(['customer_group_id' => $customerGroupIds]);

            }
        }

        // Add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['reg_date_time' => SORT_DESC]]
        ]);

        // Check validate
        if (!$this->validate()) {
            return $dataProvider;
        }

        // Grid filtering conditions
        $query->andFilterWhere([
            'customer.id' => $this->id,
            'customer.org_type' => $this->org_type,
            'customer.foreign_type' => $this->foreign_type,
            'customer.gender' => $this->gender,
            'customer.birthday' => $this->birthday,
            'customer.id_issue_date' => $this->id_issue_date,
            'customer.id_expired_date' => $this->id_expired_date,
            'customer.parent_id' => $this->parent_id,
            'customer.customer_group_id' => $this->customer_group_id,
            'customer.info_source_id' => $this->info_source_id,
            'customer.cared_user_id' => $this->cared_user_id,
            'customer.intro_user_id' => $this->intro_user_id,
            'customer.profession_id' => $this->profession_id,
            'customer.status' => $this->status,
            'customer.is_locked' => $this->is_locked,
            'customer.need_change_pw' => $this->need_change_pw,
            'customer.last_login_time' => $this->last_login_time,
            'customer.active_date_time' => $this->active_date_time,
            'customer.active_user_id' => $this->active_user_id,
            'customer.close_date_time' => $this->close_date_time,
            'customer.close_user_id' => $this->close_user_id,
            'customer.reg_date_time' => $this->reg_date_time,
            'customer.reg_user_id' => $this->reg_user_id,
            'customer.upd_date_time' => $this->upd_date_time,
            'customer.upd_user_id' => $this->upd_user_id,
        ]);

        // Check type
        if (strpos($this->name, 'KHÁCH HÀNG CŨ') !== false) {
            $query->andFilterWhere(['or', ['customer.type' => $customerType['old']]]);

        } elseif (strpos($this->name, 'KHÁCH HÀNG MỚI') !== false) {
            $query->andFilterWhere(['or', ['customer.type' => $customerType['new']]]);

        } else {
            $query->andFilterWhere(['or',
                ['like', 'customer.mobile', $this->name],
                ['like', 'customer.name', $this->name],
                ['like', 'customer.email_addr', $this->name],
                ['like', 'customer.tax_code', $this->name],
            ]);
        }

        $query->andFilterWhere(['like', 'customer.nationality', $this->nationality])
            ->andFilterWhere(['like', 'customer.prefix', $this->prefix])
            ->andFilterWhere(['like', 'customer.address1', $this->address1])
            ->andFilterWhere(['like', 'customer.address2', $this->address2])
            ->andFilterWhere(['like', 'customer.tax_code', $this->tax_code])
            ->andFilterWhere(['like', 'customer.id_number', $this->id_number])
            ->andFilterWhere(['like', 'customer.id_place', $this->id_place])
            ->andFilterWhere(['like', 'customer.bank_name', $this->bank_name])
            ->andFilterWhere(['like', 'customer.bank_branch_name', $this->bank_branch_name])
            ->andFilterWhere(['like', 'customer.bank_acco_no', $this->bank_acco_no])
            ->andFilterWhere(['like', 'customer.bank_acco_name', $this->bank_acco_name])
            ->andFilterWhere(['like', 'customer.mobile1', $this->mobile1])
            ->andFilterWhere(['like', 'customer.mobile2', $this->mobile2])
            ->andFilterWhere(['like', 'customer.email_addr1', $this->email_addr1])
            ->andFilterWhere(['like', 'customer.email_addr2', $this->email_addr2])
            ->andFilterWhere(['like', 'customer.image', $this->image])
            ->andFilterWhere(['like', 'customer.org_info', $this->org_info])
            ->andFilterWhere(['like', 'customer.character', $this->character])
            ->andFilterWhere(['like', 'customer.remarks', $this->remarks])
            ->andFilterWhere(['like', 'customer.active_ip_addr', $this->active_ip_addr])
            ->andFilterWhere(['like', 'customer.close_ip_addr', $this->close_ip_addr])
            ->andFilterWhere(['like', 'customer.password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'customer.auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'customer.reg_ip_addr', $this->reg_ip_addr])
            ->andFilterWhere(['like', 'customer.upd_ip_addr', $this->upd_ip_addr])
            ->andFilterWhere(['customer.id' => $this->group]);

        return $dataProvider;
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchGroup($params)
    {
        $customerStatus = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value');
        $query = Customer::find()->where(['status' => $customerStatus['active'], 'parent_id' => 0]);

        // Add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['reg_date_time' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // Grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'org_type' => $this->org_type,
            'foreign_type' => $this->foreign_type,
            'gender' => $this->gender,
            'birthday' => $this->birthday,
            'id_issue_date' => $this->id_issue_date,
            'id_expired_date' => $this->id_expired_date,
            'parent_id' => $this->parent_id,
            'customer_group_id' => $this->customer_group_id,
            'info_source_id' => $this->info_source_id,
            'cared_user_id' => $this->cared_user_id,
            'intro_user_id' => $this->intro_user_id,
            'profession_id' => $this->profession_id,
            'status' => $this->status,
            'is_locked' => $this->is_locked,
            'need_change_pw' => $this->need_change_pw,
            'last_login_time' => $this->last_login_time,
            'active_date_time' => $this->active_date_time,
            'active_user_id' => $this->active_user_id,
            'close_date_time' => $this->close_date_time,
            'close_user_id' => $this->close_user_id,
            'reg_date_time' => $this->reg_date_time,
            'reg_user_id' => $this->reg_user_id,
            'upd_date_time' => $this->upd_date_time,
            'upd_user_id' => $this->upd_user_id,
        ]);

        $query->andFilterWhere(['or',
            ['like', 'mobile', $this->group],
            ['like', 'name', $this->group],
            ['like', 'email_addr', $this->group],
        ])->andFilterWhere(['like', 'nationality', $this->nationality])
            ->andFilterWhere(['like', 'prefix', $this->prefix])
            ->andFilterWhere(['like', 'address1', $this->address1])
            ->andFilterWhere(['like', 'address2', $this->address2])
            ->andFilterWhere(['like', 'tax_code', $this->tax_code])
            ->andFilterWhere(['like', 'id_number', $this->id_number])
            ->andFilterWhere(['like', 'id_place', $this->id_place])
            ->andFilterWhere(['like', 'bank_name', $this->bank_name])
            ->andFilterWhere(['like', 'bank_branch_name', $this->bank_branch_name])
            ->andFilterWhere(['like', 'bank_acco_no', $this->bank_acco_no])
            ->andFilterWhere(['like', 'bank_acco_name', $this->bank_acco_name])
            ->andFilterWhere(['like', 'mobile1', $this->mobile1])
            ->andFilterWhere(['like', 'mobile2', $this->mobile2])
            ->andFilterWhere(['like', 'email_addr1', $this->email_addr1])
            ->andFilterWhere(['like', 'email_addr2', $this->email_addr2])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'org_info', $this->org_info])
            ->andFilterWhere(['like', 'character', $this->character])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'active_ip_addr', $this->active_ip_addr])
            ->andFilterWhere(['like', 'close_ip_addr', $this->close_ip_addr])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'reg_ip_addr', $this->reg_ip_addr])
            ->andFilterWhere(['like', 'upd_ip_addr', $this->upd_ip_addr]);

        return $dataProvider;
    }


}
