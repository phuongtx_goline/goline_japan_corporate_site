<?php

namespace backend\models;

use common\colorgb\ColorgbHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Feedback;
use yii\helpers\ArrayHelper;

/**
 * FeedbackColorgb represents the model behind the search form about `common\models\Feedback`.
 */
class FeedbackColorgb extends Feedback
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'status', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['subject','reply','customer', 'content', 'reg_date_time', 'reg_ip_addr', 'upd_date_time', 'upd_ip_addr'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // Bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $customerGroupIds = ColorgbHelper::getCustomerGroupIds();
        $query = Feedback::find()->where(['!=', 'feedback.status', $recordStatus['deleted']]);

        // Check user role
        /*if(Yii::$app->user->identity->is_admin == -1){
            $query->joinWith('customer');
            $query->andWhere(['customer.cared_user_id' => Yii::$app->user->identity->getId()]) ;

        }else{
            $query->joinWith('customer');
            $query->andWhere(['customer.customer_group_id' => $customerGroupIds]) ;
        }*/
        $query->joinWith('customer');
        $query->andWhere(['customer.customer_group_id' => $customerGroupIds]) ;

        $this->load($params);

        if ($this->status == $recordStatus['deleted']) {
            $query = Feedback::find()->where(['status' => $recordStatus['deleted']]);
        }

        if ($this->customer) {
            $query = Feedback::find()->joinWith('customer')->where(['!=', 'feedback.status', $recordStatus['deleted']])->andWhere(['like','customer.name',$this->customer]);
        }

        // Add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['reg_date_time' => SORT_DESC]]
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // Grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'status' => $this->status,
            'reg_date_time' => $this->reg_date_time,
            'reg_user_id' => $this->reg_user_id,
            'upd_date_time' => $this->upd_date_time,
            'upd_user_id' => $this->upd_user_id,
        ]);

        $query->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'reply', $this->reply])
            ->andFilterWhere(['like', 'reg_ip_addr', $this->reg_ip_addr])
            ->andFilterWhere(['like', 'upd_ip_addr', $this->upd_ip_addr]);

        return $dataProvider;
    }
}
