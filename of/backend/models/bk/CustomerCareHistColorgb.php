<?php

namespace backend\models;

use common\colorgb\ColorgbHelper;
use common\models\CustomerCareHist;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Customer;
use yii\helpers\ArrayHelper;

/**
 * CustomerCareHistColorgb represents the model behind the search form about `common\models\Customer`.
 */
class CustomerCareHistColorgb extends CustomerCareHist
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'customer_id', 'status', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['colorgb_date','date', 'remarks', 'reg_date_time', 'reg_ip_addr', 'upd_date_time', 'upd_ip_addr', 'customer_name'], 'safe'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // Bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $customerGroupIds = ColorgbHelper::getCustomerGroupIds();
        $query = CustomerCareHist::find()
            ->joinWith('customer')
            ->where(['!=', 'customer_care_hist.status', $recordStatus['deleted']]);

        // Check user role
        /*if(Yii::$app->user->identity->is_admin == -1){
            $query->andWhere(['customer.cared_user_id' => Yii::$app->user->identity->getId()]) ;

        }else{
            $query->andWhere(['customer.customer_group_id' => $customerGroupIds]) ;
        }*/
        $query->andWhere(['customer.customer_group_id' => $customerGroupIds]) ;

        $this->load($params);

        // Check filter
        if ($this->status == $recordStatus['deleted']) {
            $query = CustomerCareHist::find()
                ->joinWith('customer')
                ->where(['customer_care_hist.status' => $recordStatus['deleted']])
                ->andWhere(['customer.customer_group_id' => $customerGroupIds]);
        }

        // Check filter
        if ($this->customer_name) {
            $query = CustomerCareHist::find()
                ->joinWith('customer')
                ->where(['!=', 'customer_care_hist.status', $recordStatus['deleted']])
                ->andWhere(['customer.customer_group_id' => $customerGroupIds])
                ->andWhere(['like', 'customer.name', $this->customer_name]);
        }

        // Add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['reg_date_time' => SORT_DESC]]
        ]);

        // Check validate
        if (!$this->validate()) {
            return $dataProvider;
        }

        // Check date
        if($this->colorgb_date){
            $date = explode(' - ',$this->colorgb_date);
            if($date[0] == $date[1]){
                $query->andFilterWhere(['like', 'date', $date[0]]);
            }else{
                $query->andFilterWhere(['>=', 'date', $date[0]])
                    ->andFilterWhere(['<=', 'date', $date[1]]);
            }
        }

        // Grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'customer_id' => $this->customer_id,
            'reg_date_time' => $this->reg_date_time,
            'reg_user_id' => $this->reg_user_id,
            'upd_date_time' => $this->upd_date_time,
            'upd_user_id' => $this->upd_user_id,
        ]);

        $query->andFilterWhere(['like', 'date', $this->date])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'reg_ip_addr', $this->reg_ip_addr])
            ->andFilterWhere(['like', 'upd_ip_addr', $this->upd_ip_addr]);

        return $dataProvider;
    }
}
