<?php

namespace backend\models;

use common\colorgb\ColorgbHelper;
use common\models\Setting;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Contract;
use yii\helpers\ArrayHelper;

/**
 * ContractActivateForm represents the model behind the search form about `common\models\Contract`.
 */
class ContractActivateForm extends Contract
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['begin_date','active_date', 'end_date_est'], 'required'],
            [['period_val','period_type','contract_type_id','invest_value','id', 'fund_net_asset', 'fund_unit_qty', 'fund_unit_price', 'invest_unit_qty'], 'safe'],
        ];
    }

}
