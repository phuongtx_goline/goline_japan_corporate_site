<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\FundUnit;
use yii\helpers\ArrayHelper;

/**
 * InvestmentColorgb represents the model behind the search form about `common\models\FundUnit`.
 */
class InvestmentColorgb extends FundUnit
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'fund_net_asset', 'fund_unit_qty', 'fund_unit_price'], 'number'],
            [['colorgb_date','file_path', 'remarks', 'reg_date_time', 'reg_ip_addr', 'upd_date_time', 'upd_ip_addr'], 'safe'],
            [['status', 'reg_user_id', 'upd_user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // Bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $query = FundUnit::find()->where(['!=', 'status', $recordStatus['deleted']]);

        $this->load($params);

        if ($this->status == $recordStatus['deleted']) {
            $query = FundUnit::find()->where(['status' => $recordStatus['deleted']]);
        }

        // Add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['date' => SORT_DESC]]

        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // Grid filtering conditions
        $query->andFilterWhere([
            'date' => $this->date,
            'fund_net_asset' => $this->fund_net_asset,
            'fund_unit_qty' => $this->fund_unit_qty,
            'fund_unit_price' => $this->fund_unit_price,
            'status' => $this->status,
            'reg_date_time' => $this->reg_date_time,
            'reg_user_id' => $this->reg_user_id,
            'upd_date_time' => $this->upd_date_time,
            'upd_user_id' => $this->upd_user_id,
        ]);

        // Check date
        if($this->colorgb_date){
            $date = explode(' - ',$this->colorgb_date);
            $query->andFilterWhere(['>=', 'date', str_replace('-','',$date[0])])
                ->andFilterWhere(['<=', 'date', str_replace('-','',$date[1])]);
        }

        $query->andFilterWhere(['like', 'file_path', $this->file_path])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'reg_ip_addr', $this->reg_ip_addr])
            ->andFilterWhere(['like', 'upd_ip_addr', $this->upd_ip_addr]);

        return $dataProvider;
    }
}
