<?php

namespace backend\models;

use common\colorgb\ColorgbHelper;
use common\models\Setting;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Contract;
use yii\helpers\ArrayHelper;

/**
 * ContractSettleForm represents the model behind the search form about `common\models\Contract`.
 */
class ContractSettleForm extends Contract
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['end_date_real'], 'required'],
            [['remarks', 'tax_rate','representative', 'profit_amt', 'settle_fee_amt', 'recv_amt', 'id'], 'safe'],
        ];
    }

}
