<?php

namespace backend\models;

use common\colorgb\ColorgbHelper;
use common\models\Setting;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Contract;
use yii\helpers\ArrayHelper;

/**
 * ContractRenewForm represents the model behind the search form about `common\models\Contract`.
 */
class ContractRenewForm extends Contract
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['begin_date', 'end_date_est', 'new_end_date'], 'required'],
            [['remarks', 'id'], 'safe'],
            [['period_type', 'period_val',], 'integer'],
        ];
    }

}
