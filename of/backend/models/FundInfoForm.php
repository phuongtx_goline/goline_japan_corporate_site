<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CORTFUNDINFO;

/**
 * FundInfoForm represents the model behind the search form about `common\models\CORTFUNDINFO`.
 */
class FundInfoForm extends CORTFUNDINFO
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['SEC_CD', 'SEC_NAME', 'IPO_PRICE', 'TOTAL_QTY', 'MIN_IPO_AMOUNT', 'MIN_BALANCE_QTY', 'TRANS_DATE_TYPE', 'END_ORDER_DATE_NUM', 'END_ORDER_HOUR', 'STATUS'], 'required'],
            [['END_ORDER_HOUR', 'REG_DATE_TIME', 'UPD_DATE_TIME','WEEK_DAYS'], 'safe'],
            [['STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['REMARKS'], 'string'],
            [['SEC_CD'], 'trim'],
            [['SEC_NAME', 'IPO_PRICE', 'TOTAL_QTY', 'MIN_IPO_AMOUNT', 'MIN_BALANCE_QTY', 'TRANS_DATE_TYPE', 'TRANS_DATE', 'END_ORDER_DATE_NUM'], 'string', 'max' => 255],
        ];
    }


}