<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MSTTBRANCH;

/**
 * BranchColorgb represents the model behind the search form about `common\models\MSTTBRANCH`.
 */
class BranchColorgb extends MSTTBRANCH
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['a1','a2','TRANSACTION_CD', 'BRANCH_NAME', 'BRANCH_ADRS', 'TEL_NO', 'FAX_NO', 'EMAIL_ADRS', 'DEPUTY', 'REMARKS', 'REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['LOCATION_CD', 'STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
        ];
    }

    public $a1;
    public $a2;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MSTTBRANCH::find();


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        /*
         *
         */
        if($this->a1){
            $query->andFilterWhere(['or',
                ['like', 'TRANSACTION_CD', $this->a1],
                ['like', 'BRANCH_NAME', $this->a1],
            ]);
        }

        /*
         *
         */
        if($this->a2){
            $query->andFilterWhere(['or',
                ['like', 'BRANCH_ADRS', $this->a2],
                ['like', 'TEL_NO', $this->a2],
                ['like', 'FAX_NO', $this->a2],
            ]);
        }

        $query->andFilterWhere([
            'LOCATION_CD' => $this->LOCATION_CD,
            'STATUS' => $this->STATUS,
            'REG_DATE_TIME' => $this->REG_DATE_TIME,
            'REG_USER_ID' => $this->REG_USER_ID,
            'UPD_DATE_TIME' => $this->UPD_DATE_TIME,
            'UPD_USER_ID' => $this->UPD_USER_ID,
        ]);

        $query->andFilterWhere(['like', 'TRANSACTION_CD', $this->TRANSACTION_CD])
            ->andFilterWhere(['like', 'BRANCH_NAME', $this->BRANCH_NAME])
            ->andFilterWhere(['like', 'BRANCH_ADRS', $this->BRANCH_ADRS])
            ->andFilterWhere(['like', 'TEL_NO', $this->TEL_NO])
            ->andFilterWhere(['like', 'FAX_NO', $this->FAX_NO])
            ->andFilterWhere(['like', 'EMAIL_ADRS', $this->EMAIL_ADRS])
            ->andFilterWhere(['like', 'DEPUTY', $this->DEPUTY])
            ->andFilterWhere(['like', 'REMARKS', $this->REMARKS]);

        return $dataProvider;
    }
}
