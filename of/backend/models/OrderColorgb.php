<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ORDTORDER;

/**
 * OrderColorgb represents the model behind the search form about `common\models\ORDTORDER`.
 */
class OrderColorgb extends ORDTORDER
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'ALLO_DATE',  'ORD_CHANEL', 'TRADE_TYPE', 'PAYMENT_DATE', 'ENTRY_DATE', 'STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['ORDER_NO', 'TRADE_TIME', 'TRADE_DATE', 'ORD_TYPE', 'SEC_CD', 'CUST_NO', 'CUST_NAME', 'CUST_TRANSACTION_CD', 'NOTES', 'TRANSACTION_CD', 'BROKER_CD', 'REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['ORD_QTY', 'ORD_AMT', 'CUST_CD', 'FEE_RATE', 'FEE_AMT', 'TAX_RATE', 'TAX_AMT', 'RIGHT_TAX_RATE', 'RIGHT_TAX_QTY', 'RIGHT_TAX_AMT', 'MAT_QTY', 'MAT_PRICE', 'MAT_AMT'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
// bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ORDTORDER::find();

// add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
// uncomment the following line if you do not want to return any records when validation fails
// $query->where('0=1');
            return $dataProvider;
        }

        // Check date
        if ($this->TRADE_DATE) {
            $date = explode(' - ', $this->TRADE_DATE);
            $f = explode('/',$date[0]);
            $l = explode('/',$date[1]);
            $query->andFilterWhere(['>=', 'TRADE_DATE', $f[2].$f[1].$f[0]])
                ->andFilterWhere(['<=', 'TRADE_DATE', $l[2].$l[1].$l[0]]);
        }

// grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'ALLO_DATE' => $this->ALLO_DATE,
            'TRADE_DATE' => $this->TRADE_DATE,
            'TRADE_TIME' => $this->TRADE_TIME,
            'ORD_CHANEL' => $this->ORD_CHANEL,
            'TRADE_TYPE' => $this->TRADE_TYPE,
            'ORD_QTY' => $this->ORD_QTY,
            'ORD_AMT' => $this->ORD_AMT,
            'CUST_CD' => $this->CUST_CD,
            'FEE_RATE' => $this->FEE_RATE,
            'FEE_AMT' => $this->FEE_AMT,
            'TAX_RATE' => $this->TAX_RATE,
            'TAX_AMT' => $this->TAX_AMT,
            'RIGHT_TAX_RATE' => $this->RIGHT_TAX_RATE,
            'RIGHT_TAX_QTY' => $this->RIGHT_TAX_QTY,
            'RIGHT_TAX_AMT' => $this->RIGHT_TAX_AMT,
            'MAT_QTY' => $this->MAT_QTY,
            'MAT_PRICE' => $this->MAT_PRICE,
            'MAT_AMT' => $this->MAT_AMT,
            'PAYMENT_DATE' => $this->PAYMENT_DATE,
            'ENTRY_DATE' => $this->ENTRY_DATE,
            'STATUS' => $this->STATUS,
            'REG_DATE_TIME' => $this->REG_DATE_TIME,
            'REG_USER_ID' => $this->REG_USER_ID,
            'UPD_DATE_TIME' => $this->UPD_DATE_TIME,
            'UPD_USER_ID' => $this->UPD_USER_ID,
        ]);

        $query->andFilterWhere(['like', 'ORDER_NO', $this->ORDER_NO])
            ->andFilterWhere(['like', 'ORD_TYPE', $this->ORD_TYPE])
            ->andFilterWhere(['like', 'SEC_CD', $this->SEC_CD])
            ->andFilterWhere(['like', 'CUST_NO', $this->CUST_NO])
            ->andFilterWhere(['like', 'CUST_NAME', $this->CUST_NAME])
            ->andFilterWhere(['like', 'CUST_TRANSACTION_CD', $this->CUST_TRANSACTION_CD])
            ->andFilterWhere(['like', 'NOTES', $this->NOTES])
            ->andFilterWhere(['like', 'TRANSACTION_CD', $this->TRANSACTION_CD])
            ->andFilterWhere(['like', 'BROKER_CD', $this->BROKER_CD]);

        return $dataProvider;
    }
}