<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CORTFUNDNAV;

/**
 * NavColorgb represents the model behind the search form about `common\models\CORTFUNDNAV`.
 */
class NavColorgb extends CORTFUNDNAV
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID',  'STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['SEC_CD', 'VALID_DATE','REMARKS', 'REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['NAV'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
// bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CORTFUNDNAV::find();

// add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
// uncomment the following line if you do not want to return any records when validation fails
// $query->where('0=1');
            return $dataProvider;
        }

        // Check date
        if ($this->VALID_DATE) {
            $date = explode(' - ', $this->VALID_DATE);
            $f = explode('/',$date[0]);
            $l = explode('/',$date[1]);
            $query->andFilterWhere(['>=', 'VALID_DATE', $f[2].$f[1].$f[0]])
                ->andFilterWhere(['<=', 'VALID_DATE', $l[2].$l[1].$l[0]]);
        }

// grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'NAV' => $this->NAV,
            'STATUS' => $this->STATUS,
            'REG_DATE_TIME' => $this->REG_DATE_TIME,
            'REG_USER_ID' => $this->REG_USER_ID,
            'UPD_DATE_TIME' => $this->UPD_DATE_TIME,
            'UPD_USER_ID' => $this->UPD_USER_ID,
        ]);

        $query->andFilterWhere(['like', 'SEC_CD', $this->SEC_CD])
            ->andFilterWhere(['like', 'REMARKS', $this->REMARKS]);

        return $dataProvider;
    }
}