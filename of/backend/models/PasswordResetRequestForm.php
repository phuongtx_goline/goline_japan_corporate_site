<?php

namespace backend\models;

use common\colorgb\ColorgbHelper;
use common\models\User;
use Yii;
use yii\helpers\ArrayHelper;

/**
 */
class PasswordResetRequestForm extends User
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email_addr', 'trim'],
            ['email_addr', 'required'],
            ['email_addr', 'email'],
            ['email_addr', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => [
                    'status' => ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value')['active'],
                ],
                'message' => 'Không thể thiết lập lại mật khẩu với email cung cấp'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value')['active'],
            'email_addr' => $this->email_addr,
            'is_locked' => 0,
        ]);

        if (!$user) {
            return false;
        }

        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

        $sendMail = ColorgbHelper::sendMail('Thiết lập lại mật khẩu','passwordResetToken', $this->email_addr, $user);
        return $sendMail ? true : false;
    }
}
