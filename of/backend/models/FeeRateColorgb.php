<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ORDTTRADEFEERATE;

/**
 * FeeRateColorgb represents the model behind the search form about `common\models\ORDTTRADEFEERATE`.
 */
class FeeRateColorgb extends ORDTTRADEFEERATE
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'FEE_TYPE', 'STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['FEE_RATE_CODE', 'FEE_RATE_NAME', 'REMARKS','VALID_DATE', 'REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
        ];
    }

    public $VALID_DATE;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ORDTTRADEFEERATE::find();

        // Add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'FEE_TYPE' => $this->FEE_TYPE,
            'STATUS' => $this->STATUS,
            'REG_DATE_TIME' => $this->REG_DATE_TIME,
            'REG_USER_ID' => $this->REG_USER_ID,
            'UPD_DATE_TIME' => $this->UPD_DATE_TIME,
            'UPD_USER_ID' => $this->UPD_USER_ID,
        ]);

        $query->andFilterWhere(['like', 'FEE_RATE_CODE', $this->FEE_RATE_CODE])
            ->andFilterWhere(['like', 'FEE_RATE_NAME', $this->FEE_RATE_NAME])
            ->andFilterWhere(['like', 'REMARKS', $this->REMARKS]);

        return $dataProvider;
    }
}