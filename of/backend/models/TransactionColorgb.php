<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CORTSECTRANSACTION;

/**
 * TransactionColorgb represents the model behind the search form about `common\models\CORTSECTRANSACTION`.
 */
class TransactionColorgb extends CORTSECTRANSACTION
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'ALLO_DATE', 'CUST_CD', 'ACTION', 'SEC_TYPE_CD', 'STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['SEC_CD', 'TRANSACTION_CD', 'REMARKS', 'REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['QUANTITY', 'PRICE'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
// bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CORTSECTRANSACTION::find();

// add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
// uncomment the following line if you do not want to return any records when validation fails
// $query->where('0=1');
            return $dataProvider;
        }

// grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'ALLO_DATE' => $this->ALLO_DATE,
            'CUST_CD' => $this->CUST_CD,
            'ACTION' => $this->ACTION,
            'SEC_TYPE_CD' => $this->SEC_TYPE_CD,
            'QUANTITY' => $this->QUANTITY,
            'PRICE' => $this->PRICE,
            'STATUS' => $this->STATUS,
            'REG_DATE_TIME' => $this->REG_DATE_TIME,
            'REG_USER_ID' => $this->REG_USER_ID,
            'UPD_DATE_TIME' => $this->UPD_DATE_TIME,
            'UPD_USER_ID' => $this->UPD_USER_ID,
        ]);

        $query->andFilterWhere(['like', 'SEC_CD', $this->SEC_CD])
            ->andFilterWhere(['like', 'TRANSACTION_CD', $this->TRANSACTION_CD])
            ->andFilterWhere(['like', 'REMARKS', $this->REMARKS]);

        return $dataProvider;
    }
}