<?php

namespace backend\models\admin;

use common\models\ContractType;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * ContractTypeColorgb represents the model behind the search form about `common\models\ContractType`.
 */
class ContractTypeColorgb extends ContractType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'period_type', 'period_val', 'status', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['code', 'name', 'file_template_url', 'remarks', 'reg_date_time', 'reg_ip_addr', 'upd_date_time', 'upd_ip_addr'], 'safe'],
            [['min_invest_value', 'max_invest_value', 'base_profit_rate', 'profit_rate'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // Bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $query = ContractType::find()->where(['!=', 'status', $recordStatus['deleted']]);

        $this->load($params);

        if ($this->status == $recordStatus['deleted']) {
            $query = ContractType::find()->where(['status' => $recordStatus['deleted']]);
        }

        // Add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['reg_date_time' => SORT_DESC]]
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // Grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'period_type' => $this->period_type,
            'period_val' => $this->period_val,
            'min_invest_value' => $this->min_invest_value,
            'max_invest_value' => $this->max_invest_value,
            'base_profit_rate' => $this->base_profit_rate,
            'profit_rate' => $this->profit_rate,
            'status' => $this->status,
            'reg_date_time' => $this->reg_date_time,
            'reg_user_id' => $this->reg_user_id,
            'upd_date_time' => $this->upd_date_time,
            'upd_user_id' => $this->upd_user_id,
        ]);

        $query->andFilterWhere(['or',
            ['like', 'code', $this->name],
            ['like', 'name', $this->name],
        ])->andFilterWhere(['like', 'file_template_url', $this->file_template_url])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'reg_ip_addr', $this->reg_ip_addr])
            ->andFilterWhere(['like', 'upd_ip_addr', $this->upd_ip_addr]);


        return $dataProvider;
    }
}
