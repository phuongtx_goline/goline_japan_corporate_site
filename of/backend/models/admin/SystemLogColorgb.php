<?php

namespace backend\models\admin;

use common\models\MsgSendRequest;
use common\models\SystemLog;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * SystemLogColorgb represents the model behind the search form about `common\models\SystemLog`.
 */
class SystemLogColorgb extends SystemLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['colorgb_date','log_time', 'table_name', 'action', 'before_value', 'after_value', 'reg_date_time', 'reg_ip_addr', 'upd_date_time', 'upd_ip_addr'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // Bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SystemLog::find();

        // Add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['reg_date_time' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // Grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'reg_date_time' => $this->reg_date_time,
            'reg_user_id' => $this->reg_user_id,
            'upd_date_time' => $this->upd_date_time,
            'upd_user_id' => $this->upd_user_id,
        ]);

        // Check date
        if($this->colorgb_date){
            $date = explode(' - ',$this->colorgb_date);
            if($date[0] == $date[1]){
                $query->andFilterWhere(['like', 'log_time', $date[0]]);
            }else{
                $query->andFilterWhere(['>=', 'log_time', $date[0]])
                    ->andFilterWhere(['<=', 'log_time', $date[1]]);
            }
        }

        $query->andFilterWhere(['like', 'table_name', $this->table_name])
            ->andFilterWhere(['like', 'action', $this->action])
            ->andFilterWhere(['like', 'before_value', $this->before_value])
            ->andFilterWhere(['like', 'after_value', $this->after_value])
            ->andFilterWhere(['like', 'reg_ip_addr', $this->reg_ip_addr])
            ->andFilterWhere(['like', 'upd_ip_addr', $this->upd_ip_addr]);

        return $dataProvider;
    }
}
