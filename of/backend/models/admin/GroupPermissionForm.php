<?php

namespace backend\models\admin;

use common\colorgb\ColorgbHelper;
use common\models\Customer;
use common\models\CustomerGroup;
use common\models\User;
use common\models\UserCustomerGroup;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 */
class GroupPermissionForm extends UserCustomerGroup
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_group_id', 'customer_group_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_group_id' => 'Nhóm người dùng',
            'customer_group_id' => 'Nhóm khách hàng',
        ];
    }

    /**
     *
     */
    public function saveUserGroup()
    {

        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        UserCustomerGroup::updateAll(['status' => $recordStatus['deleted']], ['customer_group_id' => $this->customer_group_id]);

        // Check is array
        if (is_array($this->user_group_id)) {

            foreach ($this->user_group_id as $value) {

                // User customer group
                $userCustomerGroup = UserCustomerGroup::findOne(['user_group_id' => $value, 'customer_group_id' => $this->customer_group_id]);
                if ($userCustomerGroup) {
                    $model = $userCustomerGroup;
                    $temp = ArrayHelper::toArray($userCustomerGroup);
                } else {
                    $model = new UserCustomerGroup();
                }

                // Save data
                $model->user_group_id = $value;
                $model->customer_group_id = $this->customer_group_id;
                $model->status = $recordStatus['active'];
                $model->save();

                // User customer group
                if ($userCustomerGroup) {

                    // Save data
                    ColorgbHelper::setDataLog($model, $temp);

                } else {

                    // Save data
                    ColorgbHelper::setDataLog($model);
                }

            }
        }

        // Return
        return true;
    }

    /**
     *
     */
    public function saveCustomerGroup()
    {

        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        UserCustomerGroup::updateAll(['status' => $recordStatus['deleted']], ['user_group_id' => $this->user_group_id]);

        // Check is array
        if (is_array($this->customer_group_id)) {

            foreach ($this->customer_group_id as $value) {

                // Customer customer group
                $userCustomerGroup = UserCustomerGroup::findOne(['customer_group_id' => $value, 'user_group_id' => $this->user_group_id]);
                if ($userCustomerGroup) {
                    $model = $userCustomerGroup;
                    $temp = ArrayHelper::toArray($userCustomerGroup);
                } else {
                    $model = new UserCustomerGroup();
                }

                // Save data
                $model->customer_group_id = $value;
                $model->user_group_id = $this->user_group_id;
                $model->status = $recordStatus['active'];
                $model->save();

                // Customer customer group
                if ($userCustomerGroup) {

                    // Save data
                    ColorgbHelper::setDataLog($model, $temp);

                } else {

                    // Save data
                    ColorgbHelper::setDataLog($model);
                }

            }
        }

        // Return
        return true;
    }
}
