<?php

namespace backend\models\admin;

use common\models\MsgSendRequest;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * MsgSendRequestColorgb represents the model behind the search form about `common\models\MsgSendRequest`.
 */
class MsgSendRequestColorgb extends MsgSendRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'msg_type', 'send_type', 'status', 'failed_count', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['date'], 'number'],
            [['colorgb_date','send_to', 'from_address', 'from_name', 'subject', 'content', 'attachments', 'remarks', 'reg_date_time', 'reg_ip_addr', 'upd_date_time', 'upd_ip_addr'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // Bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $query = MsgSendRequest::find()->where(['!=', 'status', $recordStatus['deleted']]);

        $this->load($params);

        if ($this->status == $recordStatus['deleted']) {
            $query = MsgSendRequest::find()->where(['status' => $recordStatus['deleted']]);
        }

        // Add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }


        // Grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'msg_type' => $this->msg_type,
            'send_type' => $this->send_type,
            'status' => $this->status,
            'failed_count' => $this->failed_count,
            'reg_date_time' => $this->reg_date_time,
            'reg_user_id' => $this->reg_user_id,
            'upd_date_time' => $this->upd_date_time,
            'upd_user_id' => $this->upd_user_id,
        ]);


        // Check date
        if($this->colorgb_date){
            $date = explode(' - ',$this->colorgb_date);
            $query->andFilterWhere(['>=', 'date', str_replace('-','',$date[0])])
                ->andFilterWhere(['<=', 'date', str_replace('-','',$date[1])]);
        }

        $query->andFilterWhere(['like', 'send_to', $this->send_to])
            ->andFilterWhere(['like', 'from_address', $this->from_address])
            ->andFilterWhere(['like', 'from_name', $this->from_name])
            ->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'attachments', $this->attachments])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'reg_ip_addr', $this->reg_ip_addr])
            ->andFilterWhere(['like', 'upd_ip_addr', $this->upd_ip_addr]);

        return $dataProvider;
    }
}
