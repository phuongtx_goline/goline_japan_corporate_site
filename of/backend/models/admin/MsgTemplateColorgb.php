<?php

namespace backend\models\admin;

use common\models\MsgTemplate;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * MsgTemplateColorgb represents the model behind the search form about `common\models\MsgTemplate`.
 */
class MsgTemplateColorgb extends MsgTemplate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'msg_type', 'send_type', 'foreign_type', 'status', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['title', 'content', 'file_template_url', 'mail_bcc', 'remarks', 'reg_date_time', 'reg_ip_addr', 'upd_date_time', 'upd_ip_addr'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // Bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $query = MsgTemplate::find()->where(['!=', 'status', $recordStatus['deleted']]);

        $this->load($params);

        if ($this->status == $recordStatus['deleted']) {
            $query = MsgTemplate::find()->where(['status' => $recordStatus['deleted']]);
        }

        // Add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['reg_date_time' => SORT_DESC]]
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // Grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'msg_type' => $this->msg_type,
            'send_type' => $this->send_type,
            'foreign_type' => $this->foreign_type,
            'status' => $this->status,
            'reg_date_time' => $this->reg_date_time,
            'reg_user_id' => $this->reg_user_id,
            'upd_date_time' => $this->upd_date_time,
            'upd_user_id' => $this->upd_user_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'file_template_url', $this->file_template_url])
            ->andFilterWhere(['like', 'mail_bcc', $this->mail_bcc])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'reg_ip_addr', $this->reg_ip_addr])
            ->andFilterWhere(['like', 'upd_ip_addr', $this->upd_ip_addr]);

        return $dataProvider;
    }
}
