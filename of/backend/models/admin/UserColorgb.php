<?php

namespace backend\models\admin;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;
use yii\helpers\ArrayHelper;

/**
 * UserColorgb represents the model behind the search form about `common\models\User`.
 */
class UserColorgb extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['mobile', 'unique', 'targetClass' => '\common\models\Customer'],
            [['id', 'is_admin', 'group_id', 'status', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['mobile', 'password', 'name', 'address', 'email_addr', 'email_pw', 'email_signature', 'remarks', 'password_reset_token', 'auth_key', 'reg_date_time', 'reg_ip_addr', 'upd_date_time', 'upd_ip_addr'], 'safe'],
        ];
    }

    public $created_at;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // Bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $query = User::find()
            ->where(['!=', 'id', Yii::$app->user->identity->getId()])
            ->andWhere(['!=', 'status', $recordStatus['deleted']]);

        $this->load($params);

        if ($this->status == $recordStatus['deleted']) {
            $query = User::find()->where(['status' => $recordStatus['deleted']]);
        }

        // Add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => ['defaultOrder' => ['reg_date_time' => SORT_DESC]]
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // Grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_admin' => $this->is_admin,
            'group_id' => $this->group_id,
            'status' => $this->status,
            'reg_date_time' => $this->reg_date_time,
            'reg_user_id' => $this->reg_user_id,
            'upd_date_time' => $this->upd_date_time,
            'upd_user_id' => $this->upd_user_id,
        ]);

        $query->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'email_addr', $this->email_addr])
            ->andFilterWhere(['like', 'email_pw', $this->email_pw])
            ->andFilterWhere(['like', 'email_signature', $this->email_signature])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'reg_ip_addr', $this->reg_ip_addr])
            ->andFilterWhere(['like', 'upd_ip_addr', $this->upd_ip_addr]);

        return $dataProvider;
    }
}
