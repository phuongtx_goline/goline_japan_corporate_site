<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CORCUSTOMER;

/**
 * CustomerColorgb represents the model behind the search form about `common\models\CORCUSTOMER`.
 */
class CustomerColorgb extends CORCUSTOMER
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CUST_CD', 'CUST_TYPE', 'FOREIGN_TYPE', 'BIRTHDAY', 'SEX', 'EMAIL_FLG', 'APPROVAL_CD', 'ISSUE_LOCATION_CD', 'ISSUE_DATE', 'EXPIRE_DATE', 'JOB', 'ACCO_DIVIDE', 'ACCO_STS', 'OPEN_DATE', 'CLOSE_DATE', 'FATCA', 'PRES_APPROVAL_CD', 'PRES_ISSUE_LOCATION_CD', 'PRES_ISSUE_DATE', 'STATUS', 'VSD_STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['CUST_NO', 'CUST_NAME', 'TEL_NO', 'MOBILE_NO', 'EMAIL_ADRS', 'ID_NUMBER', 'ADDR1', 'ADDR2', 'COUNTRY_CD', 'BROKER_CD', 'POSITION', 'COMPANY', 'COMPANY_TEL', 'CONTRACT_NO', 'TRADING_CODE', 'PRES_NAME', 'PRES_ID_NUMBER', 'PRES_TEL_NO', 'PRES_COUNTRY_CD', 'PRES_POSITION', 'BANK_ACCO_NAME', 'BANK_ACCO_NO', 'BANK_NAME', 'BANK_BRANCH_NAME', 'TRANSACTION_CD', 'REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
// bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CORCUSTOMER::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        /*
         *
         */
        if($this->CUST_NAME){
            $query->andFilterWhere(['or',
                ['like', 'CUST_NAME', $this->CUST_NAME],
                ['like', 'MOBILE_NO', $this->CUST_NAME],
                ['like', 'EMAIL_ADRS', $this->CUST_NAME],
            ]);
        }
        $query->andFilterWhere([
            'CUST_CD' => $this->CUST_CD,
            'CUST_TYPE' => $this->CUST_TYPE,
            'FOREIGN_TYPE' => $this->FOREIGN_TYPE,
            'BIRTHDAY' => $this->BIRTHDAY,
            'SEX' => $this->SEX,
            'EMAIL_FLG' => $this->EMAIL_FLG,
            'APPROVAL_CD' => $this->APPROVAL_CD,
            'ISSUE_LOCATION_CD' => $this->ISSUE_LOCATION_CD,
            'ISSUE_DATE' => $this->ISSUE_DATE,
            'EXPIRE_DATE' => $this->EXPIRE_DATE,
            'JOB' => $this->JOB,
            'ACCO_DIVIDE' => $this->ACCO_DIVIDE,
            'ACCO_STS' => $this->ACCO_STS,
            'OPEN_DATE' => $this->OPEN_DATE,
            'CLOSE_DATE' => $this->CLOSE_DATE,
            'FATCA' => $this->FATCA,
            'PRES_APPROVAL_CD' => $this->PRES_APPROVAL_CD,
            'PRES_ISSUE_LOCATION_CD' => $this->PRES_ISSUE_LOCATION_CD,
            'PRES_ISSUE_DATE' => $this->PRES_ISSUE_DATE,
            'STATUS' => $this->STATUS,
            'VSD_STATUS' => $this->VSD_STATUS,
            'REG_DATE_TIME' => $this->REG_DATE_TIME,
            'REG_USER_ID' => $this->REG_USER_ID,
            'UPD_DATE_TIME' => $this->UPD_DATE_TIME,
            'UPD_USER_ID' => $this->UPD_USER_ID,
        ]);

        $query->andFilterWhere(['like', 'CUST_NO', $this->CUST_NO])
            ->andFilterWhere(['like', 'TEL_NO', $this->TEL_NO])
            ->andFilterWhere(['like', 'MOBILE_NO', $this->MOBILE_NO])
            ->andFilterWhere(['like', 'EMAIL_ADRS', $this->EMAIL_ADRS])
            ->andFilterWhere(['like', 'ID_NUMBER', $this->ID_NUMBER])
            ->andFilterWhere(['like', 'ADDR1', $this->ADDR1])
            ->andFilterWhere(['like', 'ADDR2', $this->ADDR2])
            ->andFilterWhere(['like', 'COUNTRY_CD', $this->COUNTRY_CD])
            ->andFilterWhere(['like', 'BROKER_CD', $this->BROKER_CD])
            ->andFilterWhere(['like', 'POSITION', $this->POSITION])
            ->andFilterWhere(['like', 'COMPANY', $this->COMPANY])
            ->andFilterWhere(['like', 'COMPANY_TEL', $this->COMPANY_TEL])
            ->andFilterWhere(['like', 'CONTRACT_NO', $this->CONTRACT_NO])
            ->andFilterWhere(['like', 'TRADING_CODE', $this->TRADING_CODE])
            ->andFilterWhere(['like', 'PRES_NAME', $this->PRES_NAME])
            ->andFilterWhere(['like', 'PRES_ID_NUMBER', $this->PRES_ID_NUMBER])
            ->andFilterWhere(['like', 'PRES_TEL_NO', $this->PRES_TEL_NO])
            ->andFilterWhere(['like', 'PRES_COUNTRY_CD', $this->PRES_COUNTRY_CD])
            ->andFilterWhere(['like', 'PRES_POSITION', $this->PRES_POSITION])
            ->andFilterWhere(['like', 'BANK_ACCO_NAME', $this->BANK_ACCO_NAME])
            ->andFilterWhere(['like', 'BANK_ACCO_NO', $this->BANK_ACCO_NO])
            ->andFilterWhere(['like', 'BANK_NAME', $this->BANK_NAME])
            ->andFilterWhere(['like', 'BANK_BRANCH_NAME', $this->BANK_BRANCH_NAME])
            ->andFilterWhere(['like', 'TRANSACTION_CD', $this->TRANSACTION_CD]);

        return $dataProvider;
    }
}