<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CORTFUNDINFO;

/**
 * FundInfoColorgb represents the model behind the search form about `common\models\CORTFUNDINFO`.
 */
class FundInfoColorgb extends CORTFUNDINFO
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['a1','SEC_CD', 'SEC_NAME', 'IPO_PRICE', 'TOTAL_QTY', 'MIN_IPO_AMOUNT', 'MIN_BALANCE_QTY', 'TRANS_DATE_TYPE', 'WEEK_DAYS', 'TRANS_DATE', 'END_ORDER_DATE_NUM', 'END_ORDER_HOUR', 'REMARKS', 'REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
        ];
    }

    public $a1;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CORTFUNDINFO::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        /*
         *
         */
        if($this->a1){
            $query->andFilterWhere(['or',
                ['like', 'SEC_CD', $this->a1],
                ['like', 'SEC_NAME', $this->a1],
                ['like', 'IPO_PRICE', $this->a1],
                ['like', 'TOTAL_QTY', $this->a1],
            ]);
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'END_ORDER_HOUR' => $this->END_ORDER_HOUR,
            'STATUS' => $this->STATUS,
            'REG_DATE_TIME' => $this->REG_DATE_TIME,
            'REG_USER_ID' => $this->REG_USER_ID,
            'UPD_DATE_TIME' => $this->UPD_DATE_TIME,
            'UPD_USER_ID' => $this->UPD_USER_ID,
        ]);

        $query->andFilterWhere(['like', 'SEC_CD', $this->SEC_CD])
            ->andFilterWhere(['like', 'SEC_NAME', $this->SEC_NAME])
            ->andFilterWhere(['like', 'IPO_PRICE', $this->IPO_PRICE])
            ->andFilterWhere(['like', 'TOTAL_QTY', $this->TOTAL_QTY])
            ->andFilterWhere(['like', 'MIN_IPO_AMOUNT', $this->MIN_IPO_AMOUNT])
            ->andFilterWhere(['like', 'MIN_BALANCE_QTY', $this->MIN_BALANCE_QTY])
            ->andFilterWhere(['like', 'TRANS_DATE_TYPE', $this->TRANS_DATE_TYPE])
            ->andFilterWhere(['like', 'WEEK_DAYS', $this->WEEK_DAYS])
            ->andFilterWhere(['like', 'TRANS_DATE', $this->TRANS_DATE])
            ->andFilterWhere(['like', 'END_ORDER_DATE_NUM', $this->END_ORDER_DATE_NUM])
            ->andFilterWhere(['like', 'REMARKS', $this->REMARKS]);

        return $dataProvider;
    }
}