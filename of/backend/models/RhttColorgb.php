<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RHTTRIGHTINFO;

/**
* RhttColorgb represents the model behind the search form about `common\models\RHTTRIGHTINFO`.
*/
class RhttColorgb extends RHTTRIGHTINFO
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['ID', 'ALLO_DATE', 'RIGHT_TYPE', 'NO_RIGHT_DATE', 'OWNER_FIX_DATE', 'RIGHT_EXC_DATE', 'EXPECTED_EXC_DATE', 'BEGIN_TRADE_DATE', 'ROUNDTYPE', 'DECIMAL_PLACE', 'ODD_DECIMAL_PLACE', 'STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['SEC_CD', 'REMARKS', 'REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['RATES1', 'RATED1', 'RATES', 'RATED', 'ROUND_PRICE', 'TAX_RATE'], 'number'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = RHTTRIGHTINFO::find();

// add conditions that should always apply here

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to return any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

// grid filtering conditions
$query->andFilterWhere([
            'ID' => $this->ID,
            'ALLO_DATE' => $this->ALLO_DATE,
            'RIGHT_TYPE' => $this->RIGHT_TYPE,
            'NO_RIGHT_DATE' => $this->NO_RIGHT_DATE,
            'OWNER_FIX_DATE' => $this->OWNER_FIX_DATE,
            'RIGHT_EXC_DATE' => $this->RIGHT_EXC_DATE,
            'EXPECTED_EXC_DATE' => $this->EXPECTED_EXC_DATE,
            'BEGIN_TRADE_DATE' => $this->BEGIN_TRADE_DATE,
            'RATES1' => $this->RATES1,
            'RATED1' => $this->RATED1,
            'RATES' => $this->RATES,
            'RATED' => $this->RATED,
            'ROUNDTYPE' => $this->ROUNDTYPE,
            'DECIMAL_PLACE' => $this->DECIMAL_PLACE,
            'ROUND_PRICE' => $this->ROUND_PRICE,
            'ODD_DECIMAL_PLACE' => $this->ODD_DECIMAL_PLACE,
            'TAX_RATE' => $this->TAX_RATE,
            'STATUS' => $this->STATUS,
            'REG_DATE_TIME' => $this->REG_DATE_TIME,
            'REG_USER_ID' => $this->REG_USER_ID,
            'UPD_DATE_TIME' => $this->UPD_DATE_TIME,
            'UPD_USER_ID' => $this->UPD_USER_ID,
        ]);

        $query->andFilterWhere(['like', 'SEC_CD', $this->SEC_CD])
            ->andFilterWhere(['like', 'REMARKS', $this->REMARKS]);

return $dataProvider;
}
}
