<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CORTSECBALANCE;

/**
 * BalanceColorgb represents the model behind the search form about `common\models\CORTSECBALANCE`.
 */
class BalanceColorgb extends CORTSECBALANCE
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'CUST_CD', 'ENTRY_DATE', 'SEC_TYPE_CD', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['SEC_CD', 'TRANSACTION_CD', 'REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['BEGIN_QTY', 'QTY'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
// bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CORTSECBALANCE::find();

// add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
// uncomment the following line if you do not want to return any records when validation fails
// $query->where('0=1');
            return $dataProvider;
        }

// grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'CUST_CD' => $this->CUST_CD,
            'ENTRY_DATE' => $this->ENTRY_DATE,
            'SEC_TYPE_CD' => $this->SEC_TYPE_CD,
            'BEGIN_QTY' => $this->BEGIN_QTY,
            'QTY' => $this->QTY,
            'REG_DATE_TIME' => $this->REG_DATE_TIME,
            'REG_USER_ID' => $this->REG_USER_ID,
            'UPD_DATE_TIME' => $this->UPD_DATE_TIME,
            'UPD_USER_ID' => $this->UPD_USER_ID,
        ]);

        $query->andFilterWhere(['like', 'SEC_CD', $this->SEC_CD])
            ->andFilterWhere(['like', 'TRANSACTION_CD', $this->TRANSACTION_CD]);

        return $dataProvider;
    }
}