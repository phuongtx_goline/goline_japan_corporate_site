<?php

namespace backend\models;

use common\models\Customer;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 */
class SellForm extends Model
{
    public $acc;
    public $ccq;
    public $amount;
    public $date;
    public $nav;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['acc', 'ccq','amount','date'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'acc' => 'Tài khoản',
            'ccq' => 'CCQ',
            'amount' => 'Số lượng',
            'date' => 'Ngày giao dịch',
            'nav' => 'NAV gần nhất',
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function saveData()
    {

        return true;
    }
}
