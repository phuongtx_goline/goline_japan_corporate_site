<?php
/**
 * Created by PhpStorm.
 * User: giaphv
 * Date: 05/12/2017
 * Time: 10:44 SA
 */

namespace common\colorgb;

use common\models\Contract;
use common\models\ContractNo;
use common\models\ContractType;
use common\models\CORTCUSTOMERSIGNER;
use common\models\CORTFUNDINFO;
use common\models\CORTSECBALANCE;
use common\models\CORTSECMOVEMENT;
use common\models\CORTSECTRANSACTION;
use common\models\CustomerGroup;
use common\models\FundUnit;
use common\models\FundUnitHsa;
use common\models\KPTTDEPOSITTRANSFER;
use common\models\MsgSendRequest;
use common\models\MsgTemplate;
use common\models\ORDTORDER;
use common\models\Setting;
use common\models\SystemLog;
use common\models\User;
use common\models\UserCustomerGroup;
use common\models\UserGroupPermission;
use MoneyToWords\MoneyToWordsConverter;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use mikehaertl\wkhtmlto\Pdf;
use common\colorgb\lib\DOCx;
use NcJoes\OfficeConverter\OfficeConverter;

class ColorgbHelper
{
    /*
     *
     */
    public static function price($str)
    {
        return str_replace(',', '', $str);
    }

    /*
     *
     */
    public static function slug($str)
    {
        $str = mb_strtolower($str, 'UTF-8');
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', '-', $str);
        $str = str_replace('---', '-', $str);
        $str = ltrim($str, '-');
        $str = rtrim($str, '-');
        $str = str_replace('-', ' ', $str);
        return $str;
    }

    /*
     * Set data record
     */
    public static function setDataRecord($model)
    {
        // Check is new record
        if ($model->isNewRecord) {
            if ($model->getTableSchema()->getColumn('reg_date_time')) $model->reg_date_time = date('Y-m-d H:i:s');
            if ($model->getTableSchema()->getColumn('reg_user_id')) $model->reg_user_id = Yii::$app->user->identity->getId();
            if ($model->getTableSchema()->getColumn('reg_ip_addr')) $model->reg_ip_addr = Yii::$app->getRequest()->getUserIP();
            if ($model->getTableSchema()->getColumn('upd_date_time')) $model->upd_date_time = date('Y-m-d H:i:s');
            if ($model->getTableSchema()->getColumn('upd_user_id')) $model->upd_user_id = Yii::$app->user->identity->getId();
            if ($model->getTableSchema()->getColumn('upd_ip_addr')) $model->upd_ip_addr = Yii::$app->getRequest()->getUserIP();
            if ($model->getTableSchema()->getColumn('REG_DATE_TIME')) $model->REG_DATE_TIME = date('Y-m-d H:i:s');
            if ($model->getTableSchema()->getColumn('UPD_DATE_TIME')) $model->UPD_DATE_TIME = date('Y-m-d H:i:s');
            if ($model->getTableSchema()->getColumn('REG_USER_ID')) $model->REG_USER_ID = Yii::$app->user->identity->getId();
            if ($model->getTableSchema()->getColumn('UPD_USER_ID')) $model->UPD_USER_ID = Yii::$app->user->identity->getId();

        } else {
            if ($model->getTableSchema()->getColumn('upd_date_time')) $model->upd_date_time = date('Y-m-d H:i:s');
            if ($model->getTableSchema()->getColumn('upd_user_id')) $model->upd_user_id = Yii::$app->user->identity->getId();
            if ($model->getTableSchema()->getColumn('upd_ip_addr')) $model->upd_ip_addr = Yii::$app->getRequest()->getUserIP();
            if ($model->getTableSchema()->getColumn('UPD_DATE_TIME')) $model->UPD_DATE_TIME = date('Y-m-d H:i:s');
            if ($model->getTableSchema()->getColumn('UPD_USER_ID')) $model->UPD_USER_ID = Yii::$app->user->identity->getId();

        }

        // Save date
        $model->save();
    }


    /*
     *
     */
    public static function convertNumberToWords($number)
    {
        return Yii::$app->formatter->asSpellout($number);
    }

    /*
    * Upload file
    */
    public static function uploadFile($model, $folderName, $fileName)
    {

        // Declare
        $fileUpload = UploadedFile::getInstance($model, 'file_upload');
        $savePath = Yii::getAlias('@common') . '/web/data/' . $folderName;

        // Check save folder
        if (!file_exists($savePath)) mkdir($savePath, 0777, true);

        // Check file upload
        if ($fileUpload && $fileUpload->size !== 0 && $model->save()) {
            $name = self::encrypt($model->getPrimaryKey()) . '.' . $fileUpload->extension;
            $fileUpload->saveAs($savePath . '/' . $name);
            $model->$fileName = $folderName . '/' . $name;

        }

        if (!$model->$fileName) {
            $model->$fileName = '';
        }

        // Return
        return $model->save();

    }

    /*
    * Upload file
    */
    public static function uploadFileAttrCe($model, $modelSave, $attr, $folderName, $fileName)
    {

        // Declare
        $fileUpload = UploadedFile::getInstance($model, $attr);
        $savePath = Yii::getAlias('@common') . '/web/data/' . $folderName;

        // Check save folder
        if (!file_exists($savePath)) mkdir($savePath, 0777, true);

        // Check file upload
        if ($fileUpload && $fileUpload->size !== 0 && $model->save()) {
            $name = self::encrypt($model->getPrimaryKey()) . '.' . $fileUpload->extension;
            $fileUpload->saveAs($savePath . '/' . $name);
            $modelSave->$fileName = $folderName . '/' . $name;

        }

        if (!$modelSave->$fileName) {
            $modelSave->$fileName = '';
        }

        // Return
        return $modelSave->save();

    }

    /*
     * Upload multi file
     */
    public static function uploadMultiFile($model, $folderName, $fileName)
    {

        // Declare
        $saveName = '';
        $savePath = Yii::getAlias('@common') . '/web/data/' . $folderName;

        // Check save folder
        if (!file_exists($savePath)) mkdir($savePath, 0777, true);

        // Check file upload
        if ($model->save() && $model->file_upload) {

            if (is_array($model->file_upload)) {

                $fileUploads = UploadedFile::getInstances($model, 'file_upload');
                foreach ($fileUploads as $key => $value) {

                    if ($value->size !== 0) {
                        $name = self::encrypt($model->getPrimaryKey() . $key) . '.' . $value->extension;
                        $value->saveAs($savePath . '/' . $name);
                        $saveName .= $key > 0 ? ';' : '';
                        $saveName .= $folderName . '/' . $name;
                    }

                }

                $model->$fileName = $saveName;

            }

        } else {

            $model->$fileName = '';

        }

        // Return
        return $model->save();

    }

    /*
     * Upload multi file
     */
    public static function uploadMultiFileCs($model, $attr, $folderName, $fileName)
    {

        // Declare
        $saveName = '';
        $savePath = Yii::getAlias('@common') . '/web/data/' . $folderName;

        // Check save folder
        if (!file_exists($savePath)) mkdir($savePath, 0777, true);

        // Check file upload
        if ($model->$attr) {

            if (is_array($model->$attr)) {

                $fileUploads = UploadedFile::getInstances($model, $attr);
                foreach ($fileUploads as $key => $value) {

                    if ($value->size !== 0) {

                        $name = self::encrypt($model->getPrimaryKey() . $key . time()) . '.' . $value->extension;
                        $value->saveAs($savePath . '/' . $name);
                        $cs = new CORTCUSTOMERSIGNER();
                        $cs->CUST_CD = $model->CUST_CD;
                        $cs->REF_NO = $key;
                        $cs->$fileName = $folderName . '/' . $name;
                        $cs->STATUS = 1;
                        self::setDataLog($cs);
                    }

                }

            }

        } else {

            $model->$fileName = '';

        }

        // Return
        return true;

    }

    /*
     * Encrypt
     */
    public static function encrypt($str)
    {

        return sha1(sha1(Yii::$app->name . $str));
    }

    /*
     *
     */
    public static function contractNo($cti)
    {

        $cn = ContractNo::findOne(1);
        $no = $cti == 3 ? $cn->id_hsa . '/' . date('Y') . '/HĐVT' : $cn->id_pi . '/' . date('Y') . '/BCC';

        return $no;
    }

    /*
     * Data src
     */
    public static function dataSrc($path)
    {
        // Return
        return Yii::$app->params['cdnUrl'] . '/data/' . $path . '?colorgb=' . time();
    }


    /*
     * Set data log
     */
    public static function setDataLog($model, $temp = '')
    {
        // Declare
        $controller = Yii::$app->controller;
        $id = Yii::$app->request->get('id') ? Yii::$app->request->get('id') : $model->getPrimaryKey();

        if (strpos($controller->action->id, 'create') !== false) {
            $action = 'create';
        } else if (strpos($controller->action->id, 'delete') !== false) {
            $action = 'delete';
        } else {
            $action = 'update';
        }

        // Save data
        $id = $id ? '#' . $id : '';
        $systemLog = new SystemLog();
        $systemLog->log_time = date('Y-m-d H:i:s');
        $systemLog->table_name = $model::tableName() . ' (' . $controller->id . '/' . $controller->action->id . $id . ')';
        $systemLog->action = $action;
        $systemLog->before_value = json_encode(ArrayHelper::toArray($temp), JSON_UNESCAPED_UNICODE);
        $systemLog->after_value = json_encode(ArrayHelper::toArray($model), JSON_UNESCAPED_UNICODE);
        self::setDataRecord($model);
        self::setDataRecord($systemLog);
    }

    /*
     * Get customer group ids
     */
    public static function getCustomerGroupIds($userId = 0)
    {
        // Declare
        $identity = Yii::$app->user->identity;
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $user = User::findOne($userId);
        $userGroupId = !empty($user) ? $user->group_id : Yii::$app->user->identity->group_id;
        $userCustomerGroup = UserCustomerGroup::findAll(['user_group_id' => $userGroupId, 'status' => $recordStatus['active']]);

        if ($identity->is_admin == 1) {
            $customerGroupIds = ArrayHelper::map(CustomerGroup::find()->all(), 'id', 'id');
            $customerGroupIds[0] = 0;

        } else {
            $customerGroupIds = ArrayHelper::map($userCustomerGroup, 'customer_group_id', 'customer_group_id');
            $customerGroupIds[0] = 0;

        }/* else {

            $customerGroupIds = ArrayHelper::map($userCustomerGroup, 'customer_group_id', 'customer_group_id');
        }*/

        // Return
        return $customerGroupIds;
    }

    /*
     * Check permission role
     */
    public static function checkPermission($role = '')
    {
        // Declare
        $controller = Yii::$app->controller;
        $identity = Yii::$app->user->identity;
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');

        // Check is admin
        if ($identity->is_admin == 1) {

            // Return
            return true;

        } else {

            // Check group id
            if ($groupId = $identity->group_id) {

                // Declare
                $userGroupPermission = UserGroupPermission::findAll(['user_group_id' => $groupId, 'status' => $recordStatus['active']]);

                // Push user group permission
                foreach ($userGroupPermission as $value) {
                    $roleCdIndex = explode('_', $value->role_cd)[0] . '_INDEX';
                    $roleCdView = explode('_', $value->role_cd)[0] . '_VIEW';
                    $array[$roleCdIndex] = $roleCdIndex;
                    $array[$roleCdView] = $roleCdView;
                    $array[$value->role_cd] = $value->role_cd;
                }

                // Role cd
                $controllerName = str_replace('/', '', $controller->id);
                $controllerName = str_replace('-', '', $controllerName);
                $roleCd = $role ? $role : $controllerName . '_' . str_replace('-', '', $controller->action->id);

                // Return
                return !empty($array[strtoupper($roleCd)]) ? true : false;

            } else {

                // Return
                return false;
            }

        }
    }

    /*
     * Send mail
     */
    public static function sendMail($title, $view, $email, $data, $type = null)
    {
        // Declare
        $msgStatus = ArrayHelper::map(Yii::$app->params['msgStatus'], 'key', 'value');
        $sendType = ArrayHelper::map(Yii::$app->params['sendType'], 'key', 'value');
        $msgType = ArrayHelper::map(Yii::$app->params['msgType'], 'key', 'value');

        // Send mail
        $mail = Yii::$app->mailer
            ->compose(
                ['html' => $view],
                ['data' => $data]
            )
            ->setFrom([Yii::$app->params['mailerEmail'] => Yii::$app->name])
            ->setTo($email)
            ->setSubject($title)
            ->send();

        // Check send mail
        $model = new MsgSendRequest();
        $model->date = date('Ymd');
        $model->msg_type = $type ? $type : $msgType['common'];
        $model->send_type = $sendType['email'];
        $model->send_to = $email;
        $model->subject = $title;
        $model->from_address = Yii::$app->user->identity->id;
        $model->status = $mail ? $msgStatus['sent'] : $msgStatus['failed'];
        self::setDataLog($model);
        return true;
    }

    /*
     * Write tsv
     */

    public static function writeTabbedFile($filePath, $fileName, $array, $content = '', $saveKeys = false)
    {
        $filePath = Yii::getAlias('@common') . '/web/data/' . $filePath;
        if (!file_exists($filePath)) mkdir($filePath, 0777, true);

        reset($array);
        $content .= "\n";
        while (list($key, $val) = each($array)) {

            // Replace tabs in keys and values to [space]
            $key = str_replace("\t", " ", $key);
            $val = str_replace("\t", " ", $val);

            if ($saveKeys) {
                $content .= $key . "\t";
            }

            // Create line:
            $content .= (is_array($val)) ? implode("\t", $val) : $val;
            $content .= "\n";
        }

        if (file_exists($filePath . $fileName) && !is_writeable($filePath . $fileName)) {
            return false;
        }
        if ($fp = fopen($filePath . $fileName, 'w+')) {
            fwrite($fp, $content);
            fclose($fp);
        } else {
            return false;
        }
        return true;
    }

    /*
     * Set data report
     */
    public static function setDataReport()
    {
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $fundUnit = ColorgbHelper::fundUnit();

        $beginDate = self::date($fundUnit['first']->date, 'Y-m-d');
        $endDate = date('Y-m-d');

        while (strtotime($beginDate) <= strtotime($endDate)) {

            $date = str_replace('-', '', $beginDate);
            $data = FundUnit::find()
                ->select('date,fund_unit_price')
                ->where(['status' => $recordStatus['active']])
                ->andWhere(['>=', 'date', $date])
                ->orderBy(['date' => SORT_ASC])
                ->asArray()
                ->all();

            if (empty($data)) {
                $data = FundUnit::find()
                    ->select('date,fund_unit_price')
                    ->where(['status' => $recordStatus['active']])
                    ->andWhere(['date' => $fundUnit['last']->date])
                    ->orderBy(['date' => SORT_ASC])
                    ->asArray()
                    ->all();
            }

            // Gen
            self::writeTabbedFile('investment/report/', $date . '.tsv', $data, "date\tclose");
            $beginDate = date("Y-m-d", strtotime("+1 day", strtotime($beginDate)));
        }


    }

    /*
     * Set data report
     */
    public static function setDataReportHsa()
    {
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $fundUnit = ColorgbHelper::fundUnitHsa();

        $beginDate = self::date($fundUnit['first']->date, 'Y-m-d');
        $endDate = date('Y-m-d');

        while (strtotime($beginDate) <= strtotime($endDate)) {

            $date = str_replace('-', '', $beginDate);
            $data = FundUnitHsa::find()
                ->select('date,fund_unit_price')
                ->where(['status' => $recordStatus['active']])
                ->andWhere(['>=', 'date', $date])
                ->orderBy(['date' => SORT_ASC])
                ->asArray()
                ->all();

            if (empty($data)) {
                $data = FundUnitHsa::find()
                    ->select('date,fund_unit_price')
                    ->where(['status' => $recordStatus['active']])
                    ->andWhere(['date' => $fundUnit['last']->date])
                    ->orderBy(['date' => SORT_ASC])
                    ->asArray()
                    ->all();
            }

            // Gen
            self::writeTabbedFile('investment/report-hsa/', $date . '.tsv', $data, "date\tclose");
            $beginDate = date("Y-m-d", strtotime("+1 day", strtotime($beginDate)));
        }


    }

    /*
     * Date
     */
    public static function date($date, $newFormat = 'd/m/Y', $oldFomat = 'Ymd')
    {
        return $date && $date > 0 ? \DateTime::createFromFormat($oldFomat, $date)->format($newFormat) : 'N/A';
    }

    /*
     *
     */
    public static function attachmentName($attachment)
    {
        $ex = explode('.', $attachment);

        if (preg_match('/contract/', $attachment)) {
            $attachment = 'HD-' . date('d-m-y');
        }

        if (preg_match('/weekly-report/', $attachment)) {
            $attachment = 'BC-' . date('d-m-y');
        }

        if (preg_match('/contract-settle/', $attachment)) {
            $attachment = 'HDTT-' . date('d-m-y');
        }

        if (preg_match('/msg-template/', $attachment)) {
            $attachment = 'DOC-' . date('d-m-y');
        }

        if (preg_match('/investment/', $attachment)) {
            $attachment = 'HQDT-' . date('d-m-y');
        }

        return $attachment . '.' . $ex[1];

    }

    /*
     *
     */
    public static function convertApi($format, $file)
    {

        $converter = new OfficeConverter($file);
        $converter->convertTo('output-file' . $format);

        //$converter = new OfficeConverter('test-file.docx', 'path-to-outdir');
    }

    /*
     *Fund unit
     */
    public static function fundUnit()
    {
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $fundUnit = FundUnit::find()->where(['!=', 'status', $recordStatus['deleted']]);
        $array['first'] = $fundUnit->orderBy(['date' => SORT_ASC])->one();
        $array['last'] = $fundUnit->where(['<', 'date', date('Ymd')])->orderBy(['date' => SORT_DESC])->one();
        return $array;
    }

    /*
     *
     */
    public static function fundUnitHsa()
    {
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $fundUnit = FundUnitHsa::find()->where(['!=', 'status', $recordStatus['deleted']]);
        $array['first'] = $fundUnit->orderBy(['date' => SORT_ASC])->one();
        $array['last'] = $fundUnit->where(['<', 'date', date('Ymd')])->orderBy(['date' => SORT_DESC])->one();
        return $array;
    }

    /*
     *Msg send request
     */
    public static function msgSendRequest($emailArray, $templateArray, $attachments = '', $contentData = array(), $model = null)
    {

        // Declare
        $type = $model->contract_type_id == 3 ? 'HSA' : 'PI';
        $fundUnit = ColorgbHelper::fundUnit();
        $fundUnitHsa = ColorgbHelper::fundUnitHsa();
        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
        $msgType = ArrayHelper::map(Yii::$app->params['msgType'], 'key', 'value');
        $msgTemplate = MsgTemplate::findOne(['type' => $type, 'send_type' => $templateArray['sendType'], 'msg_type' => $templateArray['msgType']]);
        if (!$msgTemplate) return false;
        $attachments .= ';' . $msgTemplate->file_template_url;

        // Check msg type
        if ($templateArray['msgType'] == $msgType['weekly_report']) {
            $fromArray['address'] = Setting::findOne('COMPANY_EMAIL')->param_value;
            $fromArray['name'] = Setting::findOne('COMPANY_EMAIL_NAME')->param_value;
        } else {
            $user = User::findOne(Yii::$app->user->identity->getId());
            $fromArray['address'] = $user->email_addr;
            $fromArray['name'] = $user->name;
        }

        // Insert data
        foreach ($emailArray as $id => $email) {

            // Msg template
            $content = $msgTemplate->content;
            $subject = $msgTemplate->title;

            // Check msg type
            if ($templateArray['msgType'] == $msgType['weekly_report']) {

                $fromArray['address'] = 'baocao@pif.vn';

                $msgTemplate = MsgTemplate::findOne(['type' => 'PI', 'send_type' => $templateArray['sendType'], 'msg_type' => $templateArray['msgType']]);
                $attachments = 'weekly-report/' . ColorgbHelper::encrypt($id) . '.pdf;' . $msgTemplate->file_template_url . ';' . $fundUnit['last']->file_path;
                $fundUnitFirst = Contract::find()->where(['status' => $contractStatus['active'], 'customer_id' => $id, 'contract_type_id' => [1, 2]])->orderBy(['begin_date' => SORT_ASC])->one();
                $contentData['investment_from_date'] = ColorgbHelper::date($fundUnitFirst->begin_date);

                $contractHsa = Contract::findAll(['customer_id' => $id, 'status' => $contractStatus['active'], 'contract_type_id' => [3]]);
                if ($contractHsa) {

                    $msgTemplateHsa = MsgTemplate::findOne(['type' => 'HSA', 'send_type' => $templateArray['sendType'], 'msg_type' => $templateArray['msgType']]);
                    $subjectHsa = $msgTemplateHsa->title;
                    $contentHsa = $msgTemplateHsa->content;
                    $fundUnitFirstHsa = Contract::find()->where(['status' => $contractStatus['active'], 'customer_id' => $id, 'contract_type_id' => [3]])->orderBy(['begin_date' => SORT_ASC])->one();
                    $contentDataHsa['investment_from_date'] = ColorgbHelper::date($fundUnitFirstHsa->begin_date);

                    // Content data
                    foreach ($contentDataHsa as $key => $value) {
                        $contentHsa = str_replace('${' . $key . '}', $value, $contentHsa);
                        $subjectHsa = str_replace('${' . $key . '}', $value, $subjectHsa);
                    }

                    foreach ($contentData as $key => $value) {
                        $contentHsa = str_replace('${' . $key . '}', $value, $contentHsa);
                        $subjectHsa = str_replace('${' . $key . '}', $value, $subjectHsa);
                    }

                    // Save data
                    $model = new MsgSendRequest();
                    $model->date = date('Ymd');
                    $model->msg_type = $templateArray['msgType'];
                    $model->send_type = $templateArray['sendType'];
                    $model->send_to = $email;
                    $model->from_address = $fromArray['address'];
                    $model->from_name = $fromArray['name'];
                    $model->subject = $subjectHsa;
                    $model->content = $contentHsa;
                    $model->attachments = 'weekly-report-hsa/' . ColorgbHelper::encrypt($id) . '.pdf;' . $msgTemplate->file_template_url . ';' . $fundUnit['last']->file_path;
                    $model->status = $templateArray['msgStatus'];
                    $model->failed_count = 0;
                    self::setDataLog($model);
                }


            }

            // Content data
            foreach ($contentData as $key => $value) {
                $content = str_replace('${' . $key . '}', $value, $content);
                $subject = str_replace('${' . $key . '}', $value, $subject);
            }

            // Save data
            $model = new MsgSendRequest();
            $model->date = date('Ymd');
            $model->msg_type = $templateArray['msgType'];
            $model->send_type = $templateArray['sendType'];
            $model->send_to = $email;
            $model->from_address = $fromArray['address'];
            $model->from_name = $fromArray['name'];
            $model->subject = $subject;
            $model->content = $content;
            $model->attachments = $attachments;
            $model->status = $templateArray['msgStatus'];
            $model->failed_count = 0;
            self::setDataLog($model);

        }

        // Return
        return true;
    }

    /*
     * Contract expire
     */
    public static function contractExpire($endDateEst)
    {
        $expireAlert = intval(Setting::findOne(['CONTRACT_EXPIRE_ALERT_DAY'])->param_value);
        $expire = intval(date('Ymd') + $expireAlert);

        if ($endDateEst && $endDateEst < date('Ymd')) {

            return '<label class="small label bg-danger">Đã hết hạn</label>';

        } else if ($endDateEst && $endDateEst <= $expire) {

            return '<label class="small label bg-warning">Sắp hết hạn</label>';

        } else {
            return null;
        }
    }

    /*
     *PHP PDF
     */
    public static function phpPdf($id, $templatePath, $savePath)
    {
        // Declare
        $filePath = Yii::getAlias('@common') . '/web/data/' . $templatePath;
        if (!file_exists($filePath)) mkdir($filePath, 0777, true);

        // Save data
        $pdf = new Pdf();
        $pdf->addPage(Yii::$app->params['backendUrl'] . '/tool/export?id=' . $id . '&layout=sub');
        $pdf->saveAs(Yii::getAlias('@common') . '/web/data/' . $savePath);

        // Return
        return true;
    }

    /*
     *
     */
    public static function phpPdfHsa($id, $templatePath, $savePath)
    {
        // Declare
        $filePath = Yii::getAlias('@common') . '/web/data/' . $templatePath;
        if (!file_exists($filePath)) mkdir($filePath, 0777, true);

        // Save data
        $pdf = new Pdf();
        $pdf->addPage(Yii::$app->params['backendUrl'] . '/tool/export-hsa?id=' . $id . '&layout=sub');
        $pdf->saveAs(Yii::getAlias('@common') . '/web/data/' . $savePath);

        // Return
        return true;
    }


    /*
     *HTML to PDF
     */
    public static function htmlToPdf($url, $templatePath, $savePath)
    {
        // Declare
        $filePath = Yii::getAlias('@common') . '/web/data/' . $templatePath;
        if (!file_exists($filePath)) mkdir($filePath, 0777, true);

        // Save data
        $pdf = new Pdf();
        $pdf->addPage($url);
        $pdf->saveAs(Yii::getAlias('@common') . '/web/data/' . $templatePath . '/' . $savePath . '.pdf');

        // Return
        return true;
    }

    /*
     *PHP Word
     */
    public static function phpWord($data, $templatePath, $savePath)
    {
        // Declare
        $document = new DOCx(Yii::getAlias('@common') . '/web/data/' . $templatePath);

        // Save data
        $document->setValues($data);
        $document->cleanTagVars();
        $document->save(Yii::getAlias('@common') . '/web/data/' . $savePath);
        $document->close();

        // Return
        return true;
    }

    /*
     *
     */

    public static function docxToPdf($folder, $name)
    {


        /* \PhpOffice\PhpWord\Settings::setPdfRendererPath(dirname(dirname(__DIR__)) . '/vendor/mpdf/mpdf');
         \PhpOffice\PhpWord\Settings::setPdfRendererName('MPDF');
         //\PhpOffice\PhpWord\Settings::getDefaultFontName();

         $phpWord = \PhpOffice\PhpWord\IOFactory::load(Yii::getAlias('@common') . '/web/data/' . $folder . '/' . $name . '.docx');

         //Save it
         $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'PDF');
         $xmlWriter->save(Yii::getAlias('@common') . '/web/data/' . $folder . '/' . $name . '.pdf');*/

        shell_exec('sudo /opt/libreoffice6.0/program/swriter --headless -convert-to pdf --outdir /colorgb/pif/common/web/data/' . $folder . '/ /colorgb/pif/common/web/data/' . $folder . '/' . $name . '.docx');

    }

    /*
     *
     */

    public static function docxToHtml($folder, $name)
    {

        $PHPWord = new \PhpOffice\PhpWord\PhpWord();

        // File name & directory
        $tempFile = Yii::getAlias('@common') . '/web/data/' . $folder . '/' . $name;

        // Load template
        //$template = $PHPWord->loadTemplate($tempFile . '.docx');

        // Save template has new Word file
        //$template->saveAs($tempFile . '.docx');

        // Load generated Word
        $PHPWordLoad = \PhpOffice\PhpWord\IOFactory::load($tempFile . '.docx');

        // Save generated word in HTML
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($PHPWordLoad, 'HTML');
        $objWriter->save($tempFile . '.html');

    }

    /*
     *Money to words converter
     */
    public static function moneyToWords($moneyDigit)
    {

        $money = new MoneyToWordsConverter($moneyDigit, '', 'vi');
        $str = strtolower($money->Convert());
        return $str;
    }

    /*
     *
     */
    public static function numberFormat($number, $d = 0)
    {
        return number_format($number, $d, ",", ".");
    }

    /*
     * Contract doc
     */
    public static function contractDoc($id, $colorgbForm = array())
    {

        // Declare
        $periodType = ArrayHelper::map(Yii::$app->params['periodType'], 'value', 'txt');
        $representative = ArrayHelper::map(Yii::$app->params['representative'], 'txt', 'value');

        $representativeNote = ArrayHelper::map(Yii::$app->params['representative'], 'txt', 'html');

        // Get customer & contract active
        $model = Contract::findOne($id);

        $fundUnit = $model->contract_type_id == 3 ? self::fundUnitHsa() : self::fundUnit();


        // Customer data
        $data['CUSTOMERNAME'] = mb_strtoupper($model->customer->name);
        $data['customername'] = $model->customer->name;
        $data['customer_id_number'] = $model->customer->id_number;
        $data['customer_id_place'] = $model->customer->id_place;
        $data['customer_id_issue_date'] = ColorgbHelper::date($model->customer->id_issue_date);
        $data['customer_mobile'] = $model->customer->mobile;
        $data['customer_tax_code'] = $model->customer->tax_code;
        $data['customer_email_addr'] = $model->customer->email_addr;
        $data['customer_bank_acco_name'] = $model->customer->bank_acco_name;
        $data['customer_bank_acco_no'] = $model->customer->bank_acco_no;
        $data['customer_bank_name'] = $model->customer->bank_name;
        $data['customer_bank_branch_name'] = $model->customer->bank_branch_name;
        $data['customer_address'] = $model->customer->address1;

        // Company data
        $data['COMPANYNAME'] = mb_strtoupper(Setting::findOne('COMPANY_NAME')->param_value);
        $data['companyname'] = Setting::findOne('COMPANY_NAME')->param_value;
        $data['company_erc'] = Setting::findOne('COMPANY_ERC')->param_value;
        $data['company_tax_code'] = Setting::findOne('COMPANY_TAX_CODE')->param_value;
        $data['company_tax_date'] = Setting::findOne('COMPANY_TAX_DATE')->param_value;
        $data['company_address'] = Setting::findOne('COMPANY_ADDRESS')->param_value;
        $data['company_tel'] = Setting::findOne('COMPANY_TEL')->param_value;
        $data['company_representative'] = $model->representative ? $model->representative : Setting::findOne('COMPANY_REPRESENTATIVE')->param_value;
        $data['company_representative_position'] = $representative[$data['company_representative']] ? $representative[$data['company_representative']] : 'Tổng giám đốc';
        $data['CRPN'] = $representativeNote[$data['company_representative']] ? $representativeNote[$data['company_representative']] : ' ';
        $data['company_bank_acco'] = Setting::findOne('COMPANY_BANK_ACCO')->param_value;
        $data['company_bank_name'] = Setting::findOne('COMPANY_BANK_NAME')->param_value;

        // Hsa
        $data['hsa_bank_acco_name'] = ArrayHelper::map(Yii::$app->params['bank'], 'key', 'acc')[$model->bank_id];
        $data['hsa_bank_acco_no'] = ArrayHelper::map(Yii::$app->params['bank'], 'key', 'no')[$model->bank_id];
        $data['hsa_bank_name'] = ArrayHelper::map(Yii::$app->params['bank'], 'key', 'name')[$model->bank_id];
        $data['has_transfer_note'] = $model->transfer_note;

        // Contract data
        $data['contract_no'] = $model->contract_no;
        $data['contract_begin_date'] = ColorgbHelper::date($model->begin_date);
        $data['CBDHQ'] = ColorgbHelper::date($model->begin_date - 1);
        $data['contract_begin_date_day'] = ColorgbHelper::date($model->begin_date, 'd');
        $data['contract_begin_date_month'] = ColorgbHelper::date($model->begin_date, 'm');
        $data['contract_begin_date_year'] = ColorgbHelper::date($model->begin_date, 'Y');
        $data['contract_end_date_est_day'] = ColorgbHelper::date($model->end_date_est, 'd');
        $data['contract_end_date_est_month'] = ColorgbHelper::date($model->end_date_est, 'm');
        $data['contract_end_date_est_year'] = ColorgbHelper::date($model->end_date_est, 'Y');
        $data['contract_fund_net_asset'] = self::numberFormat($model->fund_net_asset);
        $data['contract_fund_unit_qty'] = self::numberFormat($model->fund_unit_qty);
        $data['contract_fund_unit_price'] = self::numberFormat($model->fund_unit_price);
        $data['contract_invest_value'] = self::numberFormat($model->invest_value);
        $data['contract_invest_value_char'] = self::convertNumberToWords($model->invest_value);
        $data['contract_invest_unit_qty'] = self::numberFormat($model->invest_unit_qty);
        $data['contract_invest_unit_qty_char'] = self::convertNumberToWords($model->invest_unit_qty);
        $data['contract_period_val'] = self::numberFormat($model->period_val);
        $data['CPV'] = self::numberFormat($model->period_val);
        $data['contract_period_type'] = $periodType[$model->period_type];

        $data['fund_net_asset'] = self::numberFormat($fundUnit['last']->fund_net_asset);
        $data['fund_unit_qty'] = self::numberFormat($fundUnit['last']->fund_unit_qty);
        $data['fund_unit_price'] = self::numberFormat($fundUnit['last']->fund_unit_price);

        // Template path
        $templatePath = ContractType::findOne($model->contract_type_id)->file_template_url;

        if (!empty($colorgbForm)) {

            $tsdtkd = round($colorgbForm['attr_5'] / $colorgbForm['attr_1'] * 100, 2);
            $tsdtkd = $tsdtkd ? ' ' . $tsdtkd . ' ' : 'N/A';
            $data['TSDTKD'] = $tsdtkd;

            foreach ($colorgbForm as $key => $value) {
                if ($key != 'attr_2' && $key != 'attr_3') $data[$key] = is_numeric($value) ? '' . self::numberFormat($value) : '' . $value;
                if ($key == 'end_date_real') {
                    $data['contract_end_date_real'] = ColorgbHelper::date($value);
                    $data['CEDRHQ'] = ColorgbHelper::date($value - 1);
                }

            }

            // Check rate
            if ($colorgbForm['attr_5'] / $colorgbForm['attr_1'] > $colorgbForm['attr_2'] / 100) {

                if ($model->contract_type_id == 2) {
                    $cma = $cmb = 50;
                }

                if ($model->contract_type_id == 1) {
                    $cma = 80;
                    $cmb = 20;
                }

                $tta = $colorgbForm['attr_3'] + ($colorgbForm['attr_5'] - $colorgbForm['attr_3']) * $cma / 100;
                $ttb = ($colorgbForm['attr_5'] - $colorgbForm['attr_3']) * $cmb / 100;

                $data['total_a'] = self::numberFormat($colorgbForm['attr_3']) . ' +  (' . self::numberFormat($colorgbForm['attr_5']) . ' - ' . self::numberFormat($colorgbForm['attr_3']) . ')*' . $cma . '% = ' . self::numberFormat($tta) . ' đồng';
                $data['total_b'] = '(' . self::numberFormat($colorgbForm['attr_5']) . ' - ' . self::numberFormat($colorgbForm['attr_3']) . ')*' . $cmb . '% = ' . self::numberFormat($ttb) . ' đồng';

            } else {
                $data['total_a'] = self::numberFormat($colorgbForm['attr_5']) . ' đồng';
                $data['total_b'] = '0 đồng';
            }

            if ($colorgbForm['attr_12'] < 0) {
                $data['attr_11_note'] = '(do doanh thu kinh doanh thực tế nhỏ hơn 0)';
            } else {
                $data['attr_11_note'] = '(' . ($model->tax_rate * 100) . ' % của ' . self::numberFormat($colorgbForm['attr_10']) . ' đồng)';
            }
            $data['attr_2'] = self::numberFormat($colorgbForm['attr_2'], 2);
            $data['attr_3'] = self::numberFormat($colorgbForm['attr_3']);

            $templatePath = $model->contract_type_id == 3 ? 'contract-settle/template-2.docx' : 'contract-settle/template-1.docx';

            self::phpWord($data, $templatePath, 'contract-settle/' . self::encrypt($model->id) . '.docx');
            self::docxToPdf('contract-settle', self::encrypt($model->id));

        } else {

            self::phpWord($data, $templatePath, 'contract/' . self::encrypt($model->id) . '.docx');
            self::docxToPdf('contract', self::encrypt($model->id));
        }

        // Return
        return true;

    }

    public static function MakePropertyValue($name, $value, $osm)
    {
        $oStruct = $osm->Bridge_GetStruct("com.sun.star.beans.PropertyValue");
        $oStruct->Name = $name;
        $oStruct->Value = $value;
        return $oStruct;
    }

    public static function fundUnitQty($date = null)
    {
        // Declare

        /*$recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $fundUnitHq = FundUnit::find()->where(['!=', 'status', $recordStatus['deleted']])->andWhere(['<', 'date', $date])->orderBy(['date' => SORT_DESC])->one();

        $selectDate = $date ? $date : date('Ymd') - 1;
        $fundUnitHq = FundUnit::find()->where(['!=', 'status', $recordStatus['deleted']])->andWhere(['<', 'date', $selectDate])->orderBy(['date' => SORT_DESC])->one();
        $fundUnitHk = FundUnit::find()->where(['!=', 'status', $recordStatus['deleted']])->andWhere(['<', 'date', $fundUnitHq->date])->orderBy(['date' => SORT_DESC])->one();

        $fundUnitQtyActivated = Contract::find()->where(['>=', 'begin_date', $fundUnitHq->date])->andWhere(['<', 'begin_date', $selectDate])->andWhere(['>=', 'status', $contractStatus['active']])->andWhere(['<', 'status', $contractStatus['settled']])->sum('invest_unit_qty');

        $fundUnitQtySettledHq = Contract::find()->where(['like', 'settle_date_time', ColorgbHelper::date($fundUnitHq->date, 'Y-m-d')])->andWhere(['status' => $contractStatus['settled']])->sum('invest_unit_qty');
        $fundUnitQtySettledHn = Contract::find()->where(['like', 'settle_date_time', $selectDate])->andWhere(['status' => $contractStatus['settled']])->sum('invest_unit_qty');
        $fundUnitQtySettled = $fundUnitQtySettledHq + $fundUnitQtySettledHn;

        return $fundUnitHk->fund_unit_qty + $fundUnitQtyActivated - $fundUnitQtySettledHq;*/

        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
        $investUnitQty = Contract::find()
            ->select('invest_unit_qty')
            ->where(['contract_type_id' => [1, 2], 'status' => [$contractStatus['active'], $contractStatus['sent'], $contractStatus['settle']]])
            ->andWhere(['<', 'begin_date', date('Ymd')])
            ->sum('invest_unit_qty');
        return $investUnitQty;
    }

    public static function fundUnitQtyHsa($date = null)
    {
        // Declare

        /*$recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        $fundUnitHq = FundUnit::find()->where(['!=', 'status', $recordStatus['deleted']])->andWhere(['<', 'date', $date])->orderBy(['date' => SORT_DESC])->one();

        $selectDate = $date ? $date : date('Ymd') - 1;
        $fundUnitHq = FundUnit::find()->where(['!=', 'status', $recordStatus['deleted']])->andWhere(['<', 'date', $selectDate])->orderBy(['date' => SORT_DESC])->one();
        $fundUnitHk = FundUnit::find()->where(['!=', 'status', $recordStatus['deleted']])->andWhere(['<', 'date', $fundUnitHq->date])->orderBy(['date' => SORT_DESC])->one();

        $fundUnitQtyActivated = Contract::find()->where(['>=', 'begin_date', $fundUnitHq->date])->andWhere(['<', 'begin_date', $selectDate])->andWhere(['>=', 'status', $contractStatus['active']])->andWhere(['<', 'status', $contractStatus['settled']])->sum('invest_unit_qty');

        $fundUnitQtySettledHq = Contract::find()->where(['like', 'settle_date_time', ColorgbHelper::date($fundUnitHq->date, 'Y-m-d')])->andWhere(['status' => $contractStatus['settled']])->sum('invest_unit_qty');
        $fundUnitQtySettledHn = Contract::find()->where(['like', 'settle_date_time', $selectDate])->andWhere(['status' => $contractStatus['settled']])->sum('invest_unit_qty');
        $fundUnitQtySettled = $fundUnitQtySettledHq + $fundUnitQtySettledHn;

        return $fundUnitHk->fund_unit_qty + $fundUnitQtyActivated - $fundUnitQtySettledHq;*/

        $contractStatus = ArrayHelper::map(Yii::$app->params['contractStatus'], 'key', 'value');
        $investUnitQty = Contract::find()
            ->select('invest_unit_qty')
            ->where(['contract_type_id' => [3], 'status' => [$contractStatus['active'], $contractStatus['sent'], $contractStatus['settle']]])
            ->andWhere(['<', 'begin_date', date('Ymd')])
            ->sum('invest_unit_qty');
        return $investUnitQty;
    }

    public static function word2pdf($url, $folder, $name)
    {
        //echo $output_url;
        //Invoke the OpenOffice.org service manager
        $osm = new \COM("com.sun.star.ServiceManager") or die ("Please be sure that OpenOffice.org is installed.\n");
        //Set the application to remain hidden to avoid flashing the document onscreen
        $args = array(self::makePropertyValue("Hidden", true, $osm));
        //Launch the desktop
        $oDesktop = $osm->createInstance("com.sun.star.frame.Desktop");
        //Load the .doc file, and pass in the "Hidden" property from above
        $oWriterDoc = $oDesktop->loadComponentFromURL($url, "_blank", 0, $args);
        //Set up the arguments for the PDF output
        $export_args = array(self::makePropertyValue("FilterName", "writer_pdf_Export", $osm));
        //print_r($export_args);
        //Write out the PDF
        $oWriterDoc->storeToURL(Yii::getAlias('@common') . '/web/data/' . $folder . '/' . $name . '.pdf', $export_args);
        $oWriterDoc->close(true);
    }

    public static function getCustomerCcq($cd, $html = false, $scd = '',$json = false)
    {
        $CUST_CD = $cd;
        $b = CORTSECBALANCE::find()->where(['CUST_CD' => $CUST_CD, 'SEC_TYPE_CD' => 1])->groupBy('SEC_CD')->all();
        $h = '';
        if ($b) {
            foreach ($b as $v):
                $t1 = CORTSECBALANCE::find()->where(['CUST_CD' => $CUST_CD, 'SEC_TYPE_CD' => 1, 'SEC_CD' => $v->SEC_CD])->sum('QTY');
                $t2 = ORDTORDER::find()->where(['CUST_CD' => $CUST_CD, 'SEC_CD' => $v->SEC_CD])->andWhere(['in', 'STATUS', [1, 2]])->sum('ORD_QTY');
                $t3 = CORTSECTRANSACTION::find()->where(['CUST_CD' => $CUST_CD, 'SEC_TYPE_CD' => 1, 'SEC_CD' => $v->SEC_CD, 'STATUS' => 0])->andWhere(['in', 'ACTION', [1, 3]])->sum('QUANTITY');
                $t4 = KPTTDEPOSITTRANSFER::find()->joinWith('kPTTDEPOSITTRANSFERDETAIL')->where(['CUST_NO_FROM' => $CUST_CD, 'SEC_TYPE_CD' => 1, 'SEC_CD' => $v->SEC_CD])->andWhere(['in', 'STATUS', [0, 1]])->sum('TRANSFER_QTY');
                $t5 = CORTSECMOVEMENT::find()->where(['CUST_CD' => $CUST_CD, 'SEC_TYPE_CD' => 1, 'SEC_CD' => $v->SEC_CD, 'TRADE_TYPE' => 1, 'STATUS' => 0])->sum('QUANTITY');
                $t6 = $t1 - $t2 - $t3 - $t4 - $t5;
                if ($html) {
                    $ci = CORTFUNDINFO::findOne(['SEC_CD' =>$v->SEC_CD]);
                    $h .= '<tr data-ccq="' . $v->SEC_CD . '">
                            <td>' . $v->SEC_CD . '</td>
                            <td data-val="' . $t1 . '" >' . number_format($t1) . '</td>
                            <td data-val="' . $t6 . '" >' . number_format($t6) . '</td>
                            <td data-val="' . $ci->TRANS_DATE_TYPE . '" >' . ($ci->TRANS_DATE_TYPE == 2 ? $ci->TRANS_DATE.' hàng tháng' : $ci->WEEK_DAYS.' hàng tuần' ). '</td>
                        </tr>';
                } else {
                    $a[$v->SEC_CD] = $t6;
                }

            endforeach;

            if ($scd) {
                return $a[$scd];
            }

            if($json){
                return json_encode($a);
            }

            if($html){
                return $h;
            }

            return $a;

        }

        return null;


    }
}