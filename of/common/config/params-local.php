<?php
return [
    'mailerEmail' => 'mailer@colorgb.com',
    'adminEmail' => 'admin@colorgb.com',
    'backupEmail' => 'bolt@tiaset.net',
    'supportEmail' => 'support@colorgb.com',
    'userPasswordResetTokenExpire' => 3600,
    'homeUrl' => 'http://xenewstar.com/of/backend/web',
    'cdnUrl' => 'http://xenewstar.com/of/common/web',
    'backendUrl' => 'http://xenewstar.com/of/backend/web',
    'frontendUrl' => 'http://xenewstar.com/of/frontend/web'
];
