<?php

use yii\helpers\Html;
use common\models\User;
use common\models\Setting;

$user = User::findOne(['email_addr' => $data->from_address]);
/* @var $this yii\web\View */
?>
<div class="common">
    <?= $data->content ?>
    <hr style="border: 1px solid #eee;margin: 10px 0;">
    <?php if ($user): ?>
        <?= $user->email_signature ?>
    <?php else:; ?>
        <?= Setting::findOne(['COMPANY_EMAIL_SIGNATURE'])->param_value ?>
    <?php endif; ?>

</div>
