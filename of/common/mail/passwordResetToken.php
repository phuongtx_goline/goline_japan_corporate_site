<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $data common\models\User */

if ($data::tableName() == 'customer') {
    $resetLink = Yii::$app->params['frontendUrl'];
} else {
    $resetLink = Yii::$app->params['backendUrl'];
}
$resetLink = $resetLink . '/site/reset-password?token=' . $data->password_reset_token;
?>
<div class="password-reset">
    <p>Xin chào <?= Html::encode($data->name) ?>,</p>

    <p>Làm theo hướng dẫn tại liên kết dưới đây để đặt lại mật khẩu của bạn:</p>

    <p>
        <a href="<?= $resetLink ?>" style="height: 45px; display: inline-block; color: #fff; background: #2196F3; border: solid 1px #2196F3; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #3498db;">Thiết lập lại mật khẩu</a>
    </p>
</div>
