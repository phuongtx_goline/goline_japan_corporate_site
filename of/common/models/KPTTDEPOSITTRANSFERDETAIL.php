<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "KPTTDEPOSIT_TRANSFER_DETAIL".
 *
 * @property integer $ID
 * @property integer $PARENT_ID
 * @property string $SEC_CD
 * @property integer $SEC_TYPE_CD
 * @property string $TRANSFER_QTY
 * @property string $PRICE
 * @property string $VALUE
 * @property string $REG_DATE_TIME
 * @property integer $REG_USER_ID
 * @property string $UPD_DATE_TIME
 * @property integer $UPD_USER_ID
 */
class kPTTDEPOSITTRANSFERDETAIL extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'KPTTDEPOSIT_TRANSFER_DETAIL';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'PARENT_ID', 'SEC_CD', 'SEC_TYPE_CD', 'TRANSFER_QTY', 'PRICE', 'VALUE'], 'required'],
            [['ID', 'PARENT_ID', 'SEC_TYPE_CD', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['TRANSFER_QTY', 'PRICE', 'VALUE'], 'number'],
            [['REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['SEC_CD'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'PARENT_ID' => 'KPTTDEPOSIT_TRANSFER.ID',
            'SEC_CD' => 'Mã CCQ',
            'SEC_TYPE_CD' => 'Loại chứng khoán',
            'TRANSFER_QTY' => 'KL chuyển/nhận',
            'PRICE' => 'Giá (lưu đơn vị 1đ)',
            'VALUE' => 'Giá trị',
            'REG_DATE_TIME' => 'Thời gian tạo',
            'REG_USER_ID' => 'Người tạo',
            'UPD_DATE_TIME' => 'Thời gian cập nhật',
            'UPD_USER_ID' => 'Người cập nhật',
        ];
    }
}
