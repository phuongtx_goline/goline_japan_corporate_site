<?php

namespace common\models;

use Yii;

/**
* This is the model class for table "ORDTORDER_DETAIL".
*
    * @property integer $ID
    * @property integer $PARENT_ID
    * @property integer $TRADE_DATE
    * @property integer $BUY_DATE
    * @property string $QTY
    * @property string $PRICE
    * @property string $AMOUNT
    * @property string $FEE_RATE
    * @property string $FEE_AMT
    * @property string $REG_DATE_TIME
    * @property integer $REG_USER_ID
    * @property string $UPD_DATE_TIME
    * @property integer $UPD_USER_ID
*/
class ORDTORDERDETAIL extends \yii\db\ActiveRecord
{
/**
* @inheritdoc
*/
public static function tableName()
{
return 'ORDTORDER_DETAIL';
}

/**
* @inheritdoc
*/
public function rules()
{
return [
            [['PARENT_ID', 'TRADE_DATE', 'BUY_DATE', 'QTY', 'PRICE', 'AMOUNT', 'FEE_RATE', 'FEE_AMT'], 'required'],
            [['PARENT_ID', 'TRADE_DATE', 'BUY_DATE', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['QTY', 'PRICE', 'AMOUNT', 'FEE_RATE', 'FEE_AMT'], 'number'],
            [['REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
        ];
}

/**
* @inheritdoc
*/
public function attributeLabels()
{
return [
    'ID' => 'ID',
    'PARENT_ID' => 'Ngày giao dịch của chu kỳ (ORDTORDER.TRADE_DATE)',
    'TRADE_DATE' => 'Ngày CK MUA về tài khoản (CORTSEC_BALANCE.ENTRY_DATE)',
    'BUY_DATE' => 'Khối lượng đặt/ khớp',
    'QTY' => 'Khối lượng đặt/ khớp',
    'PRICE' => 'Giá trị',
    'AMOUNT' => 'GT khớp = QTY * PRICE',
    'FEE_RATE' => 'Tỉ lệ phí',
    'FEE_AMT' => 'GT phí',
    'REG_DATE_TIME' => 'Thời gian tạo',
    'REG_USER_ID' => 'Người tạo',
    'UPD_DATE_TIME' => 'Thời gian cập nhật',
    'UPD_USER_ID' => 'Người cập nhật',
];
}
}
