<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ORDTTRADE_FEE".
 *
 * @property integer $ID
 * @property string $TRANSACTION_CD
 * @property integer $CUST_TYPE
 * @property integer $FOREIGN_TYPE
 * @property integer $TRADE_TYPE
 * @property integer $FEE_RATE_ID
 * @property integer $STATUS
 * @property string $REG_DATE_TIME
 * @property integer $REG_USER_ID
 * @property string $UPD_DATE_TIME
 * @property integer $UPD_USER_ID
 */
class ORDTTRADEFEE extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ORDTTRADE_FEE';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TRANSACTION_CD', 'CUST_TYPE', 'FOREIGN_TYPE', 'TRADE_TYPE', 'FEE_RATE_ID', 'STATUS'], 'required'],
            [['CUST_TYPE', 'FOREIGN_TYPE', 'TRADE_TYPE', 'FEE_RATE_ID', 'STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['TRANSACTION_CD'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'TRANSACTION_CD' => 'Đơn vị giao dịch',
            'CUST_TYPE' => 'Loại khách hàng',
            'FOREIGN_TYPE' => 'Vùng lãnh thổ',
            'TRADE_TYPE' => 'Loại giao dịch',
            'FEE_RATE_ID' => 'Biểu phí',
            'STATUS' => 'Trạng thái',
            'REG_DATE_TIME' => 'Thời gian tạo',
            'REG_USER_ID' => 'Người tạo',
            'UPD_DATE_TIME' => 'Thời gian cập nhật',
            'UPD_USER_ID' => 'Người cập nhật',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getORDTTRADEFEERATE()
    {
        return $this->hasOne(ORDTTRADEFEERATE::className(), ['FEE_RATE_ID' => 'ID']);
    }
}
