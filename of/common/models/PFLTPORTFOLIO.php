<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "PFLTPORTFOLIO".
 *
 * @property integer $ID
 * @property integer $CUST_CD
 * @property string $SEC_CD
 * @property string $QTY
 * @property string $INVEST_AMT
 * @property string $AVG_PRICE
 * @property string $REG_DATE_TIME
 * @property integer $REG_USER_ID
 * @property string $UPD_DATE_TIME
 * @property integer $UPD_USER_ID
 */
class PFLTPORTFOLIO extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PFLTPORTFOLIO';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CUST_CD', 'SEC_CD', 'QTY', 'INVEST_AMT', 'AVG_PRICE'], 'required'],
            [['CUST_CD', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['QTY', 'INVEST_AMT', 'AVG_PRICE'], 'number'],
            [['REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['SEC_CD'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CUST_CD' => 'Khách hàng',
            'SEC_CD' => 'Mã CCQ',
            'QTY' => 'Số dư hiện tại',
            'INVEST_AMT' => 'Giá trị đầu tư',
            'AVG_PRICE' => 'Giá bình quân',
            'REG_DATE_TIME' => 'Thời gian tạo',
            'REG_USER_ID' => 'Người tạo',
            'UPD_DATE_TIME' => 'Thời gian cập nhật',
            'UPD_USER_ID' => 'Người cập nhật',
        ];
    }
}
