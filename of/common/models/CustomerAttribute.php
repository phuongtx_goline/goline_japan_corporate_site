<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "customer_attribute".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $attribute_id
 * @property string $value
 * @property string $remarks
 * @property integer $status
 * @property string $reg_date_time
 * @property integer $reg_user_id
 * @property string $reg_ip_addr
 * @property string $upd_date_time
 * @property integer $upd_user_id
 * @property string $upd_ip_addr
 */
class CustomerAttribute extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_attribute';
    }

    public $customer;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'attribute_id', 'value', 'status'], 'required'],
            [['customer_id', 'attribute_id', 'status', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['reg_date_time', 'upd_date_time'], 'safe'],
            [['value'], 'string', 'max' => 300],
            [['remarks'], 'string', 'max' => 200],
            [['reg_ip_addr', 'upd_ip_addr'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Khách hàng',
            'customer' => 'Khách hàng',
            'attribute_id' => 'Thuộc tính',
            'value' => 'Giá trị',
            'remarks' => 'Ghi chú',
            'status' => 'Trạng thái',
            'reg_date_time' => 'Thời gian tạo',
            'reg_user_id' => 'Người tạo',
            'reg_ip_addr' => 'IP tạo',
            'upd_date_time' => 'Thời gian cập nhật',
            'upd_user_id' => 'Người cập nhật',
            'upd_ip_addr' => 'IP cập nhật',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * Join with code
     */
    public function getcode()
    {
        return $this->hasOne(Code::className(), ['id' => 'attribute_id']);
    }
}
