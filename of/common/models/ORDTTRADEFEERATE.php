<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ORDTTRADE_FEE_RATE".
 *
 * @property integer $ID
 * @property string $FEE_RATE_CODE
 * @property string $FEE_RATE_NAME
 * @property integer $FEE_TYPE
 * @property integer $VALID_DATE
 * @property integer $STATUS
 * @property string $REMARKS
 * @property string $REG_DATE_TIME
 * @property integer $REG_USER_ID
 * @property string $UPD_DATE_TIME
 * @property integer $UPD_USER_ID
 */
class ORDTTRADEFEERATE extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ORDTTRADE_FEE_RATE';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FEE_RATE_CODE', 'FEE_RATE_NAME', 'FEE_TYPE', 'STATUS'], 'required'],
            [['FEE_TYPE', 'STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['REMARKS'], 'string'],
            [['REG_DATE_TIME', 'UPD_DATE_TIME','DETAIL'], 'safe'],
            [['FEE_RATE_CODE'], 'string', 'max' => 5],
            [['FEE_RATE_NAME'], 'string', 'max' => 80],
        ];
    }


    public $DETAIL;
    public $VALID_DATE;
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'FEE_RATE_CODE' => 'Mã biểu phí',
            'FEE_RATE_NAME' => 'Tên biểu phí',
            'FEE_TYPE' => 'Loại phí',
            'STATUS' => 'Trạng thái',
            'VALID_DATE' => 'Ngày hiệu lực',
            'REMARKS' => 'Ghi chú',
            'REG_DATE_TIME' => 'Thời gian tạo',
            'REG_USER_ID' => 'Người tạo',
            'UPD_DATE_TIME' => 'Thời gian cập nhật',
            'UPD_USER_ID' => 'Người cập nhật',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getORDTTRADEFEERATEDETAIL()
    {
        return $this->hasMany(ORDTTRADEFEERATEDETAIL::className(), ['FEE_RATE_ID' => 'ID']);
    }
}
