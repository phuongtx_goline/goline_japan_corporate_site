<?php

namespace common\models;

use Yii;

/**
* This is the model class for table "bank_contract".
*
    * @property integer $id
    * @property integer $bank_id
    * @property integer $contract_id
    * @property string $note
*/
class BankContract extends \yii\db\ActiveRecord
{
/**
* @inheritdoc
*/
public static function tableName()
{
return 'bank_contract';
}

/**
* @inheritdoc
*/
public function rules()
{
return [
            [['bank_id', 'contract_id'], 'integer'],
            [['note'], 'string', 'max' => 255],
        ];
}

/**
* @inheritdoc
*/
public function attributeLabels()
{
return [
    'id' => 'ID',
    'bank_id' => 'Bank ID',
    'contract_id' => 'Contract ID',
    'note' => 'Note',
];
}
}
