<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "CORCUSTOMER".
 *
 * @property integer $CUST_CD
 * @property string $CUST_NO
 * @property string $CUST_NAME
 * @property integer $CUST_TYPE
 * @property integer $FOREIGN_TYPE
 * @property integer $BIRTHDAY
 * @property integer $SEX
 * @property string $TEL_NO
 * @property string $MOBILE_NO
 * @property string $EMAIL_ADRS
 * @property integer $EMAIL_FLG
 * @property integer $APPROVAL_CD
 * @property string $ID_NUMBER
 * @property integer $ISSUE_LOCATION_CD
 * @property integer $ISSUE_DATE
 * @property integer $EXPIRE_DATE
 * @property string $ADDR1
 * @property string $ADDR2
 * @property string $COUNTRY_CD
 * @property string $BROKER_CD
 * @property integer $JOB
 * @property string $POSITION
 * @property string $COMPANY
 * @property string $COMPANY_TEL
 * @property integer $ACCO_DIVIDE
 * @property integer $ACCO_STS
 * @property integer $OPEN_DATE
 * @property integer $CLOSE_DATE
 * @property string $CONTRACT_NO
 * @property integer $FATCA
 * @property string $TRADING_CODE
 * @property string $PRES_NAME
 * @property integer $PRES_APPROVAL_CD
 * @property string $PRES_ID_NUMBER
 * @property integer $PRES_ISSUE_LOCATION_CD
 * @property integer $PRES_ISSUE_DATE
 * @property string $PRES_TEL_NO
 * @property string $PRES_COUNTRY_CD
 * @property string $PRES_POSITION
 * @property string $BANK_ACCO_NAME
 * @property string $BANK_ACCO_NO
 * @property string $BANK_NAME
 * @property string $BANK_BRANCH_NAME
 * @property string $TRANSACTION_CD
 * @property integer $STATUS
 * @property integer $VSD_STATUS
 * @property string $REG_DATE_TIME
 * @property integer $REG_USER_ID
 * @property string $UPD_DATE_TIME
 * @property integer $UPD_USER_ID
 */
class CORCUSTOMER extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CORCUSTOMER';
    }

    public $CE_CUST_CD;
    public $CE_ENTRUST_CUST_NAME;
    public $CE_ENTRUST_CUST_NO;
    public $CE_SING_DOC;
    public $CE_APPROVAL_CD;
    public $CE_ID_NUMBER;
    public $CE_ISSUE_LOCATION_CD;
    public $CE_ISSUE_DATE;
    public $CE_BIRTHDAY;
    public $CE_SEX;
    public $CE_ADDR;
    public $CE_TEL_NO;
    public $CE_EMAIL_ADRS;
    public $CE_ENTRUST_START_DATE;
    public $CE_ENTRUST_END_DATE;
    public $CE_ENTRUST_SCOPE;
    public $CE_COUNTRY_CD;
    public $CE_STATUS;
    public $CE_REMARKS;
    public $ce_file_upload;
    public $cs_file_upload;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ISSUE_DATE', 'ISSUE_LOCATION_CD', 'CUST_NAME', 'CUST_TYPE', 'FOREIGN_TYPE', 'BIRTHDAY', 'SEX', 'EMAIL_FLG', 'APPROVAL_CD', 'ID_NUMBER', 'ADDR1', 'ADDR2', 'COUNTRY_CD', 'ACCO_DIVIDE', 'CONTRACT_NO', 'FATCA', 'TRANSACTION_CD',], 'required'],
            [['CUST_CD', 'CUST_TYPE', 'FOREIGN_TYPE', 'BIRTHDAY', 'SEX', 'EMAIL_FLG', 'APPROVAL_CD', 'ISSUE_LOCATION_CD', 'ISSUE_DATE', 'EXPIRE_DATE', 'JOB', 'ACCO_DIVIDE', 'ACCO_STS', 'PRES_ID_NUMBER', 'ACCO_STS', 'STATUS', 'VSD_STATUS', 'PRES_COUNTRY_CD', 'OPEN_DATE', 'OPEN_DATE', 'CLOSE_DATE', 'CLOSE_DATE', 'FATCA', 'STATUS', 'VSD_STATUS', 'REG_USER_ID', 'UPD_USER_ID', 'PRES_APPROVAL_CD', 'PRES_ISSUE_LOCATION_CD', 'PRES_ISSUE_DATE'], 'integer'],
            [['REG_DATE_TIME', 'UPD_DATE_TIME', 'CE_CUST_CD', 'CUST_NO', 'CE_ENTRUST_CUST_NAME', 'CE_ENTRUST_CUST_NO', 'CE_SING_DOC', 'CE_APPROVAL_CD', 'CE_ID_NUMBER', 'CE_ISSUE_LOCATION_CD', 'CE_ISSUE_DATE', 'CE_BIRTHDAY', 'CE_SEX', 'CE_ADDR', 'CE_TEL_NO', 'CE_EMAIL_ADRS', 'CE_ENTRUST_START_DATE', 'CE_ENTRUST_END_DATE', 'CE_ENTRUST_SCOPE', 'CE_COUNTRY_CD', 'CE_STATUS', 'CE_REMARKS'], 'safe'],
            [['CUST_NO'], 'string', 'max' => 10],
            [['CUST_NAME', 'TEL_NO', 'MOBILE_NO', 'EMAIL_ADRS', 'ID_NUMBER', 'ADDR1', 'ADDR2', 'COUNTRY_CD', 'BROKER_CD', 'POSITION', 'COMPANY', 'COMPANY_TEL', 'CONTRACT_NO', 'TRADING_CODE', 'PRES_NAME', 'PRES_TEL_NO', 'PRES_POSITION', 'BANK_ACCO_NAME', 'BANK_ACCO_NO', 'BANK_NAME', 'BANK_BRANCH_NAME', 'TRANSACTION_CD'], 'string', 'max' => 255],
            [['ID_NUMBER', 'CUST_NO'], 'trim'],
            [['ID_NUMBER'], 'unique', 'targetClass' => '\common\models\CORCUSTOMER'],
            [['CUST_NO'], 'unique', 'targetClass' => '\common\models\CORCUSTOMER'],
            [['ce_file_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'doc, docx, pdf, png, gif, jpg, jpeg', 'maxFiles' => 10],
            [['cs_file_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'doc, docx, pdf, png, gif, jpg, jpeg', 'maxFiles' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'CUST_CD' => 'ID',
            'CUST_NO' => 'Số tài khoản khách hàng',
            'CUST_NAME' => 'Họ và tên khách hàng',
            'CUST_TYPE' => 'Loại khách hàng',
            'FOREIGN_TYPE' => 'Vùng lãnh thổ',
            'BIRTHDAY' => 'Ngày sinh',
            'SEX' => 'Giới tính',
            'TEL_NO' => 'Điện thoại',
            'MOBILE_NO' => 'Di động',
            'EMAIL_ADRS' => 'Email',
            'EMAIL_FLG' => 'Gửi email',
            'APPROVAL_CD' => 'Loại CMND',
            'ID_NUMBER' => 'Số CMND/CCCD/HC',
            'ISSUE_LOCATION_CD' => 'Nơi cấp',
            'ISSUE_DATE' => 'Ngày cấp',
            'EXPIRE_DATE' => 'Ngày hết hạn',
            'ADDR1' => 'Địa chỉ thường trú',
            'ADDR2' => 'Địa chỉ liên lạc',
            'COUNTRY_CD' => 'Quốc tịch',
            'BROKER_CD' => 'Nhân viên chăm sóc',
            'JOB' => 'Nghề nghiệp',
            'POSITION' => 'Chức vụ',
            'COMPANY' => 'Công ty',
            'COMPANY_TEL' => 'Số điện thoại công ty',
            'ACCO_DIVIDE' => 'Loại tài khoản',
            'ACCO_STS' => 'Trạng thái tài khoản',
            'OPEN_DATE' => 'Ngày mở tài khoản',
            'CLOSE_DATE' => 'Ngày đóng tài khoản',
            'CONTRACT_NO' => 'Số hợp đồng',
            'FATCA' => 'Nhóm FATCA',
            'TRADING_CODE' => 'Mã số GDCK (KH nước ngoài)',
            'PRES_NAME' => 'Họ và tên người đại diện',
            'PRES_APPROVAL_CD' => 'Loại chứng thư ',
            'PRES_ID_NUMBER' => 'Số CMND người đại diện',
            'PRES_ISSUE_LOCATION_CD' => 'Nơi cấp',
            'PRES_ISSUE_DATE' => 'Ngày cấp',
            'PRES_TEL_NO' => 'Số điện thoại người đại diện',
            'PRES_COUNTRY_CD' => 'Quốc tịch người đại diện',
            'PRES_POSITION' => 'Chức vụ người đại diện',
            'BANK_ACCO_NAME' => 'Tên tài khoản thụ hưởng',
            'BANK_ACCO_NO' => 'Số tài khoản ngân hàng',
            'BANK_NAME' => 'Tên ngân hàng',
            'BANK_BRANCH_NAME' => 'Tên chi nhánh ngân hàng',
            'TRANSACTION_CD' => 'Đơn vị giao dịch',
            'STATUS' => 'Trạng thái',
            'VSD_STATUS' => 'Trạng thái VSD',
            'REG_DATE_TIME' => 'Thời gian tạo',
            'REG_USER_ID' => 'Người tạo',
            'UPD_DATE_TIME' => 'Thời gian cập nhật',
            'UPD_USER_ID' => 'Người cập nhật',
            'ce_file_upload' => 'Chữ ký',
            'cs_file_upload' => 'Các chữ ký đính kèm',
            'CE_ENTRUST_CUST_NAME' => 'Tên người ủy quyền',
            'CE_ENTRUST_CUST_NO' => 'Số TKGDCK người ủy quyền',
            'CE_CUST_CD' => 'Số TK ủy quyền',
            'CE_APPROVAL_CD' => 'Loại CMND',
            'CE_ID_NUMBER' => 'Số CMND/CCCD/HC',
            'CE_ISSUE_LOCATION_CD' => 'Nơi cấp',
            'CE_ISSUE_DATE' => 'Ngày cấp',
            'CE_BIRTHDAY' => 'Ngày sinh',
            'CE_SEX' => 'Giới tính',
            'CE_ADDR' => 'Địa chỉ thường trú',
            'CE_TEL_NO' => 'Số điện thoại',
            'CE_EMAIL_ADRS' => 'Email',
            'CE_ENTRUST_START_DATE' => 'Ngày bắt đầu UQ',
            'CE_ENTRUST_END_DATE' => 'Ngày kết thúc UQ',
            'CE_ENTRUST_SCOPE' => 'Phạm vi ủy quyền',
            'CE_COUNTRY_CD' => 'Quốc tịch',
            'CE_STATUS' => 'Trạng thái',
            'CE_REMARKS' => 'Ghi chú',
        ];
    }
}
