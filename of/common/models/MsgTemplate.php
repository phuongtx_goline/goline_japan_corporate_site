<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "msg_template".
 *
 * @property integer $id
 * @property integer $msg_type
 * @property integer $send_type
 * @property integer $foreign_type
 * @property string $title
 * @property string $content
 * @property string $file_template_url
 * @property string $mail_bcc
 * @property string $remarks
 * @property integer $status
 * @property string $reg_date_time
 * @property integer $reg_user_id
 * @property string $reg_ip_addr
 * @property string $upd_date_time
 * @property integer $upd_user_id
 * @property string $upd_ip_addr
 * @property string $file_upload
 */
class MsgTemplate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msg_template';
    }

    public $file_upload;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msg_type', 'send_type', 'foreign_type', 'status'], 'required'],
            [['msg_type', 'send_type', 'foreign_type', 'status', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['content'], 'string'],
            [['reg_date_time', 'upd_date_time'], 'safe'],
            [['title', 'mail_bcc'], 'string', 'max' => 500],
            [['remarks', 'file_template_url'], 'string', 'max' => 200],
            [['reg_ip_addr', 'upd_ip_addr'], 'string', 'max' => 50],
            [['file_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'doc, docx, pdf, png, gif, jpg, jpeg'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'msg_type' => 'Loại thông báo',
            'send_type' => 'Cách gửi',
            'foreign_type' => 'Loại khách hàng',
            'title' => 'Tiêu đề',
            'content' => 'Nội dung',
            'file_template_url' => 'Tệp đính kèm',
            'file_upload' => 'Tệp đính kèm',
            'mail_bcc' => 'Mail BCC',
            'remarks' => 'Ghi chú',
            'status' => 'Trạng thái',
            'reg_date_time' => 'Thời gian tạo',
            'reg_user_id' => 'Người tạo',
            'reg_ip_addr' => 'IP tạo',
            'upd_date_time' => 'Thời gian cập nhật',
            'upd_user_id' => 'Người cập nhật',
            'upd_ip_addr' => 'IP cập nhật',
        ];
    }
}
