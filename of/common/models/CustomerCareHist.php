<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "customer_care_hist".
 *
 * @property integer $id
 * @property string $date
 * @property integer $user_id
 * @property integer $customer_id
 * @property string $remarks
 * @property integer $status
 * @property string $reg_date_time
 * @property integer $reg_user_id
 * @property string $reg_ip_addr
 * @property string $upd_date_time
 * @property integer $upd_user_id
 * @property string $upd_ip_addr
 * @property string $customer_name
 */
class CustomerCareHist extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_care_hist';
    }

    public $customer_name;
    public $colorgb_date;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'customer_id'], 'required'],
            [['colorgb_date','date', 'user_id', 'reg_date_time', 'upd_date_time','customer_name'], 'safe'],
            [['user_id', 'customer_id', 'status', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['remarks'], 'string', 'max' => 200],
            [['reg_ip_addr', 'upd_ip_addr'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Ngày giờ thực hiện',
            'colorgb_date' => 'Ngày giờ thực hiện',
            'user_id' => 'Người thực hiện',
            'customer_id' => 'Khách hàng',
            'customer_name' => 'Khách hàng',
            'remarks' => 'Ghi chú',
            'status' => 'Trạng thái',
            'reg_date_time' => 'Thời gian tạo',
            'reg_user_id' => 'Người tạo',
            'reg_ip_addr' => 'IP máy tạo',
            'upd_date_time' => 'Thời gian cập nhật',
            'upd_user_id' => 'Người cập nhật',
            'upd_ip_addr' => 'IP máy cập nhật',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * Join with customer
     */
    public function getcustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }
}
