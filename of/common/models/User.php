<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $mobile
 * @property string $password
 * @property integer $is_admin
 * @property integer $is_locked
 * @property string $name
 * @property string $address
 * @property string $email_addr
 * @property string $email_pw
 * @property string $email_signature
 * @property integer $group_id
 * @property string $remarks
 * @property integer $status
 * @property string $password_reset_token
 * @property string $auth_key
 * @property string $reg_date_time
 * @property integer $reg_user_id
 * @property string $reg_ip_addr
 * @property string $upd_date_time
 * @property integer $upd_user_id
 * @property string $upd_ip_addr
 */
class User extends ActiveRecord implements IdentityInterface
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    public $created_at;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_admin', 'mobile', 'name', 'status'], 'required'],
            [['is_locked', 'group_id', 'status', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['reg_date_time', 'upd_date_time', 'password_reset_token', 'auth_key'], 'safe'],
            [['mobile'], 'string', 'max' => 12],
            [['password'], 'string', 'max' => 100],
            [['name', 'address', 'remarks'], 'string', 'max' => 200],
            [['email_addr', 'email_pw'], 'string', 'max' => 50],
            [['email_signature'], 'string', 'max' => 10000],
            [['reg_ip_addr', 'upd_ip_addr'], 'string', 'max' => 50],

            [['password'], 'string', 'min' => 6],

            ['mobile', 'trim'],
            ['mobile', 'unique', 'targetClass' => '\common\models\User'],

            ['email_addr', 'trim'],
            ['email_addr', 'email'],
            ['email_addr', 'unique', 'targetClass' => '\common\models\User']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mobile' => 'Số điện thoại',
            'password' => 'Mật khẩu',
            'is_admin' => 'Vai trò',
            'is_locked' => 'Khóa đăng nhập',
            'name' => 'Họ tên',
            'address' => 'Địa chỉ',
            'email_addr' => 'Địa chỉ email',
            'email_pw' => 'Mật khẩu email',
            'email_signature' => 'Chữ ký email',
            'group_id' => 'Nhóm người dùng',
            'remarks' => 'Ghi chú',
            'status' => 'Trạng thái',
            'password_reset_token' => 'Mã thiết lập lại mật khẩu',
            'auth_key' => 'Khóa xác thực tài khoản',
            'reg_date_time' => 'Thời gian tạo',
            'reg_user_id' => 'Người tạo',
            'reg_ip_addr' => 'IP tạo',
            'upd_date_time' => 'Thời gian cập nhật',
            'upd_user_id' => 'Người cập nhật',
            'upd_ip_addr' => 'IP cập nhật',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        $recordStatus['active'] = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value')['active'];

        return static::findOne([
            'id' => $id,
            'status' => $recordStatus['active'],
            'is_locked' => 0
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        $recordStatus['active'] = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value')['active'];

        return static::findOne([
            'password_reset_token' => $token,
            'status' => $recordStatus['active'],
            'is_locked' => 0,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['userPasswordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return md5($password) == $this->password ? true : false;
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = md5($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}
