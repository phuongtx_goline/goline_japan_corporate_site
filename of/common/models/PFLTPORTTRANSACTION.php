<?php

namespace common\models;

use Yii;

/**
* This is the model class for table "PFLTPORT_TRANSACTION".
*
    * @property integer $ID
    * @property integer $TRADE_DATE
    * @property integer $CUST_CD
    * @property string $SEC_CD
    * @property integer $TRADE_TYPE
    * @property string $QTY
    * @property string $PRICE
    * @property string $AMOUNT
    * @property string $FEE_AMT
    * @property string $TAX_AMT
    * @property string $TASK_CD
    * @property integer $SRC_NO
    * @property string $REMAIN_QTY_ACM
    * @property string $INVEST_AMT_ACM
    * @property string $PROFIT_AMT
    * @property string $REMARKS
    * @property string $REG_DATE_TIME
    * @property integer $REG_USER_ID
    * @property string $UPD_DATE_TIME
    * @property integer $UPD_USER_ID
*/
class PFLTPORTTRANSACTION extends \yii\db\ActiveRecord
{
/**
* @inheritdoc
*/
public static function tableName()
{
return 'PFLTPORT_TRANSACTION';
}

/**
* @inheritdoc
*/
public function rules()
{
return [
            [['TRADE_DATE', 'CUST_CD', 'SEC_CD', 'TRADE_TYPE', 'QTY', 'PRICE', 'AMOUNT', 'FEE_AMT', 'TAX_AMT', 'TASK_CD', 'REMAIN_QTY_ACM', 'INVEST_AMT_ACM', 'PROFIT_AMT'], 'required'],
            [['TRADE_DATE', 'CUST_CD', 'TRADE_TYPE', 'SRC_NO', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['QTY', 'PRICE', 'AMOUNT', 'FEE_AMT', 'TAX_AMT', 'REMAIN_QTY_ACM', 'INVEST_AMT_ACM', 'PROFIT_AMT'], 'number'],
            [['REMARKS'], 'string'],
            [['REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['SEC_CD'], 'string', 'max' => 255],
            [['TASK_CD'], 'string', 'max' => 5],
        ];
}

/**
* @inheritdoc
*/
public function attributeLabels()
{
return [
    'ID' => 'ID',
    'TRADE_DATE' => 'Ngày giao dịch',
    'CUST_CD' => 'Khách hàng',
    'SEC_CD' => 'Mã CCQ',
    'TRADE_TYPE' => 'Phát sinh',
    'QTY' => 'Khối lượng',
    'PRICE' => 'Giá',
    'AMOUNT' => 'Giá trị',
    'FEE_AMT' => 'Phí giao dịch',
    'TAX_AMT' => 'Thuế TNCN',
    'TASK_CD' => 'Thao tác',
    'SRC_NO' => 'ID bảng nghiệp vụ tương ứng',
    'REMAIN_QTY_ACM' => 'Khối lượng tồn lũy kế',
    'INVEST_AMT_ACM' => 'Vốn đầu tư lũy kế',
    'PROFIT_AMT' => 'Lãi lỗ đã thực hiện',
    'REMARKS' => 'Ghi chú',
    'REG_DATE_TIME' => 'Thời gian tạo',
    'REG_USER_ID' => 'Người tạo',
    'UPD_DATE_TIME' => 'Thời gian cập nhật',
    'UPD_USER_ID' => 'Người cập nhật',
];
}
}
