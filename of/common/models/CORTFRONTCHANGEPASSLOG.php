<?php

namespace common\models;

use Yii;

/**
* This is the model class for table "CORTFRONT_CHANGE_PASS_LOG".
*
    * @property integer $ID
    * @property string $USER_NAME
    * @property integer $USER_TYPE
    * @property integer $CHANGE_TYPE
    * @property string $CLIENT_INFO
    * @property string $REMARKS
    * @property string $REG_DATE_TIME
    * @property integer $REG_USER_ID
*/
class CORTFRONTCHANGEPASSLOG extends \yii\db\ActiveRecord
{
/**
* @inheritdoc
*/
public static function tableName()
{
return 'CORTFRONT_CHANGE_PASS_LOG';
}

/**
* @inheritdoc
*/
public function rules()
{
return [
            [['USER_NAME'], 'required'],
            [['USER_TYPE', 'CHANGE_TYPE', 'REG_USER_ID'], 'integer'],
            [['REG_DATE_TIME'], 'safe'],
            [['USER_NAME', 'CLIENT_INFO', 'REMARKS'], 'string', 'max' => 255],
        ];
}

/**
* @inheritdoc
*/
public function attributeLabels()
{
return [
    'ID' => 'ID',
    'USER_NAME' => 'Tài khoản',
    'USER_TYPE' => 'Loại tài khoản',
    'CHANGE_TYPE' => 'Loại thay đổi',
    'CLIENT_INFO' => 'Thông tin UA',
    'REMARKS' => 'Ghi chú',
    'REG_DATE_TIME' => 'Thời gian tạo',
    'REG_USER_ID' => 'Người tạo',
];
}
}
