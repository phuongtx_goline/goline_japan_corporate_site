<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contract_extend_hist".
 *
 * @property integer $id
 * @property integer $contract_id
 * @property string $old_end_date
 * @property string $new_end_date
 * @property string $remarks
 * @property string $reg_date_time
 * @property integer $reg_user_id
 * @property string $reg_ip_addr
 * @property string $upd_date_time
 * @property integer $upd_user_id
 * @property string $upd_ip_addr
 */
class ContractExtendHist extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contract_extend_hist';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contract_id', 'old_end_date', 'new_end_date'], 'required'],
            [['contract_id', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['old_end_date', 'new_end_date'], 'number'],
            [['reg_date_time', 'upd_date_time', 'remarks'], 'safe'],
            [['remarks'], 'string', 'max' => 200],
            [['reg_ip_addr', 'upd_ip_addr'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contract_id' => 'HĐ',
            'old_end_date' => 'Ngày hết hạn cũ',
            'new_end_date' => 'Ngày hết hạn mới',
            'remarks' => 'Ghi chú',
            'reg_date_time' => 'Thời gian tạo',
            'reg_user_id' => 'Người tạo',
            'reg_ip_addr' => 'IP tạo',
            'upd_date_time' => 'Thời gian cập nhật',
            'upd_user_id' => 'Người cập nhật',
            'upd_ip_addr' => 'IP cập nhật',
        ];
    }
}
