<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "fund_unit".
 *
 * @property string $date
 * @property string $fund_net_asset
 * @property string $fund_unit_qty
 * @property string $fund_unit_price
 * @property string $file_path
 * @property string $remarks
 * @property integer $status
 * @property string $reg_date_time
 * @property integer $reg_user_id
 * @property string $reg_ip_addr
 * @property string $upd_date_time
 * @property integer $upd_user_id
 * @property string $upd_ip_addr
 * @property string $file_upload
 */
class FundUnit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fund_unit';
    }

    public $file_upload;
    public $colorgb_date;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'fund_net_asset', 'fund_unit_qty', 'fund_unit_price', 'status'], 'required'],
            [['fund_net_asset', 'fund_unit_qty', 'fund_unit_price'], 'number'],
            [['status', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['colorgb_date','reg_date_time', 'upd_date_time'], 'safe'],
            [['file_path'], 'string', 'max' => 500],
            [['remarks'], 'string', 'max' => 200],
            [['reg_ip_addr', 'upd_ip_addr'], 'string', 'max' => 50],
            [['file_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'doc, docx, pdf, png, gif, jpg, jpeg','maxFiles' => 10],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'date' => 'Ngày/Tháng/Năm',
            'colorgb_date' => 'Ngày/Tháng/Năm',
            'fund_net_asset' => 'Tổng GTTS ròng của quỹ',
            'fund_unit_qty' => 'Số lượng ĐVĐT của quỹ',
            'fund_unit_price' => 'Giá của 1 ĐVĐT',
            'file_path' => 'Hiệu quả đầu tư',
            'file_upload' => 'Hiệu quả đầu tư',
            'remarks' => 'Ghi chú',
            'status' => 'Trạng thái',
            'reg_date_time' => 'Thời gian tạo',
            'reg_user_id' => 'Người tạo',
            'reg_ip_addr' => 'IP tạo',
            'upd_date_time' => 'Thời gian cập nhật',
            'upd_user_id' => 'Người cập nhật',
            'upd_ip_addr' => 'IP cập nhật',
        ];
    }
}
