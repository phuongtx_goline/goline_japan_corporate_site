<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "MSTTBRANCH".
 *
 * @property string $TRANSACTION_CD
 * @property integer $LOCATION_CD
 * @property string $BRANCH_NAME
 * @property string $BRANCH_ADRS
 * @property string $TEL_NO
 * @property string $FAX_NO
 * @property string $EMAIL_ADRS
 * @property string $DEPUTY
 * @property integer $STATUS
 * @property string $REMARKS
 * @property string $REG_DATE_TIME
 * @property integer $REG_USER_ID
 * @property string $UPD_DATE_TIME
 * @property integer $UPD_USER_ID
 */
class MSTTBRANCH extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MSTTBRANCH';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TRANSACTION_CD', 'BRANCH_NAME', 'STATUS'], 'required'],
            [['LOCATION_CD', 'STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['REMARKS'], 'string'],
            [['REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['TRANSACTION_CD'], 'string', 'max' => 3],
            [['TRANSACTION_CD'], 'trim'],
            [['TRANSACTION_CD'], 'unique', 'targetClass' => '\common\models\MSTTBRANCH'],
            [['BRANCH_NAME', 'BRANCH_ADRS', 'TEL_NO', 'FAX_NO', 'EMAIL_ADRS', 'DEPUTY'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'TRANSACTION_CD' => 'Mã đơn vị',
            'LOCATION_CD' => 'Tỉnh thành',
            'BRANCH_NAME' => 'Tên đơn vị',
            'BRANCH_ADRS' => 'Địa chỉ 1',
            'TEL_NO' => 'Điện thoại',
            'FAX_NO' => 'Fax',
            'EMAIL_ADRS' => 'Email',
            'DEPUTY' => 'Người đại diện',
            'STATUS' => 'Trạng thái',
            'REMARKS' => 'Ghi chú',
            'REG_DATE_TIME' => 'Thời gian tạo',
            'REG_USER_ID' => 'Người tạo',
            'UPD_DATE_TIME' => 'Thời gian cập nhật',
            'UPD_USER_ID' => 'Người cập nhật',
        ];
    }
}
