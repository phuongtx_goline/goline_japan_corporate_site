<?php

namespace common\models;

use Yii;

/**
* This is the model class for table "CORCUSTOMER_ONLINE_PENDING".
*
    * @property integer $ID
    * @property integer $ENTRY_DATE
    * @property string $CUST_NAME
    * @property integer $CUST_TYPE
    * @property integer $FOREIGN_TYPE
    * @property integer $BIRTHDAY
    * @property integer $SEX
    * @property string $TEL_NO
    * @property string $MOBILE_NO
    * @property string $EMAIL_ADRS
    * @property string $ADDR1
    * @property string $ADDR2
    * @property integer $APPROVAL_CD
    * @property string $ID_NUMBER
    * @property integer $ISSUE_LOCATION_CD
    * @property integer $ISSUE_DATE
    * @property integer $EXPIRE_DATE
    * @property string $ISSUE_DOC_FRONT
    * @property string $ISSUE_DOC_BACK
    * @property string $COUNTRY_CD
    * @property string $TRANSACTION_CD
    * @property string $BROKER_CD
    * @property integer $STATUS
    * @property string $REG_DATE_TIME
    * @property integer $REG_USER_ID
    * @property string $UPD_DATE_TIME
    * @property integer $UPD_USER_ID
*/
class CORCUSTOMERONLINEPENDING extends \yii\db\ActiveRecord
{
/**
* @inheritdoc
*/
public static function tableName()
{
return 'CORCUSTOMER_ONLINE_PENDING';
}

/**
* @inheritdoc
*/
public function rules()
{
return [
            [['ENTRY_DATE', 'CUST_NAME', 'CUST_TYPE', 'FOREIGN_TYPE', 'BIRTHDAY', 'SEX', 'TEL_NO', 'MOBILE_NO', 'EMAIL_ADRS', 'ADDR1', 'ADDR2', 'APPROVAL_CD', 'ID_NUMBER', 'ISSUE_LOCATION_CD', 'ISSUE_DATE', 'COUNTRY_CD', 'STATUS'], 'required'],
            [['ENTRY_DATE', 'CUST_TYPE', 'FOREIGN_TYPE', 'BIRTHDAY', 'SEX', 'APPROVAL_CD', 'ISSUE_LOCATION_CD', 'ISSUE_DATE', 'EXPIRE_DATE', 'STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['CUST_NAME', 'TEL_NO', 'MOBILE_NO', 'EMAIL_ADRS', 'ADDR1', 'ADDR2', 'ID_NUMBER', 'ISSUE_DOC_FRONT', 'ISSUE_DOC_BACK', 'COUNTRY_CD', 'TRANSACTION_CD', 'BROKER_CD'], 'string', 'max' => 255],
        ];
}

/**
* @inheritdoc
*/
public function attributeLabels()
{
return [
    'ID' => 'ID',
    'ENTRY_DATE' => 'Ngày tạo',
    'CUST_NAME' => 'Họ và tên',
    'CUST_TYPE' => 'Loại KH',
    'FOREIGN_TYPE' => 'Vùng',
    'BIRTHDAY' => 'Ngày sinh',
    'SEX' => 'Giới tính',
    'TEL_NO' => 'Điện thoại',
    'MOBILE_NO' => 'Di động',
    'EMAIL_ADRS' => 'Email',
    'ADDR1' => 'Địa chỉ thường trú',
    'ADDR2' => 'Địa chỉ liên lạc',
    'APPROVAL_CD' => 'Loại CMND',
    'ID_NUMBER' => 'Số CMND/CCCD/HC',
    'ISSUE_LOCATION_CD' => 'Nơi cấp',
    'ISSUE_DATE' => 'Ngày cấp',
    'EXPIRE_DATE' => 'Ngày hết hạn',
    'ISSUE_DOC_FRONT' => 'Mặt trước CMND/CCCD',
    'ISSUE_DOC_BACK' => 'Mặt sau CMND/CCCD',
    'COUNTRY_CD' => 'Mặc định là Việt Nam',
    'TRANSACTION_CD' => 'Mặc định NULL. NV công ty CK sẽ chọn',
    'BROKER_CD' => 'Nhân viên chăm sóc',
    'STATUS' => 'Trạng thái',
    'REG_DATE_TIME' => 'Thời gian tạo',
    'REG_USER_ID' => 'Người tạo',
    'UPD_DATE_TIME' => 'Thời gian cập nhật',
    'UPD_USER_ID' => 'Người cập nhật',
];
}
}
