<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $subject
 * @property string $content
 * @property string $reply
 * @property integer $status
 * @property string $reg_date_time
 * @property integer $reg_user_id
 * @property string $reg_ip_addr
 * @property string $upd_date_time
 * @property integer $upd_user_id
 * @property string $upd_ip_addr
 * @property string $customer
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback';
    }

    public $customer;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id'], 'required'],
            [['customer_id', 'status', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['content','reply'], 'string'],
            [['reg_date_time', 'upd_date_time'], 'safe'],
            [['subject'], 'string', 'max' => 500],
            [['reg_ip_addr', 'upd_ip_addr'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Khách hàng',
            'customer' => 'Khách hàng',
            'subject' => 'Tiêu đề',
            'content' => 'Nội dung',
            'status' => 'Trạng thái',
            'reply' => 'Nội dung trả lời',
            'reg_date_time' => 'Thời gian tạo',
            'reg_user_id' => 'Người tạo',
            'reg_ip_addr' => 'IP máy tạo',
            'upd_date_time' => 'Thời gian cập nhật',
            'upd_user_id' => 'Người cập nhật',
            'upd_ip_addr' => 'IP máy cập nhật',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * Join with customer
     */
    public function getcustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }
}
