<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "permission".
 *
 * @property string $role_cd
 * @property string $role_name
 * @property integer $seq
 * @property string $remarks
 */
class Permission extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'permission';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_cd', 'role_name'], 'required'],
            [['seq'], 'integer'],
            [['role_cd'], 'string', 'max' => 100],
            [['role_name', 'remarks'], 'string', 'max' => 200],
            ['role_cd', 'trim'],
            ['role_cd', 'unique', 'targetClass' => '\common\models\Permission'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'role_cd' => 'Mã quyền truy cập',
            'role_name' => 'Tên quyền',
            'seq' => 'Thứ tự hiển thị',
            'remarks' => 'Ghi chú',
        ];
    }
}
