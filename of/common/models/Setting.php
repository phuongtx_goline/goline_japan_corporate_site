<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "setting".
 *
 * @property string $param_name
 * @property string $param_value
 * @property string $remarks
 * @property string $reg_date_time
 * @property integer $reg_user_id
 * @property string $reg_ip_addr
 * @property string $upd_date_time
 * @property integer $upd_user_id
 * @property string $upd_ip_addr
 */
class Setting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['param_name', 'unique', 'targetClass' => '\common\models\Setting'],
            [['param_name', 'param_value'], 'required'],
            [['reg_date_time', 'upd_date_time','remarks','param_value'], 'safe'],
            [['reg_user_id', 'upd_user_id'], 'integer'],
            [['param_name'], 'string', 'max' => 100],
            [['reg_ip_addr', 'upd_ip_addr'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'param_name' => 'Tên tham số',
            'param_value' => 'Giá trị tham số',
            'remarks' => 'Ghi chú',
            'reg_date_time' => 'Thời gian tạo',
            'reg_user_id' => 'Người tạo',
            'reg_ip_addr' => 'IP tạo',
            'upd_date_time' => 'Thời gian cập nhật',
            'upd_user_id' => 'Người cập nhật',
            'upd_ip_addr' => 'IP cập nhật',
        ];
    }
}
