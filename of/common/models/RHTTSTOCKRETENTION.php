<?php

namespace common\models;

use Yii;

/**
* This is the model class for table "RHTTSTOCK_RETENTION".
*
    * @property integer $ID
    * @property integer $ALLO_DATE
    * @property integer $PARENT_ID
    * @property integer $CUST_CD
    * @property string $SEC_CD
    * @property string $NORMAL_RETENTION
    * @property string $LIMIT_RETENTION
    * @property string $QTY
    * @property string $ROUND_AMOUNT
    * @property string $TAX_AMOUNT
    * @property string $AMOUNT
    * @property string $TRANSACTION_CD
    * @property string $PAYMENT_TRADE_DATE
    * @property string $PAYMENT_SEC_DATE
    * @property string $PAYMENT_CASH_DATE
    * @property string $PAYMENT_STS_SEC
    * @property string $PAYMENT_STS_CASH
    * @property integer $STATUS
    * @property string $REMARKS
    * @property string $REG_DATE_TIME
    * @property integer $REG_USER_ID
    * @property string $UPD_DATE_TIME
    * @property integer $UPD_USER_ID
*/
class RHTTSTOCKRETENTION extends \yii\db\ActiveRecord
{
/**
* @inheritdoc
*/
public static function tableName()
{
return 'RHTTSTOCK_RETENTION';
}

/**
* @inheritdoc
*/
public function rules()
{
return [
            [['ALLO_DATE', 'PARENT_ID', 'CUST_CD', 'SEC_CD', 'NORMAL_RETENTION', 'LIMIT_RETENTION', 'QTY', 'ROUND_AMOUNT', 'TAX_AMOUNT', 'AMOUNT'], 'required'],
            [['ALLO_DATE', 'PARENT_ID', 'CUST_CD', 'STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['NORMAL_RETENTION', 'LIMIT_RETENTION', 'QTY', 'ROUND_AMOUNT', 'TAX_AMOUNT', 'AMOUNT'], 'number'],
            [['PAYMENT_TRADE_DATE', 'PAYMENT_SEC_DATE', 'PAYMENT_CASH_DATE', 'REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['REMARKS'], 'string'],
            [['SEC_CD', 'PAYMENT_STS_SEC', 'PAYMENT_STS_CASH'], 'string', 'max' => 255],
            [['TRANSACTION_CD'], 'string', 'max' => 5],
        ];
}

/**
* @inheritdoc
*/
public function attributeLabels()
{
return [
    'ID' => 'ID',
    'ALLO_DATE' => 'Ngày tạo',
    'PARENT_ID' => 'RHTTRIGHT_INFO.ID',
    'CUST_CD' => 'Khách hàng',
    'SEC_CD' => 'Mã CK',
    'NORMAL_RETENTION' => 'SL chứng khoán tự do chuyển nhượng',
    'LIMIT_RETENTION' => 'SL chứng khoán hạn chế chuyển nhượng',
    'QTY' => 'SL cổ phiếu được nhận (Cổ tức CCQ) SL cổ phiếu được nhận (Cổ tức CCQ)',
    'ROUND_AMOUNT' => 'Tiền quy đổi cổ phiếu lẻ (Cổ tức CCQ)',
    'TAX_AMOUNT' => 'Thuế TNCN (cả 2 loại cổ tức)',
    'AMOUNT' => 'Tiền cổ tức được nhận (Cổ tức tiền mặt)',
    'TRANSACTION_CD' => 'Mã đơn vị quản lý khách hàng',
    'PAYMENT_TRADE_DATE' => 'Ngày chuyển GD',
    'PAYMENT_SEC_DATE' => 'Ngày TT CCQ',
    'PAYMENT_CASH_DATE' => 'Ngày TT tiền',
    'PAYMENT_STS_SEC' => 'Trạng thái thanh toán CCQ',
    'PAYMENT_STS_CASH' => 'Trạng thái thanh toán',
    'STATUS' => 'Trạng thái',
    'REMARKS' => 'Ghi chú',
    'REG_DATE_TIME' => 'Thời gian tạo',
    'REG_USER_ID' => 'Người tạo',
    'UPD_DATE_TIME' => 'Thời gian cập nhật',
    'UPD_USER_ID' => 'Người cập nhật',
];
}
}
