<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "CORTSEC_BALANCE".
 *
 * @property integer $CUST_CD
 * @property integer $ENTRY_DATE
 * @property string $SEC_CD
 * @property integer $SEC_TYPE_CD
 * @property string $BEGIN_QTY
 * @property string $QTY
 * @property string $TRANSACTION_CD
 * @property string $REG_DATE_TIME
 * @property integer $REG_USER_ID
 * @property string $UPD_DATE_TIME
 * @property integer $UPD_USER_ID
 */
class CORTSECBALANCE extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CORTSEC_BALANCE';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CUST_CD', 'ENTRY_DATE', 'SEC_CD', 'SEC_TYPE_CD', 'BEGIN_QTY', 'QTY'], 'required'],
            [['CUST_CD', 'ENTRY_DATE', 'SEC_TYPE_CD', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['BEGIN_QTY', 'QTY'], 'number'],
            [['REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['SEC_CD'], 'string', 'max' => 255],
            [['TRANSACTION_CD'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'CUST_CD' => 'Khách hàng',
            'ENTRY_DATE' => 'Ngày nhập kho',
            'SEC_CD' => 'Mã CCQ',
            'SEC_TYPE_CD' => 'Loại chứng khoán',
            'BEGIN_QTY' => 'Tổng số nhập kho ban đầu',
            'QTY' => 'Số dư hiện tại',
            'TRANSACTION_CD' => 'Đơn vị quản lý khách hàng',
            'REG_DATE_TIME' => 'Thời gian tạo',
            'REG_USER_ID' => 'Người tạo',
            'UPD_DATE_TIME' => 'Thời gian cập nhật',
            'UPD_USER_ID' => 'Người cập nhật',
        ];
    }
}
