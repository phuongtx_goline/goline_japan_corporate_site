<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contract_no".
 *
 * @property integer $id
 * @property string $id_pi
 * @property string $id_hsa
 */
class ContractNo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contract_no';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pi', 'id_hsa'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_pi' => 'Id Pi',
            'id_hsa' => 'Id Hsa',
        ];
    }
}
