<?php

namespace common\models;

use Yii;

/**
* This is the model class for table "CORTCUSTOMER_ENTRUST".
*
    * @property integer $ID
    * @property integer $CUST_CD
    * @property string $ENTRUST_CUST_NAME
    * @property string $ENTRUST_CUST_NO
    * @property resource $SING_DOC
    * @property integer $APPROVAL_CD
    * @property string $ID_NUMBER
    * @property integer $ISSUE_LOCATION_CD
    * @property integer $ISSUE_DATE
    * @property integer $BIRTHDAY
    * @property integer $SEX
    * @property string $ADDR
    * @property integer $TEL_NO
    * @property string $EMAIL_ADRS
    * @property integer $ENTRUST_START_DATE
    * @property integer $ENTRUST_END_DATE
    * @property integer $ENTRUST_SCOPE
    * @property string $COUNTRY_CD
    * @property integer $STATUS
    * @property string $REMARKS
    * @property string $REG_DATE_TIME
    * @property integer $REG_USER_ID
    * @property string $UPD_DATE_TIME
    * @property integer $UPD_USER_ID
*/
class CORTCUSTOMERENTRUST extends \yii\db\ActiveRecord
{
/**
* @inheritdoc
*/
public static function tableName()
{
return 'CORTCUSTOMER_ENTRUST';
}

/**
* @inheritdoc
*/
public function rules()
{
return [
            [['CUST_CD', 'ENTRUST_CUST_NAME', 'APPROVAL_CD', 'ID_NUMBER', 'ISSUE_LOCATION_CD', 'ISSUE_DATE', 'ENTRUST_SCOPE', 'COUNTRY_CD', 'STATUS'], 'required'],
            [['CUST_CD', 'APPROVAL_CD', 'ISSUE_LOCATION_CD', 'ISSUE_DATE', 'BIRTHDAY', 'SEX', 'TEL_NO', 'ENTRUST_START_DATE', 'ENTRUST_END_DATE', 'ENTRUST_SCOPE', 'STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['SING_DOC', 'REMARKS'], 'string'],
            [['REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['ENTRUST_CUST_NAME', 'ID_NUMBER', 'ADDR', 'EMAIL_ADRS'], 'string', 'max' => 255],
            [['ENTRUST_CUST_NO'], 'string', 'max' => 10],
            [['COUNTRY_CD'], 'string', 'max' => 5],
        ];
}

/**
* @inheritdoc
*/
public function attributeLabels()
{
return [
    'ID' => 'ID',
    'CUST_CD' => 'CORTCUSTOMER.CUST_CD',
    'ENTRUST_CUST_NAME' => 'Tên người ủy quyền',
    'ENTRUST_CUST_NO' => 'Số TKGDCK người ủy quyền',
    'SING_DOC' => 'Chữ ký',
    'APPROVAL_CD' => 'Loại CMND',
    'ID_NUMBER' => 'Số CMND/CCCD/HC',
    'ISSUE_LOCATION_CD' => 'Nơi cấp',
    'ISSUE_DATE' => 'Ngày cấp',
    'BIRTHDAY' => 'Ngày sinh',
    'SEX' => 'Giới tính',
    'ADDR' => 'Địa chỉ thường trú',
    'TEL_NO' => 'Số điện thoại',
    'EMAIL_ADRS' => 'Email',
    'ENTRUST_START_DATE' => 'Ngày bắt đầu UQ',
    'ENTRUST_END_DATE' => 'Ngày kết thúc UQ',
    'ENTRUST_SCOPE' => 'Phạm vi ủy quyền',
    'COUNTRY_CD' => 'Quốc tịch',
    'STATUS' => 'Trạng thái',
    'REMARKS' => 'Ghi chú',
    'REG_DATE_TIME' => 'Thời gian tạo',
    'REG_USER_ID' => 'Người tạo',
    'UPD_DATE_TIME' => 'Thời gian cập nhật',
    'UPD_USER_ID' => 'Người cập nhật',
];
}
}
