<?php

namespace common\models;

use common\colorgb\ColorgbHelper;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user_group".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $remarks
 * @property integer $status
 * @property string $reg_date_time
 * @property integer $reg_user_id
 * @property string $reg_ip_addr
 * @property string $upd_date_time
 * @property integer $upd_user_id
 * @property string $upd_ip_addr
 * @property integer $customer_group_id
 */
class UserGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_group';
    }

    public $customer_group_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name'], 'required'],
            [['status', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['reg_date_time', 'upd_date_time'], 'safe'],
            [['code'], 'string', 'max' => 30],
            [['name', 'remarks'], 'string', 'max' => 200],
            [['reg_ip_addr', 'upd_ip_addr'], 'string', 'max' => 50],

            ['code', 'trim'],
            ['code', 'unique', 'targetClass' => '\common\models\UserGroup'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Mã nhóm',
            'name' => 'Tên nhóm',
            'remarks' => 'Ghi chú',
            'status' => 'Trạng thái',
            'reg_date_time' => 'Thời gian tạo',
            'reg_user_id' => 'Người tạo',
            'reg_ip_addr' => 'IP tạo',
            'upd_date_time' => 'Thời gian cập nhật',
            'upd_user_id' => 'Người cập nhật',
            'upd_ip_addr' => 'IP cập nhật',
            'customer_group_id' => 'Nhóm khách hàng',
        ];
    }


    /**
     *
     */
    public function saveCustomerGroup()
    {

        // Declare
        $recordStatus = ArrayHelper::map(Yii::$app->params['recordStatus'], 'key', 'value');
        UserCustomerGroup::updateAll(['status' => $recordStatus['deleted']], ['user_group_id' => $this->id]);

        // Check is array
        if (is_array($this->customer_group_id)) {

            foreach ($this->customer_group_id as $value) {

                // Customer customer group
                $userCustomerGroup = UserCustomerGroup::findOne(['customer_group_id' => $value, 'user_group_id' => $this->id]);
                if ($userCustomerGroup) {
                    $model = $userCustomerGroup;
                    $temp = ArrayHelper::toArray($userCustomerGroup);
                } else {
                    $model = new UserCustomerGroup();
                }

                // Save data
                $model->customer_group_id = $value;
                $model->user_group_id = $this->id;
                $model->status = $recordStatus['active'];
                $model->save();

                // Customer customer group
                if ($userCustomerGroup) {

                    // Save data
                    ColorgbHelper::setDataLog($model, $temp);

                } else {

                    // Save data
                    ColorgbHelper::setDataLog($model);
                }

            }
        }

        // Return
        return true;
    }
}
