<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "customer".
 *
 * @property integer $id
 * @property string $mobile
 * @property string $password
 * @property string $name
 * @property string $email_addr
 * @property integer $org_type
 * @property integer $foreign_type
 * @property string $nationality
 * @property integer $gender
 * @property string $prefix
 * @property string $birthday
 * @property string $address1
 * @property string $address2
 * @property string $tax_code
 * @property string $id_number
 * @property string $id_issue_date
 * @property string $id_expired_date
 * @property string $id_place
 * @property string $bank_name
 * @property string $bank_branch_name
 * @property string $bank_acco_no
 * @property string $bank_acco_name
 * @property string $mobile1
 * @property string $mobile2
 * @property string $email_addr1
 * @property string $email_addr2
 * @property string $image
 * @property integer $parent_id
 * @property integer $customer_group_id
 * @property integer $info_source_id
 * @property integer $cared_user_id
 * @property integer $intro_user_id
 * @property string $org_info
 * @property integer $profession_id
 * @property string $character
 * @property string $remarks
 * @property integer $status
 * @property integer $is_locked
 * @property integer $need_change_pw
 * @property string $last_login_time
 * @property string $active_date_time
 * @property integer $active_user_id
 * @property string $active_ip_addr
 * @property string $close_date_time
 * @property integer $close_user_id
 * @property string $close_ip_addr
 * @property string $password_reset_token
 * @property string $auth_key
 * @property string $reg_date_time
 * @property integer $reg_user_id
 * @property string $reg_ip_addr
 * @property string $upd_date_time
 * @property integer $upd_user_id
 * @property string $upd_ip_addr
 * @property string $group
 * @property string $contract_info
 * @property string $created_at
 * @property string $file_upload
 */
class Customer extends ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer';
    }

    public $group;
    public $contract_info;
    public $created_at;
    public $updated_at;
    public $file_upload;
    public $contract_inactive;
    public $contract_active;
    public $contract_settled;
    public $contract_expire;
    public $contract_deleted;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mobile', 'name', 'org_type', 'foreign_type', 'gender','email_addr'], 'required'],
            [['org_type', 'foreign_type', 'gender', 'parent_id', 'customer_group_id', 'info_source_id', 'cared_user_id', 'intro_user_id', 'profession_id', 'status', 'is_locked', 'need_change_pw', 'active_user_id', 'close_user_id', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['birthday', 'id_issue_date', 'id_expired_date'], 'number'],
            [['type','last_login_time', 'active_date_time', 'close_date_time', 'reg_date_time', 'upd_date_time','image'], 'safe'],
            [['mobile', 'mobile1', 'mobile2'], 'string', 'max' => 12],
            [['password', 'bank_name'], 'string', 'max' => 100],
            [['name', 'id_place', 'bank_branch_name', 'bank_acco_name', 'remarks'], 'string', 'max' => 200],
            [['email_addr', 'nationality', 'email_addr1', 'email_addr2', 'active_ip_addr', 'close_ip_addr', 'reg_ip_addr', 'upd_ip_addr'], 'string', 'max' => 50],
            [['prefix'], 'string', 'max' => 20],
            [['address1', 'address2', 'character'], 'string', 'max' => 300],
            [['tax_code', 'id_number', 'bank_acco_no'], 'string', 'max' => 30],
            [['password_reset_token', 'auth_key'], 'string', 'max' => 255],
            [['org_info'], 'string', 'max' => 500],

            [['password'], 'string', 'min' => 6],

            [['mobile1', 'mobile2'], 'trim'],
            [['mobile1'], 'unique', 'targetClass' => '\common\models\Customer'],

            [['email_addr', 'email_addr1', 'email_addr2'], 'trim'],
            [['email_addr', 'email_addr1', 'email_addr2'], 'email'],
            [['email_addr'], 'unique', 'targetClass' => '\common\models\Customer'],
            [['file_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'doc, docx, pdf, png, gif, jpg, jpeg'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mobile' => 'Số điện thoại',
            'password' => 'Mật khẩu',
            'name' => 'Họ tên KH',
            'email_addr' => 'Địa chỉ email',
            'org_type' => 'Cá nhân/Tổ chức',
            'foreign_type' => 'Loại tài khoản',
            'nationality' => 'Quốc gia',
            'gender' => 'Giới tính',
            'prefix' => 'Danh xưng',
            'birthday' => 'Ngày sinh',
            'address1' => 'Địa chỉ 1',
            'address2' => 'Địa chỉ 2',
            'tax_code' => 'Mã số thuế',
            'id_number' => 'Số CMT',
            'id_issue_date' => 'Ngày cấp',
            'id_expired_date' => 'Ngày hết hạn',
            'id_place' => 'Nơi cấp',
            'bank_name' => 'Ngân hàng',
            'bank_branch_name' => 'Chi nhánh ngân hàng',
            'bank_acco_no' => 'Số TK ngân hàng',
            'bank_acco_name' => 'Tên chủ TK ngân hàng',
            'mobile1' => 'Số điện thoại phụ 1',
            'mobile2' => 'Số điện thoại phụ 2',
            'email_addr1' => 'Địa chỉ email phụ 1',
            'email_addr2' => 'Địa chỉ email phụ 2',
            'image' => 'Ảnh đại diện',
            'file_upload' => 'Ảnh đại diện',
            'parent_id' => 'KH cấp trên',
            'customer_group_id' => 'Nhóm KH',
            'info_source_id' => 'Nguồn thông tin',
            'cared_user_id' => 'Nhân viên CSKH',
            'intro_user_id' => 'Nhân viên giới thiệu',
            'org_info' => 'Thông tin tổ chức',
            'profession_id' => 'Nghề nghiệp',
            'character' => 'Đặc điểm',
            'remarks' => 'Ghi chú',
            'status' => 'Trạng thái',
            'type' => 'Loại KH',
            'is_locked' => 'Khóa đăng nhập',
            'need_change_pw' => 'Yêu cầu đổi mật khẩu',
            'last_login_time' => 'Thời điểm đăng nhập lần cuối',
            'active_date_time' => 'Thời điểm kích hoạt KH',
            'active_user_id' => 'Người kích hoạt KH',
            'active_ip_addr' => 'IP máy kích hoạt KH',
            'close_date_time' => 'Thời điểm đóng KH',
            'close_user_id' => 'Người đóng KH',
            'close_ip_addr' => 'IP máy đóng KH',
            'password_reset_token' => 'Mã thiết lập lại mật khẩu',
            'auth_key' => 'Khóa xác thực tài khoản',
            'reg_date_time' => 'Thời gian tạo',
            'reg_user_id' => 'Người tạo',
            'reg_ip_addr' => 'IP máy tạo',
            'upd_date_time' => 'Thời gian cập nhật',
            'upd_user_id' => 'Người cập nhật',
            'upd_ip_addr' => 'IP máy cập nhật',
            'contract_type' => 'Dự kiến đầu tư',
            'invest_value' => 'Số vốn đầu tư',
            'contract_info' => 'Các hợp đồng',
            'group' => 'Nhóm khách hàng',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * Join with contract
     */
    public function getcontract()
    {
        return $this->hasMany(Contract::className(), ['customer_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        $customerStatus['active'] = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value')['active'];
        return static::findOne([
            'id' => $id,
            'status' => $customerStatus['active'],
            'is_locked' => 0
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        $customerStatus['active'] = ArrayHelper::map(Yii::$app->params['customerStatus'], 'key', 'value')['active'];
        return static::findOne([
            'password_reset_token' => $token,
            'status' => $customerStatus['active'],
            'is_locked' => 0
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['userPasswordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return md5($password) == $this->password ? true : false;
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = md5($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }


}
