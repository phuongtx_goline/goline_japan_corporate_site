<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "msg_send_request".
 *
 * @property integer $id
 * @property string $date
 * @property integer $msg_type
 * @property integer $send_type
 * @property string $send_to
 * @property string $from_address
 * @property string $from_name
 * @property string $subject
 * @property string $content
 * @property string $attachments
 * @property string $remarks
 * @property integer $status
 * @property integer $failed_count
 * @property string $reg_date_time
 * @property integer $reg_user_id
 * @property string $reg_ip_addr
 * @property string $upd_date_time
 * @property integer $upd_user_id
 * @property string $upd_ip_addr
 */
class MsgSendRequest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msg_send_request';
    }

    public $colorgb_date;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'msg_type', 'send_type', 'send_to', 'status'], 'required'],
            [['date'], 'number'],
            [['msg_type', 'send_type', 'status', 'failed_count', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['content'], 'string'],
            [['colorgb_date','reg_date_time', 'upd_date_time'], 'safe'],
            [['send_to', 'subject'], 'string', 'max' => 500],
            [['from_address'], 'string', 'max' => 50],
            [['from_name', 'remarks'], 'string', 'max' => 200],
            [['attachments'], 'string', 'max' => 1000],
            [['reg_ip_addr', 'upd_ip_addr'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Ngày gửi',
            'colorgb_date' => 'Ngày gửi',
            'msg_type' => 'Loại thông báo',
            'send_type' => 'Cách gửi',
            'send_to' => 'Danh sách người nhận',
            'from_address' => 'Địa chỉ mail của người gửi',
            'from_name' => 'Tên người gửi',
            'subject' => 'Tiêu đề',
            'content' => 'Nội dung',
            'attachments' => 'Tệp đính kèm',
            'remarks' => 'Ghi chú',
            'status' => 'Trạng thái',
            'failed_count' => 'Số lần gửi lỗi',
            'reg_date_time' => 'Thời gian tạo',
            'reg_user_id' => 'Người tạo',
            'reg_ip_addr' => 'IP tạo',
            'upd_date_time' => 'Thời gian cập nhật',
            'upd_user_id' => 'Người cập nhật',
            'upd_ip_addr' => 'IP cập nhật',
        ];
    }
}
