<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "CORTSEC_TRANSACTION".
 *
 * @property integer $ID
 * @property integer $ALLO_DATE
 * @property integer $CUST_CD
 * @property string $SEC_CD
 * @property integer $ACTION
 * @property integer $SEC_TYPE_CD
 * @property string $QUANTITY
 * @property string $PRICE
 * @property string $TRANSACTION_CD
 * @property string $REMARKS
 * @property integer $STATUS
 * @property string $REG_DATE_TIME
 * @property integer $REG_USER_ID
 * @property string $UPD_DATE_TIME
 * @property integer $UPD_USER_ID
 */
class CORTSECTRANSACTION extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CORTSEC_TRANSACTION';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ALLO_DATE', 'CUST_CD', 'SEC_CD', 'ACTION', 'SEC_TYPE_CD', 'QUANTITY', 'PRICE', 'TRANSACTION_CD', 'STATUS'], 'required'],
            [['ALLO_DATE', 'CUST_CD', 'ACTION', 'SEC_TYPE_CD', 'STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['QUANTITY', 'PRICE'], 'number'],
            [['REMARKS'], 'string'],
            [['REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['SEC_CD'], 'string', 'max' => 255],
            [['TRANSACTION_CD'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ALLO_DATE' => 'Ngày tạo',
            'CUST_CD' => 'Mã khách hàng',
            'SEC_CD' => 'Mã cổ phiếu',
            'ACTION' => 'Thao tác',
            'SEC_TYPE_CD' => 'Loại CCQ',
            'QUANTITY' => 'Số lượng CCQ',
            'PRICE' => 'Giá (lưu đơn vị 1đ)',
            'TRANSACTION_CD' => 'Đơn vị quản lý khách hàng',
            'REMARKS' => 'Ghi chú',
            'STATUS' => 'Trạng thái',
            'REG_DATE_TIME' => 'Thời gian tạo',
            'REG_USER_ID' => 'Người tạo',
            'UPD_DATE_TIME' => 'Thời gian cập nhật',
            'UPD_USER_ID' => 'Người cập nhật',
        ];
    }
}
