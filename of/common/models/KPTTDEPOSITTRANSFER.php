<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "KPTTDEPOSIT_TRANSFER".
 *
 * @property integer $ID
 * @property integer $ALLO_DATE
 * @property integer $TRANSFER_TYPE
 * @property string $CUST_NO_FROM
 * @property string $CUST_NAME_FROM
 * @property string $CUST_ID_NO_FROM
 * @property integer $CUST_ISSUE_DATE_FROM
 * @property integer $CUST_ISSUE_LOCATION_FROM
 * @property string $CUST_NO_TO
 * @property string $CUST_NAME_TO
 * @property string $CUST_ID_NO_TO
 * @property integer $CUST_ISSUE_DATE_TO
 * @property integer $CUST_ISSUE_LOCATION_TO
 * @property integer $TRANSFER_DATE
 * @property integer $PERMIT_DATE
 * @property integer $REASON_ID
 * @property integer $STATUS
 * @property string $TRANSACTION_CD
 * @property string $REMARKS
 * @property string $REG_DATE_TIME
 * @property integer $REG_USER_ID
 * @property string $UPD_DATE_TIME
 * @property integer $UPD_USER_ID
 */
class KPTTDEPOSITTRANSFER extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'KPTTDEPOSIT_TRANSFER';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ALLO_DATE', 'TRANSFER_TYPE', 'CUST_NO_FROM', 'CUST_NAME_FROM', 'CUST_ID_NO_FROM', 'CUST_ISSUE_DATE_FROM', 'CUST_ISSUE_LOCATION_FROM', 'CUST_NO_TO', 'CUST_NAME_TO', 'CUST_ID_NO_TO', 'CUST_ISSUE_DATE_TO', 'CUST_ISSUE_LOCATION_TO', 'TRANSFER_DATE', 'PERMIT_DATE', 'REASON_ID', 'STATUS'], 'required'],
            [['ALLO_DATE', 'TRANSFER_TYPE', 'CUST_ISSUE_DATE_FROM', 'CUST_ISSUE_LOCATION_FROM', 'CUST_ISSUE_DATE_TO', 'CUST_ISSUE_LOCATION_TO', 'TRANSFER_DATE', 'PERMIT_DATE', 'REASON_ID', 'STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['REMARKS'], 'string'],
            [['REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['CUST_NO_FROM', 'CUST_NAME_FROM', 'CUST_ID_NO_FROM', 'CUST_NO_TO', 'CUST_NAME_TO', 'CUST_ID_NO_TO'], 'string', 'max' => 255],
            [['TRANSACTION_CD'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ALLO_DATE' => 'Ngày tạo',
            'TRANSFER_TYPE' => 'Loại giao dịch',
            'CUST_NO_FROM' => 'Số TK chuyển',
            'CUST_NAME_FROM' => 'Họ và tên bên chuyển',
            'CUST_ID_NO_FROM' => 'Số CMND/CCCD/HC',
            'CUST_ISSUE_DATE_FROM' => 'Ngày cấp',
            'CUST_ISSUE_LOCATION_FROM' => 'Nơi cấp',
            'CUST_NO_TO' => 'Số tài khoản bên nhận',
            'CUST_NAME_TO' => 'Họ và tên bên nhận',
            'CUST_ID_NO_TO' => 'CMND bên nhận',
            'CUST_ISSUE_DATE_TO' => 'Ngày cấp',
            'CUST_ISSUE_LOCATION_TO' => 'Nơi cấp',
            'TRANSFER_DATE' => 'Ngày chuyển',
            'PERMIT_DATE' => 'Ngày hiệu lực VSD',
            'REASON_ID' => 'Lý do',
            'STATUS' => 'Trạng thái',
            'TRANSACTION_CD' => 'Giao dịch',
            'REMARKS' => 'Ghi chú',
            'REG_DATE_TIME' => 'Thời gian tạo',
            'REG_USER_ID' => 'Người tạo',
            'UPD_DATE_TIME' => 'Thời gian cập nhật',
            'UPD_USER_ID' => 'Người cập nhật',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKPTTDEPOSITTRANSFERDETAIL()
    {
        return $this->hasMany(KPTTDEPOSITTRANSFERDETAIL::className(), ['PARENT_ID' => 'ID']);
    }
}
