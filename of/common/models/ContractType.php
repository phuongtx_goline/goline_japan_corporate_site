<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contract_type".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property integer $period_type
 * @property integer $period_val
 * @property string $min_invest_value
 * @property string $max_invest_value
 * @property string $base_profit_rate
 * @property string $profit_rate
 * @property string $file_template_url
 * @property string $remarks
 * @property integer $status
 * @property string $reg_date_time
 * @property integer $reg_user_id
 * @property string $reg_ip_addr
 * @property string $upd_date_time
 * @property integer $upd_user_id
 * @property string $upd_ip_addr
 * @property string $file_upload
 */
class ContractType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contract_type';
    }

    public $file_upload;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name', 'period_type', 'period_val', 'min_invest_value', 'max_invest_value', 'base_profit_rate', 'profit_rate', 'status'], 'required'],
            [['period_type', 'period_val', 'status', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['min_invest_value', 'max_invest_value', 'base_profit_rate', 'profit_rate'], 'number'],
            [['reg_date_time', 'upd_date_time'], 'safe'],
            [['code'], 'string', 'max' => 50],
            [['name', 'file_template_url', 'remarks'], 'string', 'max' => 200],
            [['reg_ip_addr', 'upd_ip_addr'], 'string', 'max' => 50],
            [['file_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'doc, docx, pdf, png, gif, jpg, jpeg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Mã gói',
            'name' => 'Tên gói',
            'period_type' => 'Loại kỳ hạn',
            'period_val' => 'Giá trị kỳ hạn',
            'min_invest_value' => 'Giá trị đầu tư tối thiểu',
            'max_invest_value' => 'Giá trị đầu tư tối đa',
            'base_profit_rate' => 'Tỷ lệ doanh thu cơ sở',
            'profit_rate' => 'Tỷ lệ lợi nhuận KH được nhận',
            'file_template_url' => 'HĐ mẫu',
            'file_upload' => 'HĐ mẫu',
            'remarks' => 'Ghi chú',
            'status' => 'Trạng thái',
            'reg_date_time' => 'Thời gian tạo',
            'reg_user_id' => 'Người tạo',
            'reg_ip_addr' => 'IP tạo',
            'upd_date_time' => 'Thời gian cập nhật',
            'upd_user_id' => 'Người cập nhật',
            'upd_ip_addr' => 'IP cập nhật',
        ];
    }
}
