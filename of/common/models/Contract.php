<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contract".
 *
 * @property integer $id
 * @property integer $bank_id
 * @property integer $customer_id
 * @property integer $active_date
 * @property string $contract_no
 * @property string $transfer_note
 * @property string $representative
 * @property string $customer_name
 * @property string $fund_net_asset
 * @property string $fund_unit_qty
 * @property string $fund_unit_price
 * @property string $invest_value
 * @property string $invest_unit_qty
 * @property integer $contract_type_id
 * @property string $contract_type_code
 * @property string $contract_type_name
 * @property integer $period_type
 * @property integer $period_val
 * @property string $base_profit_rate
 * @property string $profit_rate
 * @property string $contract_date
 * @property string $begin_date
 * @property string $end_date_est
 * @property string $end_date_real
 * @property string $new_end_date
 * @property string $settle_fee_amt
 * @property string $tax_rate
 * @property string $profit_amt
 * @property string $recv_amt
 * @property string $remarks
 * @property integer $status
 * @property integer $annouce_status
 * @property integer $response_status
 * @property string $response_content
 * @property string $active_date_time
 * @property integer $active_user_id
 * @property string $settle_date_time
 * @property integer $settle_user_id
 * @property string $reg_date_time
 * @property integer $reg_user_id
 * @property string $reg_ip_addr
 * @property string $upd_date_time
 * @property integer $upd_user_id
 * @property string $upd_ip_addr
 */
class Contract extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contract';
    }

    public $active_date;
    public $contract_info;
    public $customer_name;
    public $new_end_date;
    public $colorgb_date;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'invest_value', 'contract_type_id', 'status'], 'required'],
            [['customer_id', 'contract_type_id', 'period_type', 'period_val', 'status', 'annouce_status', 'response_status', 'active_user_id', 'settle_user_id', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['fund_net_asset', 'fund_unit_qty', 'fund_unit_price', 'invest_value', 'contract_date', 'begin_date', 'end_date_est', 'end_date_real', 'end_date_real', 'settle_fee_amt'], 'number'],
            [['bank_id','transfer_note','colorgb_date','active_date_time', 'settle_date_time', 'reg_date_time', 'upd_date_time', 'tax_rate', 'invest_unit_qty', 'profit_amt', 'recv_amt', 'settle_fee_amt', 'contract_type_code', 'contract_type_name', 'contract_no', 'contract_date', 'end_date_est', 'base_profit_rate', 'profit_rate', 'new_end_date', 'send_contract','representative'], 'safe'],
            [['contract_no', 'contract_type_code'], 'string', 'max' => 50],
            [['contract_type_name', 'remarks'], 'string', 'max' => 200],
            [['response_content'], 'string', 'max' => 500],
            [['reg_ip_addr', 'upd_ip_addr'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'active_date' => 'Ngày kích hoạt HĐ',
            'bank_id' => 'Ngân hàng',
            'transfer_note' => 'Nội dung chuyển khoản',
            'customer_id' => 'Khách hàng',
            'customer_name' => 'Khách hàng',
            'representative' => 'Người đại diện',
            'contract_no' => 'Số HĐ',
            'fund_net_asset' => 'Tổng GTTS ròng tại thời điểm ký hợp đồng',
            'fund_unit_qty' => 'Số lượng ĐVĐT tại thời điểm ký hợp đồng',
            'fund_unit_price' => 'Giá của 1 ĐVĐT tại thời điểm ký hợp đồng',
            'invest_value' => 'Số vốn đầu tư',
            'invest_unit_qty' => 'Số lượng ĐVĐT',
            'contract_type_id' => 'Loại HĐ',
            'contract_type_code' => 'Mã gói',
            'contract_type_name' => 'Tên gói',
            'period_type' => 'Loại kỳ hạn',
            'period_val' => 'Giá trị kỳ hạn',
            'base_profit_rate' => 'Tỷ lệ doanh thu cơ sở',
            'profit_rate' => 'Tỷ lệ lợi nhuận KH được nhận',
            'contract_date' => 'Ngày tạo HĐ',
            'begin_date' => 'Ngày ký HĐ',
            'colorgb_date' => 'Ngày ký HĐ',
            'end_date_est' => 'Ngày kết thúc dự kiến',
            'end_date_real' => 'Ngày kết thúc thực tế',
            'new_end_date' => 'Ngày kết thúc dự kiến sau khi gia hạn thành công',
            'settle_fee_amt' => 'Tiền phí tất toán',
            'tax_rate' => 'Tỷ lệ thuế',
            'profit_amt' => 'Lãi lỗ tại thời điểm tất toán',
            'recv_amt' => 'Tiền khách hàng được nhận khi tất toán',
            'remarks' => 'Ghi chú',
            'status' => 'Trạng thái HĐ',
            'response_status' => 'Trạng thái phản hồi',
            'response_content' => 'Nội dung phản hồi',
            'active_date_time' => 'Thời gian kích hoạt HĐ',
            'active_user_id' => 'Người kích hoạt HĐ',
            'settle_date_time' => 'Thời gian tất toán',
            'settle_user_id' => 'Người tất toán HĐ',
            'reg_date_time' => 'Thời gian tạo',
            'reg_user_id' => 'Người tạo',
            'reg_ip_addr' => 'IP tạo',
            'upd_date_time' => 'Thời gian cập nhật',
            'upd_user_id' => 'Người cập nhật',
            'upd_ip_addr' => 'IP cập nhật',
            'contract_info' => 'Thông tin HĐ',
            'annouce_status' => 'Gửi thông báo cho khách hàng',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * Join with customer
     */
    public function getcustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * Join with contract_type
     */
    public function getcontract_type()
    {
        return $this->hasOne(ContractType::className(), ['id' => 'contract_type_id']);
    }

}
