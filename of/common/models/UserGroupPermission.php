<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_group_permission".
 *
 * @property integer $id
 * @property integer $user_group_id
 * @property string $role_cd
 * @property integer $status
 * @property string $reg_date_time
 * @property integer $reg_user_id
 * @property string $reg_ip_addr
 * @property string $upd_date_time
 * @property integer $upd_user_id
 * @property string $upd_ip_addr
 */
class UserGroupPermission extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_group_permission';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_group_id', 'role_cd', 'status'], 'required'],
            [['user_group_id', 'status', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['reg_date_time', 'upd_date_time'], 'safe'],
            [['role_cd'], 'string', 'max' => 100],
            [['reg_ip_addr', 'upd_ip_addr'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_group_id' => 'Nhóm người dùng',
            'role_cd' => 'Mã quyền',
            'status' => 'Trạng thái',
            'reg_date_time' => 'Thời gian tạo',
            'reg_user_id' => 'Người tạo',
            'reg_ip_addr' => 'IP tạo',
            'upd_date_time' => 'Thời gian cập nhật',
            'upd_user_id' => 'Người cập nhật',
            'upd_ip_addr' => 'IP cập nhật',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * Join with customer
     */
    public function getpermission()
    {
        return $this->hasOne(Permission::className(), ['role_cd' => 'role_cd']);
    }
}
