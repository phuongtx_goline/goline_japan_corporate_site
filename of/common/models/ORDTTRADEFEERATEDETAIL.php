<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ORDTTRADE_FEE_RATE_DETAIL".
 *
 * @property integer $ID
 * @property integer $FEE_RATE_ID
 * @property integer $VALID_DATE
 * @property string $FROM_VALUE
 * @property string $FEE_RATE
 * @property integer $STATUS
 * @property string $REG_DATE_TIME
 * @property integer $REG_USER_ID
 * @property string $UPD_DATE_TIME
 * @property integer $UPD_USER_ID
 */
class ORDTTRADEFEERATEDETAIL extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ORDTTRADE_FEE_RATE_DETAIL';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FEE_RATE_ID', 'VALID_DATE', 'FROM_VALUE', 'FEE_RATE', 'STATUS'], 'required'],
            [['FEE_RATE_ID', 'VALID_DATE', 'STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['FROM_VALUE', 'FEE_RATE'], 'number'],
            [['REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'FEE_RATE_ID' => 'Link sang bảng ORDTTRADE_FEE_RATE',
            'VALID_DATE' => 'Ngày hiệu lực (không cho sửa đã duyệt)',
            'FROM_VALUE' => 'Từ giá trị /số tháng',
            'FEE_RATE' => 'Tỷ lệ phí',
            'STATUS' => 'Trạng thái',
            'REG_DATE_TIME' => 'Thời gian tạo',
            'REG_USER_ID' => 'Người tạo',
            'UPD_DATE_TIME' => 'Thời gian cập nhật',
            'UPD_USER_ID' => 'Người cập nhật',
        ];
    }
}
