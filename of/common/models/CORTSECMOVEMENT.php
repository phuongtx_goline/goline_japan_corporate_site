<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "CORTSEC_MOVEMENT".
 *
 * @property integer $ID
 * @property integer $ALLO_DATE
 * @property integer $ENTRY_DATE
 * @property integer $CUST_CD
 * @property string $SEC_CD
 * @property string $TASK_CD
 * @property integer $SRC_NO
 * @property string $SRC_TRANSACTION_CD
 * @property integer $SEC_TYPE_CD
 * @property integer $TRADE_TYPE
 * @property string $QUANTITY
 * @property string $PRICE
 * @property string $TRANSACTION_CD
 * @property string $REMARKS
 * @property integer $STATUS
 * @property string $REG_DATE_TIME
 * @property integer $REG_USER_ID
 * @property string $UPD_DATE_TIME
 * @property integer $UPD_USER_ID
 */
class CORTSECMOVEMENT extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CORTSEC_MOVEMENT';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ALLO_DATE', 'ENTRY_DATE', 'CUST_CD', 'SEC_CD', 'TASK_CD', 'SRC_NO', 'SRC_TRANSACTION_CD', 'SEC_TYPE_CD', 'TRADE_TYPE', 'QUANTITY', 'STATUS'], 'required'],
            [['ALLO_DATE', 'ENTRY_DATE', 'CUST_CD', 'SRC_NO', 'SEC_TYPE_CD', 'TRADE_TYPE', 'STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['QUANTITY', 'PRICE'], 'number'],
            [['REMARKS'], 'string'],
            [['REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['SEC_CD'], 'string', 'max' => 255],
            [['TASK_CD', 'SRC_TRANSACTION_CD', 'TRANSACTION_CD'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ALLO_DATE' => 'Ngày tạo',
            'ENTRY_DATE' => 'Ngày hiệu lực',
            'CUST_CD' => 'CORTCUSTOMER.CUST_CD',
            'SEC_CD' => 'Mã CCQ',
            'TASK_CD' => 'Thao tác',
            'SRC_NO' => 'ID bảng nghiệp vụ',
            'SRC_TRANSACTION_CD' => 'Đơn vị quản lý khách hàng',
            'SEC_TYPE_CD' => 'Loại chứng khoán',
            'TRADE_TYPE' => 'Phát sinh',
            'QUANTITY' => 'Số lượng CCQ',
            'PRICE' => 'Giá (lưu đơn vị 1đ)',
            'TRANSACTION_CD' => 'Số lượng CCQ',
            'REMARKS' => 'Ghi chú',
            'STATUS' => 'Trạng thái',
            'REG_DATE_TIME' => 'Thời gian tạo',
            'REG_USER_ID' => 'Người tạo',
            'UPD_DATE_TIME' => 'Thời gian cập nhật',
            'UPD_USER_ID' => 'Người cập nhật',
        ];
    }
}
