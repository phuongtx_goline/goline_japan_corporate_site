<?php

namespace common\models;

use Yii;

/**
* This is the model class for table "CORTFRONT_USER".
*
    * @property string $USER_ID
    * @property string $USER_NAME
    * @property string $ID_NUMBER
    * @property integer $ISSUE_LOCATION_CD
    * @property integer $ISSUE_DATE
    * @property integer $EXPIRE_DATE
    * @property string $MOBILE_NO
    * @property string $EMAIL_ADRS
    * @property string $TRANSACTION_CD
    * @property string $PASSWORD
    * @property string $PIN_CD
    * @property integer $LOGIN_PIN_TYPE
    * @property integer $TRADING_PIN_TYPE
    * @property string $PRE_ACCESS
    * @property string $LAST_ACCESS
    * @property integer $RETRIES
    * @property integer $TRADING_FLAG
    * @property integer $ACTIVE_FLAG
    * @property integer $USER_LOCK_FLAG
    * @property integer $CHANGE_PASS_FLAG
    * @property integer $CHANGE_PIN_FLAG
    * @property integer $PASSS_VALIDITY
    * @property integer $VALIDITY_DATE
    * @property string $REG_DATE_TIME
    * @property integer $REG_USER_ID
    * @property string $UPD_DATE_TIME
    * @property integer $UPD_USER_ID
*/
class CORTFRONTUSER extends \yii\db\ActiveRecord
{
/**
* @inheritdoc
*/
public static function tableName()
{
return 'CORTFRONT_USER';
}

/**
* @inheritdoc
*/
public function rules()
{
return [
            [['USER_ID', 'USER_NAME', 'ID_NUMBER', 'PASSWORD', 'PIN_CD', 'LOGIN_PIN_TYPE', 'TRADING_PIN_TYPE', 'ACTIVE_FLAG', 'USER_LOCK_FLAG', 'CHANGE_PASS_FLAG', 'CHANGE_PIN_FLAG'], 'required'],
            [['ISSUE_LOCATION_CD', 'ISSUE_DATE', 'EXPIRE_DATE', 'LOGIN_PIN_TYPE', 'TRADING_PIN_TYPE', 'RETRIES', 'TRADING_FLAG', 'ACTIVE_FLAG', 'USER_LOCK_FLAG', 'CHANGE_PASS_FLAG', 'CHANGE_PIN_FLAG', 'PASSS_VALIDITY', 'VALIDITY_DATE', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['PRE_ACCESS', 'LAST_ACCESS', 'REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['USER_ID'], 'string', 'max' => 10],
            [['USER_NAME', 'MOBILE_NO', 'EMAIL_ADRS', 'PASSWORD', 'PIN_CD'], 'string', 'max' => 255],
            [['ID_NUMBER'], 'string', 'max' => 20],
            [['TRANSACTION_CD'], 'string', 'max' => 3],
        ];
}

/**
* @inheritdoc
*/
public function attributeLabels()
{
return [
    'USER_ID' => 'CORCUSTOMER.CUST_NO',
    'USER_NAME' => 'CORCUSTOMER.CUST_NAME',
    'ID_NUMBER' => 'Số CMND/CCCD/HC',
    'ISSUE_LOCATION_CD' => 'Nơi cấp',
    'ISSUE_DATE' => 'Ngày cấp',
    'EXPIRE_DATE' => 'Ngày hết hạn',
    'MOBILE_NO' => 'Số di động',
    'EMAIL_ADRS' => 'Địa chỉ email',
    'TRANSACTION_CD' => 'Đơn vị quản lý khách hàng',
    'PASSWORD' => 'Mật khẩu',
    'PIN_CD' => 'Pin code - Đặt lệnh bán',
    'LOGIN_PIN_TYPE' => 'Xác thực cấp 2 - đăng nhập',
    'TRADING_PIN_TYPE' => 'Xác thực cấp 2 - đặt lệnh bán',
    'PRE_ACCESS' => 'Lần truy cập liền trước lần cuối cùng (n-1)',
    'LAST_ACCESS' => 'Lần truy cập cuối (n)',
    'RETRIES' => 'Số lần nhập sai mật khẩu khi đăng nhập',
    'TRADING_FLAG' => 'Cho phép đặt lệnh',
    'ACTIVE_FLAG' => 'Kích hoạt tài khoản',
    'USER_LOCK_FLAG' => 'Tạm khóa tài khoản',
    'CHANGE_PASS_FLAG' => 'Yêu cầu đổi mật khẩu',
    'CHANGE_PIN_FLAG' => 'Yêu cầu đổi PIN',
    'PASSS_VALIDITY' => 'Hiệu lực password - Chưa dùng',
    'VALIDITY_DATE' => 'Ngày hiệu lực password - Chưa dùng',
    'REG_DATE_TIME' => 'Thời gian tạo',
    'REG_USER_ID' => 'Người tạo',
    'UPD_DATE_TIME' => 'Thời gian cập nhật',
    'UPD_USER_ID' => 'Người cập nhật',
];
}
}
