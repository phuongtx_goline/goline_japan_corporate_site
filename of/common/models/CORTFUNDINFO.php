<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "CORTFUND_INFO".
 *
 * @property integer $ID
 * @property string $SEC_CD
 * @property string $SEC_NAME
 * @property string $IPO_PRICE
 * @property string $TOTAL_QTY
 * @property string $MIN_IPO_AMOUNT
 * @property string $MIN_BALANCE_QTY
 * @property string $TRANS_DATE_TYPE
 * @property string $WEEK_DAYS
 * @property string $TRANS_DATE
 * @property string $END_ORDER_DATE_NUM
 * @property string $END_ORDER_HOUR
 * @property integer $STATUS
 * @property string $REMARKS
 * @property string $REG_DATE_TIME
 * @property integer $REG_USER_ID
 * @property string $UPD_DATE_TIME
 * @property integer $UPD_USER_ID
 */
class CORTFUNDINFO extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CORTFUND_INFO';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['SEC_CD', 'SEC_NAME', 'IPO_PRICE', 'TOTAL_QTY', 'MIN_IPO_AMOUNT', 'MIN_BALANCE_QTY', 'TRANS_DATE_TYPE', 'END_ORDER_DATE_NUM', 'END_ORDER_HOUR', 'STATUS'], 'required'],
            [['END_ORDER_HOUR', 'REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['REMARKS'], 'string'],
            [['SEC_CD'], 'trim'],
            [['SEC_CD'], 'unique', 'targetClass' => '\common\models\CORTFUNDINFO'],
            [['SEC_NAME', 'IPO_PRICE', 'TOTAL_QTY', 'MIN_IPO_AMOUNT', 'MIN_BALANCE_QTY', 'TRANS_DATE_TYPE', 'WEEK_DAYS', 'TRANS_DATE', 'END_ORDER_DATE_NUM'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'SEC_CD' => 'Mã CCQ',
            'SEC_NAME' => 'Tên CCQ',
            'IPO_PRICE' => 'Giá phát hành',
            'TOTAL_QTY' => 'Tổng SL đang lưu hành',
            'MIN_IPO_AMOUNT' => 'GT mua tối thiểu',
            'MIN_BALANCE_QTY' => 'Số dư tối thiểu (CCQ)',
            'TRANS_DATE_TYPE' => 'Chu kỳ giao dịch',
            'WEEK_DAYS' => 'Thứ trong tuần',
            'TRANS_DATE' => 'Ngày GD',
            'END_ORDER_DATE_NUM' => 'Ngày chốt GD',
            'END_ORDER_HOUR' => 'Giờ chốt GD',
            'STATUS' => 'Trạng thái',
            'REMARKS' => 'Ghi chú',
            'REG_DATE_TIME' => 'Thời gian tạo',
            'REG_USER_ID' => 'Người tạo',
            'UPD_DATE_TIME' => 'Thời gian cập nhật',
            'UPD_USER_ID' => 'Người cập nhật',
        ];
    }
}
