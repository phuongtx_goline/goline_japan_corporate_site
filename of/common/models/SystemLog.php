<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "system_log".
 *
 * @property integer $id
 * @property string $log_time
 * @property string $table_name
 * @property string $action
 * @property string $before_value
 * @property string $after_value
 * @property string $reg_date_time
 * @property integer $reg_user_id
 * @property string $reg_ip_addr
 * @property string $upd_date_time
 * @property integer $upd_user_id
 * @property string $upd_ip_addr
 */
class SystemLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'system_log';
    }

    public $colorgb_date;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['log_time', 'table_name', 'action', 'after_value'], 'required'],
            [['colorgb_date','log_time', 'reg_date_time', 'upd_date_time'], 'safe'],
            [['before_value', 'after_value'], 'string'],
            [['reg_user_id', 'upd_user_id'], 'integer'],
            [['table_name', 'action'], 'string', 'max' => 255],
            [['reg_ip_addr', 'upd_ip_addr'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'log_time' => 'Thời điểm ghi nhận',
            'colorgb_date' => 'Thời điểm ghi nhận',
            'table_name' => 'Tên bảng',
            'action' => 'Thao tác',
            'before_value' => 'Giá trị trước khi sửa',
            'after_value' => 'Giá trị sau khi sửa',
            'reg_date_time' => 'Thời gian tạo',
            'reg_user_id' => 'Người thực hiện',
            'reg_ip_addr' => 'IP tạo',
            'upd_date_time' => 'Thời gian cập nhật',
            'upd_user_id' => 'Người cập nhật',
            'upd_ip_addr' => 'IP cập nhật',
        ];
    }
}
