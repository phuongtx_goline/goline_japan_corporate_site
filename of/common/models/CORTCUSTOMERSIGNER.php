<?php

namespace common\models;

use Yii;

/**
* This is the model class for table "CORTCUSTOMER_SIGNER".
*
    * @property integer $ID
    * @property integer $CUST_CD
    * @property integer $REF_NO
    * @property resource $SIGNER_DOC
    * @property string $REMARKS
    * @property integer $STATUS
    * @property string $REG_DATE_TIME
    * @property integer $REG_USER_ID
    * @property string $UPD_DATE_TIME
    * @property integer $UPD_USER_ID
*/
class CORTCUSTOMERSIGNER extends \yii\db\ActiveRecord
{
/**
* @inheritdoc
*/
public static function tableName()
{
return 'CORTCUSTOMER_SIGNER';
}

/**
* @inheritdoc
*/
public function rules()
{
return [
            [['CUST_CD', 'REF_NO', 'SIGNER_DOC', 'STATUS'], 'required'],
            [['CUST_CD', 'REF_NO', 'STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['SIGNER_DOC', 'REMARKS'], 'string'],
            [['REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
        ];
}

/**
* @inheritdoc
*/
public function attributeLabels()
{
return [
    'ID' => 'ID',
    'CUST_CD' => 'CORTCUSTOMER.CUST_CD',
    'REF_NO' => 'Số thứ tự',
    'SIGNER_DOC' => 'Chữ ký',
    'REMARKS' => 'Ghi chú',
    'STATUS' => 'Trạng thái',
    'REG_DATE_TIME' => 'Thời gian tạo',
    'REG_USER_ID' => 'Người tạo',
    'UPD_DATE_TIME' => 'Thời gian cập nhật',
    'UPD_USER_ID' => 'Người cập nhật',
];
}
}
