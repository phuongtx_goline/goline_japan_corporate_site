<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ORDTORDER".
 *
 * @property integer $ID
 * @property integer $ALLO_DATE
 * @property integer $TRADE_DATE
 * @property string $ORDER_NO
 * @property string $TRADE_TIME
 * @property string $ORD_TYPE
 * @property integer $ORD_CHANEL
 * @property string $SEC_CD
 * @property integer $TRADE_TYPE
 * @property string $ORD_QTY
 * @property string $ORD_AMT
 * @property string $CUST_CD
 * @property string $CUST_NO
 * @property string $CUST_NAME
 * @property string $CUST_TRANSACTION_CD
 * @property string $FEE_RATE
 * @property string $FEE_AMT
 * @property string $TAX_RATE
 * @property string $TAX_AMT
 * @property string $RIGHT_TAX_RATE
 * @property string $RIGHT_TAX_QTY
 * @property string $RIGHT_TAX_AMT
 * @property string $MAT_QTY
 * @property string $MAT_PRICE
 * @property string $MAT_AMT
 * @property string $NOTES
 * @property string $TRANSACTION_CD
 * @property string $BROKER_CD
 * @property integer $PAYMENT_DATE
 * @property integer $ENTRY_DATE
 * @property integer $STATUS
 * @property string $REG_DATE_TIME
 * @property integer $REG_USER_ID
 * @property string $UPD_DATE_TIME
 * @property integer $UPD_USER_ID
 * @property integer $SEND_EMAIL
 * @property integer $AMOUNT
 */
class ORDTORDER extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ORDTORDER';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ALLO_DATE', 'TRADE_DATE', 'ORDER_NO', 'TRADE_TIME', 'ORD_TYPE', 'ORD_CHANEL', 'SEC_CD', 'TRADE_TYPE', 'ORD_QTY', 'ORD_AMT', 'CUST_CD', 'CUST_NO', 'FEE_RATE', 'FEE_AMT', 'TAX_RATE', 'TAX_AMT', 'RIGHT_TAX_RATE', 'RIGHT_TAX_QTY', 'RIGHT_TAX_AMT', 'MAT_QTY', 'MAT_PRICE', 'MAT_AMT', 'PAYMENT_DATE', 'ENTRY_DATE', 'STATUS'], 'required'],
            [['ALLO_DATE', 'TRADE_DATE', 'ORD_CHANEL', 'TRADE_TYPE', 'PAYMENT_DATE', 'ENTRY_DATE', 'STATUS', 'REG_USER_ID', 'UPD_USER_ID','AMOUNT','SEND_EMAIL'], 'integer'],
            [['TRADE_TIME', 'REG_DATE_TIME', 'UPD_DATE_TIME','BROKER_CD'], 'safe'],
            [['ORD_QTY', 'ORD_AMT', 'CUST_CD', 'FEE_RATE', 'FEE_AMT', 'TAX_RATE', 'TAX_AMT', 'RIGHT_TAX_RATE', 'RIGHT_TAX_QTY', 'RIGHT_TAX_AMT', 'MAT_QTY', 'MAT_PRICE', 'MAT_AMT'], 'number'],
            [['ORDER_NO'], 'string', 'max' => 30],
            [['ORD_TYPE', 'CUST_TRANSACTION_CD', 'TRANSACTION_CD'], 'string', 'max' => 3],
            [['SEC_CD'], 'string', 'max' => 20],
            [['CUST_NO'], 'string', 'max' => 10],
            [['CUST_NAME', 'NOTES'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ALLO_DATE' => 'Ngày đặt lệnh',
            'TRADE_DATE' => 'Ngày giao dịch (theo chu kỳ)',
            'ORDER_NO' => 'Số hiệu lệnh của sàn, sử dụng để so khớp lệnh báo giá',
            'TRADE_TIME' => 'Thời gian đặt',
            'ORD_TYPE' => 'Loại lệnh',
            'ORD_CHANEL' => 'Kênh giao dịch',
            'SEC_CD' => 'Mã CCQ',
            'TRADE_TYPE' => 'Loại giao dịch',
            'ORD_QTY' => 'KL đặt (BÁN)',
            'ORD_AMT' => 'Giá trị lệnh (MUA)',
            'CUST_CD' => 'Mã KH',
            'CUST_NO' => 'Số TK',
            'CUST_NAME' => 'Họ và tên khách hàng',
            'CUST_TRANSACTION_CD' => 'Mã đơn vị quản lý khách hàng',
            'FEE_RATE' => 'Tỷ lệ phí',
            'FEE_AMT' => 'Tiền phí giao dịch (mặc định: 0)',
            'TAX_RATE' => 'TL thuế TNCN',
            'TAX_AMT' => 'Thuế',
            'RIGHT_TAX_RATE' => 'Tỉ lệ sau thuế',
            'RIGHT_TAX_QTY' => 'SL sau thuế',
            'RIGHT_TAX_AMT' => 'GT sau thuế',
            'MAT_QTY' => 'KL khớp',
            'MAT_PRICE' => 'Giá khớp (lưu đơn vị 1đ)',
            'MAT_AMT' => 'GT khớp',
            'NOTES' => 'Ghi chú',
            'TRANSACTION_CD' => 'Mã đơn vị giao dịch',
            'BROKER_CD' => 'NVCS',
            'PAYMENT_DATE' => 'Ngày thanh toán (tiền bán, CK mua) tại công ty chứng khoán',
            'ENTRY_DATE' => 'Ngày hiệu lực thanh toán theo VSD',
            'STATUS' => 'Trạng thái',
            'REG_DATE_TIME' => 'Thời gian tạo',
            'REG_USER_ID' => 'Người tạo',
            'UPD_DATE_TIME' => 'Thời gian cập nhật',
            'UPD_USER_ID' => 'Người cập nhật',
        ];
    }
}
