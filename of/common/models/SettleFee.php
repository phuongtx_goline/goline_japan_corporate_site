<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "settle_fee".
 *
 * @property integer $id
 * @property integer $contract_type_id
 * @property integer $from_period_val
 * @property integer $to_period_val
 * @property string $fee_rate
 * @property integer $status
 * @property string $reg_date_time
 * @property integer $reg_user_id
 * @property string $reg_ip_addr
 * @property string $upd_date_time
 * @property integer $upd_user_id
 * @property string $upd_ip_addr
 */
class SettleFee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settle_fee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contract_type_id', 'from_period_val', 'to_period_val', 'fee_rate', 'status'], 'required'],
            [['contract_type_id', 'from_period_val', 'to_period_val', 'status', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['fee_rate'], 'number'],
            [['reg_date_time', 'upd_date_time'], 'safe'],
            [['reg_ip_addr', 'upd_ip_addr'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contract_type_id' => 'Loại HĐ',
            'from_period_val' => 'Từ thời hạn',
            'to_period_val' => 'Đến thời hạn',
            'fee_rate' => 'Tỷ lệ phí',
            'status' => 'Trạng thái',
            'reg_date_time' => 'Thời gian tạo',
            'reg_user_id' => 'Người tạo',
            'reg_ip_addr' => 'IP tạo',
            'upd_date_time' => 'Thời gian cập nhật',
            'upd_user_id' => 'Người cập nhật',
            'upd_ip_addr' => 'IP cập nhật',
        ];
    }
}
