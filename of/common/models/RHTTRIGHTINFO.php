<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "RHTTRIGHT_INFO".
 *
 * @property integer $ID
 * @property integer $ALLO_DATE
 * @property integer $RIGHT_TYPE
 * @property string $SEC_CD
 * @property integer $NO_RIGHT_DATE
 * @property integer $OWNER_FIX_DATE
 * @property integer $RIGHT_EXC_DATE
 * @property integer $EXPECTED_EXC_DATE
 * @property integer $BEGIN_TRADE_DATE
 * @property string $RATES1
 * @property string $RATED1
 * @property string $RATES
 * @property string $RATED
 * @property integer $ROUNDTYPE
 * @property integer $DECIMAL_PLACE
 * @property string $ROUND_PRICE
 * @property integer $ODD_DECIMAL_PLACE
 * @property string $TAX_RATE
 * @property integer $STATUS
 * @property string $REMARKS
 * @property string $REG_DATE_TIME
 * @property integer $REG_USER_ID
 * @property string $UPD_DATE_TIME
 * @property integer $UPD_USER_ID
 */
class RHTTRIGHTINFO extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'RHTTRIGHT_INFO';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['SEC_CD', 'RIGHT_TYPE','NO_RIGHT_DATE','OWNER_FIX_DATE'], 'required'],
            [['ALLO_DATE', 'RIGHT_TYPE', 'NO_RIGHT_DATE', 'BEGIN_TRADE_DATE', 'OWNER_FIX_DATE', 'RIGHT_EXC_DATE', 'EXPECTED_EXC_DATE', 'ROUNDTYPE', 'DECIMAL_PLACE', 'ODD_DECIMAL_PLACE', 'STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['RATES1', 'RATED1', 'RATES', 'RATED', 'ROUND_PRICE', 'TAX_RATE'], 'number'],
            [['REMARKS'], 'string'],
            [['REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['SEC_CD'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ALLO_DATE' => 'Ngày tạo',
            'RIGHT_TYPE' => 'Loại quyền',
            'SEC_CD' => 'Mã CCQ',
            'NO_RIGHT_DATE' => 'Ngày KHQ',
            'OWNER_FIX_DATE' => 'Ngày chốt',
            'RIGHT_EXC_DATE' => 'Ngày TT thực tế',
            'EXPECTED_EXC_DATE' => 'Ngày TT dự kiến',
            'BEGIN_TRADE_DATE' => 'Ngày bắt đầu GD',
            'RATES1' => 'TL chốt',
            'RATED1' => 'TL quyền',
            'RATES' => 'TL gốc',
            'RATED' => 'TL nhận',
            'ROUNDTYPE' => 'Kiểu làm tròn',
            'DECIMAL_PLACE' => 'Giá trị làm tròn',
            'ROUND_PRICE' => 'Giá CCQ lẻ',
            'ODD_DECIMAL_PLACE' => 'Số lẻ sau dấu phẩy',
            'TAX_RATE' => 'Thuế TNCN',
            'STATUS' => 'Trạng thái',
            'REMARKS' => 'Ghi chú',
            'REG_DATE_TIME' => 'Thời gian tạo',
            'REG_USER_ID' => 'Người tạo',
            'UPD_DATE_TIME' => 'Thời gian cập nhật',
            'UPD_USER_ID' => 'Người cập nhật',
        ];
    }
}
