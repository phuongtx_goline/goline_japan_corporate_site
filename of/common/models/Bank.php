<?php

namespace common\models;

use Yii;

/**
* This is the model class for table "bank".
*
    * @property integer $id
    * @property string $name
    * @property string $acco
    * @property string $no
*/
class Bank extends \yii\db\ActiveRecord
{
/**
* @inheritdoc
*/
public static function tableName()
{
return 'bank';
}

/**
* @inheritdoc
*/
public function rules()
{
return [
            [['name', 'acco', 'no'], 'string', 'max' => 255],
        ];
}

/**
* @inheritdoc
*/
public function attributeLabels()
{
return [
    'id' => 'ID',
    'name' => 'Name',
    'acco' => 'Acco',
    'no' => 'No',
];
}
}
