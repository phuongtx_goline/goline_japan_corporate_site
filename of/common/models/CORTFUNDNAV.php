<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "CORTFUND_NAV".
 *
 * @property integer $ID
 * @property string $SEC_CD
 * @property integer $VALID_DATE
 * @property string $NAV
 * @property string $REMARKS
 * @property integer $STATUS
 * @property string $REG_DATE_TIME
 * @property integer $REG_USER_ID
 * @property string $UPD_DATE_TIME
 * @property integer $UPD_USER_ID
 */
class CORTFUNDNAV extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CORTFUND_NAV';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['SEC_CD', 'VALID_DATE', 'NAV', 'STATUS'], 'required'],
            [['VALID_DATE', 'STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['REMARKS'], 'string'],
            [['REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['SEC_CD'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'SEC_CD' => 'Mã CCQ',
            'VALID_DATE' => 'Ngày hiệu lực',
            'NAV' => 'NAV',
            'REMARKS' => 'Ghi chú',
            'STATUS' => 'Trạng thái',
            'REG_DATE_TIME' => 'Thời gian tạo',
            'REG_USER_ID' => 'Người tạo',
            'UPD_DATE_TIME' => 'Thời gian cập nhật',
            'UPD_USER_ID' => 'Người cập nhật',
        ];
    }
}
