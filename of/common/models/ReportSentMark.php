<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "report_sent_mark".
 *
 * @property integer $id
 * @property string $sent_date
 * @property integer $customer_id
 * @property integer $type
 * @property integer $status
 * @property string $reg_date_time
 * @property integer $reg_user_id
 * @property string $reg_ip_addr
 * @property string $upd_date_time
 * @property integer $upd_user_id
 * @property string $upd_ip_addr
 */
class ReportSentMark extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_sent_mark';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sent_date', 'customer_id'], 'required'],
            [['sent_date'], 'number'],
            [['customer_id', 'type', 'status', 'reg_user_id', 'upd_user_id'], 'integer'],
            [['reg_date_time', 'upd_date_time'], 'safe'],
            [['reg_ip_addr', 'upd_ip_addr'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sent_date' => 'Ngày gửi',
            'customer_id' => 'Khách hàng',
            'type' => 'Loại gửi',
            'status' => 'Trạng thái',
            'reg_date_time' => 'Thời gian tạo',
            'reg_user_id' => 'Người tạo',
            'reg_ip_addr' => 'IP máy tạo',
            'upd_date_time' => 'Thời gian cập nhật',
            'upd_user_id' => 'Người cập nhật',
            'upd_ip_addr' => 'IP máy cập nhật',
        ];
    }
}
