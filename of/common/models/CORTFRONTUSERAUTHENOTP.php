<?php

namespace common\models;

use Yii;

/**
* This is the model class for table "CORTFRONT_USER_AUTHEN_OTP".
*
    * @property integer $ID
    * @property integer $TRADE_DATE
    * @property integer $USER_ID
    * @property string $APPLICATION_CD
    * @property integer $SESSION_ID
    * @property string $OTP_CODE
    * @property integer $EXPIRED_TIME
    * @property integer $PIN_TYPE
    * @property integer $BUSINESS_CD
    * @property integer $RETRIES
    * @property integer $STATUS
    * @property string $REG_DATE_TIME
    * @property integer $REG_USER_ID
    * @property string $UPD_DATE_TIME
    * @property integer $UPD_USER_ID
*/
class CORTFRONTUSERAUTHENOTP extends \yii\db\ActiveRecord
{
/**
* @inheritdoc
*/
public static function tableName()
{
return 'CORTFRONT_USER_AUTHEN_OTP';
}

/**
* @inheritdoc
*/
public function rules()
{
return [
            [['TRADE_DATE', 'USER_ID'], 'required'],
            [['TRADE_DATE', 'USER_ID', 'SESSION_ID', 'EXPIRED_TIME', 'PIN_TYPE', 'BUSINESS_CD', 'RETRIES', 'STATUS', 'REG_USER_ID', 'UPD_USER_ID'], 'integer'],
            [['REG_DATE_TIME', 'UPD_DATE_TIME'], 'safe'],
            [['APPLICATION_CD', 'OTP_CODE'], 'string', 'max' => 255],
        ];
}

/**
* @inheritdoc
*/
public function attributeLabels()
{
return [
    'ID' => 'ID',
    'TRADE_DATE' => 'Ngày GD',
    'USER_ID' => 'CORCUSTOMER.CUST_NO',
    'APPLICATION_CD' => 'Kênh GD',
    'SESSION_ID' => 'Phiên GD',
    'OTP_CODE' => 'Mã OTP',
    'EXPIRED_TIME' => 'Thời gian hiệu lực',
    'PIN_TYPE' => 'Loại xác thực',
    'BUSINESS_CD' => 'Nghiệp vụ',
    'RETRIES' => 'Số lần nhập sai SMS OTP',
    'STATUS' => 'Trạng thái',
    'REG_DATE_TIME' => 'Thời gian tạo',
    'REG_USER_ID' => 'Người tạo',
    'UPD_DATE_TIME' => 'Thời gian cập nhật',
    'UPD_USER_ID' => 'Người cập nhật',
];
}
}
