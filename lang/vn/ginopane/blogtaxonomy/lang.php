<?php

return [
    // plugin
    'plugin' => [
      'name' => 'Phân loại Blog',
      'description' => 'Cho phép tổ chức bài viết theo tag và chuỗi bài (serie)',
    ],

    //settings
    'settings' => [
        'post_types_enabled' => 'Cho phép quản lý loại bài',
        'post_categories_cover_images_enabled' => 'Cho phép chủ đề có ảnh bìa',
        'post_categories_featured_images_enabled' => 'Cho phép chủ đề có ảnh nổi bật',
        'tabs' => [
            'posts' => 'Bài viết',
            'categories' => 'Chủ đề'
        ]
    ],

    // form
    'form' => [
        'errors' => [
            'unknown' => 'Đã có lỗi xảy ra'
        ],
        'fields' => [
            'tag' => 'Tag',
            'title' => 'Tiêu đề',
            'name' => 'Tên',
            'images' => 'Hình ảnh',
            'cover_image' => 'Ảnh bìa',
            'featured_images' => 'Ảnh nổi bật',
            'slug' => 'Slug',
            'description' => 'Mô tả',
            'posts' => 'Bài viết',
            'series' => 'Chuỗi bài',
            'status' => 'Tình trạng',
            'related_series' => 'Chuỗi bài liên quan'
        ],
        'tabs' => [
            'general' => 'Chung',
            'posts' => 'Bài viết',
            'series' => 'Chuỗi bài',
            'related_series' => 'Chuỗi bài liên quan'
        ],

        'categories' => [
            'no_posts_in_categories' => 'Chủ đề trống',
            'images_section' => 'Hình ảnh',
            'images_section_comment' => 'Các hình ảnh gắn với chủ đề này'
        ],
        'tags' => [
            'label' => 'Tag',

            'images_section' => 'Hình ảnh',
            'images_section_comment' => 'Các hình ảnh gắn với tag này',

            'create_title' => 'Tạo tag mới',
            'edit_title' => 'Sửa tag ":tag"',
            'list_title' => 'Quản lý tag',
            'new_tag_label' => 'Tag mới',
            'no_tags_message' => 'Không có tag',
            'tag_does_not_exist' => 'Tag không tồn tại',
            'no_posts_with_such_tag' => 'Không có bài viết gắn tag này',
            'no_series_with_such_tag' => 'Không có chuỗi bài gắn tag này',

            'delete_confirm' => 'Bạn chắc chắn muốn xóa tag?',
            'remove_orphaned_load_indicator' => 'Đang xóa các tag trống...',
            'remove_orphaned_label' => 'Xóa các tag trống',
            'remove_orphaned_confirm' => 'Bạn có chắc chắn muốn xóa các tag trống?',
            'no_orphaned_tags' => 'Không có tag trống',
            'remove_orphaned_tags_success' => 'Đã xóa các tag trống',
            'delete_bulk_confirm' => 'Bạn chắc chắn muốn xóa các tag đang chọn?',
            'delete_tags_success' => 'Đã xóa tag',

            'comment_post' => 'Chọn tag cho bài viết này',
            'comment_series' => 'Chọn tag cho chuỗi bài này',

            'name_required' => 'Tên tag không được để trống',
            'name_unique' => 'Trùng tên tag, hãy chọn tên khác',
            'name_too_short' => 'Tên tag không được ngắn hơn :min ký tự',

            'slug_invalid' => 'Slug của tag chỉ bao gồm chữ, số, khoảng trắng và gạch ngang',
            'slug_required' => 'Slug của tag là bắt buộc',
            'slug_unique' => 'Trùng slug, hãy chọn tên khác',
            'slug_too_short' => 'Slug không được ngắn hơn :min ký tự',

            'return_to_list' => 'Về danh sách tag',
            'create_load_indicator' => 'Đang tạo tag...',
            'update_load_indicator' => 'Đang cập nhật tag...',
            'delete_load_indicator' => 'Đang xóa tag...',
            'create_button_label' => 'Tạo mới',
            'save_button_label' => 'Lưu',
            'create_and_close_button_label' => 'Tạo mới và đóng',
            'save_and_close_button_label' => 'Lưu và đóng',
            'cancel_button_label' => 'Hủy bỏ',
            'or' => 'hoặc',
        ],
        'series' => [
            'label' => 'Chuỗi bài',
            'description' => 'Mô tả',
            'status' => 'Tình trạng',

            'images_section' => 'Hình ảnh',
            'images_section_comment' => 'Các hình ảnh gắn với chuỗi bài này',

            'create_title' => 'Tạo chuỗi bài mới',
            'edit_title' => 'Sửa chuỗi bài ":series"',
            'list_title' => 'Quản lý chuỗi bài',
            'series_does_not_exist' => 'Chuỗi bài không tồn tại',
            'no_series_message' => 'Không có chuỗi bài',
            'no_posts_in_series' => 'Không có bài viết nào thuộc chuỗi bài này',
            'comment' => 'Chọn chuỗi bài cho bài viết này',

            'title_required' => 'Tiêu đề Chuỗi bài không được để trống',
            'title_unique' => 'Trùng tên, hãy chọn trên khác',
            'title_too_short' => 'Tiêu đề không được ngắn hơn :min ký tự',

            'slug_invalid' => 'Slug của chuỗi bài chỉ bao gồm chữ, số, khoảng trắng và gạch ngang',
            'slug_required' => 'Slug của chuỗi bài là bắt buộc',
            'slug_unique' => 'Trùng slug, hãy chọn tên khác',
            'slug_too_short' => 'Slug không được ngắn hơn :min ký tự',

            'create_load_indicator' => 'Đang tạo mới chuỗi bài...',
            'update_load_indicator' => 'Đang cập nhật chuỗi bài...',
            'delete_load_indicator' => 'Đang xóa chuỗi bài...',
            'delete_confirm' => 'Bạn có chắc muốn xóa chuỗi bài này?',
            'delete_bulk_confirm' => 'Bạn có chắc muốn xóa các chuỗi bài đang chọn?',
            'delete_series_success' => 'Đã xóa chuỗi bài',
            'new_series_button_label' => 'Tạo mới chuỗi bài',
            'create_button_label' => 'Tạo mới',
            'save_button_label' => 'Lưu',
            'create_and_close_button_label' => 'Tạo mới và đóng',
            'save_and_close_button_label' => 'Lưu và đóng',
            'cancel_button_label' => 'Hủy bỏ',
            'or' => 'hoặc',
            'return_to_list' => 'Về danh sách chuỗi bài'
        ],
        'post_types' => [
            'label' => 'Kiểu bài viết',
            'comment' => 'Chọn kiểu cho bài viết',

            'create_title' => 'Tạo kiểu bài mới',
            'edit_title' => 'Nhập kiểu bài viết ":post_type"',
            'list_title' => 'Quản lý kiểu bài viết',

            'post_list_column' => 'Kiểu',
            'post_list_filter_scope' => 'Kiểu',

            'no_types_message' => 'Không có kiểu bài viết nào',
            'no_posts_with_type' => 'Kiểu bài này không có bài nào',

            'delete_bulk_confirm' => 'Bạn có chắc muốn xóa các kiểu bài viết này không?',
            'delete_post_types_success' => 'Đã xóa thành công',

            'name_required' => 'Tên không được để trống',
            'name_unique' => 'Trùng tên, hãy chọn tên khác',
            'name_too_short' => 'Tên không được ngắn hơn :min ký tự',

            'slug_invalid' => 'Post type slugs may only contain alpha-numeric characters, spaces and hyphens',
            'slug_invalid' => 'Slug chỉ được chứa chữ cái, chữ số, khoảng trắng và gạch ngang (-)',
            'slug_required' => 'Slug không được để trống',
            'slug_unique' => 'Trùng slug, hãy nhập giá trị khác',
            'slug_too_short' => 'Slug không được ngắn hơn :min ký tự',

            'attribute_name' => 'Tên thuộc tính',
            'attribute_name_placeholder' => 'Thuộc tính mới',
            'attribute_code' => 'Mã',
            'attribute_code_placeholder' => 'new-awesome-attribute',
            'attribute_code_comment' => 'Thuộc tính sẽ được quản lý thông qua mã này',
            'attribute_type' => 'Kiểu',
            'attribute_type_placeholder' => 'Chọn kiểu thuộc tính',
            'attribute_type_text' => 'Văn bản',
            'attribute_type_textarea' => 'Khối văn bản',
            'attribute_type_dropdown' => 'Danh sách xổ xuống',
            'attribute_type_datepicker' => 'Ngày giờ',
            'attribute_type_datepicker_mode' => 'Chế độ',
            'attribute_type_datepicker_mode_comment' => 'Sử dụng bộ chọn này để chọn ngày tháng hoặc giờ. Mặc định là ngày tháng',
            'attribute_type_datepicker_mode_placeholder' => 'Chọn chế độ cho bộ chọn',
            'attribute_type_datepicker_mode_date' => 'Ngày tháng',
            'attribute_type_datepicker_mode_time' => 'Giờ',
            'attribute_type_datepicker_mode_datetime' => 'Ngày tháng và giờ',
            'attribute_type_file' => 'File',
            'attribute_type_image' => 'Hình ảnh',
            'attribute_dropdown_options' => 'Tùy chọn cho danh sách',
            'attribute_dropdown_options_placeholder' => '1,2,3,4,5,6,7,8,9,10',
            'attribute_dropdown_options_comment' => 'Tập hợp các tùy chọn ngăn cách nhau bởi dấu phẩy (,)',
            'type_attributes' => 'Thuộc tính',
            'type_attributes_comment' => 'Chọn các thuộc tính liên kết với kiểu này',
            'type_attributes_prompt' => 'Tính năng này dùng để tạo một tập hợp thuộc tính',
            'name_comment' => 'Name of the field to be used in a select on a post form',
            'name_comment' => 'Tên của trường sẽ được đưa vào lựa chọn trên form bài viết',
            'description_comment' => 'Mô tả vì sao bạn cần nó',

            'return_to_list' => 'Về danh sách kiểu',
            'create_load_indicator' => 'Đang tạo mới kiểu bài...',
            'update_load_indicator' => 'Đang cập nhật kiểu bài...',
            'delete_load_indicator' => 'Đang xóa kiểu bài...',
            'new_type_button_label' => 'Tạo kiểu bài mới',
            'create_button_label' => 'Tạo mới',
            'save_button_label' => 'Lưu',
            'create_and_close_button_label' => 'Tạo mới và đóng',
            'save_and_close_button_label' => 'Lưu và đóng',
            'cancel_button_label' => 'Hủy bỏ',
            'or' => 'hoặc',
        ]
    ],

    'list' => [
        'columns' => [
            'description' => 'Mô tả',
            'name' => 'Tên',
            'posts' => 'Bài viết',
            'series' => 'Chuỗi bài',
            'status' => 'Tình trạng',
            'slug' => 'Slug',
            'tag' => 'Tag',
            'tags' => 'Tag',
            'title' => 'Tiêu đề',
        ]
    ],

    // navigation
    'navigation' => [
        'sidebar' => [
            'tags' => 'Tag',
            'series' => 'Chuỗi bài',
            'post_types' => 'Kiểu',
        ],
        'tab' => [
            'taxonomy' => 'Phân loại',
            'type' => 'Kiểu'
        ]
    ],

    // placeholders
    'placeholders' => [
        'tags' => 'Thêm tag...',
        'series' => 'Chọn một chuỗi bài...',
        'categories' => 'Thêm chủ đề...',
        'post_types' => 'Chọn một kiểu bài...',
        'title' => 'Tiêu đề mới',
        'name' => 'Tên mới',
        'slug' => 'ten-moi'
    ],

    // component-specific strings
    'components' => [
        'partials' => [
            'published_in' => 'đăng lúc',
            'displaying_number_of_posts' => 'Hiển thị %d của %d bài',
            'viewing_page_of' => 'Trang %d/%d',
            'pagination_back' => 'Về trước',
            'pagination_next' => 'Tiếp theo'
        ],
        'series_posts' => [
            'name' => 'Bài viết trong chuỗi bài',
            'description' => 'Liệt kê các bài viết thuộc chuỗi bài',
            'series_title' => 'Slug của chuỗi bài',
            'series_description' => 'Xác định chuỗi bài dựa vào slug trên URL',
            'posts_in_series' => 'Bài viết nằm trong chuỗi bài',
            'no_posts_message' => 'Chuỗi bài chưa có bài viết nào',
            'include_tagged_posts_title' => 'Thêm bài viết chung tag',
            'include_tagged_posts_description' => 'Thêm các bài viết có các tag của chuỗi bài hiện tại'
        ],
        'tag_list' => [
            'name' => 'Danh sách tag',
            'description' => 'Danh sách tag',

            'display_empty_title' => 'Hiện các tag trống',
            'display_empty_description' => 'Là các tag không được gắn cho bài viết nào',

            'order_title' => 'Thứ tự tag',
            'order_description' => 'Thứ tự tag',

            'tag_page_title' => 'Trang tag',
            'tag_page_description' => 'Là trang hiển thị nội dung của tag',

            'tags_page_title' => 'Trang danh sách tag',
            'tags_page_description' => 'Là trang liệt kê tất cả các tag trong hệ thống',

            'post_slug_title' => 'Slug của bài viết',
            'post_slug_description' => 'Lấy các tag của bài viết xác định qua slug trên URL',
            'fetch_posts_title' => 'Lấy các bài có tag',
            'fetch_posts_description' => 'Các bài có tag sẽ được nhúng vào thuộc tính `posts` của tag này. Sẽ làm chậm xử lý',
            'include_series_tags_title' => 'Thêm các tag trong chuỗi bài',
            'include_series_tags_description' => 'Additionally includes tags applied to the post\'s chuỗi bài if the post has chuỗi bài and the chuỗi bài has tags',
            'include_series_tags_description' => 'Thêm các tag gắn cho chuỗi bài của bài viết này',
            'fetch_series_post_count_title' => 'Lấy số bài có trong chuỗi',
            'fetch_series_post_count_description' => 'Lấy số bài có trong chuỗi',

            'no_tags_message' => 'Không tìm thấy tag',
            'all_tags_link' => 'Hiện tất cả',

            'limit_validation_message' => 'Giới hạn số tag không được nhỏ hơn 0',

            'limit_group' => 'Giới hạn',

            'limit_title' => 'Giới hạn',
            'limit_description' => 'Giới hạn lượng tag hiển thị, 0 là hiện tất cả',
            'expose_total_count_title' => 'Expose total count',
            'expose_total_count_title' => 'Hiển thị số lượng',
            'expose_total_count_description' => 'Chỉ đếm các tag số lượng hợp lệ',
            'tag_filter_title' => 'Thêm bộ lọc tag',
            'tag_filter_description' => 'Nếu chọn sẽ thêm bộ lọc tag',

            'tag_filter_options' => [
                'never' => 'Không bao giờ',
                'always' => 'Luôn luôn',
                'on_overflow' => 'Khi số lượng tag > giới hạn'
            ],

            'special_group' => 'Special',
            'debug_output_title' => 'Debug output',
            'debug_output_description' => 'Allows to enable debug output to the browser\'s console. Depends on the theme'
        ],
        'tag_posts' => [
            'name' => 'Posts With the Tag',
            'description' => 'Lists all posts with the supplied tag',
            'no_posts_message' => 'No posts with this tag',
            'posts_with_tag' => 'Posts with the tag',
            'tag_title' => 'Tag slug',
            'tag_description' => 'Look up the tag using the supplied slug value from this URL parameter',
            'include_series_posts_title' => 'Thêm bài viết thuộc chuỗi bài',
            'include_series_posts_description' => 'Thêm các bài viết trong các chuỗi bài được gắn tag này'
        ],
        'series_navigation' => [
            'name' => 'Post chuỗi bài Navigation',
            'description' => 'Provides navigation within the chuỗi bài for a single post',

            'series_page_title' => 'Series page',
            'series_page_description' => 'The page where the single chuỗi bài content is displayed',

            'post_slug_title' => 'Post slug',
            'post_slug_description' => 'Get chuỗi bài navigation for the post specified by slug value from URL parameter',

            'links_group' => 'Links',
            'part_of_a_series' => 'This post is part of a chuỗi bài called'
        ],
        'series_list' => [
            'name' => 'Series List',
            'description' => 'Displays a list of chuỗi bài',

            'series_page_title' => 'Series page',
            'series_page_description' => 'The page where the single chuỗi bài content is displayed',

            'series_slug_title' => 'Series slug parameter',
            'series_slug_description' => 'The setting must be equal to slug parameter being used for chuỗi bài Page (e.g. /blog/series/:series will give you :series)',

            'fetch_posts_title' => 'Fetch related posts',
            'fetch_posts_description' => 'Fetches related posts so they are available as `posts` property of the chuỗi bài item. Slows down performance',

            'limit_title' => 'Limit',
            'limit_description' => 'Number of chuỗi bài to display, 0 retrieves all chuỗi bài',

            'display_empty_title' => 'Display empty chuỗi bài',
            'display_empty_description' => 'Show chuỗi bài which don\'t have any posts assigned',

            'order_title' => 'Series order',
            'order_description' => 'How chuỗi bài list should be ordered',

            'no_series_message' => 'No chuỗi bài found',

            'limit_validation_message' => 'Limit of chuỗi bài must be a valid non-negative integer number'
        ],
        'related_posts' => [
            'name' => 'Related Posts',
            'description' => 'Provides a list of posts related by tags',

            'post_slug_title' => 'Post slug',
            'post_slug_description' => 'Get related posts for the post specified by slug value from URL parameter',

            'limit_title' => 'Limit',
            'limit_description' => 'Number of posts to display, 0 retrieves all related posts',
            'limit_validation_message' => 'Limit of related posts must be a valid non-negative integer number',

            'no_posts_message' => 'No related posts found',
            'related_posts' => 'Related posts',
            'links_group' => 'Links'
        ],
        'related_series' => [
            'name' => 'Chuỗi bài liên quan',
            'description' => 'Gets properly filled related chuỗi bài list',
            'no_series_message' => 'No related chuỗi bài',
        ],
        'post_list_abstract' => [
            'pagination_group' => 'Pagination',
            'page_parameter_title' => 'Page parameter',
            'page_parameter_description' => 'Calculate pagination based on this URL parameter',
            'pagination_per_page_title' => 'Items per page',
            'pagination_per_page_description' => 'How many items (if any) should be displayed per page',
            'links_group' => 'Links',
            'pagination_validation_message' => 'Per page number must be a valid non-negative integer number',
            'filters_group' => 'Filters',
            'include_categories_title' => 'Include categories',
            'include_categories_description' => 'List category ids or slugs separated by comma to include them. This option includes only listed categories. But they still can be dismissed by exception option',
            'except_posts_title' => 'Exclude posts',
            'except_posts_description' => 'List post ids or slugs separated by comma to exclude them from the list',
            'except_categories_title' => 'Exclude categories',
            'except_categories_description' => 'List category ids or slugs separated by comma to exclude their posts from the list',
        ]
    ],

    // order-by options
    'order_options' => [
        'created_at_asc' => 'Tạo mớid (ascending)',
        'created_at_desc' => 'Tạo mớid (descending)',

        'name_asc' => 'Name (ascending)',
        'name_desc' => 'Name (descending)',

        'published_at_asc' => 'Published (ascending)',
        'published_at_desc' => 'Published (descending)',

        'post_count_asc' => 'Post count (ascending)',
        'post_count_desc' => 'Post count (descending)',

        'random' => 'Random',

        'relevance_asc' => 'Relevance (ascending)',
        'relevance_desc' => 'Relevance (descending)',

        'title_asc' => 'Title (ascending)',
        'title_desc' => 'Title (descending)',

        'updated_at_asc' => 'Updated (ascending)',
        'updated_at_desc' => 'Updated (descending)',
    ]
];
