<?php return [
    'plugin' => [
        'name' => 'Nội dung đa ngôn ngữ',
        'description' => 'Xây dựng nội dung cho nhiều ngôn ngữ',
        'tab' => 'Chuyển ngữ',
        'manage_locales' => 'Quản lý địa phương',
        'manage_messages' => 'Bảng chuyển ngữ',
    ],
    'locale_picker' => [
        'component_name' => 'Bảng chọn địa phương',
        'component_description' => 'Danh sách các địa phương được sử dụng trong website',
    ],
    'alternate_hreflang' => [
        'component_name' => 'Alternate hrefLang elements',
        'component_description' => 'Injects the language alternatives for page as hreflang elements'
    ],
    'locale' => [
        'title' => 'Quản lý ngôn ngữ',
        'update_title' => 'Cập nhật ngôn ngữ',
        'create_title' => 'Tạo ngôn ngữ',
        'select_label' => 'Chọn ngôn ngữ',
        'default_suffix' => 'mặc định',
        'unset_default' => '":locale" đang được đặt làm mặc định.',
        'delete_default' => '":locale" đang là ngôn ngữ mặc định, không được xóa.',
        'disabled_default' => '":locale" đang bị vô hiệu hóa, không thể đặt làm mặc định.',
        'name' => 'Tên',
        'code' => 'Mã',
        'is_default' => 'Mặc định',
        'is_default_help' => 'Ngôn ngữ mặc định là ngôn ngữ được dùng để thể hiện nội dung nguyên thủy.',
        'is_enabled' => 'Kích hoạt',
        'is_enabled_help' => 'Ngôn ngữ không được kích hoạt sẽ không xuất hiện ở front-end.',
        'not_available_help' => 'Không có ngôn ngữ nào khác.',
        'hint_locales' => 'Tạo các ngôn ngữ mới cho nội dung front-end. Ngôn ngữ mặc định là ngôn ngữ được dùng để thể hiện nội dung nguyên thủy.',
        'reorder_title' => 'Sắp xếp lại ngôn ngữ',
        'sort_order' => 'Thứ tự sắp xếp',
    ],
    'messages' => [
        'title' => 'Bảng chuyển ngữ',
        'description' => 'Cập nhật các thông điệp theo từng ngôn ngữ',
        'clear_cache_link' => 'Xóa cache',
        'clear_cache_loading' => 'Đang xóa cache...',
        'clear_cache_success' => 'Xóa cache thành công!',
        'clear_cache_hint' => 'Bạn có thể phải <strong>Xóa cache</strong> để thấy thay đổi phía front-end.',
        'scan_messages_link' => 'Quét các thông điệp',
        'scan_messages_begin_scan' => 'Bắt đầu quét',
        'scan_messages_loading' => 'Đang quét thông điệp mới...',
        'scan_messages_success' => 'Đã quét xong!',
        'scan_messages_hint' => 'Chọn <strong>Quét các thông điệp</strong> để cập nhật các thông điệp mới trong theme đang dùng.',
        'scan_messages_process' => 'Quy trình này sẽ quét trong theme hiện tại các thông điệp có thể chuyển ngữ.',
        'scan_messages_process_limitations' => 'Một số thông điệp có thể không quét được và chỉ xuất hiện lần đầu sử dụng thôi.',
        'scan_messages_purge_label' => 'Xóa bỏ tất cả thông điệp ở lần đầu',
        'scan_messages_purge_help' => 'Nếu chọn, các thông điệp hiện tại sẽ bị xóa bỏ trước khi tiến hành quét.',
        'scan_messages_purge_confirm' => 'Bạn có chắc chắn muốn xóa bỏ các thông điệp? Hành động này không thể hoàn tác!',
        'scan_messages_purge_deleted_label' => 'Xóa các thông điệp bị thiếu sau khi quét',
        'scan_messages_purge_deleted_help' => 'Nếu chọn, sau khi quét xong, bất kỳ thông điệp nào không tìm thấy sẽ bị xóa',
        'hint_translate' => 'Here you can translate messages used on the front-end, the fields will save automatically.',
        'hint_translate' => 'Dịch các thông điệp ở front-end ở đây, lưu tự động.',
        'hide_translated' => 'Ẩn phần chuyển ngữ',
        'export_messages_link' => 'Export Thông điệp',
        'import_messages_link' => 'Import Thông điệp',
        'not_found' => 'Không tìm thấy',
        'found_help' => 'Đã có lỗi xảy ra trong quá trình quét.',
        'found_title' => 'Lỗi khi quét',
    ],
];
