<?php
return [
    'plugin' => [
        'name'          => 'Xóa cache',
        'description'   => 'Widget cho dashboard',
        'label'         => 'Công cụ xóa cache',
        'clear'         => 'Xóa',
        'success'       => 'Đã làm sạch cache',
        'nochart'       => 'Không hiện đồ thị',
        'radius'        => 'Kích thước đồ thị',
        'delthumbs'     => 'Xóa cả ảnh thumb?',
        'delthumbspath' => 'Đường dẫn đến thư mục chứa ảnh thumb',
        'thumbs_regex'  => 'Biểu thức chính quy để khớp tên ảnh thumb',
    ],
];
