<?php return [
    'plugin' => [
        'name' => 'Công ty',
        'description' => 'Lưu trữ thông tin chung, chính sách hay bất cứ thứ gì của công ty',
    ],
    'info' => [
        'label' => 'Thông tin công ty'
    ],
    'service' => [
        'label' => 'Dịch vụ'
    ],
    'faq' => [
        'label' => 'Câu hỏi thường gặp',
        'description' => 'Câu hỏi thường gặp'
    ],
];
