<?php return [
    'cms' => [
        'menu_label' => 'Trang'
    ],
    'layout' => [
        'not_found_name' => "Dàn ý trang ':name' không tìm thấy",
        'menu_label' => 'Dàn ý trang',
        'unsaved_label' => 'Dàn ý trang chưa được lưu',
        'no_list_records' => 'Không tìm thấy dàn ý trang nào',
        'new' => 'Dàn ý trang mới',
        'delete_confirm_multiple' => 'Xóa các dàn ý trang đã chọn?',
        'delete_confirm_single' => 'Xóa dàn ý trang này?'
    ],
    'theme' => [
        'theme_label' => 'Chủ đề',
        'theme_title' => 'Các chủ đề',
    ]
];
