<?php return [
    'dashboard' => [
        'menu_label' => 'Dashboard',
    ],
    'reorder' => [
        'default_title' => 'Sắp xếp các bản ghi',
    ],
    'media' => [
        'order_by' => 'Sắp xếp theo',
        'insert' => 'Chèn',
        'crop_and_insert' => 'Crop & Chèn',
    ],
    'mediafinder' => [
        'default_prompt' => 'Bấm vào nút %s để tìm kiếm'
    ],
    'editor' => [
        'menu_label' => 'Định dạng trình soạn thảo code',
        'menu_description' => 'Định dạng trình soạn thảo code cho toàn trang (kích thước chữ, màu nền...)',
    ],
    'user' => [
        'name' => 'Administrator',
        'menu_label' => 'Quản lý người dùng',
        'menu_description' => 'Cài đặt các admin, nhóm quản trị và quyền truy cập.',
        'list_title' => 'Quản lý người dùng',
        'new' => 'Thêm mới Administrator',
    ],
    'tooltips' => [
        'preview_website' => 'Xem website'
    ],
];
