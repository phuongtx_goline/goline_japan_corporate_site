<?php

return [
  'theme' => [
    'label' => 'Chủ đề',
    'unnamed' => 'Chủ đề chưa đặt tên',
    'name' => [
      'label' => 'Tên chủ đề',
      'help' => 'Đặt tên chủ đề bằng một mã độc nhất. Chẳng hạn: DuyTrung.AwesomeTheme'
    ],
  ],
  'themes' => [
    'install' => 'Cài đặt Chủ đề',
    'search' => 'Tìm kiếm chủ đề...',
    'installed' => 'Các chủ đề đã cài đặt',
    'no_themes' => 'Không có chủ đề như vậy trên marketplace',
    'recommended' => 'Gợi ý',
    'remove_confirm' => 'Bạn chắc chắn muốn xóa chủ đề này chứ?'
  ],
  'updates' => [
    'themes' => 'Chủ đề',
    'menu_label' => 'Cập nhật & Plugin',
  ],
  'system' => [
    'name' => 'Hệ thống',
    'menu_label' => 'Hệ thống',
    'categories' => [
        'cms' => 'CMS',
        'misc' => 'Khác',
        'logs' => 'Giám sát',
        'mail' => 'Mail',
        'shop' => 'Cửa hàng',
        'team' => 'Team',
        'users' => 'Người dùng',
        'system' => 'Hệ thống',
        'social' => 'Mạng xã hội',
        'backend' => 'Trang quản trị',
        'events' => 'Sự kiện',
        'customers' => 'Khách hàng',
        'my_settings' => 'Cài đặt của tôi',
        'notifications' => 'Thông báo'
    ]
],
];
