<?php

return [
    'diff' => [
        'empty' => '今',
        'ago' => [
            'year' => '1年前|:count年前',
            'month' => '1ヶ月前|:countヶ月前',
            'day' => '1日前|:count日前',
            'hour' => '1時間前|:count時間前',
            'minute' => '1分前|:count分前',
            'second' => '1秒前|:count秒前',
        ],
        'in' => [
            'second' => '1秒後|:count秒後',
            'hour' => '1日後|:count日後',
            'minute' => '1分後|:count分後',
            'day' => '1日後|:count日後',
            'month' => '1ヶ月後|:countヶ月後',
            'year' => '1年後|:count年後',
        ],
    ],
];
