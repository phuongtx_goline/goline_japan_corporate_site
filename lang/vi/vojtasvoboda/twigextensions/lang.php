<?php

return [
    'diff' => [
        'empty' => 'vừa xong',
        'ago' => [
            'year' => ':count năm trước',
            'month' => ':count tháng trước',
            'day' => ':count ngày trước',
            'hour' => ':count giờ trước',
            'minute' => ':count phút trước',
            'second' => ':count giây trước',
        ],
        'in' => [
            'second' => 'trong :count giây',
            'hour' => 'trong :count giờ',
            'minute' => 'trong :count phút',
            'day' => 'trong :count ngày',
            'month' => 'trong :count tháng',
            'year' => 'trong :count năm',
        ],
    ],
];
