# Goline Website

## Hosting

```
https://cnode005.seolover.cloud/
sgolif4b
cd115b6d
```

## Env

- PHP >= 7.2, < 7.4
  - 7.4 gặp lỗi: `implode(): Passing glue string after array is deprecated. Swap the parameters`

- Laragon

## Getting started

- Giải nén `.prepare/vendor-plugins.7z` vào root folder
- Cấu hình CSDL: `config\database.php`
- Đặt `debug = true` trong `config\app.php`
- Javascript được bundled bởi Parcel

```bash
npm install && npm run dev
```

## Issues

### Lỗi chức năng web với tiếng Nhật `ja`

Mô tả lỗi trên production

- Các nút chức năng trên trang chủ đều không hoạt động
- Không xem được tin

Kiểm tra trên local

- Hoạt động bình thường với môi trường: __PHP 7.2.11__ trên Windows 10, dựng môi trường bằng [Laragon](https://laragon.org/)

## Docs

[Tài liệu hướng dẫn sử dụng](https://goline.netlify.app/)
