import { createSlider } from './utils/carousel';
import { isMobileOrTablet } from './utils/media';

function renderMainSlider() {
  window.mainSlider = createSlider({
    id: 'site-main-slider'
  })
}

function renderProcessSlider() {
  if(!isMobileOrTablet()) {
    const homeProcessSlider = createSlider({
      id: 'home-process-slider',
      slidesPerView: 1,
      dragSpeed: 0,
    })

    const homeProcessTabs = document.querySelectorAll('.home-process__tab')
    homeProcessTabs.forEach((node, tabIdx, self) => node.addEventListener('click', function() {
      homeProcessSlider.moveToSlide(tabIdx)
      self.forEach(tab => tab.classList.remove('tab-selected', 'text-white'))
      self[tabIdx].classList.add('tab-selected', 'text-white')
    }))
  }
}

function renderPartnerSlider() {
  createSlider({
    id: 'home-partner-slider',
    slidesPerView: 1,
    dragSpeed: 0,
  })
}

function renderCustomerSlider() {
  createSlider({
    id: 'home-customer-slider',
    slidesPerView: 1,
    dragSpeed: 0,
  })
}

renderMainSlider()
renderProcessSlider()
renderPartnerSlider()
renderCustomerSlider()
