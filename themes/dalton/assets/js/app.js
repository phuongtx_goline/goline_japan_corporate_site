/* =========================
        APP ENTRY
========================= */
/**
 * Import CSS
 */
import '../css/app.css';

// NOT USED ATMM
// import 'alpinejs';

import { makeLazyLoad } from './utils/lazyload';
import { initTurbolinks } from './utils/navigate';
import { initHeader } from './utils/header';
import { isMobileOrTablet, setMobileMenu } from './utils/media';
import { smoothScrollHome } from './utils/scroll';

function onNavigate() {
  if(!isMobileOrTablet()) {
    window.mainHeader = initHeader()
  }
}

function initApp() {
  window.lazyloadInstance = makeLazyLoad({
    elements_selector: '.lazy',
    threshold: 10
  })

  window.switchLocale = code => {
    const details = {
      _session_key: document.querySelector('input[name="_session_key"]').value,
      _token: document.querySelector('input[name="_token"]').value,
      locale: code
    }
    let formBody = []
    for (const key in details) {
      let encodedKey = encodeURIComponent(key)
      let encodedValue = encodeURIComponent(details[key])
      formBody.push(encodedKey + '=' + encodedValue)
    }
    formBody = formBody.join('&')

    fetch(location.href + '/', {
      method: 'POST',
      body: formBody,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'X-OCTOBER-REQUEST-HANDLER': 'onSwitchLocale',
        'X-OCTOBER-REQUEST-PARTIALS': '',
        'X-Requested-With': 'XMLHttpRequest'
      }
    })
      .then(res => res.json())
      .then(data => {
        window.location.replace(data.X_OCTOBER_REDIRECT)
      })
      .catch(err => console.log(err))
  }

  window.setMobileMenu = setMobileMenu
  window.smoothScrollHome = smoothScrollHome

  initTurbolinks(onNavigate)
}

initApp()
