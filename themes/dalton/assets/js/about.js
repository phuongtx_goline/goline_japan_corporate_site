import { createSlider } from './utils/carousel';

function renderStaffSlider() {
  createSlider({
    id: 'about-staff-slider',
    slidesPerView: 1,
    dragSpeed: 0,
  })
}

renderStaffSlider()
