import LazyLoad from 'vanilla-lazyload'

export const makeLazyLoad = (options = {}) => new LazyLoad({
  ...options
})
