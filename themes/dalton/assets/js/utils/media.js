export const BREAK_POINTS = {
  'sm': '640px',
  // => @media (min-width: 640px) { ... }
  'md': '768px',
  // => @media (min-width: 768px) { ... }
  'lg': '1024px',
  // => @media (min-width: 1024px) { ... }
  'xl': '1280px',
  // => @media (min-width: 1280px) { ... }
  '2xl': '1536px',
  // => @media (min-width: 1536px) { ... }
}


const matchMedia = mediaStr => {
  const mql = window.matchMedia(mediaStr)
  return mql.matches
}

export const isMobile = () => !matchMedia(`(min-width: ${BREAK_POINTS.sm})`)
export const isMobileOrTablet = () => !matchMedia(`(min-width: ${BREAK_POINTS.md})`)
export const isMobileOrTabletOrIPad = () => !matchMedia(`(min-width: ${BREAK_POINTS.lg})`)

export const handleMedia = () => {
  document.querySelectorAll('[data-responsive-within] img').forEach(img => {
    const { appSrcMobile, appSrcDesktop } = img.dataset
    if(appSrcMobile && appSrcDesktop) {
      img.setAttribute('src', isMobileOrTablet() ? appSrcMobile : appSrcDesktop)
      img.removeAttribute('data-app-src-mobile')
      img.removeAttribute('data-app-src-desktop')
    }
  })
}

export const setMobileMenu = open => {
  const mm = document.getElementById('site-mobile-menu')
  if(mm) {
    if(open) {
      mm.classList.add('is-open')
      document.body.style.overflow = 'hidden'
    } else {
      mm.classList.remove('is-open')
      document.body.style.overflow = null
    }
  }
}
