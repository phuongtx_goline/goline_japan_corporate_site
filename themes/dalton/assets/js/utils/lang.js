export const toType = val => typeof val

export const is = {
  arr: val => Array.isArray(val),
  obj: val => Object.prototype.toString.call(val).includes('Object'),
  svg: val => val instanceof SVGElement,
  dom: val => val.nodeType || is.svg(val),
  str: val => toType(val) === 'string',
  date: val => val instanceof Date,
  dayjs: val => val instanceof dayjs,
  fnc: val => toType(val) === 'function',
  und: val => toType(val) === 'undefined',
  null: val => val === null,
  nullOrUnd: val => val == null,
  number: val => toType(val) === 'number',
  hex: val => /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(val),
  int: val => toType(val) === 'number' && Number.isFinite(val) && Math.floor(val) === val,
  empty: val => [Object, Array].includes((val || {}).constructor)
    && !Object.entries((val || {})).length,
  bool: val => toType(val) === 'boolean',
  NaN: val => Number.isNaN(val)
}
