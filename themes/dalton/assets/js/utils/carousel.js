import KeenSlider from 'keen-slider'
import { is } from './lang';

export const createSlider = (options = {}) => {
  let { autoPlay: autoPlayInterval, canOptItem, id, dragSpeed, slidesPerView, loop, ...config } = options;

  const mountPoint = document.getElementById(id)

  if(!mountPoint) return null

  if(is.nullOrUnd(autoPlayInterval)) {
    autoPlayInterval = +mountPoint.dataset['autoPlay']
  }
  if(is.nullOrUnd(loop)) {
    loop = !!mountPoint.dataset['loop']
  }
  if(is.nullOrUnd(slidesPerView)) {
    slidesPerView = mountPoint.dataset['slidesPerView']
  }
  if(is.nullOrUnd(dragSpeed)) {
    dragSpeed = +mountPoint.dataset['dragSpeed'] ? 5 : 0
  }

  const main = mountPoint.querySelector('.keen-slider')
  const leftArrow = mountPoint.querySelector('.arrow--left')
  const rightArrow = mountPoint.querySelector('.arrow--right')
  const dots_wrapper = mountPoint.querySelector('.dots')

  const showArrows = leftArrow && rightArrow
  const showDots = !!dots_wrapper

  const newSlider = new KeenSlider(main, {
    loop,
    slidesPerView,
    dragSpeed,
    ...config,
    created: function (inst) {
      const slides = mountPoint.querySelectorAll('.keen-slider__slide')

      if(showArrows) {
        leftArrow.addEventListener('click', function () {
          inst.prev()
        })

        rightArrow.addEventListener('click', function () {
          inst.next()
        })
      }

      showDots && !dots_wrapper.querySelector('.dot') && slides.length > 1 && slides.forEach((item, idx) => {
        const dot = document.createElement('button')
        dot.classList.add('dot')
        dot.setAttribute('aria-label', 'Slide navigation button')
        dots_wrapper.appendChild(dot)
        dot.addEventListener('click', function () {
          inst.moveToSlide(idx)
        })
      })

      if(canOptItem) {
        slides[0].classList.add('is-keen-opted')
        slides.forEach((item, idx, self) => {
          item.classList.add('cursor-pointer')
          item.addEventListener('click', () => {
            self.forEach(slide => {
              slide.classList.remove('is-keen-opted')
            })
            self[idx].classList.add('is-keen-opted')
            inst.moveToSlide(idx)
          })
        })
      }

      if(is.int(autoPlayInterval) && autoPlayInterval > 0) {
        let itv = 0
        function autoplay(run) {
          clearInterval(itv)
          itv = setInterval(() => {
            if (run && inst) {
              inst.next();
              (showArrows || showDots) && updateClasses(inst, { showArrows, showDots, leftArrow, rightArrow, dots_wrapper })
            }
          }, autoPlayInterval)
        }
        mountPoint.addEventListener("mouseover", () => {
          autoplay(false)
        })
        mountPoint.addEventListener("mouseout", () => {
          autoplay(true)
        })
        autoplay(true)
      }

      (showArrows || showDots) && updateClasses(inst, { showArrows, showDots, leftArrow, rightArrow, dots_wrapper })
    },
    slideChanged(inst) {
      (showArrows || showDots) && updateClasses(inst, { showArrows, showDots, leftArrow, rightArrow, dots_wrapper })
    },
  })

  return newSlider
}

function updateClasses(inst, options) {
  const { showArrows, showDots, leftArrow, rightArrow, dots_wrapper } = options

  const slide = inst.details().relativeSlide

  if(showArrows) {
    slide === 0
      ? leftArrow.classList.add('arrow--disabled')
      : leftArrow.classList.remove('arrow--disabled')
    slide === inst.details().size - 1
      ? rightArrow.classList.add('arrow--disabled')
      : rightArrow.classList.remove('arrow--disabled')
  }

  if(showDots) {
    const dots = dots_wrapper.querySelectorAll('.dot')
    dots.forEach(function (dot, idx) {
      idx === slide
        ? dot.classList.add('dot--active')
        : dot.classList.remove('dot--active')
    })
  }
}
