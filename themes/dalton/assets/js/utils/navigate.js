import { handleMedia } from './media';

export const initTurbolinks = onLoad => {
  const Turbolinks = require('turbolinks');
  Turbolinks.start();

  // clean up before TBL caches
  document.addEventListener('turbolinks:before-cache', function() {
    setMobileMenu(false)
  })

  // fires once after the initial page load, and again after every TBL visit
  document.addEventListener('turbolinks:load', function (evt) {
    // console.log('turbolinks:load', evt && evt.data, lazyloadInstance)
    window.lazyloadInstance && window.lazyloadInstance.update()
    handleMedia()
    onLoad && onLoad()
  })
}
