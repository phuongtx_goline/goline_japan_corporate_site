import smoothscroll from 'smoothscroll-polyfill'
import { is } from './lang'
import { setMobileMenu } from './media'
smoothscroll.polyfill()

export const smoothScrollHome = id => {
  const srcElem = is.dom(id) ? id : document.getElementById(id)
  console.log('🚀 ~ file: scroll.js ~ line 8 ~ elem', srcElem)

  if(!srcElem) return

  console.log(window.location)

  if(isHomePage()) {
    let targetId = srcElem.dataset.target
    targetId = targetId.slice(targetId.indexOf('#') + 1)
    const target = document.getElementById(targetId)
    console.log('🚀 ~ file: scroll.js ~ line 17 ~ target', target, targetId)
    target && target.scrollIntoView({ behavior: 'smooth' })
    setMobileMenu(false)
  } else {
    // Thay location hiện tại bằng data-href
    window.location.replace(srcElem.dataset.href)
  }
}

export const isHomePage = () => {
  const { pathname } = window.location
  return pathname && pathname.lastIndexOf('/') === 0
}
