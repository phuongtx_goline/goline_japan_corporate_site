import Headroom from 'headroom.js';

export const initHeader = () => {
  const headRoom = new Headroom(document.getElementById('site-header'), {
    tolerance: 5,
    offset: 120,
  })
  headRoom.init()
  return headRoom
}
